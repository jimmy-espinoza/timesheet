﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Clases
{
    public class Util
    {
        public static string EncriptarBase64(string str)
        {
            return System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(str));
        }

        public static string DesencriptarBase64(string str)
        {
            return System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(str));
        }

        public static string EnviarMail(string mailDestino, string asunto, string mensaje, string[] mailDestinos = null, bool isHtml = true)
        {
            try
            {
                var emisor = ConfigurationManager.AppSettings["CorreoElectronico"];
                var clave = ConfigurationManager.AppSettings["CorreoClave"];
                var display = ConfigurationManager.AppSettings["CorreoMostrar"];
                var host = ConfigurationManager.AppSettings["CorreoHost"];
                var port = ConfigurationManager.AppSettings["CorreoPort"];
                var ssl = ConfigurationManager.AppSettings["CorreoSSL"];

                System.Net.Mail.MailMessage mmsg = new System.Net.Mail.MailMessage();
                if (mailDestinos == null)
                {
                    mmsg.To.Add(mailDestino);
                }
                else
                {
                    mailDestinos.ToList().ForEach(mail =>
                    {
                        mmsg.To.Add(mail);
                    });
                }

                mmsg.Subject = asunto;
                mmsg.SubjectEncoding = System.Text.Encoding.UTF8;
                mmsg.Body = mensaje;
                mmsg.BodyEncoding = System.Text.Encoding.UTF8;
                mmsg.IsBodyHtml = isHtml;
                mmsg.From = new System.Net.Mail.MailAddress(emisor, display);

                System.Net.Mail.SmtpClient cliente = new System.Net.Mail.SmtpClient();
                cliente.Credentials = new System.Net.NetworkCredential(emisor, clave);
                cliente.Port = int.Parse(port);
                cliente.EnableSsl = bool.Parse(ssl);
                cliente.Host = host;
                cliente.Send(mmsg);
                return "";
            }
            catch (Exception ex)
            {
                //throw new Exception("Error Mail: " + ex.Message);
                return ex.Message;
            }
        }

        public static string NombreMes(int Mes)
        {
            switch (Mes)
            {
                case 1:
                    return "Enero";
                case 2:
                    return "Febrero";
                case 3:
                    return "Marzo";
                case 4:
                    return "Abril";
                case 5:
                    return "Mayo";
                case 6:
                    return "Junio";

                case 7:
                    return "Julio";

                case 8:
                    return "Agosto";

                case 9:
                    return "Septiembre";

                case 10:
                    return "Octubre";
                case 11:
                    return "Noviembre";

                case 12:
                    return "Diciembre";
                default:
                    return "Error";
            }
        }

        public static List<DateTime> SemanaLunDom(DateTime Fecha)
        {
            List<DateTime> Fechas = new List<DateTime>();

            var DiaSemana = (byte)Fecha.DayOfWeek;
            DateTime Lunes = new DateTime();
            DateTime Domingo = new DateTime();
            if (DiaSemana == 0)
            {
                Lunes = Fecha.AddDays(-6);
                Domingo = Fecha;
            }
            else
            {
                Lunes = Fecha.AddDays((1 - DiaSemana));
                Domingo = Lunes.AddDays(6);
            }

            Fechas.Add(Lunes);
            Fechas.Add(Domingo);

            return Fechas;
        }

        public static int SemanaAnio(DateTime Fecha)
        {



            return 0;
        }

        public static string DecimalToHrMin(double dHours)
        {
            TimeSpan ts = TimeSpan.FromHours(dHours);

            //string formattedTimespan = ts.ToString("hh\\:mm");

            //return formattedTimespan;

            var horasdia = 0;

            if (ts.Days > 0)
            {
                horasdia = ts.Days * 24;
            }

            int H = ts.Hours;
            int M = ts.Minutes;

            int ntotalhoras = H + horasdia;
            string totalhoras = "";
            if (ntotalhoras > 99)
            {
                totalhoras = ntotalhoras.ToString("D3");
            }
            else
            {
                totalhoras = ntotalhoras.ToString("D2");
            }

            string timeFormat = totalhoras + ":" + M.ToString("D2");

            return timeFormat;
        }

        public static int GetIso8601WeekOfYear(DateTime time)
        {
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        public static DateTime FirstDateOfWeek(int year, int weekOfYear, System.Globalization.CultureInfo ci)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = (int)ci.DateTimeFormat.FirstDayOfWeek - (int)jan1.DayOfWeek;
            DateTime firstWeekDay = jan1.AddDays(daysOffset);
            int firstWeek = ci.Calendar.GetWeekOfYear(jan1, ci.DateTimeFormat.CalendarWeekRule, ci.DateTimeFormat.FirstDayOfWeek);
            if ((firstWeek <= 1 || firstWeek >= 52) && daysOffset >= -3)
            {
                weekOfYear -= 1;
            }
            return firstWeekDay.AddDays(weekOfYear * 7);
        }

        public static int DiasLaborales(DateTime inicio, DateTime fin)
        {
            var acumulado = 0;
            var fecha = inicio.Date;

            while (fecha <= fin)
            {
                var value = 0;
                var ano = fecha.Year;

                if (fecha.DayOfWeek == DayOfWeek.Sunday || fecha.DayOfWeek == DayOfWeek.Saturday)
                {
                    value = 0;
                }
                else
                {
                    List<DateTime> feriados = new List<DateTime>();
                    //irrenunciables
                    feriados.Add(new DateTime(ano, 1, 1));
                    feriados.Add(new DateTime(ano, 5, 1));
                    feriados.Add(new DateTime(ano, 9, 18));
                    feriados.Add(new DateTime(ano, 9, 19));
                    feriados.Add(new DateTime(ano, 12, 25));
                    //renunciables
                    feriados.Add(new DateTime(ano, 5, 21));
                    feriados.Add(new DateTime(ano, 6, 28));//varia segun año
                    feriados.Add(new DateTime(ano, 7, 16));
                    feriados.Add(new DateTime(ano, 8, 15));
                    feriados.Add(new DateTime(ano, 7, 17));//varia segun año
                    feriados.Add(new DateTime(ano, 10, 11));//varia segun año
                    feriados.Add(new DateTime(ano, 10, 31));
                    feriados.Add(new DateTime(ano, 11, 1));
                    feriados.Add(new DateTime(ano, 12, 8));

                    if (feriados.Contains(fecha))
                    {
                        value = 0;
                    }
                    else
                    {
                        value = 1;
                    }
                }
                fecha = fecha.AddDays(1);
                acumulado = acumulado + value;
            }



        
            return acumulado;
        }

        public static string formatomoneda(int valor)
        {
            var txt = "";
            decimal numero = valor;

            NumberFormatInfo formato = new CultureInfo("es-CL").NumberFormat;

            formato.CurrencyGroupSeparator = ".";
            formato.NumberDecimalSeparator = ",";

            //puedes cambiar el simbolo $ por otro.
            //formato.CurrencySymbol = "$";

            txt = numero.ToString("C", formato);

            return txt;
        }

        public static string tipomoneda (int id)
        {
            var txt = "";
            switch (id)
            {
                case 1:
                    txt = "USD";
                    break;

                case 2:
                    txt = "CLP";
                    break;

                case 3:
                    txt = "UF";
                    break;

                default:
                    txt = "CLP";
                    break;

            }
            return txt;

        }
    }
}

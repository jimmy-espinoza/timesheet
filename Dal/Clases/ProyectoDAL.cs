﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Clases
{
    public class ProyectoDAL
    {
        public static Dao.Model.FactProyecto DatosProyecto(int IdProyecto)
        {
            try
            {
                return new Dao.Acceso.Select.ProyectoDAO().DatosProyecto(IdProyecto);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static List<Dao.Model.FactProyecto> ListaProyectos()
        {
            try
            {
                return new Dao.Acceso.Select.ProyectoDAO().ListaProyectos().ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static List<Dao.Model.RelUsuarioProyecto> ListaProyectosDash(int anio, int usuario)
        {
            try
            {
                return new Dao.Acceso.Select.ProyectoDAO().ListaProyectosDash(anio, usuario).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int GuardarProyecto(int IdProyecto, string NombreProyecto, int IdCliente, string Descripcion, DateTime InicioProyecto, DateTime FinProyecto, int DuracionDias, int TotalHH, int IsActivo)
        {
            try
            {
                if (IdProyecto == 0)
                {
                    return new Dao.Acceso.Insert.ProyectoDAO().GuardarProyecto(NombreProyecto, IdCliente, Descripcion, InicioProyecto, FinProyecto, DuracionDias, TotalHH, IsActivo);
                }
                else
                {
                    Dao.Model.FactProyecto ProyectoUpd = new Dao.Acceso.Select.ProyectoDAO().DatosProyecto(IdProyecto);
                    ProyectoUpd.NombreProyecto = NombreProyecto;
                    ProyectoUpd.IdCliente = IdCliente;
                    ProyectoUpd.Descripcion = Descripcion;
                    ProyectoUpd.InicioProyecto = InicioProyecto;
                    ProyectoUpd.FinProyecto = FinProyecto;
                    ProyectoUpd.DuracionDias = DuracionDias;
                    ProyectoUpd.TotalHH = TotalHH;
                    ProyectoUpd.IsActivo = IsActivo;
                    return new Dao.Acceso.Update.ProyectoDAO().ActualizarProyecto(ProyectoUpd);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static Dao.Model.FactProyecto DatosProyectoEP(int IdProyecto)
        {
            try
            {
                return new Dao.Acceso.Select.ProyectoDAO().DatosProyectoEP(IdProyecto);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static List<string> JPProyecto(int Proyecto, int Empresa)
        {
            try
            {
                return new Dao.Acceso.Select.ProyectoDAO().JPProyecto(Proyecto, Empresa).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static List<Dao.Model.FactUsuario> JPProyectoList(int Proyecto, int Empresa)
        {
            try
            {
                return new Dao.Acceso.Select.ProyectoDAO().JPProyectoList(Proyecto, Empresa).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static List<Dao.Model.RelUsuarioProyecto> ListaProyectosPerfil(int usuario)
        {
            try
            {
                return new Dao.Acceso.Select.ProyectoDAO().ListaProyectosPerfil(usuario).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static List<Dao.Model.RelUsuarioProyecto> ListaProyectosPerfilConsultor(int usuario)
        {
            try
            {
                return new Dao.Acceso.Select.ProyectoDAO().ListaProyectosPerfilConsultor(usuario).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int ProyectoExistente(string Proyecto)
        {
            try
            {
                var u = new Dao.Acceso.Select.ProyectoDAO().ProyectoExistente(Proyecto);
                return (u == null ? 0 : 1);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int GuardarProyectoConsultor(int IdProyecto, string NombreProyecto, int IdCliente, string Descripcion, string NombreContacto, string TelefonoContacto, string CorreoContacto, string PmCliente, string OrdenDeCompra, DateTime InicioProyecto, DateTime FinProyecto, int TipoMoneda, int Monto, string RangoEDP, int IsActivo)
        {
            try
            {
                if (IdProyecto == 0)
                {
                    return new Dao.Acceso.Insert.ProyectoDAO().GuardarProyectoConsultor(NombreProyecto, IdCliente, Descripcion, NombreContacto, TelefonoContacto, CorreoContacto, PmCliente, OrdenDeCompra, InicioProyecto, FinProyecto, TipoMoneda, Monto, RangoEDP, IsActivo);
                }
                else
                {
                    Dao.Model.FactProyecto ProyectoUpd = new Dao.Acceso.Select.ProyectoDAO().DatosProyecto(IdProyecto);

                    ProyectoUpd.NombreProyecto = NombreProyecto;
                    ProyectoUpd.IdCliente = IdCliente;
                    ProyectoUpd.Descripcion = Descripcion;
                    ProyectoUpd.NombreContacto = NombreContacto;
                    ProyectoUpd.TelefonoContacto = TelefonoContacto;
                    ProyectoUpd.CorreoContacto = CorreoContacto;
                    ProyectoUpd.PmCliente = PmCliente;
                    ProyectoUpd.OrdenDeCompra = OrdenDeCompra;
                    ProyectoUpd.InicioProyecto = InicioProyecto;
                    ProyectoUpd.FinProyecto = FinProyecto;                    
                    ProyectoUpd.TipoMoneda = TipoMoneda;
                    ProyectoUpd.Monto = Monto;
                    ProyectoUpd.RangoEDP = RangoEDP;
                    ProyectoUpd.IsActivo = IsActivo;
                    return new Dao.Acceso.Update.ProyectoDAO().ActualizarProyecto(ProyectoUpd);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int ProyectoConsultorExistente(string Proyecto)
        {
            try
            {
                var u = new Dao.Acceso.Select.ProyectoDAO().ProyectoExistente(Proyecto);
                return (u == null ? 0 : 1);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

    }
}

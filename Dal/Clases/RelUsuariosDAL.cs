﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Clases
{
    public class RelUsuariosDAL
    {
        public static List<Dao.Model.RelUsuarioProyecto> ListaUsuarioProyecto(int Proyecto, int Proveedor)
        {
            try
            {
                return new Dao.Acceso.Select.RelUsuariosDAO().ListaUsuarioProyecto(Proyecto, Proveedor).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int GuardarRelacion(int IdUsuarioProyecto, int IdUsuario, int IdProyecto, double PorcentajeAsignacion, int IsJp)
        {
            try
            {
                if (IdUsuarioProyecto == 0)
                {
                    return new Dao.Acceso.Insert.RelUsuariosDAO().GuardarRelacion(IdUsuario, IdProyecto, PorcentajeAsignacion, IsJp);
                }
                else
                {
                    Dao.Model.RelUsuarioProyecto RelacionUpd = new Dao.Acceso.Select.RelUsuariosDAO().DatosUsuarioProyecto(IdUsuarioProyecto);
                    RelacionUpd.IdUsuario = IdUsuario;
                    RelacionUpd.IdProyecto = IdProyecto;
                    RelacionUpd.PorcentajeAsignacion = PorcentajeAsignacion;
                    RelacionUpd.IsJp = IsJp;

                    return new Dao.Acceso.Update.RelUsuariosDAO().ActualizarRelacion(RelacionUpd);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static Dao.Model.RelUsuarioProyecto DatosUsuarioProyecto(int IdUsuarioProyecto)
        {
            try
            {
                return new Dao.Acceso.Select.RelUsuariosDAO().DatosUsuarioProyecto(IdUsuarioProyecto);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int EliminarAsignacion(int IdUsuarioProyecto)
        {
            try
            {
                Dao.Model.RelUsuarioProyecto RelacionUpd = new Dao.Acceso.Select.RelUsuariosDAO().DatosUsuarioProyecto(IdUsuarioProyecto);
                RelacionUpd.IsActivo = 0;
                return new Dao.Acceso.Update.RelUsuariosDAO().ActualizarRelacion(RelacionUpd);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static List<Dao.Model.FactUsuario> ListaUsuariosEnProyecto(int Proyecto, int Proveedor)
        {
            try
            {
                return new Dao.Acceso.Select.RelUsuariosDAO().ListaUsuariosEnProyecto(Proyecto, Proveedor).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static float AsignacionProyecto(int Proyecto)
        {
            try
            {
                var resultado = new Dao.Acceso.Select.RelUsuariosDAO().AsignacionProyecto(Proyecto).ToList();
                return float.Parse(resultado.Sum(s => s.PorcentajeAsignacion.Value).ToString());
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static List<int> ProyectosAsignados(int Usuario)
        {
            try
            {
                var resultado = new Dao.Acceso.Select.RelUsuariosDAO().ProyectosAsignados(Usuario).ToList();
                return resultado.Select(s => s.IdUsuarioProyecto).Distinct().ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static List<Dao.Model.RelUsuarioProyecto> SinTSUsuario(DateTime fecha)
        {
            try
            {
                return new Dao.Acceso.Select.RelUsuariosDAO().SinTSUsuario(fecha).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static List<Dao.Model.RelUsuarioProyecto> ProyectosJP(int IdUsuario)
        {
            try
            {
                return new Dao.Acceso.Select.RelUsuariosDAO().ProyectosJP(IdUsuario).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static List<Dao.Model.FactUsuario> UsuariosProyectoDash(List<int> proyectos)
        {
            try
            {
                return new Dao.Acceso.Select.RelUsuariosDAO().UsuariosProyectoDash(proyectos).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int GetUsuarioProyecto(int Proyecto, int Usuario)
        {
            try
            {
                var resultado = new Dao.Acceso.Select.RelUsuariosDAO().GetUsuarioProyecto(Proyecto, Usuario);
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int GetUsuario(int Usuario)
        {
            try
            {
                var resultado = new Dao.Acceso.Select.RelUsuariosDAO().GetUsuario(Usuario);
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }
    }
}

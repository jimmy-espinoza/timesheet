﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Clases
{
    public class TimeSheetConsultorDAL
    {
        /*public static List<Dao.Model.FactTimesheetConsultor> ListadoTimesheetConsultor()
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetConsultorDAO().ListadoTimesheetConsultor().ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }*/

        public static IQueryable<FactTimesheetConsultor> ListadoTimesheetConsultor(DateTime desde, DateTime hasta, int usuario, int proyecto)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetConsultorDAO().ListadoTimesheetConsultor(desde, hasta, usuario, proyecto);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static IQueryable<FactTimesheetConsultor> ListadoTimesheetConsultor(DateTime desde, DateTime hasta, List<int> usuario, List<int> proyecto)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetConsultorDAO().ListadoTimesheetConsultor(desde, hasta, usuario, proyecto);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int GuardarTimesheetConsultor(int Proyecto, int Usuario, string Descripcion, DateTime Fecha)
        {
            try
            {
                Usuario = Dal.Clases.RelUsuariosDAL.GetUsuarioProyecto(Proyecto, Usuario);

                return new Dao.Acceso.Insert.TimeSheetConsultorDAO().GuardarTimesheetConsultor(Proyecto, Usuario, Fecha, Descripcion);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int GuardarTimesheetConsultor(List<FactTimesheetConsultor> datos)
        {
            try
            {
                return new Dao.Acceso.Insert.TimeSheetConsultorDAO().GuardarTimesheetConsultor(datos);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static IQueryable<RelUsuarioProyecto> ListaProyectosTimesheetConsultor(int usuario)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetConsultorDAO().ListaProyectosTimesheetConsultor(usuario);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static Dao.Model.FactTimesheetConsultor ObjetoTimesheetConsultorEditar(int id)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetConsultorDAO().ObjetoTimesheetConsultorEditar(id);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static Dao.Model.FactEntregables ObjetoTimesheetEntregableEditar(int id)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetConsultorDAO().ObjetoTimesheetEntregableEditar(id);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static Dao.Model.FactEntregables ObjetoTimesheetEntregableEditar(string txt)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetConsultorDAO().ObjetoTimesheetEntregableEditar(txt);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int EditarTimesheetConsultor(FactTimesheetConsultor timesheetConsultor)
        {
            try
            {
                return new Dao.Acceso.Update.TimeSheetConsultorDAO().EditarTimesheetConsultor(timesheetConsultor);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int EditarEntregableConsultor(FactEntregables EntregableConsultor)
        {
            try
            {
                return new Dao.Acceso.Update.TimeSheetConsultorDAO().EditarEntregableConsultor(EntregableConsultor);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static IQueryable<FactTimesheetConsultor> ListaTimesheetConsultorByProyecto(List<int> proyecto)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetConsultorDAO().ListaTimesheetConsultorByProyecto(proyecto);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int AprobarActividad(int IdRegistro, int IdAprobador)
        {
            try
            {
                FactTimesheetConsultor registro = DatosTimeSheet(IdRegistro);
                registro.IsAprobado = 1;
                registro.FechaAprobacion = DateTime.Now;
                registro.IdUsuarioAprobacion = IdAprobador;
                return new Dao.Acceso.Update.TimeSheetConsultorDAO().EditarTimesheetConsultor(registro);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static FactTimesheetConsultor DatosTimeSheet(int IdTimesheet)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetConsultorDAO().DatosTimesheet(IdTimesheet);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int EliminarTimeSheetConsultor(int IdTimesheetConsultor)
        {
            try
            {
                return new Dao.Acceso.Delete.TimeSheetConsultorDAO().EliminarTimeSheetConsultor(IdTimesheetConsultor);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Clases
{
    public class UsuarioDAL
    {
        public static Dao.Model.FactUsuario LoginUsuario(string usuario, string password)
        {
            try
            {
                return new Dao.Acceso.Select.UsuarioDAO().LoginUsuario(usuario, password);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static List<Dao.Model.FactUsuario> ListaUsuarios()
        {
            try
            {
                return new Dao.Acceso.Select.UsuarioDAO().ListaUsuarios().ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static Dao.Model.FactUsuario UsuarioCorreo(string Correo)
        {
            try
            {
                return new Dao.Acceso.Select.UsuarioDAO().UsuarioCorreo(Correo);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int GuardarUsuario(int IdUsuario, string NombreUsuario, string Nombre , string Clave, string Correo, int IdEmpresa, int IdPerfil, int Admin, int IsActivo, string ArchivoFirma)
        {
            try
            {
                if (IdUsuario == 0)
                {
                    return new Dao.Acceso.Insert.UsuarioDAO().GuardarUsuario(NombreUsuario, Nombre, Clave, Correo, IdEmpresa, IdPerfil, Admin, IsActivo, ArchivoFirma);
                }
                else
                {
                    Dao.Model.FactUsuario UsuarioUpd = new Dao.Acceso.Select.UsuarioDAO().DatosUsuario(IdUsuario);
                    UsuarioUpd.NombreUsuario = NombreUsuario;
                    UsuarioUpd.Nombre = Nombre;
                    UsuarioUpd.Clave = Clave;
                    UsuarioUpd.Correo = Correo;
                    UsuarioUpd.IdEmpresa = IdEmpresa;
                    UsuarioUpd.IdPerfil = IdPerfil;
                    UsuarioUpd.IsAdmin = Admin;
                    UsuarioUpd.IsActivo = IsActivo;
                    UsuarioUpd.ArchivoFirma = ArchivoFirma;
                    return new Dao.Acceso.Update.UsuarioDAO().ActualizarUsuario(UsuarioUpd);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static Dao.Model.FactUsuario DatosUsuario(int IdUsuario)
        {
            try
            {
                return new Dao.Acceso.Select.UsuarioDAO().DatosUsuario(IdUsuario);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int UsuarioExistente(string usuario)
        {
            try
            {
                var u = new Dao.Acceso.Select.UsuarioDAO().UsuarioExistente(usuario);
                return (u == null ? 0 : 1);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Clases
{
    public class ActividadDAL
    {
        public static Dao.Model.FactTipoActividad DatosActividad(int idActividad)
        {
            try
            {
                return new Dao.Acceso.Select.ActividadDAO().DatosActividad(idActividad);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int GuardarActividad(int IdActividad, int IdProyecto, string Descripcion, int IsActivo)
        {
            try
            {

                if (IdActividad == 0)
                {
                    return new Dao.Acceso.Insert.ActividadDAO().GuardarActividad(IdProyecto, Descripcion, IsActivo);
                }
                else
                {
                    var actividad = new Dao.Acceso.Select.ActividadDAO().DatosActividad(IdActividad);
                    actividad.Descripcion = Descripcion;
                    actividad.IsActivo = IsActivo;

                    return new Dao.Acceso.Update.ActividadDAO().ActualizarActividad(actividad);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static List<Dao.Model.FactTipoActividad> ActividadesProyecto(int IdProyecto)
        {
            try
            {
                return new Dao.Acceso.Select.ActividadDAO().ActividadesProyecto(IdProyecto).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }



        public static string[] ProcesoCargaMasiva(int idProyecto, System.Data.DataTable dt)
        {
            try
            {
                var CantidadRegistros = dt.Rows.Count;
                var RegistrosCargados = 0;
                var RegistrosRechazados = 0;
                var RegistrosLeidos = 0;
                var descripcionError = "";

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    RegistrosLeidos += 1;

                    try
                    {
                        Exception exception = new Exception();

                        var actividad = (dt.Rows[i][0].ToString()).Trim();

                        if (String.IsNullOrEmpty(actividad))
                            exception.Data.Add("Item gantt", "Campo item vacio o nulo.");

                        if (actividad.Length > 300)
                            exception.Data.Add("Item gantt", "Campo item contiene más de 300 caracteres.");

                        var actividadesProy = ActividadesProyecto(idProyecto);
                        var cant = actividadesProy.Where(a => a.Descripcion == actividad).Count();
                        if (cant > 0)
                            exception.Data.Add("Item gantt", "El item '" + actividad + "' ya se encuentra registrado en el sistema.");

                        if (exception.Data.Count > 0)
                        {
                            throw exception;
                        }

                        var resultado = GuardarActividad(0, idProyecto, actividad, 1);

                        if (resultado != 0)
                        {
                            RegistrosCargados += 1;
                        }
                    }
                    catch (Exception e)
                    {
                        RegistrosRechazados += 1;

                        string descripcion = "<b>Registro N°</b> {0}.<br><ul>{1}</ul><div " + (e.Data.Count > 0 ? "style='display:none;'" : "") + "><br><b>Mensaje Error:</b> {2}<br><code style='white-space: normal;'><b>Excepción:</b> {3}</code></div>";
                        string detalle = (e.Data.Count == 0) ? "Excepción Controlada" : "";

                        foreach (DictionaryEntry item in e.Data)
                        {
                            detalle += string.Format("<li>{0}</li>", item.Value.ToString());
                        }
                        descripcion = string.Format(descripcion, (i + 1), detalle, e.Message, e.ToString());

                        descripcionError = descripcionError + descripcion;
                    }
                }

                string[] result =
                    {
                        CantidadRegistros.ToString(),
                        RegistrosCargados.ToString(),
                        RegistrosRechazados.ToString(),
                        RegistrosLeidos.ToString(),
                        descripcionError
                    };

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción Controlada: " + ex.Message);
            }
        }





    }
}

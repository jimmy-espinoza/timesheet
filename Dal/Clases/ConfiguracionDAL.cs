﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Clases
{
    public class ConfiguracionDAL
    {
        public static List<Dao.Model.CfgParametrosGenerales> ListaConfiguracion()
        {
            try
            {
                return new Dao.Acceso.Select.ConfiguracionDAO().ListaConfiguracion().ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int GuardarConfiguracion(int IdCfg, string DesCfg,  string ValCfg, DateTime FecCfg)
        {
            try
            {
                if (IdCfg == 0)
                {
                    return new Dao.Acceso.Insert.ConfiguracionDAO().GuardarConfiguracion(ValCfg, DesCfg, FecCfg);
                }
                else
                {
                    Dao.Model.CfgParametrosGenerales ConfigUpd = new Dao.Acceso.Select.ConfiguracionDAO().DatosConfiguracion(IdCfg);
                    //ConfigUpd.Descripcion = DesCfg;
                    ConfigUpd.Valor = ValCfg;
                    ConfigUpd.FechaRegistro = FecCfg;
                    return new Dao.Acceso.Update.ConfiguracionDAO().EditarConfiguracion(ConfigUpd);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static Dao.Model.CfgParametrosGenerales DatosConfiguracion(int IdCfg)
        {
            try
            {

                return new Dao.Acceso.Select.ConfiguracionDAO().DatosConfiguracion(IdCfg);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }
    }
}

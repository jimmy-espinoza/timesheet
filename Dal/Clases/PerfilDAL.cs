﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Clases
{
    public class PerfilDAL
    {
        public static List<Dao.Model.DimPerfil> ListaPerfiles()
        {
            try
            {
                return new Dao.Acceso.Select.PerfilDAO().ListaPerfiles().ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int GuardarPerfil(int Id, string DescPerfil, int IsActivo)
        {
            try
            {
                if (Id == 0)
                {
                    return new Dao.Acceso.Insert.PerfilDAO().GuardarPerfil(DescPerfil, IsActivo);
                }
                else
                {
                    Dao.Model.DimPerfil PerfilUpd = new Dao.Acceso.Select.PerfilDAO().DatosPerfil(Id);
                    PerfilUpd.DescPerfil = DescPerfil;
                    PerfilUpd.IsActivo = IsActivo;
                    return new Dao.Acceso.Update.PerfilDAO().ActualizarPerfil(PerfilUpd);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static Dao.Model.DimPerfil DatosPerfil(int Id)
        {
            try
            {
                return new Dao.Acceso.Select.PerfilDAO().DatosPerfil(Id);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }
    }
}

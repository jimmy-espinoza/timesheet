﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Clases
{
    public class ConversionMonedaDAL
    {
        /// <summary>
        /// Listado de conversión de moneda
        /// </summary>
        /// <param name="Moneda">Valor correspondiente al tipo de Moneda ['UF','UTM','USD','EUR']</param>
        /// <param name="Anio">Año correspondiente para obtener datos, "0" para omitir filtro</param>
        /// <param name="Mes">Mes correspondiente para obtener datos, "0" para omitir filtro</param>
        /// <returns></returns>
        public static List<Dao.Model.CfgConversionMoneda> ListaConversion(string Moneda, int Anio, int Mes)
        {
            try
            {
                return new Dao.Acceso.Select.ConversionMonedaDAO().ListaConversion(Moneda, Anio, Mes).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        /// <summary>
        /// Listado de conversión de moneda
        /// </summary>
        /// <param name="Moneda">Valor correspondiente al tipo de Moneda ['UF','UTM','USD','EUR']</param>
        /// <param name="Desde">Fecha desde obtención de datos</param>
        /// <param name="Desde">Fecha hasta obtención de datos</param>
        /// <returns></returns>
        public static List<Dao.Model.CfgConversionMoneda> ListaConversion(string Moneda, DateTime Desde, DateTime Hasta)
        {
            try
            {
                return new Dao.Acceso.Select.ConversionMonedaDAO().ListaConversion(Moneda, Desde, Hasta).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int GuardarConversionMoneda(string Moneda, DateTime Fecha, double Valor)
        {
            try
            {
                Dao.Model.CfgConversionMoneda conversion = new Dao.Model.CfgConversionMoneda();
                conversion.Moneda = Moneda;
                conversion.Fecha = Fecha;
                conversion.Valor = Valor;

                return new Dao.Acceso.Insert.ConversionMonedaDAO().GuardarConversionMoneda(conversion);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int ActualizarConversionMoneda(int Id, string Moneda, DateTime Fecha, double Valor)
        {
            try
            {
                Dao.Model.CfgConversionMoneda conversion = new Dao.Model.CfgConversionMoneda();
                conversion.Id = Id;
                conversion.Moneda = Moneda;
                conversion.Fecha = Fecha;
                conversion.Valor = Valor;

                return new Dao.Acceso.Update.ConversionMonedaDAO().ActualizarConversionMoneda(conversion);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

    }
}

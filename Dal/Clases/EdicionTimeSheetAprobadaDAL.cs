﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Clases
{
    public class EdicionTimeSheetAprobadaDAL
    {

        public static int RegistrarTimesheetAprobada(int IdFactEdicionTimeSheetAprobada, DateTime Fecha, int Semana, int IdTipoCosto, string Actividad, string HoraInicio, string HoraFin, double TotalHoras, int IsActivo, DateTime FechaCarga, string IdTipoActividad)
        {
            try
            {
                var timesheet = new FactEdicionTimeSheetAprobada();

                timesheet.IdFactEdicionTimeSheetAprobada = IdFactEdicionTimeSheetAprobada;
                timesheet.Fecha = Fecha;
                timesheet.Semana = Semana;
                timesheet.IdTipoCosto = IdTipoCosto;
                timesheet.Actividad = Actividad;
                timesheet.HoraInicio = HoraInicio;
                timesheet.HoraFin = HoraFin;
                timesheet.TotalHoras = TotalHoras;
                timesheet.IsActivo = IsActivo;
                timesheet.FechaCarga = FechaCarga;
                timesheet.IsAprobado = 0;

                if (IdTipoActividad == null || IdTipoActividad == "0")
                {
                    timesheet.IdTipoActividad = null;
                }
                else
                {
                    timesheet.IdTipoActividad = int.Parse(IdTipoActividad);
                }

                return new Dao.Acceso.Insert.EdicionTimesheetAprobadaDAO().RegistrarTimesheetAprobada(timesheet);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static IQueryable<FactEdicionTimeSheetAprobada> ListaEdicionTimesheetByProyecto(List<int> proyecto)
        {
            try
            {
                return new Dao.Acceso.Select.EdicionTimesheetAprobadaDAO().ListaByProyecto(proyecto);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static FactEdicionTimeSheetAprobada DatosRegistro(int id)
        {
            try
            {
                return new Dao.Acceso.Select.EdicionTimesheetAprobadaDAO().GetRegistro(id);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int RechazarEdicionTimesheet(int IdRegistro, int usuario, string motivo)
        {
            try
            {
                var registro = DatosRegistro(IdRegistro);
                registro.IsAprobado = 2;
                registro.FechaAprobacion = DateTime.Now;
                registro.IdUsuarioAprobacion = usuario;
                registro.Motivo = motivo;
                return new Dao.Acceso.Update.EdicionTimesheetAprobadaDAO().EditarRegistro(registro);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int AprobarEdicionTimesheet(int IdRegistro, int usuario, string motivo)
        {
            try
            {
                var registro = DatosRegistro(IdRegistro);
                registro.IsAprobado = 1;
                registro.FechaAprobacion = DateTime.Now;
                registro.IdUsuarioAprobacion = usuario;
                registro.Motivo = motivo;
                return new Dao.Acceso.Update.EdicionTimesheetAprobadaDAO().EditarRegistro(registro);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }



    }
}

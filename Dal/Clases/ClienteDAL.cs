﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Clases
{
    public class ClienteDAL
    {
        public static List<Dao.Model.DimCliente> ListaClientes()
        {
            try
            {
                return new Dao.Acceso.Select.ClienteDAO().ListaClientes().ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int GuardarCliente(int IdCliente, string Nombre, string Rut)
        {
            try
            {
                if (IdCliente == 0)
                {
                    return new Dao.Acceso.Insert.ClienteDAO().GuardarCliente(Nombre, Rut);
                }
                else
                {
                    Dao.Model.DimCliente ClienteUpd = new Dao.Acceso.Select.ClienteDAO().DatosCliente(IdCliente);
                    ClienteUpd.Nombre = Nombre;
                    ClienteUpd.Rut = Rut;
                    return new Dao.Acceso.Update.ClienteDAO().ActualizarCliente(ClienteUpd);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static Dao.Model.DimCliente DatosCliente(int IdCliente)
        {
            try
            {
                return new Dao.Acceso.Select.ClienteDAO().DatosCliente(IdCliente);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int ClienteRut(string Rut)
        {
            try
            {
                var u = new Dao.Acceso.Select.ClienteDAO().ClienteRut(Rut);
                return (u == null ? 0 : 1);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int ClienteExiste(string Nombre)
        {
            try
            {
                var u = new Dao.Acceso.Select.ClienteDAO().ClienteExiste(Nombre);
                return (u == null ? 0 : 1);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }
    }
}

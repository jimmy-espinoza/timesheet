﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Clases
{
    public class TimeSheetDAL
    {
        public static IQueryable<RelUsuarioProyecto> ListaEmpresasDash(int proyecto)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetDAO().ListaEmpresasDash(proyecto);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static IQueryable<RelUsuarioProyecto> ListaRecursosDash(int proyecto, int empresa)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetDAO().ListaRecursosDash(proyecto, empresa);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static IQueryable<FactTimeSheet> ListadoTimesheet(DateTime desde, DateTime hasta, int usuario, int proyecto)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetDAO().ListadoTimesheet(desde, hasta, usuario, proyecto);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static IQueryable<FactTimeSheet> ListadoTimesheet(DateTime desde, DateTime hasta, List<int> usuario, List<int> proyecto)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetDAO().ListadoTimesheet(desde, hasta, usuario, proyecto);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static IQueryable<RelUsuarioProyecto> ListaProyectosTimesheet(int usuario)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetDAO().ListaProyectosTimesheet(usuario);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        /*
        public static IQueryable<RelUsuarioProyecto> ListaProyectosPerfil(int usuario)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetDAO().ListaProyectosPerfil(usuario);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }
        */

        public static int GuardarTimesheet(int Registro, DateTime Fecha, string Actividad, int Proyecto, int Costo, string Inicio, string Termino)
        {
            try
            {
                if (Registro == 0)
                {
                    return new Dao.Acceso.Insert.TimesheetDAO().GuardarTimesheet(Fecha, Actividad, Proyecto, Costo, Inicio, Termino);
                }
                else
                {
                    /*
                    Dao.Model.FactUsuario UsuarioUpd = new Dao.Acceso.Select.UsuarioDAO().DatosUsuario(IdUsuario);
                    UsuarioUpd.NombreUsuario = NombreUsuario;
                    UsuarioUpd.Nombre = Nombre;
                    UsuarioUpd.Clave = Clave;
                    UsuarioUpd.Correo = Correo;
                    UsuarioUpd.IdEmpresa = IdEmpresa;
                    UsuarioUpd.IdPerfil = IdPerfil;
                    UsuarioUpd.IsAdmin = Admin;
                    UsuarioUpd.IsActivo = IsActivo;
                    return new Dao.Acceso.Update.UsuarioDAO().ActualizarUsuario(UsuarioUpd);
                    */
                    return 1;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int GuardarTimesheet(List<FactTimeSheet> datos)
        {
            try
            {
                return new Dao.Acceso.Insert.TimesheetDAO().GuardarTimesheet(datos);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static FactTimeSheet DatosTimeSheet(int IdTimesheet)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetDAO().DatosTimesheet(IdTimesheet);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static FactTimeSheet DetalleTimeSheet(int IdTimesheet)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetDAO().DatosTimesheetInclude(IdTimesheet);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }


        public static FactTimeSheet DatosTimeSheetNoTrack(int IdTimesheet)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetDAO().DatosTimesheetNoTrack(IdTimesheet);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int EditarTimesheet(FactTimeSheet datos)
        {
            try
            {
                return new Dao.Acceso.Update.TimeSheetDAO().EditarTimesheet(datos);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static IQueryable<FactTimeSheet> HorariosRegistrados(List<int> pros, DateTime Fecha)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetDAO().HorariosRegistrados(pros, Fecha);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int EliminarTimesheet(int IdTimesheet)
        {
            try
            {
                return new Dao.Acceso.Delete.TimeSheetDAO().EliminarTimesheet(IdTimesheet);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static IQueryable<FactTimeSheet> HorasProyectoEmpresa(DateTime Inicio, DateTime Termino, int Proyecto, int Empresa)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetDAO().HorasProyectoEmpresa(Inicio, Termino, Proyecto, Empresa);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static IQueryable<FactTimeSheet> HorariosFecha(DateTime Fecha, int IdUsuarioProyecto)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetDAO().HorariosFecha(Fecha, IdUsuarioProyecto);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static IQueryable<FactTimeSheet> ListaTimesheetByProyecto(List<int> proyecto)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetDAO().ListaTimesheetByProyecto(proyecto);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int AprobarActividad(int IdRegistro, int IdAprobador)
        {
            try
            {
                FactTimeSheet registro = DatosTimeSheet(IdRegistro);
                registro.IsAprobado = 1;
                registro.FechaAprobacion = DateTime.Now;
                registro.IdUsuarioAprobacion = IdAprobador;
                return new Dao.Acceso.Update.TimeSheetDAO().EditarTimesheet(registro);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static IQueryable<FactTimeSheet> HorasRegistradasSemana(int IdUsuario, List<int> Semanas)
        {
            try
            {

                return new Dao.Acceso.Select.TimeSheetDAO().HorasRegistradasSemana(IdUsuario, Semanas);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }


        public static FactTimesheetConsultor GetregistroTimesheetConsultor(DateTime fecha, int usuario)
        {
            try
            {
                return new Dao.Acceso.Select.TimeSheetDAO().GetregistroTimesheetConsultor(fecha, usuario);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int GetCantidadRegistroTimesheetConsultor(DateTime inicio, DateTime fin, int proyecto)
        {
            try
            {
                var lista = new Dao.Acceso.Select.TimeSheetDAO().GetregistroTimesheetConsultor(inicio, fin, proyecto).ToList();
                return lista.Count;
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

    }

}

﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Clases
{
    public class NotificacionDAL
    {
        public static IQueryable<FactNotificacion> ListadoNotificacionUsuario(int IdUsuario, int Cantidad)
        {
            try
            {
                return new Dao.Acceso.Select.NotificacionDAO().ListadoNotificacionUsuario(IdUsuario, Cantidad);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static FactNotificacion DatosNotificacion(int IdNotificacion)
        {
            try
            {
                return new Dao.Acceso.Select.NotificacionDAO().DatosNotificacion(IdNotificacion);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int MarcarVisto(int IdNotificacion, int accion)
        {
            try
            {
                FactNotificacion r = new FactNotificacion();
                r = DatosNotificacion(IdNotificacion);
                r.IdNotificacion = IdNotificacion;
                r.Visto = accion;
                r.FechaVisto = (accion == 0 ? (DateTime?)null : DateTime.Now);
                return new Dao.Acceso.Update.NotificacionDAO().MarcarVisto(r);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int RegistrarNotificacion(int? IdPro, int? IdUserAccion, DateTime Registro, DateTime Accion, int IdUserVisualiza, string Titulo, string Mensaje, int Importancia, int Visto)
        {
            try
            {
                FactNotificacion r = new FactNotificacion();
                r.IdProyecto = IdPro;
                r.IdUsuario=IdUserAccion;
                r.FechaRegistro = Registro;
                r.FechaAccion = Accion;
                r.IdUsuarioVisualiza = IdUserVisualiza;
                r.Titulo = Titulo;
                r.Mensaje = Mensaje;
                r.Importancia = Importancia;
                r.Visto = Visto;
                return new Dao.Acceso.Insert.NotificacionDAO().RegistrarNotificacion(r);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }




        public static void Proceso()
        {

        }


    }

}

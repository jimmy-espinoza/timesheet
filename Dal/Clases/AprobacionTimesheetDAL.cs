﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Clases
{
    public class AprobacionTimesheetDAL
    {
        public static List<Dao.Model.FactAprobacionTimesheet> ListaAtrasos(List<int> IdPros)
        {
            try
            {
                return new Dao.Acceso.Select.AprobacionTimesheetDAO().ListaAtrasos(IdPros).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static Dao.Model.FactAprobacionTimesheet DatosAtraso(int IdRegistro)
        {
            try
            {
                return new Dao.Acceso.Select.AprobacionTimesheetDAO().DatosAtraso(IdRegistro);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static Dao.Model.FactAprobacionTimesheet AprobarAtraso(int IdRegistro, int IdAprobador)
        {
            try
            {
                Dao.Model.FactAprobacionTimesheet registro = DatosAtraso(IdRegistro);
                registro.EstadoSolicitud = 1;
                registro.FechaGestion = DateTime.Now;
                registro.UsuarioGestion = IdAprobador;
                registro.FechaValidez = DateTime.Today.AddDays(2);
                return new Dao.Acceso.Update.AprobacionTimesheetDAO().AprobarAtraso(registro);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int RegistrarAtraso(int IdUsuarioProyecto)
        {
            try
            {
                Dao.Model.FactAprobacionTimesheet registro = new Dao.Model.FactAprobacionTimesheet();
                registro.IdUsuarioProyecto = IdUsuarioProyecto;
                registro.EstadoSolicitud = 0;
                registro.FechaSolicitud = DateTime.Now;
                var existe = ExisteAtraso(IdUsuarioProyecto);
                if (existe == null)
                {
                    return new Dao.Acceso.Insert.AprobacionTimesheetDAO().RegistrarAtraso(registro);
                }
                else
                {
                    if (existe.EstadoSolicitud == 1)
                    {
                        if (DateTime.Today > existe.FechaValidez)
                        {
                            return new Dao.Acceso.Insert.AprobacionTimesheetDAO().RegistrarAtraso(registro);
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static Dao.Model.FactAprobacionTimesheet ExisteAtraso(int IdUsuarioProyecto)
        {
            try
            {
                return new Dao.Acceso.Select.AprobacionTimesheetDAO().ExisteAtraso(IdUsuarioProyecto);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }
    }
}

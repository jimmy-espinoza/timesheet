﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Clases
{
    public class EntregableDAL
    {

        public static IQueryable<FactEntregables> ListaEntregables(DateTime desde, DateTime hasta, List<int> usuario, List<int> proyecto)
        {
            try
            {
                return new Dao.Acceso.Select.EntregableDAO().ListadoEntregables(desde, hasta, usuario, proyecto);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int GuardarEntregableConsultor(int Usuario, int Proyecto, string Entregable, int Avance, int Pendiente, int VersionFinal, string Observacion, string CodigoDocumento)
        {
            try
            {
                Usuario = Dal.Clases.RelUsuariosDAL.GetUsuario(Usuario);

                return new Dao.Acceso.Insert.EntregableDAO().GuardarEntregableConsultor(Usuario, Proyecto, Entregable, Avance, Pendiente, VersionFinal, Observacion, CodigoDocumento);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int GuardarEntregableConsultor(List<FactEntregables> datos)
        {
            try
            {
                return new Dao.Acceso.Insert.EntregableDAO().GuardarEntregableConsultor(datos);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static List<Dao.Model.FactEntregables> ListaEntregables(int proyecto)
        {
            try
            {
                return new Dao.Acceso.Select.EntregableDAO().ListadoEntregables(proyecto).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

    }
}

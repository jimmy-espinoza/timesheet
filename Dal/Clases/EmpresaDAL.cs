﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Clases
{
    public class EmpresaDAL
    {
        public static List<Dao.Model.DimEmpresa> ListaEmpresas()
        {
            try
            {
                return new Dao.Acceso.Select.EmpresaDAO().ListaEmpresas().ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int GuardarEmpresa(int IdEmpresa, string NombreEmpresa, string RutEmpresa, string Contacto, int TelefonoContacto, string MailContacto, int AplicaCosto, double UfHora, int IsActivo)
        {
            try
            {
                if (IdEmpresa == 0)
                {
                    return new Dao.Acceso.Insert.EmpresaDAO().GuardarEmpresa(NombreEmpresa, RutEmpresa, Contacto, TelefonoContacto, MailContacto, AplicaCosto, UfHora, IsActivo);
                }
                else
                {
                    Dao.Model.DimEmpresa EmpresaUpd = new Dao.Acceso.Select.EmpresaDAO().DatosEmpresa(IdEmpresa);
                    EmpresaUpd.NombreEmpresa = NombreEmpresa;
                    EmpresaUpd.RutEmpresa = RutEmpresa;
                    EmpresaUpd.Contacto = Contacto;
                    EmpresaUpd.TelefonoContacto = TelefonoContacto;
                    EmpresaUpd.MailContacto = MailContacto;
                    EmpresaUpd.AplicaCosto = AplicaCosto;
                    EmpresaUpd.UfHora = UfHora;
                    EmpresaUpd.IsActivo = IsActivo;
                    return new Dao.Acceso.Update.EmpresaDAO().ActualizarEmpresa(EmpresaUpd);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static Dao.Model.DimEmpresa DatosEmpresa(int IdEmpresa)
        {
            try
            {
                return new Dao.Acceso.Select.EmpresaDAO().DatosEmpresa(IdEmpresa);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int EmpresaRut(string Rut)
        {
            try
            {
                var u = new Dao.Acceso.Select.EmpresaDAO().EmpresaRut(Rut);
                return (u == null ? 0 : 1);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }

        public static int ProveedorExistente(string Empresa)
        {
            try
            {
                var u = new Dao.Acceso.Select.EmpresaDAO().ProveedorExistente(Empresa);
                return (u == null ? 0 : 1);
            }
            catch (Exception ex)
            {
                throw new Exception("Excepción controlada: " + ex.Message);
            }
        }
    }
}

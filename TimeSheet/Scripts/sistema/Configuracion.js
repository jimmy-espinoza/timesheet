﻿$(document).ready(function () {

    for (c = 1; c <= 24; c++) {
        var h = c.toString().padStart(2, "0") + ":00";
        $("#HoraEjecucion").append("<option value='" + h + "'>" + h + "</option>");
    }

    DatosNotificacion();

    $("#form-horario").on("submit", function (e) {
        e.preventDefault();

        var l = Ladda.create(document.querySelector('#btnGuardarHora'));
        l.start();

        var datos = $(this).serialize();
        $.ajax({
            url: "/Configuracion/GuardarHoraEjecucion",
            data: datos,
            cache: false,
            dataType: "json",
            type: 'POST',
            success: function (result) {
                if (result.Cod == 1) {
                    swal("Configuración", result.Msg, "success");
                    l.stop();
                }
                else {
                    swal("Configuración", result.Msg, "warning");
                    l.stop();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                swal("Configuración", jqXHR.responseText, "error");
                l.stop();
            }
        });
    });

    $("#form-limite").on("submit", function (e) {
        e.preventDefault();

        var l = Ladda.create(document.querySelector('#btnGuardarLimite'));
        l.start();

        var datos = $(this).serialize();
        $.ajax({
            url: "/Configuracion/Guardarlimite",
            data: datos,
            cache: false,
            dataType: "json",
            type: 'POST',
            success: function (result) {
                if (result.Cod == 1) {
                    swal("Configuración", result.Msg, "success");
                    $("#mdlLimite").modal("toggle");
                    l.stop();
                }
                else {
                    swal("Configuración", result.Msg, "warning");
                    l.stop();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                swal("Configuración", jqXHR.responseText, "error");
                l.stop();
            }
        });
    });

});

function DatosNotificacion() {

    $.ajax({
        url: "/Configuracion/ListaConfiguracion",
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        success: function (result) {

            if (result.Cod == 1) {
                $("#HoraEjecucion").val(result.Datos.find(o => o.IdParametroGeneral === 1).Valor);
                $("#DiasSinRegistro").val(result.Datos.find(o => o.IdParametroGeneral === 2).Valor);
                $("#LimiteDias").val(result.Datos.find(o => o.IdParametroGeneral === 3).Valor);
            }
            else {
                swal("Configuración", result.Msg, "warning");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            swal("Configuración", jqXHR.responseText, "error");
        }
    });
}


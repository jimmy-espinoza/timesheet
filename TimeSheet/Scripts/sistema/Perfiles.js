﻿$(document).ready(function () {

    window.tabla = $('#tblPerfiles').DataTable({

        paging: true,
        ordering: true,
        info: false,
        bFilter: true,
        //columnDefs: [{ targets: [2], class: "text-nowrap" }, { targets: [3], class: "text-nowrap" }],
        columnDefs: [{ "orderable": false, "targets": 2 }],
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10

    });

    ListaPerfiles();


    $("#btnNuevo").on('click', function () {
        $("#titModal").html('Nuevo Perfil');
        $("#mdlPerfiles").modal("show");
        $("#formPerfil")[0].reset();
        fPerfil.resetForm();
        $("#Id").val(0);
    });


    $('#tblPerfiles').on('click', '.edit', function () {
        var Id = $(this).attr('id');
        $("#titModal").html('Editar Perfil');
        fPerfil.resetForm();

        $.ajax({
            url: 'Perfiles/DatosPerfil',
            data: { Id: Id },
            type: 'POST',
            success: function (resultado) {
                $("#Id").val(resultado.Datos.Id);
                $("#DescPerfil").val(resultado.Datos.DescPerfil);
                $("#Estado").val(resultado.Datos.IsActivo);

                $("#mdlPerfiles").modal("show");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });
    });

    var fPerfil = $("#formPerfil").validate({
        submitHandler: function (form) {

            var l = Ladda.create(document.querySelector('#btnGuardar'));
            l.start();

            var formulario = $('#formPerfil').serialize();

            $.ajax({
                url: "Perfiles/GuardarPerfil",
                data: formulario,
                cache: false,
                dataType: "json",
                type: 'POST',
                success: function (resultado) {
                    if (resultado.Cod == 0) {
                        swal("Perfiles", resultado.Msg, "warning");
                        l.stop();
                    }
                    else if (resultado.Cod == 1) {
                        ListaPerfiles();
                        $("#mdlPerfiles").modal("toggle");
                        swal("Perfiles", resultado.Msg, "success");
                        l.stop();
                    }
                    else {
                        swal("Perfiles", resultado.Msg, "error");
                        l.stop();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    l.stop();
                }
            });
        },
        invalidHandler: function (event, validator) {
        }

    });


});

function ListaPerfiles() {
    tabla.clear();

    $.ajax({
        url: 'Perfiles/ListaPerfiles',
        type: 'POST',
        success: function (data) {
            //console.log(data);
            if (data.Cod == 1) {
                $.each(data.Datos, function (i, item) {
                    var btn = "<center><button class='btn btn-sm btn-info edit' id='" + item.Id + "'> Editar </button></center>";
                    var Estado = "";
                    if (item.Estado == 1) {
                        var Estado = "Activo";
                    }
                    else {
                        var Estado = "Inactivo";
                    }
                    tabla.row.add([
                        item.Perfil,
                        Estado,
                        btn
                    ]).draw(false);
                });
            }
            else {
                swal("Perfiles", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });
}
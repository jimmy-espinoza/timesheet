﻿$(document).ready(function () {

    $('.datepicker').datepicker({
        language: 'es',
        autoclose: true,
        format: 'dd-mm-yyyy',
        orientation: 'bottom'
    });

    $('#tblActividades').DataTable({
        paging: true,
        ordering: true,
        info: false,
        bFilter: true,
        columnDefs: [{ targets: [0], class: "text-nowrap" }, { "orderable": false, "targets": 2 }],
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10

    });


    window.tabla = $('#tblProyectos').DataTable({

        paging: true,
        ordering: true,
        info: false,
        bFilter: true,
        columnDefs: [{ targets: [0, 1, 3, 4, 8], class: "text-nowrap" }, { "orderable": false, "targets": 8 }],
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10

    });

    $('#tblProyectosConsultoria').DataTable({
        paging: true,
        ordering: true,
        info: false,
        bFilter: true,
        columnDefs: [{ targets: [0, 1,2, 3, 4, 5, 6, 7, 8, 9, 10, 11], class: "text-nowrap" }, { "orderable": false, "targets": 9 }],
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10
    });

    ListaProyectos();

    $("#btnNuevo").on('click', function () {
        $("#titModal").html('Nuevo Proyecto');
        $("#mdlProyectos").modal("show");
        $("#formProyectos")[0].reset();
        fProyecto.resetForm();
        $("#IdProyecto").val(0);
        $(".datepicker").datepicker('setDate', '');
    });

    $("#btnConsultoria").on('click', function () {
        $("#titModal").html('Nuevo Proyecto');
        $("#mdlConsultoria").modal("show");
        $("#formConsultoria")[0].reset();
        fProyectoConsultoria.resetForm();
        $("#IdConsultoria").val(0);
        $(".datepicker").datepicker('setDate', '');
    });

    $("#btnAddActividad").on('click', function () {

        var l = Ladda.create(document.querySelector('#btnAddActividad'));
        l.start();

        var IdActividad = 0;
        var IdProyecto = $('#idProyecto').val();
        var Descripcion = $('#txtActividad').val();
        var IsActivo = 1;

        $.ajax({
            url: 'Proyectos/GuardarActividad',
            data: { IdActividad, IdProyecto, Descripcion, IsActivo },
            type: 'POST',
            success: function (result) {
                if (result.Cod == 0) {
                    swal("Actividades", result.Msg, "warning");
                    l.stop();
                }
                else if (result.Cod == 1) {
                    cargaActividades(IdProyecto);
                    $('#txtActividad').val("");
                    swal("Item Gant", result.Msg, "success");
                    l.stop();
                }
                else {
                    swal("Item Gant", result.Msg, "error");
                    l.stop();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
                l.stop();
            }
        });
    });

    $("#btnmdleditar").on('click', function () {
        var IdProyecto = $('#idProyecto').val();
        cargaActividades(IdProyecto);
    });

    $("#mdlResultadoCarga").on('click', function () {
        var IdProyecto = $('#idProyecto').val();
        cargaActividades(IdProyecto);
    });

    $('#tblProyectos').on('click', '.upld', function () {
        var idProyect = $(this).attr('id');
        $('#txtActividad').val("");
        cargaActividades(idProyect);
    });

    $('#tblActividades').on('click', '.edt', function () {
        var idActividad = $(this).attr('id');
        $.ajax({
            url: 'Proyectos/DatosActividad',
            data: { idActividad },
            type: 'POST',
            success: function (result) {

                $("#idActividad").val(result.Datos.id);
                $("#DescripcionAct").val(result.Datos.descripcion);
                $("#Estadoact").val(result.Datos.estado);

                $("#mdlCarga").modal("hide");
                $("#mdlEditar").modal("show");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
                swal("Item Gant", jqXHR, "error");
            }
        });
    });

    $("#btnGuardaredt").on('click', function () {

        var IdActividad = $("#idActividad").val();
        var IdProyecto = $('#idProyecto').val();
        var Descripcion = $("#DescripcionAct").val();
        var IsActivo = $("#Estadoact").val();

        $.ajax({
            url: 'Proyectos/GuardarActividad',
            data: { IdActividad, IdProyecto, Descripcion, IsActivo },
            type: 'POST',
            success: function (result) {
                if (result.Cod == 0) {
                    swal("Item Gant", result.Msg, "warning");
                }
                else if (result.Cod == 1) {
                    swal("Item Gant", result.Msg, "success");
                    $("#mdlEditar").modal("hide");
                    cargaActividades(IdProyecto);
                }
                else {
                    swal("Item Gant", result.Msg, "error");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });
    });

    $('#tblProyectos').on('click', '.edit', function () {
        var idProyect = $(this).attr('id');
        $("#titModal").html('Editar Proyecto');
        fProyecto.resetForm();

        $.ajax({
            url: 'Proyectos/DatosProyecto',
            data: { idProyecto: idProyect },
            type: 'POST',
            success: function (resultado) {

                $("#IdProyecto").val(resultado.Datos.IdProyecto);
                $("#Proyecto").val(resultado.Datos.NombreProyecto);
                $("#ProyectoOriginal").val(resultado.Datos.NombreProyecto);
                $("#Cliente").val(resultado.Datos.Cliente);
                $("#Descripcion").val(resultado.Datos.Descripcion);
                $("#Inicio").val(resultado.Datos.InicioProyecto);
                $("#Fin").val(resultado.Datos.FinProyecto);
                $("#Duracion").val(resultado.Datos.DuracionDias);
                $("#TotalHH").val(resultado.Datos.TotalHH);
                $("#Estado").val(resultado.Datos.IsActivo);

                $(".datepicker").each(function () {
                    $(this).datepicker('setDate', $(this).val());
                });

                $("#mdlProyectos").modal("show");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });
    });

    $.validator.addMethod("ProyectoExiste", function (value, element) {
        if ($("#EProyecto").val() == 1) {
            return false;
        }
        else {
            return true;
        }
    }, "El Proyecto ingresado ya existe");

    var fProyecto = $("#formProyectos").validate({
        rules: {
            Proyecto: { ProyectoExiste: true }
        },
        submitHandler: function (form) {

            var l = Ladda.create(document.querySelector('#btnGuardar'));
            l.start();

            var formulario = $('#formProyectos').serialize();

            $.ajax({
                url: "Proyectos/GuardarProyecto",
                data: formulario,
                cache: false,
                dataType: "json",
                type: 'POST',
                success: function (resultado) {
                    if (resultado.Cod == 0) {
                        swal("Proyectos", resultado.Msg, "warning");
                        l.stop();
                    }
                    else if (resultado.Cod == 1) {
                        ListaProyectos();
                        $("#mdlProyectos").modal("toggle");
                        swal("Proyectos", resultado.Msg, "success");
                        l.stop();
                    }
                    else {
                        swal("Proyectos", resultado.Msg, "error");
                        l.stop();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    l.stop();
                }
            });
        },
        invalidHandler: function (event, validator) {
        }
    });

    var fProyectoConsultoria = $("#formConsultoria").validate({
        rules: {
            Proyecto: { ProyectoExiste: true }
        },
        submitHandler: function (form) {

            var l = Ladda.create(document.querySelector('#btnGuardarConsultoria'));
            l.start();

            var formulario = $('#formConsultoria').serialize();

            $.ajax({
                url: "Proyectos/GuardarProyectoConsultor",
                data: formulario,
                cache: false,
                dataType: "json",
                type: 'POST',
                success: function (resultado) {
                    if (resultado.Cod == 0) {
                        swal("Consultoria", resultado.Msg, "warning");
                        l.stop();
                    }
                    else if (resultado.Cod == 1) {
                        ListaConsultoria();
                        $("#mdlConsultoria").modal("toggle");
                        swal("Consultoria", resultado.Msg, "success");
                        l.stop();
                    }
                    else {
                        swal("Consultoria", resultado.Msg, "error");
                        l.stop();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    l.stop();
                }
            });
        },
        invalidHandler: function (event, validator) {
        }
    });

    $("#Proyecto").on('focus', function () {
        $("#EProyecto").val(0);
    });

    $("#ProyectoC").on('focus', function () {
        $("#EConsultoria").val(0);
    });

    $("#Proyecto").on('focusout', function () {
        var proy1 = $(this).val();

        if (proy1 == $("#ProyectoOriginal").val()) {
            return false;
        }

        $.ajax({
            url: "Proyectos/ExisteProyecto",
            data: { Proyecto: proy1 },
            async: true,
            dataType: "json",
            type: 'POST',
            success: function (resultado) {
                if (resultado.Cod == 1) {
                    $("#EProyecto").val(resultado.Existe);
                    if (resultado.Existe == 1) { swal("Proyecto", "El Proyecto ingresado ya existe", "info"); }
                }
                else {
                    swal("Proyecto", resultado.Msg, "error");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            }
        });

    });

    $("#ProyectoC").on('focusout', function () {
        var proy1 = $(this).val();

        if (proy1 == $("#ConsultoriaOriginal").val()) {
            return false;
        }

        $.ajax({
            url: "Proyectos/ExisteProyectoConsultor",
            data: { ProyectoC: proy1 },
            async: true,
            dataType: "json",
            type: 'POST',
            success: function (resultado) {
                if (resultado.Cod == 1) {
                    $("#EConsultoria").val(resultado.Existe);
                    if (resultado.Existe == 1) { swal("Consultoria", "La Consultoria ingresada ya existe", "info"); }
                }
                else {
                    swal("Proyecto", resultado.Msg, "error");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            }
        });

    });

    ListaClientes();

    ListaClientesConsultoria();

    ListaConsultoria();

    $('#frmUpload .submit-link').on({
        click: function (event) {
            event.preventDefault();
            $(this).closest('form').submit();
        }
    });

    $("#frmUpload").on("submit", function (e) {
        e.preventDefault();

        var l = Ladda.create(document.querySelector('#btnCargaExcel'));
        l.start();

        var ext = $("#fExcel").val().substr($("#fExcel").val().lastIndexOf(".") + 1, 3);
        if (ext != "xls") {
            swal("Carga Masiva", "Solo archivos de extensión XLS, XLSX", "info");
            l.stop();
            return false;
        }
        //setTimeout(function () {
        var f = $(this);
        var formData = new FormData(document.getElementById("frmUpload"));
        $.ajax({
            url: "Proyectos/CargaMasiva",
            type: "post",
            dataType: "json",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            error: function (jqXHR, textStatus, errorThrown) {
                $('#mdlLoading').modal('toggle');
                //console.error(textStatus, errorThrown, jqXHR.responseText);
                console.error(jqXHR);
                swal({ title: "Carga masiva items gant", text: jqXHR, type: "error" });
                l.stop();
            },
            success: function (data, textStatus, jqXHR) {
                if (data.Cod == 1) {



                    //$('#frmUpload').trigger("reset");
                    //$('#resetform').click();

                    $(".fileinput").fileinput("clear");

                    if (data.Resumen.RegistrosRechazados > 0) {
                        $("#idRespResumen").text('El proceso de carga ha finalizado con errores.');
                        $("#Detalle").empty();
                        $("#Detalle").append('</br><h3 class="text-center">Resumen de errores</h3>');
                        $("#Detalle").append(data.Resumen.DescripcionError);
                    }
                    else {
                        $("#Detalle").empty();
                        $("#idRespResumen").text('El proceso de carga ha finalizado correctamente.');
                    }
                    $("#idCantidadRegistrosResumen").text(data.Resumen.CantidadRegistros);
                    $("#idCantidadLeidosResumen").text(data.Resumen.RegistrosLeidos);
                    $("#idCantidadAceptadosResumen").text(data.Resumen.RegistrosCargados);
                    $("#idCantidadRechazadosResumen").text(data.Resumen.RegistrosRechazados);

                    $("#mdlCarga").modal("hide");
                    $("#mdlResumenCarga").modal('show');

                    l.stop();

                    //var IdProyecto = $('#idProyecto').val();
                    //cargaActividades(IdProyecto);



                }
                else {
                    swal({ title: "Carga masiva items gantt", text: data.Msg, type: "error" });
                    l.stop();
                }

            }
        });
        //}, 1000);
    });

    $("#btnDescaExcel").on('click', function () {

        var l = Ladda.create(document.querySelector('#btnDescaExcel'));
        l.start();

        $.ajax({
            url: 'Proyectos/GeneraExcelBase',
            type: 'POST',
            success: function (result) {
                if (result.Cod == 1) {
                    swal("Excel base", result.Msg, "success");
                    window.location.href = "Proyectos/DescargaArchivo?handler=" + result.Handler;
                    l.stop();
                }
                else {
                    swal("Excel base", result.Msg, "warning");
                    l.stop();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
                swal("Excel base", "Se ha producido un error al generar el archivo.", "error");
                l.stop();
            }
        });
    });

    $('#tblProyectosConsultoria').on('click', '.edit', function () {
        var idProyect = $(this).attr('id');
        $("#titModal").html('Editar Consultoria');
        fProyectoConsultoria.resetForm();

        $.ajax({
            url: 'Proyectos/DatosConsultoria',
            data: { idProyecto: idProyect },
            type: 'POST',
            success: function (resultado) {

                $("#IdConsultoria").val(resultado.Datos.IdProyecto);
                $("#ProyectoC").val(resultado.Datos.Proyecto);
                $("#ProyectoOriginal").val(resultado.Datos.NombreProyecto);
                $("#ClienteCon").val(resultado.Datos.Cliente);
                $("#RutCliente").val(resultado.Datos.Rut);
                $("#NombreContacto").val(resultado.Datos.NombreContacto);
                $("#TelefonoContacto").val(resultado.Datos.TelefonoContacto);
                $("#CorreoContacto").val(resultado.Datos.CorreoContacto);
                $("#DescripcionC").val(resultado.Datos.Descripcion);
                $("#PmCliente").val(resultado.Datos.PmCliente);
                $("#PO").val(resultado.Datos.OrdenDeCompra);
                $("#Monto").val(resultado.Datos.Monto);
                $("#InicioC").val(resultado.Datos.Inicio);
                $("#FinC").val(resultado.Datos.Fin);
                $("#TipoMoneda").val(resultado.Datos.TipoMoneda);
                $("#RangoEDP").val(resultado.Datos.RangoEDP);
                $("#Estado").val(resultado.Datos.IsActivo);

                $(".datepicker").each(function () {
                    $(this).datepicker('setDate', $(this).val());
                });

                $("#mdlConsultoria").modal("show");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });
    });

});

function cargaActividades(idProyect) {

    $("#tblActividades").DataTable().clear().draw();

    $.ajax({
        url: 'Proyectos/ActividadesProyecto',
        data: { idProyecto: idProyect },
        type: 'POST',
        success: function (result) {

            $("#nombreProyecto").html('Proyecto ' + result.proyecto);



            $.each(result.Datos, function (i, item) {

                var btn = "<center><button class='btn btn-sm btn-primary edt' data-toggle='tooltip' title='Editar item' id = '" + item.IdTipoActividad + "'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button></center>";


                var Estado = "";
                if (item.IsActivo == 1) {
                    Estado = "Activo";
                }
                else {
                    Estado = "Inactivo";
                }

                $("#tblActividades").DataTable().row.add([
                    item.Descripcion,
                    Estado,
                    btn
                ]).draw(false);
            });

            $('#tblActividades').DataTable().columns.adjust().draw();
            $('#idProyecto').val(idProyect);
            $('#idProyectoM').val(idProyect);
            $("#mdlCarga").modal("show");
            $('[data-toggle="tooltip"]').tooltip();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });
};

function ListaProyectos() {
    tabla.clear();

    $.ajax({
        url: 'Proyectos/ListaProyectos',
        type: 'POST',
        success: function (data) {

            if (data.Cod == 1) {

                $.each(data.Datos, function (i, item) {
                    var btn = "<center><div class='btn-group'><button class='btn btn-sm btn-primary upld' data-toggle='tooltip' title='Cargar items gantt' id = '" + item.IdProyecto + "'><i class='fa fa-upload' aria-hidden='true'></i></button><button class='btn btn-sm btn-info edit' data-toggle='tooltip' title='Editar proyecto' id='" + item.IdProyecto + "'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button></div></center>";
                    var Estado = "";
                    if (item.Estado == 1) {
                        Estado = "Activo";
                    }
                    else {
                        Estado = "Inactivo";
                    }
                    tabla.row.add([
                        item.Proyecto,
                        item.Cliente,
                        item.Descripcion,
                        item.Inicio,
                        item.Fin,
                        item.Duracion,
                        item.TotalHH,
                        Estado,
                        btn
                    ]).draw(false);
                });

                tabla.columns.adjust().responsive.recalc();
            }
            else {
                swal("Proyectos", data.Msg, "error");
            }

            $('[data-toggle="tooltip"]').tooltip();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });
}

function ListaClientes() {
    $.ajax({
        url: 'Clientes/ListaClientes',
        type: 'POST',
        success: function (data) {

            if (data.Cod == 1) {
                $.each(data.Datos, function (i, item) {
                    var opcion = "<option value='" + item.IdCliente + "'>" + item.Nombre + "</option>";
                    $("#Cliente").append(opcion);
                });
            }
            else {
                swal("Clientes", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });

}

function ListaClientesConsultoria() {
    $.ajax({
        url: 'Clientes/ListaClientes',
        type: 'POST',
        success: function (data) {

            if (data.Cod == 1) {
                $.each(data.Datos, function (i, item) {
                    var opcion = "<option value='" + item.IdCliente + "'>" + item.Nombre + "</option>";
                    $("#ClienteCon").append(opcion);
                });
            }
            else {
                swal("ClienteCon", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });

}

function mostrarDesarrollo() {
    $('#divDesarrollo').show();
    $('#divConsultoria').hide();
    $('#botonesProyectos').hide();
}

function mostrarConsultoria() {
    $('#divDesarrollo').hide();
    $('#divConsultoria').show();
    $('#botonesProyectos').hide();
}

function volverProyectos() {
    $('#divDesarrollo').hide();
    $('#divConsultoria').hide();
    $('#botonesProyectos').show();
}

function ListaConsultoria() {
    $.ajax({
        url: 'Proyectos/ListaConsultoria',
        type: 'POST',
        success: function (data) {
            //console.log(data);

            $("#tblProyectosConsultoria").DataTable().clear().draw();

            if (data.Cod == 1) {
                $.each(data.Datos, function (i, item) {
                    var btn = "<center><button class='btn btn-sm btn-info edit' id='" + item.IdProyecto + "'> Editar </button></center>";
                    var Estado = "";
                    if (item.Estado == 1) {
                        Estado = "Activo";
                    }
                    else {
                        Estado = "Inactivo";
                    }
                    $("#tblProyectosConsultoria").DataTable().row.add([
                        item.Proyecto,
                        item.Cliente,
                        item.Rut,
                        item.NombreContacto,
                        item.TelefonoContacto,
                        item.CorreoContacto,
                        item.Descripcion,
                        item.PmCliente,
                        item.OrdenDeCompra,
                        item.Monto,
                        item.Inicio,
                        item.Fin,
                        item.RangoEDP,
                        Estado,
                        btn
                    ]).draw(false);
                });
                $('#tblProyectosConsultoria').DataTable().columns.adjust().draw();
            }
            else {
                swal("Proyectos", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });

}
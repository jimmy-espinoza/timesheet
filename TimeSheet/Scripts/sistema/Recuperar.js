﻿$(document).ready(function () {

    $("#RecuperarForm").on('submit', function (e) {
        e.preventDefault();

        var datos = $("#RecuperarForm").serialize();

        var l = Ladda.create(document.querySelector('#btnrecuperar'));
        l.start();

        $.ajax({
            url: 'RecuperarContrasena',
            data: datos,
            type: "POST",
            dataType: "json",
            success: function (resultado) {

                //console.log(resultado);
                if (resultado.Cod == 1) {
                    swal('Recuperación de contraseña', resultado.Msg, 'success');
                }
                else {
                    swal('Recuperación de contraseña', resultado.Msg, 'warning');
                }
                l.stop();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                l.stop();
            }
        });

    });

});
﻿$(document).ready(function () {

    /*
    $('.datepicker').datepicker({
        language: 'es',
        autoclose: true,
        format: 'dd-mm-yyyy',
        orientation: 'bottom'
    });
    */
    window.IdRegs = [];

    //ListaProyectos();
    //ListaUsuarios();
    var tipo = ($("#tblAtrasos").length ? 1 : 2);

    ListaProyectoUsuario(tipo);

    if ($('#tblAtrasos').length) {

        window.tblAtrasos = $('#tblAtrasos').DataTable({

            paging: true,
            ordering: true,
            info: false,
            bFilter: true,
            //columnDefs: [{ targets: [2], class: "text-nowrap" }, { targets: [3], class: "text-nowrap" }],
            columnDefs: [{ "orderable": false, "targets": 4 }],
            responsive: true,
            language: {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "No existen registros pendientes de aprobación",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            pageLength: 10

        });

        ListaAtrasos();



    }

    if ($('#tblActividades').length) {

        window.tblActividades = $('#tblActividades').DataTable({

            paging: true,
            ordering: true,
            info: false,
            bFilter: true,
            columnDefs: [{ targets: [0, 1, 2], class: "text-nowrap" }, { "orderable": false, "targets": 8 }],
            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            responsive: true,
            autoWidth: false,
            language: {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "No existen actividades pendientes de aprobación",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            pageLength: 5

        });
        ListaActividades();
    }

    if ($('#tblAprobados').length) {

        window.tblAprobados = $('#tblAprobados').DataTable({
            paging: true,
            ordering: true,
            info: false,
            bFilter: true,
            columnDefs: [{ targets: [0, 1, 2], class: "text-nowrap" }, { "orderable": false, "targets": 2 }],
            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            responsive: true,
            autoWidth: false,
            language: {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "No existen actividades pendientes de aprobación",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            pageLength: 5
        });

        ListaAprobados();
    }



    $("#tblAtrasos").on('click', '.apr', function () {
        var id = $(this).attr('id');

        swal({
            title: "Confirmación",
            text: "¿Está seguro que desea aprobar el registro?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: '#5cb85c',
            confirmButtonText: 'Aceptar',
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: '/Aprobacion/AprobarAtraso',
                        data: { IdRegistro: id },
                        type: 'POST',
                        success: function (result) {
                            if (result.Cod == 1) {
                                ListaAtrasos();
                                swal("Aprobación", result.Msg, "success");
                            }
                            else {
                                swal("Aprobación", result.Msg, "warning");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            //console.log(jqXHR, textStatus, errorThrown);
                            console.log(jqXHR);
                        }
                    });
                }
            });

    });

    $("#tblActividades").on('click', '.apr', function () {
        var id = $(this).attr('id');


        swal({
            title: "Confirmación",
            text: "¿Está seguro que desea aprobar el registro?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: '#5cb85c',
            confirmButtonText: 'Aceptar',
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {


                    $.ajax({
                        url: '/Aprobacion/AprobarActividad',
                        data: { IdRegistro: id },
                        type: 'POST',
                        success: function (result) {
                            if (result.Cod == 1) {
                                ListaActividades();
                                swal("Aprobación", result.Msg, "success");

                            }
                            else {
                                swal("Aprobación", result.Msg, "warning");

                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            //console.log(jqXHR, textStatus, errorThrown);
                            console.log(jqXHR);

                        }
                    });
                }
            });

    });

    $("#tblAprobados").on('click', '.edapr', function () {
        var id = $(this).attr('id');

        $("input").removeClass("error")

        $.ajax({
            url: '/Aprobacion/DatosActividadEdt',
            data: { IdRegistro: id },
            type: 'POST',
            success: function (result) {
                if (result.Cod == 1) {

                    $("#fechaapr").val(result.DatosApr.fecha);
                    $("#fechaedt").val(result.DatosEdt.fecha);
                    if (result.DatosApr.fecha != result.DatosEdt.fecha) {
                        $("#fechaedt").addClass("error");
                    }

                    $("#inicioapr").val(result.DatosApr.inicio);
                    $("#inicioedt").val(result.DatosEdt.inicio);
                    if (result.DatosApr.inicio != result.DatosEdt.inicio) {
                        $("#inicioedt").addClass("error");
                    }

                    $("#terminoapr").val(result.DatosApr.termino);
                    $("#terminoedt").val(result.DatosEdt.termino);
                    if (result.DatosApr.termino != result.DatosEdt.termino) {
                        $("#terminoedt").addClass("error");
                    }

                    $("#proyectoapr").val(result.DatosApr.proyecto);
                    $("#proyectoedt").val(result.DatosEdt.proyecto);
                    if (result.DatosApr.proyecto != result.DatosEdt.proyecto) {
                        $("#proyectoedt").addClass("error");
                    }

                    $("#itemapr").val(result.DatosApr.item);
                    $("#itemedt").val(result.DatosEdt.item);
                    if (result.DatosApr.item != result.DatosEdt.item) {
                        $("#itemedt").addClass("error");
                    }

                    $("#actividadapr").val(result.DatosApr.actividad);
                    $("#actividadedt").val(result.DatosEdt.actividad);
                    if (result.DatosApr.actividad != result.DatosEdt.actividad) {
                        $("#actividadedt").addClass("error");
                    }

                    $("#costoapr").val(result.DatosApr.costo);
                    $("#costoedt").val(result.DatosEdt.costo);
                    if (result.DatosApr.costo != result.DatosEdt.costo) {
                        $("#costoedt").addClass("error");
                    }

                    $("#idRegEdt").val(id);

                    $("#mdlEdicionAprobada").modal("toggle");

                }
                else {
                    swal("Aprobación", result.Msg, "warning");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
                swal("Aprobación", result.Msg, "error");
            }
        });
    });

    $("#btnAprobarEdt").on('click', function () {

        $("#mdlEdicionAprobada").modal("toggle");

        swal({
            title: "Confirmación",
            text: "¿Está seguro que desea aprobar el cambio?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: '#21b9bb',
            confirmButtonText: 'Si, aprobar',
            cancelButtonText: "cancelar",
            closeOnConfirm: false,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    $('#mdlLoading').modal('toggle');
                    swal.close();
                    AprobarActividadEdit(1);
                }
                else {
                    $("#mdlEdicionAprobada").modal("toggle");
                }
            });
    });

    $("#btnRechazarEdt").on('click', function () {

        $("#mdlEdicionAprobada").modal("toggle");

        swal({
            title: "Confirmación",
            text: "¿Está seguro que desea rechazar el cambio?",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            inputPlaceholder: "Ingrese motivo del rechazo",
            confirmButtonColor: '#ec4758',
            confirmButtonText: 'Si, rechazar',
            cancelButtonText: "cancelar"
        }, function (inputValue) {
            if (inputValue === false) {
                $("#mdlEdicionAprobada").modal("toggle");
                return false;
            }
            if (inputValue === "") {
                swal.showInputError("Favor ingrese motivo para notificar a usuario.");
                return false
            }
            if (inputValue.length >= 1000) {
                swal.showInputError("Ha excedido el máximo de caracteres permitido.");
                return false
            }
            AprobarActividadEdit(2, inputValue);
        });
    });

    $("#btnAprobar").on('click', function () {

        if (IdRegs.length > 0) {

            actividades = IdRegs.toString();
            swal({
                title: "Confirmación",
                text: "¿Está seguro que desea aprobar todas las actividades?",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: '#5cb85c',
                confirmButtonText: 'Aceptar',
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: true
            },
                function (isConfirm) {
                    if (isConfirm) {

                        var l = Ladda.create(document.querySelector('#btnAprobar'));
                        l.start();

                        $.ajax({
                            url: '/Aprobacion/AprobarActividadMasivo',
                            data: { actividades: actividades },
                            type: 'POST',
                            success: function (result) {
                                if (result.Cod == 1) {
                                    ListaActividades();
                                    swal("Aprobación", result.Msg, "success");
                                    l.stop();
                                }
                                else {
                                    swal("Aprobación", result.Msg, "warning");
                                    l.stop();
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                //console.log(jqXHR, textStatus, errorThrown);
                                console.log(jqXHR);
                                l.stop();
                            }
                        });
                    }
                });
        }
        else {
            swal("Aprobación", 'No existen actividades pendientes de aprobación.', "info");
        }
    });

    $("#proyecto").on('change', function () {
        if ($("#tblAtrasos").length) { ListaAtrasos(); }
        if ($("#tblActividades").length) { ListaActividades(); }
        if ($("#tblAprobados").length) { ListaAprobados(); }
    });

    $("#usuario").on('change', function () {
        if ($("#tblAtrasos").length) { ListaAtrasos(); }
        if ($("#tblActividades").length) { ListaActividades(); }
        if ($("#tblAprobados").length) { ListaAprobados(); }
    });

});

function ListaProyectoUsuario(tipo) {
    var url = (tipo == 1 ? '/Aprobacion/ListaProyectoUsuarioAtrasos' : '/Aprobacion/ListaProyectoUsuarioActividades')
    $.ajax({
        url: url,
        type: 'POST',
        success: function (data) {
            //console.log(data);
            if (data.Cod == 1) {
                var Pros = data.Pros1.concat(data.Pros2);
                $.each(Pros, function (i, item) {
                    $("#proyecto").append("<option value='" + item.id + "'>" + item.nombre + "</value>");
                });
                var Usus = data.Usus1.concat(data.Usus2);
                $.each(Usus, function (i, item) {
                    $("#usuario").append("<option value='" + item.id + "'>" + item.nombre + "</value>");
                });

            }
            else {
                swal("Aprobación", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });
}

function ListaProyectos() {

    $.ajax({
        url: '/Aprobacion/ListaProyectos',
        type: 'POST',
        success: function (data) {

            if (data.Cod == 1) {
                $.each(data.Datos, function (i, item) {
                    $("#proyecto").append("<option value='" + item.id + "'>" + item.nombre + "</value>");
                });
            }
            else {
                swal("Aprobación", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });
}

function ListaUsuarios() {

    $.ajax({
        url: '/Aprobacion/ListaUsuarios',
        type: 'POST',
        success: function (data) {
            if (data.Cod == 1) {
                $.each(data.Datos, function (i, item) {
                    $("#usuario").append("<option value='" + item.id + "'>" + item.nombre + "</value>");
                });
            }
            else {
                swal("Aprobación", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });
}

function ListaAtrasos() {

    var idp = $("#proyecto").val();
    var idu = $("#usuario").val();

    $.ajax({
        data: { proyecto: idp, usuario: idu },
        url: '/Aprobacion/ListaAtrasos',
        type: 'POST',
        success: function (data) {

            if (data.Cod == 1) {
                tblAtrasos.clear().draw();
                $.each(data.Datos, function (i, item) {
                    var icono = "<a href='#' id='" + item.Id + "' class='apr'><i class='fa fa-check fa-2x'></i></a>";
                    var Estado = "";
                    if (item.Estado == 1) {
                        Estado = "Activo";
                    }
                    else {
                        Estado = "Inactivo";
                    }
                    tblAtrasos.row.add([
                        item.Proyecto,
                        item.Usuario,
                        item.FechaSol,
                        item.FechaGes,
                        icono
                    ]).draw(false);
                });
            }
            else {
                swal("Aprobación", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });
}

function ListaActividades() {

    var idp = $("#proyecto").val();
    var idu = $("#usuario").val();

    $.ajax({
        data: { proyecto: idp, usuario: idu },
        url: '/Aprobacion/ListaActividades',
        type: 'POST',
        success: function (data) {

            if (data.Cod == 1) {
                tblActividades.clear().draw();
                IdRegs = [];
                var Datos = data.Datos.concat(data.Datos2);
                $.each(Datos, function (i, item) {
                    var icono = "<center><a id='" + item.codigo + "' class='apr' data-toggle='tooltip' data-placement='top' title='Aprobar solicitud'><i class='fa fa-check fa-2x'></i></a></center>";

                    tblActividades.row.add([
                        item.NombreProyecto,
                        item.Nombre,
                        item.FechaRegistro,
                        item.Itemgantt,
                        item.Actividad,
                        item.HoraInicio,
                        item.HoraFin,
                        item.TotalHoras,
                        icono
                    ]).draw(false);
                    IdRegs[i] = item.IdRegistro;
                });
            }
            else {
                swal("Aprobación", data.Msg, "error");
            }
            $('[data-toggle="tooltip"]').tooltip();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });
}

function ListaAprobados() {

    var idp = $("#proyecto").val();
    var idu = $("#usuario").val();

    $.ajax({
        data: { proyecto: idp, usuario: idu },
        url: '/Aprobacion/ListaAprobados',
        type: 'POST',
        success: function (data) {

            if (data.Cod == 1) {
                tblAprobados.clear().draw();
                $.each(data.Datos, function (i, item) {
                    var icono = "<center><a id='" + item.IdRegistro + "' class='edapr' data-toggle='tooltip' data-placement='top' title='Presione para revisar la solicitud'><i class='fa fa-search fa-2x' aria-hidden='true'></i></a></center>";
                    tblAprobados.row.add([
                        item.NombreProyecto,
                        item.Nombre,
                        icono
                    ]).draw(false);
                });
            }
            else {
                swal("Aprobación", data.Msg, "error");
            }
            $('[data-toggle="tooltip"]').tooltip();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });
}

function AprobarActividadEdit(valor, motivo) {



    var id = $("#idRegEdt").val();
    //var motivo = $("#motivo").val();

    $.ajax({
        url: '/Aprobacion/AprobarActividadEditada',
        data: { id: id, estado: valor, motivo: motivo },
        type: 'POST',
        success: function (result) {
            $('#mdlLoading').modal('hide');
            if (result.Cod == 1) {
                swal("Aprobación", result.Msg, "success");
            }
            else {
                swal("Aprobación", result.Msg, "warning");
            }
            ListaAprobados();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);

        }
    });
}
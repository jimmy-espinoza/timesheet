﻿$(document).ready(function () {

    var hoy = new Date();
    window.dd = hoy.getDate().toString().padStart(2, "0");
    window.mm = (hoy.getMonth() + 1).toString().padStart(2, "0");
    window.yyyy = hoy.getFullYear();

    window.contador = 0;
    var pathname = window.location.pathname;
    if (pathname == "/Timesheet") {
        ListadoProyectosPerfilConsultor();
        ListadoProyectosRegistro();
    }
    else {
        ListadoProyectosRegistro();
        ListaProyectosPerfilConsultor();
    }

    $("#FechaConsultoria").datepicker({
        language: 'es',
        autoclose: true,
        format: 'dd-mm-yyyy',
        orientation: 'bottom'
    });

    /*if ($('#periodo').length) {
        moment.locale('es', {
            months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
            monthsShort: 'Enero._Feb._Mar_Abr._May_Jun_Jul._Ago_Sept._Oct._Nov._Dec.'.split('_'),
            weekdays: 'Domingo_Lunes_Martes_Miercoles_Jueves_Viernes_Sabado'.split('_'),
            weekdaysShort: 'Dom._Lun._Mar._Mier._Jue._Vier._Sab.'.split('_'),
            weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_')
        }
        );


        //window.fdesde = moment().subtract(6, 'days');
        window.fdesde = moment().startOf('isoWeek');
        //window.fhasta = moment();
        window.fhasta = moment().isoWeekday(5);


        $('#periodo').daterangepicker({
            "locale": {
                "format": "DD-MM-YYYY",
                "separator": " - ",
                "applyLabel": "Guardar",
                "cancelLabel": "Cancelar",
                "fromLabel": "Desde",
                "toLabel": "Hasta",
                "customRangeLabel": "Personalizar",
                "daysOfWeek": [
                    "Do",
                    "Lu",
                    "Ma",
                    "Mi",
                    "Ju",
                    "Vi",
                    "Sa"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Setiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "firstDay": 1
            },
            startDate: fdesde,
            endDate: fhasta,    
           
        }, OkFecha);

        OkFecha(fdesde, fhasta);
    }*/

    if ($('#periodo').length) {
        moment.locale('es', {
            months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
            monthsShort: 'Enero._Feb._Mar_Abr._May_Jun_Jul._Ago_Sept._Oct._Nov._Dec.'.split('_'),
            weekdays: 'Domingo_Lunes_Martes_Miercoles_Jueves_Viernes_Sabado'.split('_'),
            weekdaysShort: 'Dom._Lun._Mar._Mier._Jue._Vier._Sab.'.split('_'),
            weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_')
        }
        );


        //window.fdesde = moment().subtract(6, 'days');
        window.fdesde = moment().startOf('isoWeek');
        //window.fhasta = moment();
        window.fhasta = moment().endOf('isoWeek');


        $('#periodo').daterangepicker({
            "locale": {
                "format": "DD-MM-YYYY",
                "separator": " - ",
                "applyLabel": "Guardar",
                "cancelLabel": "Cancelar",
                "fromLabel": "Desde",
                "toLabel": "Hasta",
                "customRangeLabel": "Personalizar",
                "daysOfWeek": [
                    "Do",
                    "Lu",
                    "Ma",
                    "Mi",
                    "Ju",
                    "Vi",
                    "Sa"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Setiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "firstDay": 1
            },
            startDate: fdesde,
            endDate: fhasta,
            ranges: {
                //'Today': [moment(), moment()],
                //'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Semana Actual': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
                'Últimos 7 Días': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 Días': [moment().subtract(29, 'days'), moment()],
                'Mes Actual': [moment().startOf('month'), moment().endOf('month')],
                'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, OkFecha);

        OkFecha(fdesde, fhasta);
    }

    if ($('#tblTimesheetConsultoria').length) {

        var jp = $('#UserJP').val();
        var adm = $('#UserAdmin').val();

        if (adm == 1 || jp == 1) {
            window.tabla = $('#tblTimesheetConsultoria').DataTable({
                paging: true,
                ordering: true,
                info: false,
                bFilter: true,
                autoWidth: false,
                columnDefs: [{ targets: [0, 1] }, { "orderable": false, "targets": 5 }/*{ targets: [8], width: 70 }*/],
                responsive: true,
                language: {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                pageLength: 10
            });
        }
        else {
            window.tabla = $('#tblTimesheetConsultoria').DataTable({
                paging: true,
                ordering: true,
                info: false,
                bFilter: true,
                autoWidth: false,
                columnDefs: [{ targets: [0], class: "text-nowrap" }, { "orderable": false, "targets": 4 }/*{ targets: [8], width: 70 }*/],
                responsive: true,
                language: {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                pageLength: 10
            });
        }

        if ($('#tblTimesheetEntregables').length) {

            var jp = $('#UserJP').val();
            var adm = $('#UserAdmin').val();

            if (adm == 1 || jp == 1) {
                window.tabla2 = $('#tblTimesheetEntregables').DataTable({
                    paging: true,
                    ordering: true,
                    info: false,
                    bFilter: true,
                    autoWidth: false,
                    columnDefs: [{ targets: [0, 1], class: "text-nowrap" }, { "orderable": false, "targets": 8 }/*{ targets: [8], width: 70 }*/],
                    responsive: true,
                    language: {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    pageLength: 10
                });
            }
            else {
                window.tabla2 = $('#tblTimesheetEntregables').DataTable({
                    paging: true,
                    ordering: true,
                    info: false,
                    bFilter: true,
                    autoWidth: false,
                    columnDefs: [{ targets: [0, 1], class: "text-nowrap" }, { "orderable": false, "targets": 7 }/*{ targets: [8], width: 70 }*/],
                    responsive: true,
                    language: {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    pageLength: 10
                });
            }
        }

        ListadoUsuarios();
        ListadoProyectos();
        ListadoEntregables(true);
        ListadoTimesheetConsultor(true);
        ListaProyectosPerfilConsultor(true);


        //ListadoProyectos(); //Todos los Proyectos
        //ListadoProyectosRegistro();
        //ListadoProyectosRegistro();
        //ListadoProyectosConsultoria();
        //ListadoProyectosPerfil();


    }

    $("#btnBuscar").on('click', function () {
        ListadoTimesheetConsultor(false);
        ListadoEntregables(false);
    });

    $("#solicitarexcepcion").on('click', function () {
        CargaModalExcepcionCarga();
    });

    $("#btnGenerarSolicitud").on('click', function () {
        SolicitarExcepcionCarga();
    });

    $("#CierremodalETSC").on('click', function () {
        $("#mdlEditarTSC").toggle();
    });

    $("#CierremodalEC").on('click', function () {
        $("#mdlEditarTSE").toggle();
    });

    $("#btnAdd").on('click', function () {
        if (ValidarFormulario(0) == 1) {
            CrearFila();
        }
    });

    $('#form-timesheet').on('submit', function (e) {
        e.preventDefault();
        var err = 0;
        $("#form-timesheet input").removeClass("erI");
        $("#form-timesheet select").removeClass("erI");

        if (ValidarFormulario(1) == 1) {
            var datos = $("#form-timesheet").serialize();

            var l = Ladda.create(document.querySelector('#btnGuardar'));
            l.start();

            $.ajax({
                url: "GuardarTimesheet",
                data: datos,
                cache: false,
                dataType: "json",
                type: 'POST',
                success: function (result) {
                    if (result.Cod == 1) {
                        swal({
                            title: "TimeSheet",
                            text: result.Msg,
                            html: result.Msg,
                            type: "success"
                        },
                            function () {
                                window.location.href = '/Timesheet';
                            });
                    } else {
                        swal({
                            title: 'TimeSheet',
                            text: result.Msg,
                            html: result.Msg,
                            type: 'warning'
                        });
                    }
                    l.stop();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    swal("TimeSheet", jqXHR.responseText, "error");
                    l.stop();
                }
            });
        }
    });

    $('#form-editar').on('submit', function (e) {
        e.preventDefault();
        var err = 0;
        $("#form-editar input").removeClass("erI");
        $("#form-editar select").removeClass("erI");
        var l = Ladda.create(document.querySelector('#btnGuardar'));
        l.start();
        if (ValidarEdicion() == 1) {
            var datos = $("#form-editar").serialize();
            $.ajax({
                url: "/Timesheet/EditarTimesheet",
                data: datos,
                cache: false,
                dataType: "json",
                type: 'POST',
                success: function (result) {
                    if (result.Cod == 1) {
                        swal({
                            title: "TimeSheet",
                            text: result.Msg,
                            html: result.Msg,
                            type: "success"
                        },
                            function () {
                                ListadoTimesheet(false);
                                $("#mdlEditar").modal("toggle");
                                l.stop();
                            });
                    }
                    else {
                        swal({
                            title: 'TimeSheet',
                            text: result.Msg,
                            html: result.Msg,
                            type: 'warning'
                        });
                        l.stop();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    swal("TimeSheet", jqXHR.responseText, "error");
                    l.stop();
                }
            });
        }
        else { l.stop(); }
    });

    $('#form-editarTSC').on('submit', function (e) {
        e.preventDefault();
        var err = 0;
        $("#form-editarTSC input").removeClass("erI");
        $("#form-editarTSC select").removeClass("erI");
        var l = Ladda.create(document.querySelector('#btnGuardarTSC'));
        l.start();
        var datos = $("#form-editarTSC").serialize();
        $.ajax({
            url: "/Timesheet/EditarTimesheetConsultor",
            data: datos,
            cache: false,
            dataType: "json",
            type: 'POST',
            success: function (result) {
                if (result.Cod == 1) {
                    $("#mdlEditarTSC").toggle();
                    l.stop();
                    swal({
                        title: "TimeSheet",
                        text: result.Msg,
                        html: result.Msg,
                        type: "success"
                    },
                        function () {
                            ListadoTimesheetConsultor(false);
                        });
                }
                else {
                    swal({
                        title: 'TimeSheet',
                        text: result.Msg,
                        html: result.Msg,
                        type: 'warning'
                    });
                    l.stop();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                swal("TimeSheet", jqXHR.responseText, "error");
                l.stop();
            }
        });
    });

    $('#form-editarTSE').on('submit', function (e) {
        e.preventDefault();
        var err = 0;
        $("#form-editarTSE input").removeClass("erI");
        $("#form-editarTSE select").removeClass("erI");
        var l = Ladda.create(document.querySelector('#btnGuardarTSE'));
        l.start();
        var datos = $("#form-editarTSE").serialize();
        $.ajax({
            url: "/Timesheet/EditarEntregableConsultor",
            data: datos,
            cache: false,
            dataType: "json",
            type: 'POST',
            success: function (result) {
                if (result.Cod == 1) {
                    $("#mdlEditarTSE").toggle();
                    l.stop();
                    swal({
                        title: "TimeSheet",
                        text: result.Msg,
                        html: result.Msg,
                        type: "success"
                    },
                        function () {
                            ListadoEntregables(false);
                        });
                }
                else {
                    swal({
                        title: 'TimeSheet',
                        text: result.Msg,
                        html: result.Msg,
                        type: 'warning'
                    });
                    l.stop();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                swal("TimeSheet", jqXHR.responseText, "error");
                l.stop();
            }
        });
    });

    $(document).on('change', '#form-timesheet input', function () {
        $(this).removeClass("erI");
    });

    $(document).on('change', '#form-editar select', function () {
        $(this).removeClass("erI");
    });

    $(document).on('change', '#form-editar input', function () {
        $(this).removeClass("erI");
    });

    $(document).on('change', '#form-timesheet select', function () {
        $(this).removeClass("erI");
    });

    $("#tblTimesheet").on('click', '.edt', function () {

        $("titModal").html("Editar Timesheet");

        $("#IdTimesheetAprobada").val(0);

        var idt = $(this).attr('id');
        $("#IdTimesheet").val(idt);
        $("#Proyecto").empty();
        var IU = $("#IdUsuario").val();
        $.ajax({
            data: { usuario: IU },
            url: '/Timesheet/ListadoProyectosRegistro',
            type: 'POST',
            success: function (result) {
                if (result.Datos.length) {
                    $.each(result.Datos, function (i, item) {
                        var str = '<option value="' + item.IdRelacion + '">' + item.Proyecto + '</value>';
                        var res = str.replaceAll("Ç", "'");
                        $("#Proyecto").append(res);
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });

        var gant = 0;

        $.ajax({
            url: 'Timesheet/DatosTimeSheet',
            data: { IdTimesheet: idt },
            type: 'POST',
            success: function (result) {
                if (result.Cod == 1) {
                    $("#Fecha").datepicker("destroy");
                    $("#Fecha").val(result.Datos.Fecha);
                    $("#Proyecto").val(result.Datos.Proyecto);
                    gant = result.Datos.Gantt;
                    $("#Inicio").val(result.Datos.Inicio);
                    $("#Termino").val(result.Datos.Termino);
                    $("#Costo").empty();
                    if (result.Datos.AplicaCosto == 1) {
                        $("#Costo").append('<option value="2">Directo</option>');
                        $("#Costo").append('<option value="3">Indirecto</option>');
                        $("#Costo").val(result.Datos.Costo);
                    }
                    else {
                        $("#Costo").append('<option value="1">No Aplica</option>');
                    }
                    $("#Actividad").val(result.Datos.Actividad);
                    $("#mdlEditar").modal("toggle");
                    $("#Fecha").datepicker({
                        language: 'es',
                        autoclose: true,
                        format: 'dd-mm-yyyy',
                        orientation: 'bottom'
                    });
                    $("#Fecha").datepicker("refresh");

                    CargaOpcionesProyecto(gant);

                    //$.ajax({
                    //    url: 'Timesheet/CargaListaActividades',
                    //    data: { project: result.Datos.Proyecto },
                    //    type: 'POST',
                    //    success: function (result) {
                    //        $("#Gantt").empty();
                    //        if (result.Cod == 1) {

                    //            $("#Gantt").append('<option value="" disabled>Seleccione</option>');

                    //            $.each(result.Datos, function (i, item) {
                    //                $("#Gantt").append("<option value='" + item.id + "'>" + item.descripcion + "</value>");
                    //            });

                    //            $("#Gantt").append("<option value='0'>Otro item</value>");

                    //            $("#Gantt").val(gant);
                    //        }
                    //        else {
                    //            $("#Gantt").append("<option value='0'>Sin items asignados</value>");
                    //        }
                    //    },
                    //    error: function (jqXHR, textStatus, errorThrown) {
                    //        //console.log(jqXHR, textStatus, errorThrown);
                    //        console.log(jqXHR);
                    //        swal("Items Gantt", "Ocurrió un error al procesar la solicitud", "error");
                    //        $("#Gantt").empty();
                    //    }
                    //});
                }
                else {
                    swal("TimeSheet", result.Msg, "warning");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });
    });

    $("#tblTimesheet").on('click', '.edapro', function () {

        $("#titModal").html("Edición Timesheet aprobada");
        $("#IdTimesheetAprobada").val(1);

        var idt = $(this).attr('id');
        $("#IdTimesheet").val(idt);

        $("#Proyecto").empty();
        var IU = $("#IdUsuario").val();
        $.ajax({
            data: { usuario: IU },
            url: '/Timesheet/ListadoProyectosRegistro',
            type: 'POST',
            success: function (result) {
                if (result.Datos.length) {
                    $.each(result.Datos, function (i, item) {

                        var str = '<option value="' + item.IdRelacion + '">' + item.Proyecto + '</value>';
                        var res = str.replaceAll("Ç", "'");
                        $("#Proyecto").append(res);
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });

        var gant = 0;

        $.ajax({
            url: 'Timesheet/DatosTimeSheet',
            data: { IdTimesheet: idt },
            type: 'POST',
            success: function (result) {
                if (result.Cod == 1) {
                    $("#Fecha").datepicker("destroy");
                    $("#Fecha").val(result.Datos.Fecha);
                    $("#Proyecto").val(result.Datos.Proyecto);
                    gant = result.Datos.Gantt;
                    $("#Inicio").val(result.Datos.Inicio);
                    $("#Termino").val(result.Datos.Termino);
                    $("#Costo").empty();
                    if (result.Datos.AplicaCosto == 1) {
                        $("#Costo").append('<option value="2">Directo</option>');
                        $("#Costo").append('<option value="3">Indirecto</option>');
                        $("#Costo").val(result.Datos.Costo);
                    }
                    else {
                        $("#Costo").append('<option value="1">No Aplica</option>');
                    }
                    $("#Actividad").val(result.Datos.Actividad);
                    $("#mdlEditar").modal("toggle");
                    $("#Fecha").datepicker({
                        language: 'es',
                        autoclose: true,
                        format: 'dd-mm-yyyy',
                        orientation: 'bottom'
                    });
                    $("#Fecha").datepicker("refresh");

                    CargaOpcionesProyecto(gant);

                    //$.ajax({
                    //    url: 'Timesheet/CargaListaActividades',
                    //    data: { project: result.Datos.Proyecto },
                    //    type: 'POST',
                    //    success: function (result) {
                    //        $("#Gantt").empty();
                    //        if (result.Cod == 1) {

                    //            $("#Gantt").append('<option value="" disabled>Seleccione</option>');

                    //            $.each(result.Datos, function (i, item) {
                    //                $("#Gantt").append("<option value='" + item.id + "'>" + item.descripcion + "</value>");
                    //            });

                    //            $("#Gantt").append("<option value='0'>Otro item</value>");

                    //            $("#Gantt").val(gant);
                    //        }
                    //        else {
                    //            $("#Gantt").append("<option value='0'>Sin items asignados</value>");
                    //        }
                    //    },
                    //    error: function (jqXHR, textStatus, errorThrown) {
                    //        //console.log(jqXHR, textStatus, errorThrown);
                    //        console.log(jqXHR);
                    //        swal("Items Gantt", "Ocurrió un error al procesar la solicitud", "error");
                    //        $("#Gantt").empty();
                    //    }
                    //});
                }
                else {
                    swal("TimeSheet", result.Msg, "warning");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });
    });

    $("#tblTimesheet").on('click', '.del', function () {
        var idt = $(this).attr('id');

        var l = Ladda.create(this);

        swal({
            title: "Confirmación",
            text: "¿Está seguro que desea eliminar el registro seleccionado?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Aceptar',
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    l.start();
                    $.ajax({
                        url: 'Timesheet/EliminarTimeSheet',
                        data: { IdTimesheet: idt },
                        type: 'POST',
                        success: function (result) {
                            if (result.Cod == 1) {
                                ListadoTimesheet(false);
                                swal("TimeSheet", result.Msg, "success");
                            }
                            else {
                                swal("TimeSheet", result.Msg, "warning");
                                l.stop();
                            }

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            //console.log(jqXHR, textStatus, errorThrown);
                            console.log(jqXHR);
                            l.stop();
                        }
                    });

                }
            });
    });

    $(document).on('change', '#Proyecto', function () {
        CargaOpcionesProyecto();
    });

    $('#form-timesheetconsultor').on('submit', function (e) {
        e.preventDefault();
        var err = 0;
        $("#form-timesheet input").removeClass("erI");
        $("#form-timesheet select").removeClass("erI");

        if (ValidarFormulario(1) == 1) {
            var datos = $("#form-timesheetconsultor").serialize();

            var l = Ladda.create(document.querySelector('#btnGuardarTSC'));
            l.start();

            $.ajax({
                url: "../Timesheet/GuardarTimesheetConsultor",
                data: datos,
                cache: false,
                dataType: "json",
                type: 'POST',
                success: function (result) {
                    if (result.Cod == 1) {
                        swal({
                            title: "TimeSheet",
                            text: result.Msg,
                            html: result.Msg,
                            type: "success"
                        },
                            function () {
                                window.location.href = '/Timesheet/Consultor';
                            });
                    } else {
                        swal({
                            title: 'TimeSheet',
                            text: result.Msg,
                            html: result.Msg,
                            type: 'warning'
                        });
                    }
                    l.stop();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    swal("TimeSheet", jqXHR.responseText, "error");
                    l.stop();
                }
            });
        }
    });

    $('#form-EntregableConsultor').on('submit', function (e) {
        e.preventDefault();
        var err = 0;
        $("#form-timesheet input").removeClass("erI");
        $("#form-timesheet select").removeClass("erI");

        if (ValidarFormulario(1) == 1) {
            var datos = $("#form-EntregableConsultor").serialize();

            var l = Ladda.create(document.querySelector('#btnGuardarEC'));
            l.start();

            $.ajax({
                url: "../Timesheet/GuardarEntregableConsultor",
                data: datos,
                cache: false,
                dataType: "json",
                type: 'POST',
                success: function (result) {
                    if (result.Cod == 1) {
                        swal({
                            title: "Entregable Consultor",
                            text: result.Msg,
                            html: result.Msg,
                            type: "success"
                        },
                            function () {
                                window.location.href = '/Timesheet/Consultor';
                            });
                    } else {
                        swal({
                            title: 'Entregable Consultor',
                            text: result.Msg,
                            html: result.Msg,
                            type: 'warning'
                        });
                    }
                    l.stop();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    swal("TimeSheet", jqXHR.responseText, "error");
                    l.stop();
                }
            });
        }
    });

    var countries = [];

    autocomplete(document.getElementById("Entregable"), countries);

    $("#ProyectoC").change(function () {
        var id = $("#ProyectoC").val();

        $.ajax({
            url: '../Timesheet/ListaEntregablesAutocompletar',

            type: 'POST',
            data: { id: id },
            success: function (result) {

                switch (result.Cod) {
                    case 1:
                        result.lista.forEach(function (elemento, indice, array) {
                            countries.push(elemento);
                        });
                        break;
                    case 2:
                        swal("TimeSheet", result.Msg, "warning");
                        break;
                    default:
                        swal("TimeSheet", result.Msg, "error");
                        break;
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });
    });

});

function ListadoProyectosPerfilConsultor() {
    var usu = $("#IdUsuario").val();

    var urlp = ($("#UserAdmin").val() == "1" ? 'Proyectos/ListaProyectosConsultoria' : '/Timesheet/ListadoProyectosPerfilConsultor');
    $.ajax({
        url: urlp,
        data: { usuario: usu },
        type: 'POST',
        success: function (result) {
            $.each(result.Datos, function (i, item) {
                $("#proyectoB").append("<option value='" + item.IdProyecto + "'>" + item.Proyecto + "</value>");
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });
}

function ListadoProyectosConsultoria() {
    $.ajax({
        url: '../Proyectos/ListaProyectosConsultoria',
        type: 'POST',
        success: function (result) {
            $.each(result.Datos, function (i, item) {
                $("#proyectoB").append("<option value='" + item.IdProyecto + "'>" + item.Proyecto + "</value>");
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });
}

function OkFecha(start, end) {
    //$('#periodo span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    $('#periodo span').html(start.format('DD MMM YYYY') + ' al ' + end.format('DD MMM YYYY'));
    window.fdesde = start.format('DD-MM-YYYY');
    window.fhasta = end.format('DD-MM-YYYY');
}

function ListadoUsuarios() {
    if ($("#UserAdmin").val() == "1") {
        $.ajax({
            url: '../Usuarios/ListaUsuarios',
            type: 'POST',
            success: function (result) {
                $.each(result.Datos, function (i, item) {
                    $("#usuarioB").append("<option value='" + item.IdUsuario + "'>" + item.Nombre + "</value>");
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });
    }
    else if ($("#UserJP").val() == "1") {
        var UserJp = $("#IdUsuario").val();
        $.ajax({
            url: 'Timesheet/UsuariosProyectoDash',
            data: { IdJP: UserJp },
            type: 'POST',
            success: function (result) {
                $.each(result.Datos, function (i, item) {
                    $("#usuarioB").append("<option value='" + item.IdUsuario + "'>" + item.Nombre + "</value>");
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });
    }

}

function ListadoProyectos() {
    $.ajax({
        url: '../Proyectos/ListaProyectosConsultoria',
        type: 'POST',
        success: function (result) {
            $.each(result.Datos, function (i, item) {
                $("#proyectoB").append("<option value='" + item.IdProyecto + "'>" + item.Proyecto + "</value>");
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });
}

function ListadoProyectosPerfil() {
    var usu = $("#IdUsuario").val();

    var urlp = ($("#UserAdmin").val() == "1" ? '../Proyectos/ListaProyectos' : '../Timesheet/ListadoProyectosPerfil');
    $.ajax({
        url: urlp,
        data: { usuario: usu },
        type: 'POST',
        success: function (result) {
            $.each(result.Datos, function (i, item) {
                $("#proyectoB").append("<option value='" + item.IdProyecto + "'>" + item.Proyecto + "</value>");
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });
}

function ListadoProyectosRegistro() {

    var IU = $("#IdUsuario").val();
    $.ajax({
        data: { usuario: IU },
        url: '/Timesheet/ListadoProyectosRegistro',
        type: 'POST',
        success: function (result) {
            if (result.Datos.length) {
                $("#SinPro").hide();
                $("#tblRegistroconsultoria").show();
                $.each(result.Datos, function (i, item) {
                    $(".ProyectoB").append("<option value='" + item.IdRelacion + "'>" + item.Proyecto + "</value>");
                });
            }
            else {
                $("#tblRegistroconsultoria").hide();
                $("#SinPro").show();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });
}

function ListadoProyectosRegistroCarga(id) {
    var IU = $("#IdUsuario").val();
    $.ajax({
        data: { usuario: IU },
        url: '/Timesheet/ListadoProyectosRegistro',
        type: 'POST',
        success: function (result) {
            if (result.Datos.length) {
                $("#SinPro").hide();
                $("#tblRegistroconsultoria").show();
                $.each(result.Datos, function (i, item) {
                    $("." + id).append("<option value='" + item.IdRelacion + "'>" + item.Proyecto + "</value>");
                });
            }
            else {
                $("#tblRegistroconsultoria").hide();
                $("#SinPro").show();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });
}

function ListadoEntregables(cargainicial) {

    var IdUsuario = $("#usuarioB").val();
    var IdProyecto = $("#proyectoB").val();

    $.ajax({
        url: '../Proyectos/ListaEntregablesProyectosConsultoria',
        type: 'POST',
        data: { desde: window.fdesde, hasta: window.fhasta, usuario: IdUsuario, proyecto: IdProyecto },
        success: function (result) {

            tabla2.clear().draw();

            switch (result.Cod) {
                case 1:
                    $.each(result.Datos, function (i, item) {

                        var btn = "<center><div class='btn-group'>";

                        if (item.IdEntregable > 0) {
                            btn += "<button type='button' onclick = 'MostralModalEditarTSE(" + item.IdEntregable + ")' id='" + item.IdEntregable + "' class='btn btn-sm btn-success edt' data-toggle='tooltip' data-placement='top' title='' data-original-title='Editar actividad.'><i class='fa fa-edit'></i></button>";
                            btn += "<button type='button' id='" + item.IdEntregable + "' class='btn btn-sm btn-danger ladda-button del' data-style='zoom-in'><i class='fa fa-times'></i></button>";

                        }


                        btn += '</div></center>';

                        if ($("#UserAdmin").val() == 1 || $("#UserJP").val() == 1) {
                            tabla2.row.add([
                                item.usuario,
                                item.Proyecto,
                                item.Entregable,
                                item.Avance,
                                item.Pendiente,
                                item.VersionFinal,
                                item.Observaciones,
                                item.CodigoDocumento,
                                btn
                            ]).draw(false);
                        }
                        else {
                            tabla2.row.add([
                                item.Proyecto,
                                item.Entregable,
                                item.Avance,
                                item.Pendiente,
                                item.VersionFinal,
                                item.Observaciones,
                                item.CodigoDocumento,
                                btn
                            ]).draw(false);
                        }

                    });
                    $('[data-toggle="tooltip"]').tooltip();
                    break;
                case 2:
                    if (!cargainicial) {
                        swal("Entregables", result.Msg, "warning");
                    }
                    break;
                default:
                    swal("Entregables", result.Msg, "error");
                    break;
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });

}

function ListadoTimesheetConsultor(cargainicial) {

    var IdUsuario = $("#usuarioB").val();
    var IdProyecto = $("#proyectoB").val();

    $.ajax({
        url: '../Timesheet/ListadoTimesheetConsultor',

        type: 'POST',
        data: { desde: window.fdesde, hasta: window.fhasta, usuario: IdUsuario, proyecto: IdProyecto },
        success: function (result) {
            tabla.clear().draw();

            switch (result.Cod) {
                case 1:
                    $.each(result.Datos, function (i, item) {
                        var btn = "<center><div class='btn-group'>";

                        if (item.IdTimesheetConsultor > 0) {
                            if (item.Aprobado == 0) {
                                btn += "<button type='button' onclick = 'MostralModalEditarTSC(" + item.IdTimesheetConsultor + ")' id='" + item.IdTimesheetConsultor + "' class='btn btn-sm btn-success edt' data-toggle='tooltip' data-placement='top' title='' data-original-title='Editar actividad.'><i class='fa fa-edit'></i></button>";

                            }
                            else {
                                btn += "<button type='button' id='" + item.IdTimesheetConsultor + "' class='btn btn-sm btn-default edapro' data-toggle='tooltip' data-placement='top' title='' data-original-title='Actividad aprobada. Presione para editar.'><i class='fa fa-edit'></i></button>";

                            }
                            btn += "<button type='button' id='" + item.IdTimesheetConsultor + "' class='btn btn-sm btn-danger ladda-button del' data-style='zoom-in'><i class='fa fa-times'></i></button>";
                        }
                        btn += '</div></center>';

                        if ($("#UserAdmin").val() == 1 || $("#UserJP").val() == 1) {
                            tabla.row.add([
                                item.usuario,
                                item.proyecto,
                                item.descripcion,
                                item.fecha,
                                item.dias,
                                btn
                            ]).draw(false);
                        }
                        else {
                            tabla.row.add([
                                item.proyecto,
                                item.descripcion,
                                item.fecha,
                                item.dias,
                                btn
                            ]).draw(false);
                        }
                    });
                    $('[data-toggle="tooltip"]').tooltip();
                    break;
                case 2:
                    if (!cargainicial) {
                        swal("TimeSheet", result.Msg, "warning");
                    }
                    break;
                default:
                    swal("TimeSheet", result.Msg, "error");
                    break;
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });

}

function ValidarFormulario(form = 0) {

    var filas;
    var err = 0;

    if (form == 0) {
        filas = $("#tblRegistroconsultoria tbody tr:last");
    }
    else {
        filas = $("#tblRegistroconsultoria tbody tr");
    }

    $.each(filas, function (index, item) {

        var tds = $(item).find("td");

        /*var f = tds.find("[name='Fecha']");
        var i = tds.find("[name='Inicio']");
        var t = tds.find("[name='Termino']");

        if (i.val() != "" && t.val() != "") {

            var fs = f.val().split("-");
            var is = i.val().split(":");
            var ts = t.val().split(":");

            //var fi = new Date();
            var fi = new Date(fs[2], fs[1] - 1, fs[0]);
            fi.setHours(is[0], is[1], 0, 0);
            //var ft = new Date();
            var ft = new Date(fs[2], fs[1] - 1, fs[0]);
            ft.setHours(ts[0], ts[1], 0, 0);
        }
        */
        /* Validaciones */
        //i.removeClass("erI");
        //t.removeClass("erI");
        $(tds.find("input")).each(function (i, n) {
            if ($(this).val().length < 3) {
                $(this).addClass("erI");
                err++;
            }
        });
        $(tds.find("select")).each(function (i, n) {

            if ($(this).val() == null) {
                $(this).addClass("erI");
                err++;
            }
            else {
                if ($(this).val().length == 0) {
                    $(this).addClass("erI");
                    err++;
                }
                if ($(this).val() == '') {
                    $(this).addClass("erI");
                    err++;
                }
            }

        });

        /*if (fi >= ft) {
            swal("Timesheet", "La hora de término no puede ser menor o igual a la hora de inicio ", "info");
            //i.addClass("erI");
            t.addClass("erI");
            err++;
            return 0;
        }*/

        /*var ante = $(item).closest('tr').prev().find("td");
        if (ante.length) {
            var fAnt = ante.find("[name='Fecha']");
            var tAnt = ante.find("[name='Termino']");
            var tsAnt = tAnt.val().split(":");

            var fsAnt = fAnt.val().split("-");
            var ftAnt = new Date(fsAnt[2], fsAnt[1] - 1, fsAnt[0]);



            //var FV1 = fi;
            var FV1 = new Date(fs[2], fs[1] - 1, fs[0]);
            //FV1 = FV1.setHours(0, 0, 0, 0);
            FV1.setHours(0, 0, 0, 0);
            var FV2 = new Date(fsAnt[2], fsAnt[1] - 1, fsAnt[0]);
            //FV2 = FV2.setHours(0, 0, 0, 0);
            FV2.setHours(0, 0, 0, 0);

            //var ftAnt = new Date();
            ftAnt.setHours(tsAnt[0], tsAnt[1], 0, 0);

            if (i.val() != "" && t.val() != "") {



                if (FV1.valueOf() == FV2.valueOf()) {

                    //console.log("I Actual " + fi);
                    //console.log("F Anteri " + ftAnt);

                    if (fi.valueOf() < ftAnt.valueOf()) {
                        swal("Timesheet", "La hora inicio no puede ser menor a la hora de término anterior.", "info");
                        //i.addClass("erI");
                        i.addClass("erI");
                        err++;
                        return 0;
                    }
                }
            }
        }
        */


        /*
        if (fi >= ft) {
            swal("Timesheet", "La hora de termino no puede ser menor o igual a la hora de inicio ", "info");
            //i.addClass("erI");
            t.addClass("erI");
            err++;
        }
        else if (ftAnt > fi ) {
            swal("Timesheet", "La hora inicio no puede ser menor a la hora de termino anterior.", "info");
            //i.addClass("erI");
            i.addClass("erI");
            err++;
        }
        else {
            //i.removeClass("erI");

            i.removeClass("erI");
            t.removeClass("erI");
            $(tds.find("input")).each(function (i, n) {
                if ($(this).val().length < 3) {
                    $(this).addClass("erI");
                    err++;
                }
            });
            $(tds.find("select")).each(function (i, n) {
                if ($(this).val().length == 0) {
                    $(this).addClass("erI");
                    err++;
                }
            });
        }
        */


    });

    return (err == 0 ? 1 : 0);

}

function ListaProyectosPerfilConsultor() {

    var IU = $("#IdUsuario").val();
    $.ajax({
        data: { usuario: IU },
        url: '../Timesheet/ListaProyectosPerfilConsultor',
        type: 'POST',
        success: function (result) {
            if (result.Datos.length) {
                $("#SinPro").hide();
                $("#tblRegistroconsultoria").show();
                $.each(result.Datos, function (i, item) {
                    $(".ProyectoC").append("<option value='" + item.IdProyecto + "'>" + item.Proyecto + "</value>");
                });
            }
            else {
                $("#tblRegistroconsultoria").hide();
                $("#SinPro").show();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });
}

function MostralModalEditarTSC(id) {
    $.ajax({
        url: '../Proyectos/ListaProyectosConsultoria',
        type: 'POST',
        success: function (result) {
            $.each(result.Datos, function (i, item) {
                $("#ProyectoC").append("<option value='" + item.IdProyecto + "'>" + item.Proyecto + "</value>");
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });

    $.ajax({
        url: '../Timesheet/ObjetoTimesheetConsultorEditar',

        type: 'POST',
        data: { id: id },
        success: function (result) {

            switch (result.Cod) {
                case 1:
                    $("#ProyectoC").val(result.proyecto);
                    $("#DescripcionC").val(result.descripcion);
                    $("#FechaC").val(result.fecha);
                    $("#IdTimesheetConsultor").val(id);
                    $("#IsAprobado").val(result.aprobado);

                    $("#mdlEditarTSC").toggle();

                    $('[data-toggle="tooltip"]').tooltip();
                    break;
                case 2:
                    if (!cargainicial) {
                        swal("TimeSheet", result.Msg, "warning");
                    }
                    break;
                default:
                    swal("TimeSheet", result.Msg, "error");
                    break;
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });
}

function MostralModalEditarTSE(id) {
    $.ajax({
        url: '../Proyectos/ListaProyectosConsultoria',
        type: 'POST',
        success: function (result) {
            $.each(result.Datos, function (i, item) {
                $("#ProyectoEC").append("<option value='" + item.IdProyecto + "'>" + item.Proyecto + "</value>");
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });

    $.ajax({
        url: '../Timesheet/ObjetoTimesheetEntregableEditar',

        type: 'POST',
        data: { id: id },
        success: function (result) {

            switch (result.Cod) {
                case 1:
                    $("#IdEntregable").val(id);
                    $("#ProyectoEC").val(result.ProyectoC);
                    $("#EntregableC").val(result.EntregableC);
                    $("#AvanceC").val(result.AvanceC);
                    $("#PendienteC").val(result.PendienteC);
                    $("#VersionFinal").val(result.VersionFinal);
                    $("#Observaciones").val(result.Observaciones);
                    $("#CodigoDocumento").val(result.CodigoDocumento);

                    $("#mdlEditarTSE").toggle();

                    $('[data-toggle="tooltip"]').tooltip();
                    break;
                case 2:
                    if (!cargainicial) {
                        swal("TimeSheet", result.Msg, "warning");
                    }
                    break;
                default:
                    swal("TimeSheet", result.Msg, "error");
                    break;
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });
}

function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function (e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
                getdataentregable($("#Entregable").val());
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}

function getdataentregable(txt) {

        $.ajax({
            url: '../Timesheet/getdataentregable',

            type: 'POST',
            data: { txt: txt },
            success: function (result) {

                switch (result.Cod) {
                    case 1:
                        $("#Avance").val(result.AvanceC);
                        $("#Pendiente").val(result.PendienteC);
                        $("#VersionFinal").val(result.VersionFinal);
                        $("#Observacion").val(result.Observaciones);
                        $("#CodigoDocumento").val(result.CodigoDocumento);
                        break;
                    case 2: 
                        break;
                    default:
                        swal("TimeSheet", result.Msg, "error");
                        break;
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });
}

function CrearFila() {

    var anterior = contador;
    contador += 1;


    var filas;

    filas = $("#tblRegistroconsultoria tbody tr:last");

    var f;
    var c;

    $.each(filas, function (index, item) {
        var tds = $(item).find("td");

        f = tds.find("[name='Fecha']").val();

    });

    var fila = "<tr>";

    var str = '<td><select class="form-control input-sm ProyectoC' + contador + '" name="ProyectoC" id="ProyectoC" onchange="ListadoTipoActividades(ÇProyectoC' + contador + 'Ç,ÇTipoAct' + contador + 'Ç,' + contador + ')"><option value="">Seleccione</option></select></td>';
    var res = str.replaceAll("Ç", "'");
    fila += res;

    fila += "<td><input type='text' class='form-control input-sm datepicker fecha" + contador + "' value='" + f + "' name='Fecha' id='Fecha' autocomplete='off'></td>";


    //var str = '<td><select class="form-control input-sm TipoAct' + contador + '" name="TipoAct" id="TipoAct"><option value="">Seleccione proyecto</option></select></td>';
    //var res = str.replaceAll("Ç", "'");
    //fila += res;

    fila += "<td><input type='text' class='form-control input-sm' name='Actividad' id='Actividad' autocomplete='off'></td>";

    var aplicacosto = $('#AplicaCosto').val();

    if (aplicacosto == 1) {
        fila += '<td><select class="form-control input-sm" name="Costo" id = "Costo"><option value="">Seleccione</option><option value="2" ' + (c == 2 ? "selected" : "") + ' >Directo</option><option value="3" ' + (c == 3 ? "selected" : "") + '>Indirecto</option></select></td>';
    }
    else {
        fila += '<td><select class="form-control input-sm" name="Costo' + contador + '" id="Costo' + contador + '"><option value="1"' + (c == 1 ? "selected" : "") + '>No Aplica</option></select></td>';
    }

    fila += "<td><input type='text' class='form-control input-sm clockpicker' name='Inicio' id='Inicio' autocomplete='off'></td>";
    fila += "<td><input type='text' class='form-control input-sm clockpicker' name='Termino' id='Termino'  autocomplete='off'></td>";
    fila += "<td><button type='button' class='btn btn-sm btn-danger btn-circle del'><i class='fa fa-minus'></i></td>";
    fila += "</tr>";
    $("#tblRegistro").append(fila);

    //$("#Proyecto").clone().appendTo("#divP" + contador);
    ListadoProyectosRegistroCarga('Proyecto' + contador);

    //$("#Costo0").clone().appendTo("#divC" + contador);

    CrearDateP();
    CrearClockP();




    setTimeout(function () {
        var p = $('.Proyecto' + anterior).val();
        $('.Proyecto' + contador).val(p);

        ListadoTipoActividades('Proyecto' + contador, 'TipoAct' + contador, contador);

        setTimeout(function () {
            var p = $('.TipoAct' + anterior).val();
            $('.TipoAct' + contador).val(p);
        }, 500);
    }, 500);

}

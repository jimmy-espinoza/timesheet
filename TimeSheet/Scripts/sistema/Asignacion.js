﻿window.porcasig = 0;
window.asigusuario = 0;

$(document).ready(function () {

    //$('#Porcentaje').keyup(function () {
    //    this.value = (this.value + '').replace(/[^0-9]/g, '');
    //});

    window.tabla = $('#tblUsuarioProyecto').DataTable({

        paging: true,
        ordering: true,
        info: false,
        bFilter: true,
        //columnDefs: [{ targets: [2], class: "text-nowrap" }, { targets: [3], class: "text-nowrap" }],
        columnDefs: [{ "orderable": false, "targets": 1 }],
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar: ",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10

    });

    ListaUsuarios2();

    $('#tblUsuarioAsignado').on('click', '.edit1', function () {
        var IdUsuarioProyecto2 = $(this).attr('id');
        $("#titModal").html('Editar Asignación');
        fUsuarioProyecto.resetForm();
        var Nombre = $("#NomUsuario" + IdUsuarioProyecto2).html();

        $.ajax({
            url: 'Asociacion/DatosUsuarioProyecto',
            data: { IdUsuarioProyecto: IdUsuarioProyecto2 },
            type: 'POST',
            success: function (resultado) {

                $("#IdUsuarioProyecto").val(resultado.Datos.IdUsuarioProyecto);
                $("#Usuario").val(Nombre);
                $("#IdUsuario").val(resultado.Datos.IdUsuario);
                $("#Proyecto2").val(resultado.Datos.IdProyecto);
                $("#NombreProyecto").val(resultado.Datos.NombreProyecto);
                $("#Porcentaje").val(resultado.Datos.PorcentajeAsignacion);
                $("#IsJp").val(resultado.Datos.IsJp);
                porcasig = resultado.Datos.TotalProyecto;
                asigusuario = resultado.Datos.PorcentajeAsignacion;

                var resultado = (100 - (porcasig - asigusuario)).toFixed(2);
                $("#porcmax").html("Asignación Disponible (" + resultado + "%)");

                $("#mdlAsignacion").modal("show");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });
    });

        //$.validator.addMethod("maximoDisponible", function (value, element) {
    //    var maximo = (100 - (porcasig - asigusuario));
    //    if (parseInt(value) > parseInt(maximo)) {
    //        return false;
    //    }
    //    else {
    //        return true;
    //    }
    //}, "El % asignado supera el máximo disponible ");

    $.validator.addMethod("maximoDisponible", function (value, element) {
        var maximo = (100 - (porcasig - asigusuario));
        if (parseFloat(value) > parseFloat(maximo)) {
            return false;
        }
        else {
            return true;
        }

    }, "El % asignado supera el máximo disponible ");

    var fUsuarioProyecto = $("#formAsignacion").validate({
        rules: {
            Porcentaje: { maximoDisponible: true }
        },
        submitHandler: function (form) {

            var l = Ladda.create(document.querySelector('#btnGuardar'));
            l.start();

            var formulario = $('#formAsignacion').serialize();

            $.ajax({
                url: "Asociacion/GuardarRelacion",
                data: formulario,
                cache: false,
                dataType: "json",
                type: 'POST',
                success: function (resultado) {
                    if (resultado.Cod == 0) {
                        swal("Asociación", resultado.Msg, "warning");
                        l.stop();
                    }
                    else if (resultado.Cod == 1) {
                        swal("Asociación", resultado.Msg, "success");
                        $("#mdlAsignacion").modal("toggle");
                        ListaUsuarioProyecto();
                        ListaUsuarios2();
                        l.stop();

                    }
                    else {
                        swal("Asociación", resultado.Msg, "error");
                        l.stop();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    l.stop();
                }
            });
        },
        invalidHandler: function (event, validator) {
        }

    });
       
    ListaProyectos2();
    ListaEmpresas();

    $("#Proyecto").on('change', function () {
        if ($(this).val() == 0) {
            $('#divtabla').hide();
            $('#divas').show();
        }
        else {
            $('#divtabla').show();
            $('#divas').hide();
            ListaUsuarios2();
        }

        ListaUsuarioProyecto();

    });

    $("#Empresa").on('change', function () {
        ListaUsuarioProyecto();
        ListaUsuarios2();
    });

});

function ListaUsuarios2() {

    tabla.clear().draw();
    var Proyecto = $("#Proyecto").val();
    var Proveedor = $("#Empresa").val();
    $.ajax({
        data: { proy: Proyecto, prov: Proveedor },
        url: 'Asociacion/ListaUsuariosEnProyecto',
        type: 'POST',
        success: function (data) {

            if (data.Cod == 1) {
                $.each(data.Datos, function (i, item) {
                    var btnAgregar = "<button class='btn btn-primary btn-sm' id='btnAgregar' onclick='Mostrarmodal(" + item.IdUsuario + ")' name='btnAgregar'><i class= 'fa fa-plus'></i></button>";
                    var vnombre = "<span id='NomUsuario" + item.IdUsuario + "'>" + item.Nombre + "</span>";
                    var cajaTP = "<input type='hidden' id='TP" + item.IdUsuario + "' value='" + item.TotalProyecto + "' >";
                    tabla.row.add([
                        vnombre + cajaTP,
                        btnAgregar
                    ]).draw(false);
                });
            }
            else {
                swal("Asociación", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });

}

function Mostrarmodal(Id) {
    porcasig = $("#TP" + Id).val();

    if (porcasig == 100) {
        swal('Asignación Usuarios', 'Proyecto con el 100% de recursos asignados', 'info');
    }
    else {

        var Nombre = $("#NomUsuario" + Id).html();
        $("#titModal").html('Agregar usuario a proyecto');
        $("#mdlAsignacion").modal("show");
        $("#formAsignacion")[0].reset();
        $("#IdUsuario").val(Id);
        /*fUsuarioProyecto.resetForm();*/
        $("#IdUsuarioProyecto").val(0);
        $("#Usuario").val(Nombre);
        $("#Proyecto2").val($("#Proyecto").val());
        $("#NombreProyecto").val($("#Proyecto option:selected").text());
        asigusuario = 0;

        var resultado = (100 - porcasig).toFixed(2);
        $("#porcmax").html("Asignación Disponible (" + resultado + "%)");
    }
}

function ListaProyectos2() {
    $.ajax({
        url: 'Proyectos/ListaProyectos2',
        type: 'POST',
        success: function (data) {

            if (data.Cod == 1) {
                $.each(data.Datos, function (i, item) {
                    var opcion = "<option value='" + item.IdProyecto + "'>" + item.Proyecto + "</option>";
                    $("#Proyecto").append(opcion);
                });
            }
            else {
                swal("Proyectos", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });

}

function ListaEmpresas() {
    $.ajax({
        url: 'Empresas/ListaEmpresas',
        type: 'POST',
        success: function (data) {

            if (data.Cod == 1) {
                $.each(data.Datos, function (i, item) {
                    var opcion = "<option value='" + item.IdEmpresa + "'>" + item.NombreEmpresa + "</option>";
                    $("#Empresa").append(opcion);
                });
            }
            else {
                swal("Empresas", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });
}

if ($('#tblUsuarioAsignado').ready) {
    window.tabla2 = $('#tblUsuarioAsignado').DataTable({

        paging: true,
        ordering: true,
        info: false,
        bFilter: true,
        //columnDefs: [{ targets: [2], class: "text-nowrap" }, { targets: [3], class: "text-nowrap" }],
        columnDefs: [{ "orderable": false, "targets": 4 }],
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:  ",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10
    });

    if ($('#Proyecto').val() == 0) {
        $('#divtabla').hide();
        $('#divas').show();

    }
    else {
        $('#divtabla').show();
        $('#divas').hide();

    }
    ListaUsuarioProyecto();

    $('#tblUsuarioAsignado').on('click', '.edit2', function () {
        var IdUsuarioProyecto3 = $(this).attr('id2');

        swal({
            title: "Confirmación",
            text: "¿Está seguro que desea quitar la asignación seleccionada?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Aceptar',
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {

                    $.ajax({
                        url: 'Asociacion/EliminarAsignacion',
                        data: { IdUsuarioProyecto: IdUsuarioProyecto3 },
                        type: 'POST',
                        success: function (resultado) {

                            if (resultado.Cod == 0) {
                                swal("Asociación", resultado.Msg, "warning");
                            }
                            else if (resultado.Cod == 1) {
                                ListaUsuarioProyecto();
                                ListaUsuarios2();
                                swal("Asociación", resultado.Msg, "success");
                            }
                            else {
                                swal("Asociación", resultado.Msg, "error");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            //console.log(jqXHR, textStatus, errorThrown);
                            console.log(jqXHR);
                        }
                    });
                } else {
                    swal("Asignación de Usuario", "Asignación sin cambios", "info");
                }
            });
    });

    var fUsuarioProyectoEliminacion = $("#formEliminacion").validate({
        submitHandler: function (form) {

            var formulario1 = $('#formEliminacion').serialize();

            $.ajax({
                url: "Asociacion/EliminarAsociacion",
                data: formulario1,
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                success: function (resultado) {
                    if (resultado.Cod == 0) {
                        swal("Asociacion", resultado.Msg, "warning");
                    }
                    else if (resultado.Cod == 1) {
                        ListaUsuarioProyecto();
                        $("#mdlAsignacionEliminacion").modal("toggle");
                        swal("Asociación", resultado.Msg, "success");
                    }
                    else {
                        swal("Asociación", resultado.Msg, "error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });
        },
        invalidHandler: function (event, validator) {
        }

    });
}

function ListaUsuarioProyecto() {

    tabla2.clear().draw();
    var Proyecto = $("#Proyecto").val();
    var Proveedor = $("#Empresa").val();
    //porcasig = 0;

    $.ajax({
        data: { proy: Proyecto, prov: Proveedor },
        url: 'Asociacion/ListaUsuarioProyecto',
        type: 'POST',
        success: function (data) {


            if (data.Cod == 1) {
                $.each(data.Datos, function (i, item) {
                    //porcasig += item.Porcentaje
                    var btnEliminar = "<button class='btn btn-sm btn-danger edit2' id2='" + item.IdUsuarioProyecto + "'> <i class = 'fa fa-times'></i> </button>";
                    var btnEditar = "<button class='btn btn-sm btn-warning edit1' id='" + item.IdUsuarioProyecto + "'> <i class = 'fa fa-edit'></i> </button>";
                    var vnombre = "<span id='NomUsuario" + item.IdUsuarioProyecto + "'>" + item.Usuario + "</span>";
                    tabla2.row.add([
                        vnombre,
                        item.Proveedor,
                        item.Proyecto,
                        item.Porcentaje + '%',
                        btnEditar + ' ' + btnEliminar
                    ]).draw(false);
                });
            }
            else {
                swal("Asociación", data.Msg, "error");
            }

            porcasig = data.TotalProyecto;

            var resultado = (100 - porcasig).toFixed(2);
            $("#porcmax").html("Asignación disponible (" + resultado + "%)");

            if (porcasig > 0) {
                if (porcasig < 100) {
                    swal('Porcentaje Asignación', 'El porcentaje total asignado es menor a 100%', 'warning');
                }
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });
}

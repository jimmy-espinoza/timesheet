﻿$(document).ready(function () {
    //Horas Combobox
    for (c = 1; c <= 24; c++) {
        var h = c.toString().padStart(2, "0") + ":00";
        $("#HoraEjecucion").append("<option value='" + h + "'>" + h + "</option>");
    }

    window.tabla = $('#tblNotifica').DataTable({
        paging: true,
        ordering: false,
        info: false,
        bFilter: true,
        scrollY: 400,
        //columnDefs: [{ targets: [2], class: "text-nowrap" }, { targets: [3], class: "text-nowrap" }],
        responsive: false,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10

    });

    ListaNotificaciones2();

    $("#btnHorario").on('click', function () {

        $.ajax({
            url: "/Configuracion/DatosNotificacion",
            data: { IdParametroGeneral: 1 },
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            success: function (result) {
                if (result.Cod == 1) {
                    $("#HoraEjecucion").val(result.Datos.Valor);
                    $("#mdlHorario").modal("toggle");
                }
                else {
                    swal("Configuración", result.Msg, "warning");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                swal("Configuración", jqXHR.responseText, "error");
            }
        });
    });

    $("#form-horario").on("submit", function (e) {
        e.preventDefault();
        var datos = $(this).serialize();
        $.ajax({
            url: "/Configuracion/GuardarHoraEjecucion",
            data: datos,
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            success: function (result) {
                if (result.Cod == 1) {
                    swal("Configuración", result.Msg, "success");
                    $("#mdlHorario").modal("toggle");
                }
                else {
                    swal("Configuración", result.Msg, "warning");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                swal("Configuración", jqXHR.responseText, "error");
            }
        });
    });

    $("#tblNotifica").on('click', '.visto', function () {
        var idn = $(this).attr("id");
        idn = idn.replace("Visto_", "");
        var visto = $(this).attr("data-visto");
        visto = (visto == 1 ? 0 : 1);

        var objeto = $(this);
        $.ajax({
            data: { IdNotificacion: idn, Accion: visto },
            url: "/Notificaciones/MarcarVisto",
            dataType: "json",
            type: 'POST',
            success: function (result) {

                if (result.Cod == 1) {
                    var men = "";
                    if (visto == 0) {
                        men = "Marcar como Leída.";
                        $("#icnL_" + idn).removeClass("text-info");
                        $("#icnL_" + idn).removeClass("fa-eye");
                        $("#icnL_" + idn).addClass("text-danger");
                        $("#icnL_" + idn).addClass("fa-eye-slash");

                        $("#icn_" + idn).removeClass("text-info");
                        $("#icn_" + idn).removeClass("fa-eye");
                        $("#icn_" + idn).addClass("text-danger");
                        $("#icn_" + idn).addClass("fa-eye-slash");

                        $("#tit_" + idn).css("font-weight", "bold");
                        $("#titL_" + idn).css("font-weight", "bold");
                        $("#menL_" + idn).css("font-weight", "bold");
                        tnl++;
                    }
                    else {
                        men = "Marcar como no Leída.";
                        $("#icnL_" + idn).removeClass("text-danger");
                        $("#icnL_" + idn).removeClass("fa-eye-slash");
                        $("#icnL_" + idn).addClass("text-info");
                        $("#icnL_" + idn).addClass("fa-eye");

                        $("#icn_" + idn).removeClass("text-danger");
                        $("#icn_" + idn).removeClass("fa-eye-slash");
                        $("#icn_" + idn).addClass("text-info");
                        $("#icn_" + idn).addClass("fa-eye");

                        $("#tit_" + idn).css("font-weight", "normal");
                        $("#titL_" + idn).css("font-weight", "normal");
                        $("#menL_" + idn).css("font-weight", "normal");
                        tnl--;
                    }

                    $(objeto).attr("data-visto", visto);
                    $(objeto).attr("data-original-title", men);

                    $(objeto).tooltip('hide');
                    $("#spanNL").html(tnl);

                    //CargarNot();
                }
                else {
                    swal("Notificaciones", result.Msg, "warning");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                swal("Notificaciones", jqXHR.responseText, "error");
            }
        });

    });

});

function ListaNotificaciones() {
    var c = 0;
    var iu = $("#IDU").val();
    $.ajax({
        data: { IdUsuario: iu, Cantidad: 0 },
        url: "/Notificaciones/ListadoNotificaciones",
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        success: function (result) {
            $.each(result.Datos, function (i, v) {

                var visto = (v.Visto == 0 ? 'Marcar como Leída.' : 'Marcar como no Leída.');
                var icono = "<i id='icnL_" + v.IdNotificacion + "' class='fa fa-2x fa-" + (v.Visto == 1 ? "eye text-info" : "eye-slash text-danger") + " fa-fw'></i>";
                var cFLag = "";
                var imp = "";
                switch (v.Importancia) {
                    case 1:
                        cFLag = "danger";
                        imp = "Alta";
                        break;
                    case 2:
                        cFLag = "warning";
                        imp = "Media";
                        break;
                    case 3:
                        cFLag = "info";
                        imp = "Baja";
                        break;
                }
                var flag = "<i class='fa fa-flag text-" + cFLag + " fa-fw' title='Importancia: " + imp + "'></i>";

                var color = "";
                switch (v.Importancia) {
                    case 1:
                        color = "alert-danger";
                        break;
                    case 2:
                        color = "alert-success";
                        break;
                    case 3:
                        color = "alert-info";
                        break;
                }


                /*
                var notifica = "<div class='alert " + color + "' role='alert'>";
                notifica += "<h4>" + v.Titulo + "</h4><div>" + v.Mensaje + "</div>";
                notifica += "<div class='pull-right'><i class='fa fa-clock-o'></i> <span>" + v.Hace + "</span></div>";
                notifica += "</div>";
                */

                var cssclass = (v.Visto == 1 ? "font-weight: normal" : "font-weight: bold");

                var notifica = "<div class='chat-element'>";
                notifica += "<a id='Visto_" + v.IdNotificacion + "' data-visto='" + v.Visto + "' data-toggle='tooltip' data-placement='bottom' title='' data-original-title='" + visto + "' href='#' class='pull-left visto'>" + icono + "</a>";
                notifica += "<div class='media-body'>";
                notifica += "<small class='pull-right text-navy'>" + v.Hace + "</small>";
                notifica += "<h3 style='" + cssclass + "' id='titL_" + v.IdNotificacion + "'>" + flag + " " + v.Titulo + "</h3>";
                notifica += "<h4 class='m-b-xs'>" + v.Mensaje + "</h4>";
                notifica += "<p class='text-muted'>" + v.HoraRegistro + " " + v.FechaRegistroDMY + "</p>";
                notifica += "</div>";
                notifica += "</div >";

                $("#ListaNotifica").append(notifica);

                $('[data-toggle="tooltip"]').tooltip();
            });

            if (result.Datos.length == 0) {

                var notifica = "<div class='chat-element'>";
                notifica += "<a href='#' class='pull-left'><i class='fa fa-thumbs-o-up text-info fa-2x fa-fw'></i></a>";
                notifica += "<div class='media-body'>";
                notifica += "<small class='pull-right text-navy'>Hace instantes</small>";
                notifica += "<h3>Sin Notificaciones</h3>";
                notifica += "<h4 class='m-b-xs'>No existen notificaciones para mostrar.</h4>";
                notifica += "</div>";
                notifica += "</div >";

                $("#ListaNotifica").append(notifica);
            }


            //console.log(result);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            swal("TimeSheet", jqXHR.responseText, "error");
        }
    });



}

function ListaNotificaciones2() {
    var c = 0;
    var iu = $("#IDU").val();
    $.ajax({
        data: { IdUsuario: iu, Cantidad: 0 },
        url: "/Notificaciones/ListadoNotificaciones",
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        success: function (result) {
            $.each(result.Datos, function (i, v) {

                var visto = (v.Visto == 0 ? 'Marcar como Leída.' : 'Marcar como no Leída.');
                var icono = "<i id='icnL_" + v.IdNotificacion + "' class='fa fa-2x fa-" + (v.Visto == 1 ? "eye text-info" : "eye-slash text-danger") + " fa-fw'></i>";
                var cFLag = "";
                var imp = "";
                switch (v.Importancia) {
                    case 1:
                        cFLag = "danger";
                        imp = "Alta";
                        break;
                    case 2:
                        cFLag = "warning";
                        imp = "Media";
                        break;
                    case 3:
                        cFLag = "info";
                        imp = "Baja";
                        break;
                }
                var flag = "<i class='fa fa-flag text-" + cFLag + " fa-fw' title='Importancia: " + imp + "'></i>";

                var color = "";
                switch (v.Importancia) {
                    case 1:
                        color = "alert-danger";
                        break;
                    case 2:
                        color = "alert-success";
                        break;
                    case 3:
                        color = "alert-info";
                        break;
                }


                /*
                var notifica = "<div class='alert " + color + "' role='alert'>";
                notifica += "<h4>" + v.Titulo + "</h4><div>" + v.Mensaje + "</div>";
                notifica += "<div class='pull-right'><i class='fa fa-clock-o'></i> <span>" + v.Hace + "</span></div>";
                notifica += "</div>";
                */

                var cssclass = (v.Visto == 1 ? "font-weight: normal" : "font-weight: bold");

                var notifica = "<div class='chat-element'>";
                notifica += "<a id='Visto_" + v.IdNotificacion + "' data-visto='" + v.Visto + "' data-toggle='tooltip' data-placement='rigth' title='' data-original-title='" + visto + "' href='#' class='pull-left visto'>" + icono + "</a>";
                notifica += "<div class='media-body'>";
                notifica += "<small class='pull-right text-navy'>" + v.Hace + "</small>";
                notifica += "<h3 style='" + cssclass + "' id='titL_" + v.IdNotificacion + "'>" + flag + " " + v.Titulo + "</h3>";
                notifica += "<h4 class='m-b-xs'>" + v.Mensaje + "</h4>";
                notifica += "<p class='text-muted'>" + v.HoraRegistro + " " + v.FechaRegistroDMY + "</p>";
                notifica += "</div>";
                notifica += "</div >";

                //$("#ListaNotifica").append(notifica);
                tabla.row.add([
                    "<a id='Visto_" + v.IdNotificacion + "' data-visto='" + v.Visto + "' data-toggle='tooltip' data-placement='rigth' title='' data-original-title='" + visto + "' href='#' class='pull-left visto'>" + icono + "</a>",
                    "<h3 style='" + cssclass + "' id='titL_" + v.IdNotificacion + "'>" + flag + " " + v.Titulo + "</h3>" +
                    "<span style='" + cssclass + "' id='menL_" + v.IdNotificacion + "'>" + v.Mensaje + "</span>" +
                    "<p class='text-muted'>" + v.HoraRegistro + " " + v.FechaRegistroDMY + "</p>",
                    "<small class='pull-right text-navy'>" + v.Hace + "</small>"
                ]).draw(false);

                $('[data-toggle="tooltip"]').tooltip();
            });

            if (result.Datos.length == 0) {

                var notifica = "<div class='chat-element'>";
                notifica += "<a href='#' class='pull-left'><i class='fa fa-thumbs-o-up text-info fa-2x fa-fw'></i></a>";
                notifica += "<div class='media-body'>";
                notifica += "<small class='pull-right text-navy'>Hace instantes</small>";
                notifica += "<h3>Sin Notificaciones</h3>";
                notifica += "<h4 class='m-b-xs'>No existen notificaciones para mostrar.</h4>";
                notifica += "</div>";
                notifica += "</div >";

                $("#ListaNotifica").append(notifica);
            }


            //console.log(result);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            swal("TimeSheet", jqXHR.responseText, "error");
        }
    });



}

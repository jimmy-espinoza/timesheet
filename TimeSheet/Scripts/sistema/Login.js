﻿$(document).ready(function () {

    var actual = window.location.toString();
    var final = actual.substring(actual.length - 1, actual.length);

    var login = "";
    var actividad = "";
    if (final == "/") {
        login = "Home/Login";
        actividad = "Timesheet";
    }
    else {
        login = "/Home/Login";
        actividad = "/Timesheet";
    }


    $("#frmLogin").on('submit', function (e) {
        e.preventDefault();

        if ($("#usuario").val() == "" || $("#password").val() == "") {
            swal('Inicio Sesión', 'Debe completar usuario y password.', 'info');
            return 0;
        }

        var datos = $("#frmLogin").serialize();

        var l = Ladda.create(document.querySelector('#btnLogin'));
        l.start();

        $.ajax({
            url: login,
            data: datos,
            type: "POST",
            dataType: "json",
            success: function (resultado) {

                //console.log(resultado);
                if (resultado.Cod == 1) {
                    //swal('Inicio Sesión', resultado.Msg, 'success');
                    window.location.href = resultado.Url;
                }
                else if (resultado.Cod == 2) {
                    swal('Inicio Sesión', resultado.Msg, 'warning');
                    l.stop();
                }
                else {
                    swal('Inicio Sesión', resultado.Msg, 'error');
                    l.stop();
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                l.stop();
            }
        });

    });

});
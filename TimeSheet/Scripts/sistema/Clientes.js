﻿$(document).ready(function () {

    window.tabla = $('#tblClientes').DataTable({

        paging: true,
        ordering: true,
        info: false,
        bFilter: true,
        //columnDefs: [{ targets: [2], class: "text-nowrap" }, { targets: [3], class: "text-nowrap" }],
        columnDefs: [{ "orderable": false, "targets": 2 }],
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10

    });

    ListaClientes();

    $("#btnNuevo").on('click', function () {
        $("#titModal").html('Nuevo Cliente');
        $("#mdlClientes").modal("show");
        $("#formClientes")[0].reset();
        fClientes.resetForm();
        $("#IdCliente").val(0);
    });

    $('#tblClientes').on('click', '.edit', function () {
        var IdCliente = $(this).attr('id');
        $("#titModal").html('Editar Cliente');
        fClientes.resetForm();

        $.ajax({
            url: 'Clientes/DatosCliente',
            data: { IdCliente: IdCliente },
            type: 'POST',
            success: function (resultado) {

                //console.log(resultado);

                $("#IdCliente").val(resultado.Datos.IdCliente);
                $("#Nombre").val(resultado.Datos.Nombre);
                $("#ClienteOriginal").val(resultado.Datos.Nombre);
                $("#Rut").val(resultado.Datos.Rut);
                $("#RutOriginal").val(resultado.Datos.Rut);

                $("#mdlClientes").modal("show");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });

    });

    $.validator.addMethod("ClienteExiste", function (value, element) {
        if ($("#ECliente").val() == 1) {
            return false;
        }
        else {
            return true;
        }
    }, "El Cliente ingresado ya existe");

    $.validator.addMethod("rutValido", function (value, element) {
        return Fn.validaRut(value);
    }, "El rut ingresado no es válido ");

    /*
    $.validator.addMethod("RutUnico", function (value, element) {
        if ($("#Rut").value == ListaClientes.Rut);
    }, "El rut ingresado ya existe");
    */

    $.validator.addMethod("RutExiste", function (value, element) {
        if ($("#ERUT").val() == 1) {
            return false;
        }
        else {
            return true;
        }
    }, "El rut ingresado ya existe");

    var fClientes = $("#formClientes").validate({
        rules: {
            //Rut: { rutValido: true, RutUnico: false }
            Rut: { rutValido: true, RutExiste: true },
            Nombre: { ClienteExiste: true }
        },
       
        submitHandler: function (form) {
            var l = Ladda.create(document.querySelector('#btnGuardar'));
            l.start();

            var formulario = $('#formClientes').serialize();

            $.ajax({
                url: "Clientes/GuardarCliente",
                data: formulario,
                cache: false,
                dataType: "json",
                type: 'POST',
                success: function (resultado) {
                    if (resultado.Cod == 0) {
                        swal("Clientes", resultado.Msg, "warning");
                        l.stop();
                    }
                    else if (resultado.Cod == 1) {
                        ListaClientes();
                        $("#mdlClientes").modal("toggle");
                        swal("Clientes", resultado.Msg, "success");
                        l.stop();
                    }
                    else {
                        swal("Clientes", resultado.Msg, "error");
                        l.stop();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    l.stop();
                }
            });
        },
        invalidHandler: function (event, validator) {
            //alert('Completar');
        }

    });

    $("#Nombre").on('focus', function () {
        $("#ECliente").val(0);
    });

    $("#Nombre").on('focusout', function () {
        var NombreCli = $(this).val();

        if (NombreCli == $("#ClienteOriginal").val()) {
            return false;
        }

        $.ajax({
            url: "Clientes/ExisteNombreCliente",
            data: { Nombre: NombreCli },
            async: true,
            dataType: "json",
            type: 'POST',
            success: function (resultado) {
                if (resultado.Cod == 1) {
                    $("#ECliente").val(resultado.Existe);
                    if (resultado.Existe == 1) { swal("Clientes", "El cliente ingresado ya existe", "info"); }
                }
                else {
                    swal("Clientes", resultado.Msg, "error");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            }
        });

    });

    $("#Rut").on('focus', function () {
        $("#ERUT").val(0);
    });

    $("#Rut").on('focusout', function () {
        var rutcli = $(this).val();

        if (rutcli == $("#RutOriginal").val())
        {
            return false;
        }

        $.ajax({
            url: "Clientes/ExisteCliente",
            data: { Rut: rutcli },
            async: true,
            dataType: "json",
            type: 'POST',
            success: function (resultado) {
                if (resultado.Cod == 1) {
                    $("#ERUT").val(resultado.Existe);
                    if (resultado.Existe == 1) { swal("Clientes", "El RUT ingresado ya existe", "info"); }
                }
                else {
                    swal("Clientes", resultado.Msg, "error");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            }
        });

    });
    
});

function ListaClientes() {
    tabla.clear();

    $.ajax({
        url: 'Clientes/ListaClientes',
        type: 'POST',
        success: function (data) {
            //console.log(data);
            if (data.Cod == 1) {
                $.each(data.Datos, function (i, item) {
                    var btn = "<center><button class='btn btn-sm btn-info edit' id='" + item.IdCliente + "'> Editar </button></center>";
                    tabla.row.add([
                        item.Nombre,
                        item.Rut,
                        btn
                    ]).draw(false);
                });
            }
            else {
                swal("Clientes", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });

}

var Fn = {
    // Valida el rut con su cadena completa "XXXXXXXX-X"
    validaRut: function (rutCompleto) {
        if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test(rutCompleto))
            return false;
        var tmp = rutCompleto.split('-');
        var digv = tmp[1];
        var rut = tmp[0];
        if (digv == 'K') digv = 'k';
        return (Fn.dv(rut) == digv);
    },
    dv: function (T) {
        var M = 0, S = 1;
        for (; T; T = Math.floor(T / 10))
            S = (S + T % 10 * (9 - M++ % 6)) % 11;
        return S ? S - 1 : 'k';
    }
}
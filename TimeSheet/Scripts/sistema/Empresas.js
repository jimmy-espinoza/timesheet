﻿$(document).ready(function () {

    $('input[type="checkbox"]').iCheck({ checkboxClass: 'icheckbox_square-green' });

    window.tabla = $('#tblEmpresas').DataTable({

        paging: true,
        ordering: true,
        info: false,
        bFilter: true,
        columnDefs: [{ targets: [0, 1], class: "text-nowrap" }, { "orderable": false, "targets": 7 }],
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10

    });

    ListaEmpresas();

    $("#btnNuevo").on('click', function () {
        $("#titModal").html('Nuevo Proveedor');
        $("#mdlEmpresas").modal("show");
        $("#formEmpresas")[0].reset();
        fEmpresa.resetForm();
        $("#IdEmpresa").val(0);
        $("#AplicaCosto").prop('checked', false);
        $("#AplicaCosto").iCheck('update');
    });


    $('#tblEmpresas').on('click', '.edit', function () {
        var IdEmpresa = $(this).attr('id');
        $("#titModal").html('Editar Proveedor');
        fEmpresa.resetForm();

        $.ajax({
            url: 'Empresas/DatosEmpresa',
            data: { IdEmpresa: IdEmpresa },
            type: 'POST',
            success: function (resultado) {
                $("#IdEmpresa").val(resultado.Datos.IdEmpresa);
                $("#Empresa").val(resultado.Datos.NombreEmpresa);
                $("#ProveedorOriginal").val(resultado.Datos.NombreEmpresa);
                $("#Rut").val(resultado.Datos.RutEmpresa);
                $("#RutOriginal").val(resultado.Datos.RutEmpresa);
                $("#Contacto").val(resultado.Datos.Contacto);
                $("#TelefonoContacto").val(resultado.Datos.TelefonoContacto);
                $("#MailContacto").val(resultado.Datos.MailContacto);
                var ac = (resultado.Datos.AplicaCosto == 1 ? true : false);
                $("#AplicaCosto").prop('checked', ac);
                $("#AplicaCosto").iCheck('update');
                $("#UfHora").val(resultado.Datos.UfHora.toString().replace('.', ','));
                $("#Estado").val(resultado.Datos.IsActivo);

                $("#mdlEmpresas").modal("show");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });
    });

    $.validator.addMethod("rutValido", function (value, element) {
        return Fn.validaRut(value);
    }, "El RUT ingresado no es válido ");
    /*
    $.validator.addMethod("RutUnico", function (value, element) {
        if ($("#Rut").value == ListaEmpresas.Rut);
    }, "El rut ingresado ya existe");
    */

    $.validator.addMethod("RutExiste", function (value, element) {
        if ($("#ERUT").val() == 1) {
            return false;
        }
        else {
            return true;
        }
    }, "El RUT ingresado ya existe");

    $.validator.addMethod("ProveedorExiste", function (value, element) {
        if ($("#EProveedor").val() == 1) {
            return false;
        }
        else {
            return true;
        }
    }, "El Proveedor ingresado ya existe");

    var fEmpresa = $("#formEmpresas").validate({
        rules: {
            //Rut: { rutValido: true, RutUnico: false }
            Rut: { rutValido: true, RutExiste: true },
            Empresa: { ProveedorExiste: true }
        },

        submitHandler: function (form) {

            var l = Ladda.create(document.querySelector('#btnGuardar'));
            l.start();

            var formulario = $('#formEmpresas').serialize();

            $.ajax({
                url: "Empresas/GuardarEmpresa",
                data: formulario,
                cache: false,
                dataType: "json",
                type: 'POST',
                success: function (resultado) {
                    if (resultado.Cod == 0) {
                        swal("Proveedor", resultado.Msg, "warning");
                    }
                    else if (resultado.Cod == 1) {
                        ListaEmpresas();
                        $("#mdlEmpresas").modal("toggle");
                        swal("Proveedor", resultado.Msg, "success");
                    }
                    else {
                        swal("Proveedor", resultado.Msg, "error");
                    }
                    l.stop();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    l.stop();
                }
            });
        },
        invalidHandler: function (event, validator) {
        }

    });

    $("#Rut").on('focus', function () {
        $("#ERUT").val(0);
    });

    $("#Rut").on('focusout', function () {
        var rutemp = $(this).val();

        if (rutemp == $("#RutOriginal").val()) {
            return false;
        }

        $.ajax({
            url: "Empresas/ExisteEmpresa",
            data: { Rut: rutemp },
            async: true,
            dataType: "json",
            type: 'POST',
            success: function (resultado) {
                if (resultado.Cod == 1) {
                    $("#ERUT").val(resultado.Existe);
                    if (resultado.Existe == 1) { swal("Proveedor", "El RUT ingresado ya existe", "info"); }
                }
                else {
                    swal("Proveedor", resultado.Msg, "error");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            }
        });

    });


    $("#Empresa").on('focus', function () {
        $("#EProveedor").val(0);
    });

    $("#Empresa").on('focusout', function () {
        var prov1 = $(this).val();

        if (prov1 == $("#ProveedorOriginal").val()) {
            return false;
        }

        $.ajax({
            url: "Empresas/Existeproveedor",
            data: { Empresa: prov1 },
            async: true,
            dataType: "json",
            type: 'POST',
            success: function (resultado) {
                if (resultado.Cod == 1) {
                    $("#EProveedor").val(resultado.Existe);
                    if (resultado.Existe == 1) { swal("Proveedor", "El Proveedor ingresado ya existe", "info"); }
                }
                else {
                    swal("Proveedor", resultado.Msg, "error");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            }
        });

    });



    $('.decimales').on('input', function () {
        this.value = this.value.replace(/[^0-9,]/g, '').replace(/,/g, ',');
    });

});

function ListaEmpresas() {
    tabla.clear();

    $.ajax({
        url: 'Empresas/ListaEmpresas',
        type: 'POST',
        success: function (data) {
            //console.log(data);
            if (data.Cod == 1) {
                $.each(data.Datos, function (i, item) {
                    var btn = "<button class='btn btn-sm btn-info edit' id='" + item.IdEmpresa + "'> Editar </button>";
                    var Estado = "";
                    if (item.Estado == 1) {
                        var Estado = "Activo";
                    }
                    else {
                        var Estado = "Inactivo";
                    }
                    tabla.row.add([
                        item.NombreEmpresa,
                        item.RutEmpresa,
                        item.Contacto,
                        item.TelefonoContacto,
                        item.MailContacto,
                        item.UfHora,
                        Estado,
                        btn
                    ]).draw(false);
                });
            }
            else {
                swal("Proveedor", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });
}

var Fn = {
    // Valida el rut con su cadena completa "XXXXXXXX-X"
    validaRut: function (rutCompleto) {
        if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test(rutCompleto))
            return false;
        var tmp = rutCompleto.split('-');
        var digv = tmp[1];
        var rut = tmp[0];
        if (digv == 'K') digv = 'k';
        return (Fn.dv(rut) == digv);
    },
    dv: function (T) {
        var M = 0, S = 1;
        for (; T; T = Math.floor(T / 10))
            S = (S + T % 10 * (9 - M++ % 6)) % 11;
        return S ? S - 1 : 'k';
    }
}
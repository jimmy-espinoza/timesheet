﻿using System.Web;
using System.Web.Optimization;

namespace TimeSheet
{
    public class BundleConfig
    {

        public static void RegisterBundles(BundleCollection bundles)
        {



            // Vendor scripts
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-3.1.1.min.js"));

            // jQuery Validation
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            "~/Scripts/jquery.validate.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js"));

            // registroTimesheet script
            bundles.Add(new ScriptBundle("~/bundles/resgitroTimesheet").Include(
                      "~/Scripts/app/registroTimesheet.js"));

            // Inspinia script
            bundles.Add(new ScriptBundle("~/bundles/inspinia").Include(
                      "~/Scripts/app/inspinia.js"));

            // SlimScroll
            bundles.Add(new ScriptBundle("~/plugins/slimScroll").Include(
                      "~/Scripts/plugins/slimScroll/jquery.slimscroll.min.js"));

            // datatable
            bundles.Add(new ScriptBundle("~/plugins/dataTables").Include(
                      "~/Scripts/plugins/dataTables/jquery.dataTables.min.js",
                      "~/Scripts/plugins/dataTables/dataTables.bootstrap.min.js",
                      "~/Scripts/plugins/dataTables/dataTables.responsive.min.js"));

            // jQuery plugins
            bundles.Add(new ScriptBundle("~/plugins/metsiMenu").Include(
                      "~/Scripts/plugins/metisMenu/metisMenu.min.js"));

            bundles.Add(new ScriptBundle("~/plugins/pace").Include(
                      "~/Scripts/plugins/pace/pace.min.js"));

            // moment
            bundles.Add(new ScriptBundle("~/plugins/moment").Include(
                      "~/Scripts/plugins/fullcalendar/moment.min.js"));

            // FullCalendar
            bundles.Add(new ScriptBundle("~/plugins/fullcalendar").Include(
                      "~/Scripts/plugins/fullcalendar/fullcalendar.min.js"));

            // datepicker
            bundles.Add(new ScriptBundle("~/plugins/datepicker").Include(
                      "~/Scripts/plugins/datepicker/bootstrap-datepicker.min.js",
                      "~/Scripts/plugins/datepicker/bootstrap-datepicker.es.min.js"));

            // daterangepicker
            bundles.Add(new ScriptBundle("~/plugins/daterangepicker").Include(
                      "~/Scripts/moment.min.js",
                      "~/Scripts/plugins/daterangepicker/daterangepicker.js"));

            // select2
            bundles.Add(new ScriptBundle("~/plugins/select2").Include(
                      "~/Scripts/plugins/select2/select2.full.min.js"));

            // CSS select2
            bundles.Add(new StyleBundle("~/Content/select2").Include(
                      "~/Content/plugins/select2/select2.min.css"));

            // CSS datatbale
            bundles.Add(new StyleBundle("~/Content/dataTables").Include(
                      "~/Content/plugins/dataTables/datatables.min.css",
                      "~/Content/plugins/dataTables/responsive.bootstrap.min.css"));

            // CSS datepicker
            bundles.Add(new StyleBundle("~/Content/datepicker").Include(
                      "~/Content/plugins/datepicker/bootstrap-datepicker3.min.css"));

            // CSS daterangepicker
            bundles.Add(new StyleBundle("~/Content/daterangepicker").Include(
                      "~/Content/plugins/daterangepicker/daterangepicker.css"));

            // CSS fullcalendar
            bundles.Add(new StyleBundle("~/Content/fullcalendar").Include(
                      "~/Content/plugins/fullcalendar/fullcalendar.css",
                      "~/Content/plugins/fullcalendar/fullcalendar.print.css"));

            // CSS style (bootstrap/inspinia)
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/animate.css",
                      "~/Content/style.css"));

            // Font Awesome icons
            bundles.Add(new StyleBundle("~/font-awesome/css").Include(
                      "~/fonts/font-awesome/css/font-awesome.min.css", new CssRewriteUrlTransform()));


            // Sweet alert Styless
            bundles.Add(new StyleBundle("~/plugins/sweetAlertStyles").Include(
                      "~/Content/plugins/sweetalert/sweetalert.css"));

            // Sweet alert
            bundles.Add(new ScriptBundle("~/plugins/sweetAlert").Include(
                      "~/Scripts/plugins/sweetalert/sweetalert.min.js"));

            // ChartJS Styless
            bundles.Add(new StyleBundle("~/Content/chartjs").Include(
                      "~/Content/plugins/chartjs/Chart.min.css"));

            // ChartJS
            bundles.Add(new ScriptBundle("~/plugins/chartjs").Include(
                      "~/Scripts/plugins/chartjs/Chart.min.js"));

            // iCheck Styless
            bundles.Add(new StyleBundle("~/Content/iCheck").Include(
                      "~/Content/plugins/iCheck/custom.css"));

            // iCheck
            bundles.Add(new ScriptBundle("~/plugins/iCheck").Include(
                      "~/Scripts/plugins/iCheck/icheck.min.js"));

            // Clockpicker styles
            bundles.Add(new StyleBundle("~/plugins/clockpickerStyles").Include(
                      "~/Content/plugins/clockpicker/clockpicker.css"));

            // Clockpicker
            bundles.Add(new ScriptBundle("~/plugins/clockpicker").Include(
                      "~/Scripts/plugins/clockpicker/clockpicker.js"));

            // Ladda styles
            bundles.Add(new StyleBundle("~/plugins/laddaStyles").Include(
                      "~/Content/plugins/ladda/ladda.min.css"));

            // Ladda
            bundles.Add(new ScriptBundle("~/plugins/ladda").Include(
                      "~/Scripts/plugins/ladda/spin.min.js",
                      "~/Scripts/plugins/ladda/ladda.min.js"));

            // jasnyBootstrap styles
            bundles.Add(new StyleBundle("~/plugins/jasnyBootstrapStyles").Include(
                      "~/Content/plugins/jasny/jasny-bootstrap.min.css"));

            // jasnyBootstrap 
            bundles.Add(new ScriptBundle("~/plugins/jasnyBootstrap").Include(
                      "~/Scripts/plugins/jasny/jasny-bootstrap.min.js"));

        }
    }
}

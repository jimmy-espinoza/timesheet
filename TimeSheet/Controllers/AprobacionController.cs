﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TimeSheet.Controllers
{
    public class AprobacionController : Controller
    {
        // GET: Aprobacion
        public ActionResult Index()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public ActionResult Atrasos()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

            var DB = new Dao.Model.DB_TSEntities();
            var user = (Dao.Model.FactUsuario)Session["usuario"];
            var proyectoatraso = DB.FactAprobacionTimesheet.Where(u => u.RelUsuarioProyecto.IdUsuario == user.IdUsuario).Where(a => a.RelUsuarioProyecto.IsJp == 1).Where(a => a.EstadoSolicitud == 0).Select(x => x).ToList();

            var Pros = (from x in proyectoatraso
                        group x by x.RelUsuarioProyecto.IdProyecto into grup
                        select new
                        {
                            id = grup.Key,
                            nombre = grup.FirstOrDefault().RelUsuarioProyecto.FactProyecto.NombreProyecto
                        }).ToList();

            return View();
        }

        public ActionResult Actividades()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        /*
        [HttpPost]
        public JsonResult ListaProyectos()
        {
            try
            {
                var use = (Dao.Model.FactUsuario)Session["usuario"];
                var ProAsig = Dal.Clases.ProyectoDAL.ListaProyectosPerfil(use.IdUsuario);

                var Datos = from x in ProAsig
                            where x.IsJp == 1
                            select new
                            {
                                id = x.IdProyecto,
                                nombre = x.FactProyecto.NombreProyecto
                            };


                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ListaUsuarios()
        {
            try
            {
                var use = (Dao.Model.FactUsuario)Session["usuario"];
                var ProAsig = Dal.Clases.ProyectoDAL.ListaProyectosPerfil(use.IdUsuario);

                List<Dao.Model.FactUsuario> usuarios = new List<Dao.Model.FactUsuario>();
                foreach (var p in ProAsig.Where(j => j.IsJp == 1).ToList())
                {
                    var nuse = Dal.Clases.RelUsuariosDAL.ListaUsuarioProyecto(p.IdProyecto, 0);
                    foreach (var u in nuse.Where(j => j.IsJp == 0).ToList())
                    {
                        usuarios.Add(u.FactUsuario);

                    }
                }

                var Datos = from x in usuarios
                            select new
                            {
                                id = x.IdUsuario,
                                nombre = x.Nombre
                            };

                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        */

        [HttpPost]
        public JsonResult ListaProyectoUsuarioAtrasos()
        {
            try
            {
                var use = (Dao.Model.FactUsuario)Session["usuario"];
                var ProAsig = Dal.Clases.ProyectoDAL.ListaProyectosPerfil(use.IdUsuario);
                List<int> IdPros = ProAsig.Where(j => j.IsJp == 1).Select(s => s.IdProyecto).ToList();
                var DatosP = Dal.Clases.TimeSheetDAL.ListaTimesheetByProyecto(IdPros);

                var Pros = from x in DatosP
                           group x by x.RelUsuarioProyecto.IdProyecto into grup
                           select new
                           {
                               id = grup.Key,
                               nombre = grup.FirstOrDefault().RelUsuarioProyecto.FactProyecto.NombreProyecto
                           };

                List<Dao.Model.FactUsuario> usuarios = new List<Dao.Model.FactUsuario>();
                foreach (var p in ProAsig.Where(j => j.IsJp == 1).ToList())
                {
                    var nuse = Dal.Clases.RelUsuariosDAL.ListaUsuarioProyecto(p.IdProyecto, 0);
                    //foreach (var u in nuse.Where(j => j.IsJp == 0).ToList())
                    foreach (var u in nuse)
                    {
                        usuarios.Add(u.FactUsuario);

                    }
                }

                var Usus = from x in usuarios
                           select new
                           {
                               id = x.IdUsuario,
                               nombre = x.Nombre
                           };

                return Json(new { Cod = 1, Msg = "OK", Pros, Usus }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ListaProyectoUsuarioActividades()
        {
            try
            {
                var use = (Dao.Model.FactUsuario)Session["usuario"];
                var ProAsig = Dal.Clases.ProyectoDAL.ListaProyectosPerfil(use.IdUsuario);
                List<int> IdPros = ProAsig.Where(j => j.IsJp == 1).Select(s => s.IdProyecto).ToList();
                var Datos = Dal.Clases.TimeSheetDAL.ListaTimesheetByProyecto(IdPros);
                var Datos2 = Dal.Clases.TimeSheetConsultorDAL.ListaTimesheetConsultorByProyecto(IdPros);
                var Pros1 = from x in Datos
                           group x by x.RelUsuarioProyecto.IdProyecto into grup
                           select new
                           {
                               id = grup.Key,
                               nombre = grup.FirstOrDefault().RelUsuarioProyecto.FactProyecto.NombreProyecto
                           };

                var Pros2 = from x in Datos2
                           group x by x.RelUsuarioProyecto.IdProyecto into grup
                           select new
                           {
                               id = grup.Key,
                               nombre = grup.FirstOrDefault().RelUsuarioProyecto.FactProyecto.NombreProyecto
                           };

                var Usus1 = from x in Datos
                               //where x.RelUsuarioProyecto.IsJp==0
                           group x by x.RelUsuarioProyecto.IdUsuario into grup
                           select new
                           {
                               id = grup.Key,
                               nombre = grup.FirstOrDefault().RelUsuarioProyecto.FactUsuario.Nombre          
                           };

                var Usus2 = from x in Datos2
                               //where x.RelUsuarioProyecto.IsJp==0
                           group x by x.RelUsuarioProyecto.IdUsuario into grup
                           select new
                           {
                               id = grup.Key,
                               nombre = grup.FirstOrDefault().RelUsuarioProyecto.FactUsuario.Nombre
                           };

                //var Usus = Usus1.Union(Usus2).Distinct().ToList();

                //var Pros = Pros1.Union(Pros2).Distinct().ToList();

                /*
                List<Dao.Model.FactUsuario> usuarios = new List<Dao.Model.FactUsuario>();
                foreach (var p in ProAsig.Where(j => j.IsJp == 1).ToList())
                {
                    var nuse = Dal.Clases.RelUsuariosDAL.ListaUsuarioProyecto(p.IdProyecto, 0);
                    foreach (var u in nuse.Where(j => j.IsJp == 0).ToList())
                    {
                        usuarios.Add(u.FactUsuario);

                    }
                }

                var Datos = from x in usuarios
                            select new
                            {
                                id = x.IdUsuario,
                                nombre = x.Nombre
                            };
                            */

                return Json(new { Cod = 1, Msg = "OK", Pros1, Usus1, Pros2, Usus2 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ListaAtrasos()
        {
            try
            {
                int IdPro = int.Parse(Request.Form["proyecto"]);
                int IdUsu = int.Parse(Request.Form["usuario"]);

                var use = (Dao.Model.FactUsuario)Session["usuario"];
                var ProAsig = Dal.Clases.ProyectoDAL.ListaProyectosPerfil(use.IdUsuario);
                List<int> IdPros = ProAsig.Where(j => j.IsJp == 1).Select(s => s.IdProyecto).ToList();
                var lista = Dal.Clases.AprobacionTimesheetDAL.ListaAtrasos(IdPros);

                var Datos = from x in lista
                            select new
                            {
                                Id = x.IdSolicitud,
                                Proyecto = x.RelUsuarioProyecto.FactProyecto.NombreProyecto,
                                Usuario = x.RelUsuarioProyecto.FactUsuario.Nombre,
                                FechaSol = x.FechaSolicitud.ToString("dd-MM-yyyy"),
                                FechaGes = (x.FechaGestion.HasValue ? x.FechaGestion.Value.ToString("dd-MM-yyyy") : "-"),
                                Estado = x.EstadoSolicitud
                            };

                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult AprobarAtraso()
        {
            try
            {
                int IdRegistro = int.Parse(Request.Form["IdRegistro"]);
                var use = (Dao.Model.FactUsuario)Session["usuario"];

                var res = Dal.Clases.AprobacionTimesheetDAL.AprobarAtraso(IdRegistro, use.IdUsuario);

                if (res == null)
                {
                    return Json(new { Cod = 3, Msg = "No se registraron los datos, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Dal.Clases.NotificacionDAL.RegistrarNotificacion(res.RelUsuarioProyecto.IdProyecto, res.RelUsuarioProyecto.IdUsuario, DateTime.Now, res.FechaSolicitud, res.RelUsuarioProyecto.IdUsuario, "Autorización Registro", "Se ha aprobado el registro de actividades fuera de plazo límite para el proyecto: <b>" + res.RelUsuarioProyecto.FactProyecto.NombreProyecto + "</b>, esta excepción estará vigente hasta el " + res.FechaValidez.Value.ToString("dd-MM-yyyy"), 2, 0);
                    return Json(new { Cod = 1, Msg = "Datos registrados exitosamente" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ListaActividades()
        {
            try
            {
                int IdPro = int.Parse(Request.Form["proyecto"]);
                int IdUsu = int.Parse(Request.Form["usuario"]);

                var use = (Dao.Model.FactUsuario)Session["usuario"];
                var ProAsig = Dal.Clases.ProyectoDAL.ListaProyectosPerfil(use.IdUsuario);
                List<int> IdPros = new List<int>();
                if (IdPro == 0)
                {
                    IdPros = ProAsig.Where(j => j.IsJp == 1).Select(s => s.IdProyecto).ToList();
                }
                else
                {
                    IdPros.Add(IdPro);
                }
                var lista = Dal.Clases.TimeSheetDAL.ListaTimesheetByProyecto(IdPros);
                var lista2 = Dal.Clases.TimeSheetConsultorDAL.ListaTimesheetConsultorByProyecto(IdPros);
                var listaSA = lista.Where(a => a.IsAprobado == 0).ToList();
                var listaSA2 = lista2.Where(a => a.IsAprobado == 0).ToList();
                if (IdUsu > 0)
                {
                    listaSA = listaSA.Where(u => u.RelUsuarioProyecto.IdUsuario == IdUsu).ToList();
                }

                var Datos = from x in listaSA
                            select new
                            {
                                x.IdRegistro,
                                x.RelUsuarioProyecto.FactProyecto.NombreProyecto,
                                x.RelUsuarioProyecto.FactUsuario.Nombre,
                                FechaRegistro = x.Fecha.Value.ToString("dd-MM-yyyy"),
                                x.Actividad,
                                x.HoraInicio,
                                x.HoraFin,
                                //TotalHoras = Math.Round(x.TotalHoras.Value, 1),
                                TotalHoras = Dal.Clases.Util.DecimalToHrMin(x.TotalHoras.Value),
                                Itemgantt = x.FactTipoActividad == null ? "Otro item" : x.FactTipoActividad.Descripcion,
                                codigo = x.IdRegistro+"AP"
                            };

                if (IdUsu > 0)
                {
                    listaSA2 = listaSA2.Where(u => u.RelUsuarioProyecto.IdUsuario == IdUsu).ToList();
                }

                var Datos2 = from x in listaSA2
                            select new
                            {
                                IdRegistro = x.IdTimesheetConsultor,
                                x.RelUsuarioProyecto.FactProyecto.NombreProyecto,
                                x.RelUsuarioProyecto.FactUsuario.Nombre,
                                FechaRegistro = x.Fecha.Value.ToString("dd-MM-yyyy"),
                                Actividad = x.Descripcion,
                                HoraInicio= "N/A",
                                HoraFin = "N/A",
                                TotalHoras = "N/A",
                                Itemgantt = "N/A",
                                codigo = x.IdTimesheetConsultor + "AC"
                            };

                return Json(new { Cod = 1, Msg = "OK", Datos, Datos2 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ListaAprobados()
        {
            try
            {
                int IdPro = int.Parse(Request.Form["proyecto"]);
                int IdUsu = int.Parse(Request.Form["usuario"]);

                var use = (Dao.Model.FactUsuario)Session["usuario"];
                var ProAsig = Dal.Clases.ProyectoDAL.ListaProyectosPerfil(use.IdUsuario);
                List<int> IdPros = new List<int>();
                if (IdPro == 0)
                {
                    IdPros = ProAsig.Where(j => j.IsJp == 1).Select(s => s.IdProyecto).ToList();
                }
                else
                {
                    IdPros.Add(IdPro);
                }

                var lista = Dal.Clases.EdicionTimeSheetAprobadaDAL.ListaEdicionTimesheetByProyecto(IdPros).ToList();

                lista = lista.Where(a => a.IsAprobado == 0).ToList();

                if (IdUsu > 0)
                {
                    lista = lista.Where(u => u.FactTimeSheet.RelUsuarioProyecto.IdUsuario == IdUsu).ToList();
                }

                var Datos = from x in lista
                            select new
                            {
                                x.IdRegistro,
                                x.FactTimeSheet.RelUsuarioProyecto.FactProyecto.NombreProyecto,
                                x.FactTimeSheet.RelUsuarioProyecto.FactUsuario.Nombre,
                            };

                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DatosActividadEdt()
        {
            try
            {
                var use = (Dao.Model.FactUsuario)Session["usuario"];
                if (use == null)
                {
                    return Json(new { Cod = 2, Msg = "Debe iniciar sesión para utilizar este recurso" }, JsonRequestBehavior.AllowGet);
                }

                int IdRegistro = int.Parse(Request.Form["IdRegistro"]);

                var ED = Dal.Clases.EdicionTimeSheetAprobadaDAL.DatosRegistro(IdRegistro);

                var DatosEdt = new
                {
                    fecha = ED.Fecha.Value.ToString("dd-MM-yyyy"),
                    proyecto = ED.FactTimeSheet.RelUsuarioProyecto.FactProyecto.NombreProyecto,
                    costo = ED.DimTipoCosto.Descripcion,
                    actividad = ED.Actividad,
                    inicio = ED.HoraInicio,
                    termino = ED.HoraFin,
                    item = ED.FactTipoActividad == null ? "Otro item" : ED.FactTipoActividad.Descripcion
                };

                var TS = Dal.Clases.TimeSheetDAL.DetalleTimeSheet(ED.IdFactEdicionTimeSheetAprobada);

                var DatosApr = new
                {
                    fecha = TS.Fecha.Value.ToString("dd-MM-yyyy"),
                    proyecto = TS.RelUsuarioProyecto.FactProyecto.NombreProyecto,
                    costo = TS.DimTipoCosto.Descripcion,
                    actividad = TS.Actividad,
                    inicio = TS.HoraInicio,
                    termino = TS.HoraFin,
                    item = TS.FactTipoActividad == null ? "Otro item" : TS.FactTipoActividad.Descripcion
                };

                return Json(new { Cod = 1, Msg = "OK", DatosEdt, DatosApr }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult AprobarActividad()
        {
            try
            {
                var codigo = Request.Form["IdRegistro"];
                var use = (Dao.Model.FactUsuario)Session["usuario"];
                var IdRegistro = int.Parse(codigo.Split('A')[0]);
                var res = 0;

                if (codigo.Split('A')[1]== "P")
                {
                     res = Dal.Clases.TimeSheetDAL.AprobarActividad(IdRegistro, use.IdUsuario);
                }
                else
                {
                     res = Dal.Clases.TimeSheetConsultorDAL.AprobarActividad(IdRegistro, use.IdUsuario);
                }

                
                if (res == 0)
                {
                    return Json(new { Cod = 3, Msg = "Ocurrió un error al aprobar el registro, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //Dal.Clases.NotificacionDAL.RegistrarNotificacion(res.RelUsuarioProyecto.IdProyecto, res.RelUsuarioProyecto.IdUsuario, DateTime.Now, res.FechaSolicitud, res.RelUsuarioProyecto.IdUsuario, "Autorización Registro", "Se ha aprobado el registro de actividades fuera de plazo limite para el proyecto: <b>" + res.RelUsuarioProyecto.FactProyecto.NombreProyecto + "</b>, esta excepción estará vigente hasta el " + res.FechaValidez.Value.ToString("dd-MM-yyyy"), 2, 0);
                    return Json(new { Cod = 1, Msg = "Registro timesheet aprobado exitosamente" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult AprobarActividadEditada()
        {
            try
            {
                var use = (Dao.Model.FactUsuario)Session["usuario"];
                if (use == null)
                {
                    return Json(new { Cod = 2, Msg = "Debe iniciar sesión para utilizar este recurso" }, JsonRequestBehavior.AllowGet);
                }

                int id = int.Parse(Request.Form["id"]);
                int estado = int.Parse(Request.Form["estado"]);
                string motivo = Request.Form["motivo"];

                if (string.IsNullOrWhiteSpace(motivo))
                {
                    motivo = "El jefe de proyecto no ha entregado motivo.";
                }
                else
                {
                    motivo = motivo.Trim();
                }

                var ED = Dal.Clases.EdicionTimeSheetAprobadaDAL.DatosRegistro(id);

                if (estado == 1)
                {

                    int Id = ED.IdFactEdicionTimeSheetAprobada;
                    string Fecha = ED.Fecha.Value.ToString("dd-MM-yyyy");
                    string Actividad = ED.Actividad;
                    int IdUsuarioProyecto = ED.FactTimeSheet.IdUsuarioProyecto;
                    var Idgantt = ED.IdTipoActividad;
                    int Costo = ED.IdTipoCosto.Value;
                    string Inicio = ED.HoraInicio;
                    string Termino = ED.HoraFin;

                    var dias = Dal.Clases.ConfiguracionDAL.DatosConfiguracion(3);
                    var FechaMaxima = DateTime.Today.AddDays(int.Parse(dias.Valor) * -1);

                    var FE = DateTime.ParseExact(Fecha, "dd-MM-yyyy", null);

                    var TSO = Dal.Clases.TimeSheetDAL.DatosTimeSheet(Id);

                    DateTime fi = DateTime.ParseExact(FE.ToString("yyyy-MM-dd") + " " + Inicio + ":00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime ft = DateTime.ParseExact(FE.ToString("yyyy-MM-dd") + " " + Termino + ":00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);


                    var FEO = DateTime.ParseExact(TSO.Fecha.Value.ToString("dd-MM-yyyy"), "dd-MM-yyyy", null);
                    DateTime fiO = DateTime.ParseExact(FEO.ToString("yyyy-MM-dd") + " " + TSO.HoraInicio + ":00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime ftO = DateTime.ParseExact(FEO.ToString("yyyy-MM-dd") + " " + TSO.HoraFin + ":00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                    var TS = Dal.Clases.TimeSheetDAL.DatosTimeSheet(Id);
                    var ProAsig = Dal.Clases.RelUsuariosDAL.ProyectosAsignados(TS.RelUsuarioProyecto.IdUsuario);

                    var proy = Dal.Clases.RelUsuariosDAL.DatosUsuarioProyecto(IdUsuarioProyecto);

                    if (fi < proy.FactProyecto.InicioProyecto)
                    {
                        return Json(new { Cod = 3, Msg = "No se puede aprobar el cambio porque la fecha registrada es anterior al inicio del proyecto." }, JsonRequestBehavior.AllowGet);
                    }

                    if (fi > proy.FactProyecto.FinProyecto)
                    {
                        return Json(new { Cod = 3, Msg = "No se puede aprobar el cambio porque la fecha registrada es posterior al termino del proyecto." }, JsonRequestBehavior.AllowGet);
                    }

                    var registros = Dal.Clases.TimeSheetDAL.HorariosRegistrados(ProAsig, FE);

                    int ExisteInicio = 0;
                    int ExisteTermino = 0;

                    foreach (var x in registros.Where(i => i.IdRegistro != Id))
                    {
                        DateTime fiCom = DateTime.ParseExact(FE.ToString("yyyy-MM-dd") + " " + x.HoraInicio + ":00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        DateTime ffCom = DateTime.ParseExact(FE.ToString("yyyy-MM-dd") + " " + x.HoraFin + ":00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                        if (fi >= fiCom && fi < ffCom)
                        {
                            ExisteInicio++;
                        }

                        if (ft > fiCom && ft <= ffCom)
                        {
                            ExisteTermino++;
                        }

                        if (ft > ffCom && fi < ffCom)
                        {
                            ExisteTermino++;
                        }

                    }

                    if (ExisteInicio > 0)
                    {
                        return Json(new { Cod = 3, Msg = "No se puede aprobar el cambio porque la hora de inicio provoca conflicto con otra actividad registrada.", registros }, JsonRequestBehavior.AllowGet);
                    }

                    if (ExisteTermino > 0)
                    {
                        return Json(new { Cod = 3, Msg = "No se puede aprobar el cambio porque la hora de término provoca conflicto con otra actividad registrada.", registros }, JsonRequestBehavior.AllowGet);
                    }

                    var duracion = (ft - fi).TotalHours;


                    Dao.Model.FactTimeSheet Modificado = Dal.Clases.TimeSheetDAL.DatosTimeSheetNoTrack(Id);

                    Modificado.IdRegistro = Id;
                    Modificado.Fecha = FE;
                    Modificado.Semana = Dal.Clases.Util.GetIso8601WeekOfYear(FE);
                    Modificado.Actividad = Actividad;
                    Modificado.IdUsuarioProyecto = IdUsuarioProyecto;
                    Modificado.IdTipoCosto = Costo;
                    Modificado.HoraInicio = Inicio;
                    Modificado.HoraFin = Termino;
                    Modificado.TotalHoras = duracion;
                    Modificado.IsActivo = 1;
                    Modificado.FechaEdicion = DateTime.Now;
                    Modificado.IdTipoActividad = Idgantt;

                    var update = Dal.Clases.TimeSheetDAL.EditarTimesheet(Modificado);

                    if (update == 0)
                    {
                        return Json(new { Cod = 3, Msg = "Ocurrió un error al editar el registro timesheet, intente nuevamente.", registros }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var res = Dal.Clases.EdicionTimeSheetAprobadaDAL.AprobarEdicionTimesheet(id, use.IdUsuario, "Edicion timesheet aprobada");

                        if (res == 0)
                        {
                            return Json(new { Cod = 3, Msg = "Ocurrió un error al aprobar la edición del registro, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            var not = Dal.Clases.NotificacionDAL.RegistrarNotificacion(ED.FactTimeSheet.RelUsuarioProyecto.IdProyecto, use.IdUsuario, DateTime.Now, ED.FechaCarga.Value, ED.FactTimeSheet.RelUsuarioProyecto.IdUsuario, "Edición Registro", "Se ha aprobado la edición del registro timesheet para el proyecto: <b>" + ED.FactTimeSheet.RelUsuarioProyecto.FactProyecto.NombreProyecto + "</b>", 3, 0);

                            if (not == 1)
                            {
                                return Json(new { Cod = 1, Msg = "Se ha aprobado la edición del registro timesheet y notificado al usuario la decisión." }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { Cod = 1, Msg = "Se ha aprobado la edición del registro timesheet, pero ocurrió un problema al generar la notificación al usuario" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }

                }
                else if (estado == 2)
                {
                    var res = Dal.Clases.EdicionTimeSheetAprobadaDAL.RechazarEdicionTimesheet(id, use.IdUsuario, motivo);

                    if (res == 0)
                    {
                        return Json(new { Cod = 3, Msg = "Ocurrió un error al rechazar la edición del registro, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var not = Dal.Clases.NotificacionDAL.RegistrarNotificacion(ED.FactTimeSheet.RelUsuarioProyecto.IdProyecto, use.IdUsuario, DateTime.Now, ED.FechaCarga.Value, ED.FactTimeSheet.RelUsuarioProyecto.IdUsuario, "Edición Registro", "Se ha rechazado la edición del registro timesheet para el proyecto: <b>" + ED.FactTimeSheet.RelUsuarioProyecto.FactProyecto.NombreProyecto + "</b><br>Motivo: <b>" + motivo + "</b>", 1, 0);

                        if (not == 1)
                        {
                            return Json(new { Cod = 1, Msg = "Se ha rechazado la edición del registro timesheet y notificado al usuario la decisión." }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { Cod = 1, Msg = "Se ha rechazado la edición del registro timesheet, pero ocurrió un problema al generar la notificación al usuario" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    return Json(new { Cod = 2, Msg = "No se reconoce el estado." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult AprobarActividadMasivo()
        {
            try
            {
                string actividades = Request.Form["actividades"];
                string[] ids = actividades.Split(',');
                var use = (Dao.Model.FactUsuario)Session["usuario"];

                var res = 0;


                for (int c = 0; c < ids.Length; c++)
                {
                    var codigo = ids[c];
                    var idreg = int.Parse(codigo.Split('A')[0]);

                    if (codigo.Split('A')[1] == "P")
                    {
                        res += Dal.Clases.TimeSheetDAL.AprobarActividad(idreg, use.IdUsuario);
                    }

                }

                for (int c = 0; c < ids.Length; c++)
                {
                    var codigo = ids[c];
                    var idreg = int.Parse(codigo.Split('A')[0]);

                    if (codigo.Split('A')[1] == "C")
                    {
                        res += Dal.Clases.TimeSheetConsultorDAL.AprobarActividad(idreg, use.IdUsuario);
                    }
           
                }

                if (res == 0)
                {
                    if (ids.Length == 1) {
                        return Json(new { Cod = 3, Msg = "Ocurrió un error al aprobar el registro, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                    }
                    else {
                        return Json(new { Cod = 3, Msg = "Ocurrió un error al aprobar los registros, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    //Dal.Clases.NotificacionDAL.RegistrarNotificacion(res.RelUsuarioProyecto.IdProyecto, res.RelUsuarioProyecto.IdUsuario, DateTime.Now, res.FechaSolicitud, res.RelUsuarioProyecto.IdUsuario, "Autorización Registro", "Se ha aprobado el registro de actividades fuera de plazo limite para el proyecto: <b>" + res.RelUsuarioProyecto.FactProyecto.NombreProyecto + "</b>, esta excepción estará vigente hasta el " + res.FechaValidez.Value.ToString("dd-MM-yyyy"), 2, 0);

                    if (res == 1) {
                        return Json(new { Cod = 1, Msg = "Registro timesheet aprobado exitosamente" }, JsonRequestBehavior.AllowGet);
                    }
                    else {
                        return Json(new { Cod = 1, Msg = "Se aprobaron " + res + " registros timesheet exitosamente" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }

}
﻿using Rotativa;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Net;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using static TimeSheet.Controllers.ConfiguracionController;

namespace TimeSheet.Controllers
{
    public class DashboardController : Controller
    {
        // GET: Dashboard
        public ActionResult Index()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

            var usr = (Dao.Model.FactUsuario)Session["usuario"];
            ViewBag.Admin = usr.IsAdmin;
            var pjp = Dal.Clases.RelUsuariosDAL.ProyectosJP(usr.IdUsuario);
            ViewBag.JefePro = (pjp.Count() > 0 ? 1 : 0);


            var pr = Dal.Clases.ProyectoDAL.ListaProyectos();
            List<int> anios = pr.Where(p => p.IdTipoProyecto == 1).OrderByDescending(o => o.InicioProyecto.Value.Year).Select(a => a.InicioProyecto.Value.Year).Distinct().ToList();
            ViewBag.anios = anios;
            List<int> aniosConsultoria = pr.Where(p => p.IdTipoProyecto == 2).OrderByDescending(o => o.InicioProyecto.Value.Year).Select(a => a.InicioProyecto.Value.Year).Distinct().ToList();
            ViewBag.aniosConsultoria = aniosConsultoria;

            return View();
        }

        #region PROYECTOS DESARROLLO

        public ActionResult Proyecto(int id)
        {
            var usu = (Dao.Model.FactUsuario)Session["usuario"];
            if (usu == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var pro = Dal.Clases.ProyectoDAL.DatosProyecto(id);

            if (usu.IsAdmin == 1)
            {
                ViewBag.Permitido = 1;
            }
            else
            {
                if (pro.RelUsuarioProyecto.Where(u => u.IdUsuario == usu.IdUsuario).FirstOrDefault() != null)
                {
                    ViewBag.Permitido = 1;
                }
                else
                {
                    ViewBag.Permitido = 0;
                }
            }

            var PorAsig = pro.RelUsuarioProyecto.Where(i => i.IsActivo == 1).Sum(s => s.PorcentajeAsignacion);
            var alertaPorc = (PorAsig < 100 ? "danger" : "success");

            ViewBag.fechaInicio = pro.InicioProyecto.Value.ToString("dd-MM-yyyy");
            ViewBag.fechafin = pro.FinProyecto.Value.ToString("dd-MM-yyyy");

            ViewBag.PorAsig = PorAsig;
            ViewBag.alertaPorc = alertaPorc;
            ViewBag.pro = pro;
            ViewBag.jp = (pro.RelUsuarioProyecto.Where(u => u.IdUsuario == usu.IdUsuario).Where(i => i.IsJp == 1).FirstOrDefault() == null ? 0 : 1);

            return View();
        }

        [HttpPost]
        public JsonResult ListadoProyectos()
        {
            int anio = int.Parse(Request.Form["anio"]);
            var logue = (Dao.Model.FactUsuario)Session["usuario"];

            var IdUs = (logue.IsAdmin == 1 ? 0 : logue.IdUsuario);

            Random random = new Random();
            try
            {
                var Listado = Dal.Clases.ProyectoDAL.ListaProyectosDash(anio, IdUs).ToList();



                /*var Datos = from x in Listado
                            let TC = x.RelUsuarioProyecto.Sum(s => s.FactTimeSheet.Select(s2 => s2.TotalHoras.Value).FirstOrDefault())
                            select new
                            {
                                IdProyecto = x.IdProyecto,
                                Proyecto = x.NombreProyecto,
                                JP = x.RelUsuarioProyecto.Where(a => a.IsJp.Value == 1).Select(s => s.FactUsuario.Nombre),
                                Recursos = x.RelUsuarioProyecto.Count,
                                Consumidas = TC,
                                Totales = x.TotalHH,
                                Restantes = x.TotalHH - TC,
                                Estado = (TC > x.TotalHH ? 0 : 1)
                            };*/

                var Datos = from x in Listado
                            where x.FactProyecto.IdTipoProyecto == 1
                            group x by x.FactProyecto.NombreProyecto into grupo
                            let TC = double.Parse(grupo.Sum(s => s.FactTimeSheet.Sum(s2 => s2.TotalHoras)).ToString())
                            let TH = double.Parse(grupo.FirstOrDefault().FactProyecto.TotalHH.ToString())
                            //let JP = grupo.Where(j => j.IsJp == 1).Where(i => i.IsActivo == 1).Select(s => s.FactUsuario.Nombre)
                            let JP = Dal.Clases.ProyectoDAL.JPProyecto(grupo.FirstOrDefault().IdProyecto, 0)
                            select new
                            {
                                IdProyecto = grupo.FirstOrDefault().IdProyecto,
                                Proyecto = grupo.Key,
                                //JP = (JP.Count() == 0 ? new string[] { "Sin JP asignado." } : JP),
                                JP,
                                Recursos = grupo.Where(i => i.IsActivo == 1).Select(s => s.IdUsuario).Distinct().Count(),
                                Consumidas = String.Format("{0:n}", Math.Round(TC, 2)),
                                Totales = TH,
                                Restantes = String.Format("{0:n}", Math.Round((TH - TC), 2)),
                                Estado = (TC > TH ? 0 : 1)
                            };


                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ListaEmpresas()
        {
            try
            {
                string desde = Request.Form["desde"];
                string hasta = Request.Form["hasta"];
                int proyecto = int.Parse(Request.Form["proyecto"]);

                //var JPs = Dal.Clases.ProyectoDAL.JPProyecto(proyecto, 0);
                var JPs = Dal.Clases.ProyectoDAL.JPProyectoList(proyecto, 0);

                if (JPs.Count() == 0)
                {
                    JPs.Add(new Dao.Model.FactUsuario() { IdUsuario = 0, Nombre = "Sin JP asignado" });
                }
                var Listado = Dal.Clases.TimeSheetDAL.ListaEmpresasDash(proyecto).ToList();

                var fdesde = DateTime.ParseExact(desde, "dd-MM-yyyy", null);
                var fhasta = DateTime.ParseExact(hasta, "dd-MM-yyyy", null);

                var Datos = (from x in Listado
                             where (x.FactTimeSheet.Sum(th => th.TotalHoras).Value > 0 || x.IsActivo == 1)
                             group x by x.FactUsuario.DimEmpresa.NombreEmpresa into grupo
                             let SinAprobar = grupo.Sum(s => s.FactTimeSheet.Where(f => f.Fecha.Value >= fdesde && f.Fecha.Value <= fhasta).Where(a => a.IsAprobado == 0).Sum(ss => ss.TotalHoras))
                             let UtilPeriodo = grupo.Sum(s => s.FactTimeSheet.Where(f => f.Fecha.Value >= fdesde && f.Fecha.Value <= fhasta).Sum(ss => ss.TotalHoras))
                             let UtilTotal = grupo.Sum(ss => ss.FactTimeSheet.Sum(s => s.TotalHoras))
                             let HHT = grupo.FirstOrDefault().FactProyecto.TotalHH.Value * (double.Parse((grupo.Where(i => i.IsActivo == 1).Sum(p => p.PorcentajeAsignacion.Value).ToString())) / 100)

                             select new
                             {
                                 Id = grupo.FirstOrDefault().FactUsuario.IdEmpresa,
                                 Nombre = grupo.Key,
                                 UtilizadasPeriodo = String.Format("{0:n}", Math.Round(UtilPeriodo.Value, 2)),
                                 UtilizadasTotaltxt = String.Format("{0:n}", Math.Round(UtilTotal.Value, 2)),
                                 UtilizadasTotal = Math.Round(UtilTotal.Value, 2),
                                 Faltantes = String.Format("{0:n}", Math.Round((HHT - double.Parse(UtilTotal.ToString())), 2)),
                                 HHProyecto = grupo.FirstOrDefault().FactProyecto.TotalHH.Value,
                                 HHTotalestxt = String.Format("{0:n}", Math.Round(HHT, 2)),
                                 HHTotales = Math.Round(HHT, 2),
                                 SinAprobar
                             }).OrderBy(n => n.Nombre).ToList();

                /*
                select new
                {
                    IdEmpresa = x.RelUsuarioProyecto.FactUsuario.IdEmpresa,
                    IdProyecto = x.RelUsuarioProyecto.IdProyecto,
                    Empresa = x.RelUsuarioProyecto.FactUsuario.DimEmpresa.NombreEmpresa,
                    Utilizadas = ,
                    Faltantes = 0,
                    Totales = x.RelUsuarioProyecto.FactProyecto.TotalHH
                };
                */

                /*
                var Datos = new[]
                 {
                new { ID=1, Nombre = "INNOVASIS", Utilizadas = 13, Faltantes = 7, Totales = 20},
                new { ID=2, Nombre = "VMICA", Utilizadas = 62, Faltantes = 28, Totales = 90},
                new { ID=3, Nombre = "ACTIVETRAINER", Utilizadas = 11, Faltantes = 9, Totales = 20}
                };
                */

                return Json(new { Cod = 1, Msg = "OK", Datos = Datos, JPs }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ListaRecursos()
        {
            try
            {
                string desde = Request.Form["desde"];
                string hasta = Request.Form["hasta"];
                int proyecto = int.Parse(Request.Form["proyecto"]);
                int empresa = int.Parse(Request.Form["empresa"]);

                var Listado = Dal.Clases.TimeSheetDAL.ListaRecursosDash(proyecto, empresa).ToList();

                var fdesde = DateTime.ParseExact(desde, "dd-MM-yyyy", null);
                var fhasta = DateTime.ParseExact(hasta, "dd-MM-yyyy", null);
                /*
                var Datos = new[]
                {
                new { Empresa="VMICA", Nombre = "Omar Chávez", Utilizadas = 13, Faltantes = 7, Totales = 20},
                new { Empresa="VMICA", Nombre = "Gabriel Santibáñez", Utilizadas = 62, Faltantes = 28, Totales = 90},
                new { Empresa="VMICA", Nombre = "Esteban Valera", Utilizadas = 11, Faltantes = 9, Totales = 20},
                new { Empresa="VMICA", Nombre = "Oscar Garín", Utilizadas = 11, Faltantes = 9, Totales = 20}
                };
                */

                var DatosAcumulado = (from x in Listado
                                      group x by x.FactUsuario.Nombre into grupo
                                      let Utili = grupo.FirstOrDefault().FactTimeSheet.Sum(s => s.TotalHoras)
                                      let HHT = grupo.FirstOrDefault().FactProyecto.TotalHH.Value * (double.Parse((grupo.FirstOrDefault().PorcentajeAsignacion.ToString())) / 100)
                                      select new
                                      {
                                          Empresa = grupo.FirstOrDefault().FactUsuario.DimEmpresa.NombreEmpresa,
                                          Nombre = grupo.Key,
                                          Utilizadas = String.Format("{0:n}", Math.Round(Utili.Value, 2)),
                                          Faltantes = String.Format("{0:n}", Math.Round((HHT - Utili.Value), 2)),
                                          Totales = String.Format("{0:n}", Math.Round(HHT, 2))
                                      }).OrderBy(n => n.Nombre).ToList();

                var DatosPeriodo = (from x in Listado
                                    group x by x.FactUsuario.Nombre into grupo
                                    let Utili = grupo.FirstOrDefault().FactTimeSheet.Where(f => f.Fecha >= fdesde && f.Fecha <= fhasta).Sum(s => s.TotalHoras)
                                    select new
                                    {
                                        Empresa = grupo.FirstOrDefault().FactUsuario.DimEmpresa.NombreEmpresa,
                                        Nombre = grupo.Key,
                                        Utilizadas = String.Format("{0:n}", Math.Round(Utili.Value, 2)),
                                        Ultima = (Utili == 0 ? "Sin Registros" : grupo.FirstOrDefault().FactTimeSheet.Max(m => m.Fecha).Value.ToString("dd-MM-yyyy"))
                                    }).OrderBy(n => n.Nombre).ToList();


                return Json(new { Cod = 1, Msg = "OK", DatosA = DatosAcumulado, DatosP = DatosPeriodo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult PeriodosProyecto()
        {
            try
            {
                int pro = int.Parse(Request.Form["proyecto"]);

                var proy = Dal.Clases.ProyectoDAL.DatosProyecto(pro);

                var difAnio = ((proy.FinProyecto.Value.Year - proy.InicioProyecto.Value.Year) * 12);
                var difMes = proy.FinProyecto.Value.Month - proy.InicioProyecto.Value.Month;
                var diferencia = difAnio + difMes;

                List<Pers> periodos = new List<Pers>();

                for (var i = 0; i <= diferencia; i++)
                {
                    DateTime ff = proy.InicioProyecto.Value.AddMonths(i);
                    Pers pe = new Pers();
                    pe.Fecha = ff;
                    System.Globalization.TextInfo ti = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
                    pe.MesPalabra = ti.ToTitleCase(ff.ToString("MMMMM yyyy"));
                    pe.Periodo = int.Parse(ff.ToString("yyyyMM"));
                    var se = 0;
                    if (ff.Year == DateTime.Today.Year)
                    {
                        if (DateTime.Today.Month - 1 == ff.Month)
                        {
                            se = 1;
                        }
                    }
                    pe.Seleccionado = se;
                    periodos.Add(pe);
                }

                return Json(new
                {
                    Cod = 1,
                    Msg = "OK",
                    Datos = periodos,
                    Inicio = proy.InicioProyecto.Value.ToString("dd-MM-yyyy"),
                    Fin = proy.FinProyecto.Value.ToString("dd-MM-yyyy")
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Cod = 2,
                    Msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ValorUF()
        {
            try
            {
                string tipo = Request.Form["periodo"];
                string fecha = "";

                if (tipo != "999")
                {
                    string per = Request.Form["periodo"];

                    int anio = 0;
                    int mes = 0;
                    string txtperido;

                    anio = int.Parse(per.ToString().Substring(0, 4));
                    mes = int.Parse(per.ToString().Substring(4, 2));
                    txtperido = Dal.Clases.Util.NombreMes(mes);

                    fecha = DateTime.DaysInMonth(anio, mes) + "-" + per.ToString().Substring(4, 2) + "-" + anio;

                    if (Convert.ToDateTime(fecha) > DateTime.Now)
                    {
                        fecha = DateTime.Now.ToString("dd-MM-yyyy");
                    }
                }
                else
                {
                    fecha = Request.Form["fecha"];
                    //if (Convert.ToDateTime(fecha) > DateTime.Now)
                    //{
                    //    fecha = DateTime.Now.ToString("dd-MM-yyyy");
                    //}
                }

                string apiUrl = "https://www.mindicador.cl/api/uf/" + fecha;
                string jsonString = "";
                WebClient http = new WebClient();
                JavaScriptSerializer jss = new JavaScriptSerializer();

                http.Headers.Add(HttpRequestHeader.Accept, "application/json");
                jsonString = http.DownloadString(apiUrl);
                var indicatorsObject = JsonConvert.DeserializeObject<Valores>(jsonString);

                var txt = "";
                if (indicatorsObject.serie.Count > 0)
                {
                    var valor = indicatorsObject.serie.FirstOrDefault().valor;
                    txt = "$" + String.Format("{0:n}", valor) + " (Al " + fecha + ")";
                }
                else
                {
                    txt = "No se encontró valor UF para el " + fecha;
                }
                return Json(new
                {
                    Cod = 1,
                    Msg = "",
                    Datos = txt,
                    Fecha = fecha
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GenerarEP()
        {
            try
            {
                if (Session["usuario"] == null)
                {
                    return RedirectToAction("Index", "Home");
                }

                if (Request.Form["proyectoEP"] == null)
                {
                    ViewBag.error = "Los parámetros no son validos";
                    return View("Error");
                }

                int pro = int.Parse(Request.Form["proyectoEP"]);
                int emp = int.Parse(Request.Form["empresaEP"]);
                string per = Request.Form["periodoEP"];
                int iva = int.Parse(Request.Form["ivaEP"]);
                string obs = Request.Form["observaciones"];
                string jp = Request.Form["jpEP"];

                string fechainicioper = "";
                string fechafinper = "";

                string fechaUF = Request.Form["FechaUFtxt"];
                var fechaUFEP = DateTime.ParseExact(fechaUF, "dd-MM-yyyy", null);

                if (per == "999")
                {
                    fechainicioper = Request.Form["FechaPersonalizadaInicio"];
                    fechafinper = Request.Form["FechaPersonalizadaFin"];
                }

                //double ValorUf = 28716.52; //Falta agregar UF en mantenedores

                var Pro = Dal.Clases.ProyectoDAL.DatosProyecto(pro);
                var Emp = Dal.Clases.EmpresaDAL.DatosEmpresa(emp);

                int anio = 0;
                int mes = 0;
                string txtperido, txtfechainicio, txtfechafin;

                if (per != "999")
                {
                    anio = int.Parse(per.ToString().Substring(0, 4));
                    mes = int.Parse(per.ToString().Substring(4, 2));
                    txtperido = Dal.Clases.Util.NombreMes(mes);
                    txtfechainicio = "01 de " + Dal.Clases.Util.NombreMes(mes) + " del " + anio;
                    txtfechafin = DateTime.DaysInMonth(anio, mes) + " de " + Dal.Clases.Util.NombreMes(mes) + " del " + anio;

                    fechainicioper = "01-" + per.ToString().Substring(4, 2) + "-" + anio;
                    fechafinper = DateTime.DaysInMonth(anio, mes) + "-" + per.ToString().Substring(4, 2) + "-" + anio;
                }
                else
                {
                    txtperido = fechainicioper.ToString().Substring(6, 4) + fechainicioper.ToString().Substring(3, 2) + fechainicioper.ToString().Substring(0, 2);

                    txtfechainicio = fechainicioper.ToString().Substring(0, 2) + " de " + Dal.Clases.Util.NombreMes(int.Parse(fechainicioper.ToString().Substring(3, 2))) + " del " + fechainicioper.ToString().Substring(6, 4);
                    txtfechafin = fechafinper.ToString().Substring(0, 2) + " de " + Dal.Clases.Util.NombreMes(int.Parse(fechafinper.ToString().Substring(3, 2))) + " del " + fechafinper.ToString().Substring(6, 4);
                }

                /* Fechas Actual */
                var fdesdeEP = DateTime.ParseExact(fechainicioper, "dd-MM-yyyy", null);
                var fhastaEP = DateTime.ParseExact(fechafinper, "dd-MM-yyyy", null);


                var Listado = Dal.Clases.TimeSheetDAL.ListaEmpresasDash(pro).ToList();

                var Datos = (from x in Listado
                             where (x.FactTimeSheet.Sum(th => th.TotalHoras).Value > 0 || x.IsActivo == 1)
                             where x.FactUsuario.IdEmpresa == emp
                             group x by x.FactUsuario.DimEmpresa.NombreEmpresa into grupo
                             let SinAprobar = grupo.Sum(s => s.FactTimeSheet.Where(f => f.Fecha.Value >= fdesdeEP && f.Fecha.Value <= fhastaEP).Where(a => a.IsAprobado == 0).Sum(ss => ss.TotalHoras))
                             select new
                             {
                                 Nombre = grupo.Key,
                                 SinAprobar
                             }).FirstOrDefault();

                if (Datos.SinAprobar > 0)
                {
                    var tx = "existen horas sin aprobar para el periodo seleccionado, presione <a href=¬/Aprobacion/Actividades¬><b>AQUÍ</b></a> para revisar las actividades";
                    ViewBag.error = tx.Replace('¬', '"');
                    return View("Error");
                }

                ViewBag.proyecto = Pro;
                ViewBag.NombreEmpresa = Emp.NombreEmpresa;
                ViewBag.RutEmpresa = Emp.RutEmpresa;
                ViewBag.Observaciones = obs;
                ViewBag.Periodo = per == "999" ? "Personalizado" : per;

                ViewBag.PeriodoCobro = txtperido;
                ViewBag.FechaInicio = txtfechainicio;
                ViewBag.FechaTermino = txtfechafin;

                /* UF/H Empresa */
                var UfHora = Emp.UfHora;


                /* Fechas Anterior */
                var fdesdeEPAnt_ = fdesdeEP.AddMonths(-1);
                var fechadesdetxt = "01-" + fdesdeEPAnt_.Month + "-" + fdesdeEPAnt_.Year;
                var fdesdeEPAnt = DateTime.ParseExact(fechadesdetxt, "dd-M-yyyy", null);
                var ft = DateTime.DaysInMonth(fdesdeEPAnt.Year, fdesdeEPAnt.Month) + "-" + fdesdeEPAnt.Month + "-" + fdesdeEPAnt.Year;
                var fhastaEPAnt = DateTime.ParseExact(ft, "dd-M-yyyy", null);

                /* uf's */
                var UfPer = Dal.Clases.ConversionMonedaDAL.ListaConversion("UF", Pro.InicioProyecto.Value, fhastaEP);
                if (UfPer.Count == 0)
                {
                    ViewBag.error = "No se han encontrado los valores de UF para los periodos del proyecto";
                    return View("Error");
                }

                var UfMax = UfPer.OrderByDescending(f => f.Fecha.Value).FirstOrDefault();


                //if (UfMax.Fecha != fhastaEP)
                if (UfMax.Fecha != fechaUFEP)
                {

                    //string fecha = fhastaEP.ToString("dd-MM-yyyy");
                    string fecha = fechaUFEP.ToString("dd-MM-yyyy");
                    string apiUrl = "https://www.mindicador.cl/api/uf/" + fecha;
                    string jsonString = "";
                    WebClient http = new WebClient();
                    JavaScriptSerializer jss = new JavaScriptSerializer();

                    http.Headers.Add(HttpRequestHeader.Accept, "application/json");
                    jsonString = http.DownloadString(apiUrl);
                    var indicatorsObject = JsonConvert.DeserializeObject<Valores>(jsonString);

                    if (indicatorsObject.serie.Count > 0)
                    {
                        var m = new Dao.Model.CfgConversionMoneda();

                        m.Id = UfMax.Id + 1;
                        m.Fecha = fhastaEP;
                        m.Moneda = "UF";
                        m.Valor = indicatorsObject.serie.FirstOrDefault().valor;
                        UfPer.Add(m);
                    }

                    UfMax = UfPer.OrderByDescending(f => f.Fecha.Value).FirstOrDefault();
                }



                var UfAct = UfPer.Where(f => f.Fecha.Value.Year == anio && f.Fecha.Value.Month == mes).FirstOrDefault();
                UfAct = (UfAct == null ? UfMax : UfAct);
                var UfAnt = UfPer.Where(f => f.Fecha.Value.Year == fdesdeEPAnt.Year && f.Fecha.Value.Month == fdesdeEPAnt.Month).FirstOrDefault();
                UfAnt = (UfAnt == null ? UfMax : UfAnt);

                /*********** Periodo Actual ***********/
                var TS = Dal.Clases.TimeSheetDAL.HorasProyectoEmpresa(fdesdeEP, fhastaEP, pro, emp);
                /* if (TS.Count() == 0) { ViewBag.Error = "No existen horas registradas en el periodo seleccionado."; return View("Error"); } */
                var hrs = (TS.Count() == 0 ? 0 : Math.Round(TS.Sum(s => s.TotalHoras).Value, 2));
                var ValorNeto = (UfHora * UfAct.Valor) * hrs;
                var ValorIva = (iva == 1 ? ValorNeto * 0.19 : 0);
                var ValorBruto = ValorNeto + ValorIva;

                /*********** Periodo Anterior ***********/
                var TSAnt = Dal.Clases.TimeSheetDAL.HorasProyectoEmpresa(fdesdeEPAnt, fhastaEPAnt, pro, emp);
                var hrsAnt = (TSAnt.Count() == 0 ? 0 : Math.Round(TSAnt.Sum(s => s.TotalHoras).Value, 2));
                var ValorNetoAnt = (UfHora * UfAnt.Valor) * hrsAnt;
                var ValorIvaAnt = (iva == 1 ? ValorNetoAnt * 0.19 : 0);
                var ValorBrutoAnt = ValorNetoAnt + ValorIvaAnt;

                /*********** Periodo Anterior Acumulado ***********/
                List<Dao.Model.FactTimeSheet> TSAntAcum = Dal.Clases.TimeSheetDAL.HorasProyectoEmpresa(Pro.InicioProyecto.Value, fhastaEPAnt, pro, emp).ToList();
                //var hrsAntAcum = (TSAntAcum.Count() == 0 ? 0 : Math.Round(TSAntAcum.Sum(s => s.TotalHoras).Value, 1));
                //var ValorNetoAntAcum = (UfHora * ValorUf) * hrsAntAcum;

                var AAnt = from x in TSAntAcum
                           group x by x.Fecha.Value.ToString("yyyy-MM") into g
                           select new
                           {
                               AM = g.Key,
                               TH = Math.Round(g.Sum(s => s.TotalHoras.Value), 2),
                               UF = UfPer.Where(p => p.Fecha.Value.ToString("yyyy-MM") == g.Key).FirstOrDefault().Valor
                           };

                double ValorNetoAntAcum = 0;

                foreach (var x in AAnt)
                {
                    if (x.TH > 0)
                    {
                        ValorNetoAntAcum += (UfHora.Value * x.UF.Value) * x.TH;
                    }
                }

                var ValorIvaAntAcum = (iva == 1 ? ValorNetoAntAcum * 0.19 : 0);
                var ValorBrutoAntAcum = ValorNetoAntAcum + ValorIvaAntAcum;

                /*********** Periodo Acumulado a la fecha***********/
                List<Dao.Model.FactTimeSheet> TSAcum = Dal.Clases.TimeSheetDAL.HorasProyectoEmpresa(Pro.InicioProyecto.Value, fhastaEP, pro, emp).ToList();
                //var hrsAcum = (TSAcum.Count() == 0 ? 0 : Math.Round(TSAcum.Sum(s => s.TotalHoras).Value, 1));
                //var ValorNetoAcum = (UfHora * ValorUf) * hrsAcum;

                var AAct = from x in TSAcum
                           group x by x.Fecha.Value.ToString("yyyy-MM") into g
                           select new
                           {
                               AM = g.Key,
                               TH = Math.Round(g.Sum(s => s.TotalHoras.Value), 2),
                               UF = UfPer.Where(p => p.Fecha.Value.ToString("yyyy-MM") == g.Key).FirstOrDefault().Valor
                           };

                double ValorNetoAcum = 0;

                foreach (var x in AAct)
                {
                    if (x.TH > 0)
                    {
                        ValorNetoAcum += (UfHora.Value * x.UF.Value) * x.TH;
                    }
                }

                var ValorIvaAcum = (iva == 1 ? ValorNetoAcum * 0.19 : 0);
                var ValorBrutoAcum = ValorNetoAcum + ValorIvaAcum;

                /*
                NumberFormatInfo nfi = new CultureInfo("es-CL", false).NumberFormat;
                nfi.CurrencyPositivePattern = 0;
                ViewBag.ValorNeto = string.Format(nfi, "{0:C}", Math.Round(ValorNeto.Value, 0));
                ViewBag.ValorIva = string.Format(nfi, "{0:C}", Math.Round(ValorIva.Value, 0));
                ViewBag.ValorBruto = string.Format(nfi, "{0:C}", Math.Round(ValorBruto.Value, 0));
                */

                ViewBag.ValorUf = String.Format("{0:n}", UfAct.Valor);
                var fechaactual = UfAct.Fecha < DateTime.Now ? UfAct.Fecha.Value : DateTime.Now;
                ViewBag.FechaUf = fechaUF;

                ViewBag.ValorNeto = String.Format("{0:n}", Math.Round(ValorNeto.Value, 2));
                ViewBag.ValorIva = String.Format("{0:n}", Math.Round(ValorIva.Value, 2));
                ViewBag.ValorBruto = String.Format("{0:n}", Math.Round(ValorBruto.Value, 2));

                ViewBag.ValorBrutoAnt = String.Format("{0:n}", Math.Round(ValorBrutoAnt.Value, 2));
                ViewBag.ValorBrutoAntAcum = String.Format("{0:n}", Math.Round(ValorBrutoAntAcum, 2));
                ViewBag.ValorBrutoAcum = String.Format("{0:n}", Math.Round(ValorBrutoAcum, 2));

                /*
                var JPs = Dal.Clases.ProyectoDAL.JPProyecto(pro, emp);

                if (JPs.Count() == 0)
                {
                    JPs.Add("Sin JP Asignado");
                }
                */

                if (jp == "0")
                {
                    ViewBag.JP = "Sin JP asignado";
                    ViewBag.FirmaJp = "";
                }
                else
                {
                    var dus = Dal.Clases.UsuarioDAL.DatosUsuario(int.Parse(jp));
                    ViewBag.JP = dus.Nombre;
                    ViewBag.FirmaJp = dus.ArchivoFirma;
                }



                return new ViewAsPdf("GenerarEP")
                {

                    PageSize = Rotativa.Options.Size.A4
               //,FileName = "CustomersLista.pdf" // SI QUEREMOS QUE EL ARCHIVO SE DESCARGUE DIRECTAMENTE
               ,
                    PageMargins = new Rotativa.Options.Margins(10, 10, 10, 10)
                };

            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        internal class Pers
        {
            public DateTime Fecha { get; set; }
            public string MesPalabra { get; set; }
            public int Periodo { get; set; }
            public int Seleccionado { get; set; }
            public DateTime? Fechainicio { get; set; }
            public DateTime? Fechatermino { get; set; }
        }

        public ActionResult Reporte()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

            var usr = (Dao.Model.FactUsuario)Session["usuario"];
            var pjp = Dal.Clases.RelUsuariosDAL.ProyectosJP(usr.IdUsuario);
            var JefePro = (pjp.Count() > 0 ? 1 : 0);

            List<Dal.Clases.ListaGenerica> listaProyecto = new List<Dal.Clases.ListaGenerica>();


            if (usr.IsAdmin == 1)
            {
                var ListaProyectos = Dal.Clases.ProyectoDAL.ListaProyectos().ToList();
                var datos = (from p in ListaProyectos
                             orderby p.NombreProyecto
                             select new Dal.Clases.ListaGenerica
                             {
                                 id = p.IdProyecto,
                                 name = p.NombreProyecto,
                             }).ToList();

                listaProyecto = datos;

                ViewBag.listProyectos = listaProyecto;


                return View();
            }
            else
            {
                var listadoproyectos = Dal.Clases.TimeSheetDAL.ListaProyectosTimesheet(usr.IdUsuario).ToList();

                var datos = (from p in listadoproyectos
                             orderby p.FactProyecto.NombreProyecto
                             select new Dal.Clases.ListaGenerica
                             {
                                 id = p.IdProyecto,
                                 name = p.FactProyecto.NombreProyecto
                             }).ToList();

                ViewBag.listProyectos = datos;

                return View();
            }
        }

        [HttpPost]
        public JsonResult CargaCboProyectos()
        {
            try
            {
                var usr = (Dao.Model.FactUsuario)Session["usuario"];
                if (usr == null)
                {
                    return Json(new { Cod = 2, Msg = "Debe iniciar sesión para utilizar este recurso." });
                }

                int year = int.Parse(Request.Form["year"]);

                if (usr.IsAdmin == 1)
                {
                    var ListaProyectos = Dal.Clases.ProyectoDAL.ListaProyectos().ToList();

                    var datos = (from p in ListaProyectos
                                 where p.InicioProyecto.Value.Year == year
                                 select new
                                 {
                                     IdProyecto = p.IdProyecto,
                                     Proyecto = p.NombreProyecto,
                                 }).OrderBy(o => o.Proyecto).ToList();

                    if (datos.Count == 0)
                    {
                        return Json(new { Cod = 2, Msg = "No se encontraron proyectos para el año " + year });
                    }

                    return Json(new { Cod = 1, Msg = "OK", Datos = datos });
                }
                else
                {
                    var Listado = Dal.Clases.ProyectoDAL.ListaProyectosPerfil(usr.IdUsuario).ToList();

                    var datos = (from p in Listado
                                 where p.FactProyecto.InicioProyecto.Value.Year == year
                                 select new
                                 {
                                     IdProyecto = p.IdProyecto,
                                     Proyecto = p.FactProyecto.NombreProyecto
                                 }).OrderBy(o => o.Proyecto).ToList();

                    if (datos.Count == 0)
                    {
                        return Json(new { Cod = 2, Msg = "No se encontraron proyectos para el año." });
                    }

                    return Json(new { Cod = 1, Msg = "OK", Datos = datos });
                }

            }
            catch (Exception e)
            {
                return Json(new { Cod = 0, Msg = e.Message });
            }
        }

        [HttpPost]
        public JsonResult CargaCboUsuarios()
        {
            try
            {
                var usr = (Dao.Model.FactUsuario)Session["usuario"];
                if (usr == null)
                {
                    return Json(new { Cod = 2, Msg = "Debe iniciar sesión para utilizar este recurso." });
                }

                int proyecto = int.Parse(Request.Form["project"]);

                var pjp = Dal.Clases.RelUsuariosDAL.ProyectosJP(usr.IdUsuario).Where(p => p.IdProyecto == proyecto).Select(p => p);
                var JefePro = (pjp.Count() > 0 ? 1 : 0);

                List<Dal.Clases.ListaGenerica> lista = new List<Dal.Clases.ListaGenerica>();

                if (usr.IsAdmin == 1 || JefePro == 1)
                {
                    var listproyecto = new List<int>();

                    listproyecto.Add(proyecto);

                    var Listado = Dal.Clases.RelUsuariosDAL.UsuariosProyectoDash(listproyecto).ToList();

                    lista = (from x in Listado
                             group x by x.IdUsuario into agru
                             select new Dal.Clases.ListaGenerica
                             {
                                 id = agru.Key,
                                 name = agru.FirstOrDefault().Nombre
                             }).OrderBy(o => o.name).ToList();
                }
                else
                {
                    var dataUser = new Dal.Clases.ListaGenerica();

                    dataUser.id = usr.IdUsuario;
                    dataUser.name = usr.Nombre;

                    lista.Add(dataUser);
                }

                if (lista.Count == 0)
                {
                    return Json(new { Cod = 2, Msg = "No se encontraron usuarios para el proyecto." });
                }

                return Json(new { Cod = 1, Msg = "OK", Datos = lista });
            }
            catch (Exception e)
            {
                return Json(new { Cod = 0, Msg = e.Message });
            }
        }

        [HttpPost]
        public JsonResult GeneraRepoprte()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(Request.Form["proyecto"]))
                {
                    return Json(new { Cod = 0, Msg = "Debe seleccionar un proyecto para generar el reporte." });
                }

                int proyecto = int.Parse(Request.Form["proyecto"]);

                string desde = Request.Form["desde"];
                string hasta = Request.Form["hasta"];

                int usuario = 0;
                if (!string.IsNullOrWhiteSpace(Request.Form["usuario"]))
                {
                    usuario = int.Parse(Request.Form["usuario"]);
                }



                var listadousuarios = new List<int>();
                var listadoproyectos = new List<int>();

                var fdesde = DateTime.ParseExact(desde, "dd-MM-yyyy", null);
                var fhasta = DateTime.ParseExact(hasta, "dd-MM-yyyy", null);

                if (usuario == 0 && proyecto == 0)
                {
                    var usr = (Dao.Model.FactUsuario)Session["usuario"];
                    int usuariologin = usr.IdUsuario;
                    var pjp = Dal.Clases.RelUsuariosDAL.ProyectosJP(usuariologin);
                    var Listado1 = Dal.Clases.RelUsuariosDAL.UsuariosProyectoDash(pjp.Select(p => p.IdProyecto).ToList());
                    listadousuarios = (from x in Listado1 group x by x.IdUsuario into agru select new { IdUsuario = agru.Key }).Select(p => p.IdUsuario).ToList();

                    var Listado2 = Dal.Clases.TimeSheetDAL.ListaProyectosTimesheet(usuariologin).ToList();
                    listadoproyectos = Listado2.Select(p => p.IdProyecto).ToList();
                }

                if (usuario == 0 && proyecto != 0)
                {
                    var listproyecto = new List<int>();
                    listproyecto.Add(proyecto);
                    var Listado1 = Dal.Clases.RelUsuariosDAL.UsuariosProyectoDash(listproyecto).ToList();
                    listadousuarios = (from x in Listado1 group x by x.IdUsuario into agru select new { IdUsuario = agru.Key }).Select(p => p.IdUsuario).ToList();

                    listadoproyectos.Add(proyecto);
                }

                if (usuario != 0 && proyecto == 0)
                {
                    var usr = (Dao.Model.FactUsuario)Session["usuario"];
                    int usuariologin = usr.IdUsuario;
                    var Listado1 = Dal.Clases.TimeSheetDAL.ListaProyectosTimesheet(usuariologin).ToList();
                    listadoproyectos = Listado1.Select(p => p.IdProyecto).ToList();

                    listadousuarios.Add(usuario);
                }

                if (usuario != 0 && proyecto != 0)
                {
                    listadousuarios.Add(usuario);
                    listadoproyectos.Add(proyecto);
                }

                var Listado = Dal.Clases.TimeSheetDAL.ListadoTimesheet(fdesde, fhasta, listadousuarios, listadoproyectos).ToList();

                var Datos = (from x in Listado
                             select new
                             {
                                 proyecto = x.RelUsuarioProyecto.FactProyecto.NombreProyecto,
                                 idusuario = x.RelUsuarioProyecto.IdUsuario,
                                 usuario = x.RelUsuarioProyecto.FactUsuario.Nombre,
                                 fecha = x.Fecha,
                                 tipoactividad = x.FactTipoActividad,
                                 actividad = x.Actividad,
                                 costo = x.DimTipoCosto.Descripcion,
                                 horainicio = x.HoraInicio,
                                 horafin = x.HoraFin,
                                 //totalhoras = Math.Round(x.TotalHoras.Value, 1),
                                 totalhoras = x.TotalHoras.Value,
                                 totalhorasfrm = Dal.Clases.Util.DecimalToHrMin(x.TotalHoras.Value),
                                 aprobado = x.IsAprobado,
                                 fechaaprobado = x.FechaAprobacion,
                                 useraprobacion = x.IdUsuarioAprobacion
                             }).OrderBy(x => x.proyecto).ThenBy(x => x.usuario).ThenBy(x => x.fecha).ToList();

                if (Datos.Count == 0)
                {
                    return Json(new { Cod = 2, Msg = "No se encontraron resultados en la búsqueda." });
                }
                else
                {
                    var listausuariosproyecto = Datos.GroupBy(l => l.idusuario).Select(u => u.First().idusuario).ToList();

                    SLDocument sl = new SLDocument();

                    int cont = 0;
                    foreach (var u in listadousuarios)
                    {
                        var nombreusuario = Dal.Clases.UsuarioDAL.DatosUsuario(u).Nombre;
                        if (cont == 0)
                        {
                            sl.RenameWorksheet(SLDocument.DefaultFirstSheetName, nombreusuario);
                            SLPageSettings ps = new SLPageSettings();
                            ps.TabColor = System.Drawing.Color.Navy;
                            sl.SetPageSettings(ps, nombreusuario);
                        }
                        else
                        {
                            sl.AddWorksheet(nombreusuario);
                            SLPageSettings ps = new SLPageSettings();
                            ps.TabColor = System.Drawing.Color.Navy;
                            sl.SetPageSettings(ps, nombreusuario);
                        }


                        var Datosaux = Datos.Where(l => l.idusuario == u).Select(x => x).ToList();

                        if (Datosaux.Count == 0)
                        {
                            sl.SetCellValue("A1", "Reporte TimeSheet desde el " + fdesde.ToString("dd-MM-yyyy") + " al " + fhasta.ToString("dd-MM-yyyy"));
                            sl.SetCellValue("A3", "El usuario no registra timesheet para el periodo seleccionado.");
                            SLStyle style = sl.CreateStyle();
                            style.SetFont(FontSchemeValues.Minor, 18);
                            style.SetFontBold(true);
                            style.Font.FontColor = System.Drawing.Color.Navy;
                            sl.SetCellStyle("A1", style);
                            sl.SetCellStyle("A3", style);
                        }
                        else
                        {
                            sl.SetCellValue("A1", "Reporte TimeSheet");
                            sl.SetCellValue("E1", "Desde el " + fdesde.ToString("dd-MM-yyyy") + " al " + fhasta.ToString("dd-MM-yyyy"));
                            SLStyle style = sl.CreateStyle();
                            style.SetFont(FontSchemeValues.Minor, 18);
                            style.SetFontBold(true);
                            style.Font.FontColor = System.Drawing.Color.Navy;
                            sl.SetCellStyle("A1", style);
                            sl.SetCellStyle("E1", style);

                            sl.SetCellValue(3, 1, "PROYECTO");
                            sl.SetCellValue(3, 2, "USUARIO");
                            sl.SetCellValue(3, 3, "FECHA CARGA");
                            sl.SetCellValue(3, 4, "ITEM GANTT");
                            sl.SetCellValue(3, 5, "ACTIVIDAD");
                            sl.SetCellValue(3, 6, "TIPO COSTO");
                            sl.SetCellValue(3, 7, "HORA INICIO");
                            sl.SetCellValue(3, 8, "HORA FIN");
                            sl.SetCellValue(3, 9, "TOTAL HORAS");
                            sl.SetCellValue(3, 10, "ESTADO APROB.");
                            sl.SetCellValue(3, 11, "FECHA APROB.");
                            sl.SetCellValue(3, 12, "USUARIO APROB.");



                            int i = 3;
                            foreach (var item in Datosaux)
                            {
                                i++;
                                sl.SetCellValue(i, 1, item.proyecto);
                                sl.SetCellValue(i, 2, item.usuario);
                                sl.SetCellValue(i, 3, item.fecha.Value.ToString("dd-MM-yyyy"));
                                sl.SetCellValue(i, 4, item.tipoactividad == null ? "Otro item" : item.tipoactividad.Descripcion);
                                sl.SetCellValue(i, 5, item.actividad);
                                sl.SetCellValue(i, 6, item.costo);
                                sl.SetCellValue(i, 7, item.horainicio);
                                sl.SetCellValue(i, 8, item.horafin);
                                sl.SetCellValue(i, 9, item.totalhorasfrm);
                                sl.SetCellValue(i, 10, item.aprobado == 1 ? "Aprobado" : "No aprobado");
                                sl.SetCellValue(i, 11, item.fechaaprobado == null ? " " : item.fechaaprobado.Value.ToString("dd-MM-yyyy"));
                                sl.SetCellValue(i, 12, item.useraprobacion == null ? " " : Dal.Clases.UsuarioDAL.DatosUsuario(item.useraprobacion.Value).Nombre);
                            }

                            sl.AutoFitColumn(1, 12);
                            SLStyle estilo = sl.CreateStyle();
                            SLStyle estilo2 = sl.CreateStyle();
                            estilo.Font.FontName = "Calibri";
                            estilo.Font.FontSize = 12;
                            estilo.Font.Bold = true;

                            estilo.SetVerticalAlignment(VerticalAlignmentValues.Center);
                            estilo.Alignment.Horizontal = HorizontalAlignmentValues.Left;
                            sl.SetCellStyle(3, 1, 3, 12, estilo);


                            var listaproy = (from x in Datosaux
                                             group x by x.proyecto into agru
                                             select new
                                             {
                                                 nombre = agru.Key,
                                             }).OrderBy(o => o.nombre).ToList();

                            i = i + 5;
                            foreach (var t in listaproy)
                            {
                                style = sl.CreateStyle();
                                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.Navy, System.Drawing.Color.Navy);
                                style.SetFontBold(true);
                                style.Font.FontColor = System.Drawing.Color.White;
                                sl.SetCellStyle(i, 1, style);
                                sl.SetCellStyle(i, 2, style);
                                sl.SetCellValue(i, 1, "Total horas " + t.nombre + ":");
                                var sumaTotalHoral = Datosaux.Where(r => r.proyecto == t.nombre).Sum(s => s.totalhoras);
                                sl.SetCellValue(i, 2, Dal.Clases.Util.DecimalToHrMin(sumaTotalHoral));
                                i++;

                                var result2 = Datosaux.Where(r => r.proyecto == t.nombre).GroupBy(l => l.costo)
                                    .Select(cl => new
                                    {
                                        tipocosto = cl.First().costo,
                                        total = cl.Sum(c => c.totalhoras),
                                    }).ToList();

                                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.CornflowerBlue, System.Drawing.Color.CornflowerBlue);
                                style.SetFontBold(false);
                                style.Font.FontColor = System.Drawing.Color.Black;
                                foreach (var cost in result2)
                                {
                                    sl.SetCellStyle(i, 1, style);
                                    sl.SetCellStyle(i, 2, style);
                                    sl.SetCellValue(i, 1, "Total horas " + cost.tipocosto + ":");
                                    sl.SetCellValue(i, 2, Dal.Clases.Util.DecimalToHrMin(cost.total));
                                    i++;
                                }

                                i++;
                            }

                            sl.AutoFitColumn(1, 11);


                        }

                        cont++;
                    }

                    MemoryStream ms = new MemoryStream();

                    sl.SaveAs(ms);
                    ms.Position = 0;

                    var Handler = Guid.NewGuid().ToString();
                    TempData[Handler] = ms.GetBuffer();

                    return Json(new { Cod = 1, Msg = "La descarga del reporte comenzará en breve.", Handler = Handler }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { Cod = 0, Msg = e.Message });
            }
        }

        public virtual ActionResult DescargaArchivo(string handler)
        {
            if (TempData[handler] != null)
            {
                var data = TempData[handler] as byte[];
                return File(data, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Reporte TimeSheet.xlsx");
            }
            else
            {
                return new EmptyResult();
            }

        }

        #endregion

        #region PROYECTOS Consultoria

        internal class PeriodosEDPC
        {
            public int periodo { get; set; }
            public DateTime inicioPeriodo { get; set; }
            public DateTime terminoPeriodo { get; set; }
            public string mesTxt { get; set; }
            public string hashPeriodo { get; set; }
        }

        [HttpPost]
        public JsonResult ListadoConsultorias()
        {
            try
            {
                var usu = (Dao.Model.FactUsuario)Session["usuario"];
                if (usu == null)
                {
                    return Json(new { Cod = 0, Msg = "Debe iniciar sesión para utilizar este recuro." });
                }

                int anio = int.Parse(Request.Form["anio"]);
                var logue = (Dao.Model.FactUsuario)Session["usuario"];
                var IdUs = (logue.IsAdmin == 1 ? 0 : logue.IdUsuario);

                var Listado = Dal.Clases.ProyectoDAL.ListaProyectos().ToList();

                Listado = Listado.Where(l => l.IdTipoProyecto == 2).Where(l => l.InicioProyecto.Value.Year == anio).Select(a => a).ToList();

                if (IdUs != 0)
                {
                    Listado = Listado.Where(l => l.RelUsuarioProyecto.Any(u => u.IdUsuario == IdUs && u.IsActivo == 1)).Select(a => a).ToList();
                }

                var Datos = from x in Listado
                            select new
                            {
                                id = x.IdProyecto,
                                nombre = x.NombreProyecto,
                                descripcion = x.Descripcion,
                                oc = x.OrdenDeCompra,
                                inicio = x.InicioProyecto.Value.ToString("dd-MM-yyyy"),
                                fin = x.FinProyecto.Value.ToString("dd-MM-yyyy")
                            };

                return Json(new { Cod = 1, Msg = "OK", Datos });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult PeriodosConsultoria()
        {
            try
            {
                int id = int.Parse(Request.Form["proyecto"]);
                var proyecto = Dal.Clases.ProyectoDAL.DatosProyecto(id);

                var inicioProyecto = proyecto.InicioProyecto.Value;
                var terminoProyecto = proyecto.FinProyecto.Value;

                var fechainicioprimerperiodo = new DateTime();
                var fechafinultimoperiodo = new DateTime();
                var fechaauxinicio = new DateTime();
                var fechaauxTermino = new DateTime();

                switch (proyecto.RangoEDP)
                {
                    case "07 - 06":
                        fechaauxinicio = new DateTime(inicioProyecto.Year, inicioProyecto.Month, 07);
                        fechainicioprimerperiodo = inicioProyecto >= fechaauxinicio ? fechaauxinicio : fechaauxinicio.AddMonths(-1);

                        fechaauxTermino = new DateTime(terminoProyecto.Year, terminoProyecto.Month, 06);
                        fechafinultimoperiodo = terminoProyecto >= fechaauxTermino ? fechaauxTermino.AddMonths(1) : fechaauxTermino;

                        break;
                    case "15 - 14":
                        fechaauxinicio = new DateTime(inicioProyecto.Year, inicioProyecto.Month, 15);
                        fechainicioprimerperiodo = inicioProyecto >= fechaauxinicio ? fechaauxinicio : fechaauxinicio.AddMonths(-1);

                        fechaauxTermino = new DateTime(terminoProyecto.Year, terminoProyecto.Month, 14);
                        fechafinultimoperiodo = terminoProyecto >= fechaauxTermino ? fechaauxTermino.AddMonths(1) : fechaauxTermino;

                        break;
                    case "21 - 20":
                        fechaauxinicio = new DateTime(inicioProyecto.Year, inicioProyecto.Month, 21);
                        fechainicioprimerperiodo = inicioProyecto >= fechaauxinicio ? fechaauxinicio : fechaauxinicio.AddMonths(-1);

                        fechaauxTermino = new DateTime(terminoProyecto.Year, terminoProyecto.Month, 20);
                        fechafinultimoperiodo = terminoProyecto >= fechaauxTermino ? fechaauxTermino.AddMonths(1) : fechaauxTermino;

                        break;
                    default:
                        fechainicioprimerperiodo = new DateTime(inicioProyecto.Year, inicioProyecto.Month, 1);

                        fechaauxTermino = new DateTime(terminoProyecto.Year, terminoProyecto.Month, 01).AddMonths(1);
                        fechafinultimoperiodo = fechaauxTermino.AddDays(-1);

                        break;
                }

                List<Pers> periodos = new List<Pers>();
                int i = 0;
                while (i != 1)
                {
                    Pers pe = new Pers();
                    pe.Fechainicio = fechainicioprimerperiodo;

                    fechaauxTermino = fechainicioprimerperiodo.AddMonths(1);
                    pe.Fechatermino = fechaauxTermino.AddDays(-1);

                    DateTime ff = pe.Fechainicio.Value;
                    
                    pe.Fecha = ff;
                    System.Globalization.TextInfo ti = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
                    pe.MesPalabra = ti.ToTitleCase(ff.ToString("MMMMM yyyy"));
                    pe.Periodo = int.Parse(ff.ToString("yyyyMM"));
                    var se = 0;
                    if (ff.Year == DateTime.Today.Year)
                    {
                        if (DateTime.Today.Month - 1 == ff.Month)
                        {
                            se = 1;
                        }
                    }
                    pe.Seleccionado = se;
                    periodos.Add(pe);

                    fechainicioprimerperiodo = fechainicioprimerperiodo.AddMonths(1);

                    if (fechafinultimoperiodo == pe.Fechatermino) {
                        i = 1;
                    }
                }
                return Json(new { Cod = 1, Msg = "OK", periodos });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GetEDP()
        {
            try
            {
                var usu = (Dao.Model.FactUsuario)Session["usuario"];
                if (usu == null)
                {
                    return Json(new { Cod = 0, Msg = "Debe iniciar sesión para utilizar este recuro." });
                }

                var id = Request.Form["id"];
                var per = Request.Form["periodo"];
                var responsable = Request.Form["responsable"];

                int idProyecto;
                bool success = Int32.TryParse(id, out idProyecto);
                if (!success)
                {
                    return Json(new { Cod = 0, Msg = "Identificador del proyecto no reconocido." });
                }

                int idresponsable;
                bool success_ = Int32.TryParse(responsable, out idresponsable);

                var proyecto = Dal.Clases.ProyectoDAL.DatosProyecto(idProyecto);

                if (proyecto != null)
                {
                    var usuarioProyecto = proyecto.RelUsuarioProyecto.Select(u => u).ToList();
                    var listaentregables = Dal.Clases.EntregableDAL.ListaEntregables(proyecto.IdProyecto);

                    var anio = int.Parse(per.ToString().Substring(0, 4));
                    var mes = int.Parse(per.ToString().Substring(4, 2));





                    var inicioProyecto = proyecto.InicioProyecto.Value;
                    var terminoProyecto = proyecto.FinProyecto.Value;
                    var numeroEDPC = 0;
                    var fechainicioprimerperiodo = new DateTime();
                    var fechafinultimoperiodo = new DateTime();
                    var fechaauxinicio = new DateTime();
                    var fechaauxTermino = new DateTime();

                    switch (proyecto.RangoEDP)
                    {
                        case "07 - 06":
                            fechaauxinicio = new DateTime(inicioProyecto.Year, inicioProyecto.Month, 07);
                            fechainicioprimerperiodo = inicioProyecto >= fechaauxinicio ? fechaauxinicio : fechaauxinicio.AddMonths(-1);

                            fechaauxTermino = new DateTime(terminoProyecto.Year, terminoProyecto.Month, 06);
                            fechafinultimoperiodo = terminoProyecto >= fechaauxTermino ? fechaauxTermino.AddMonths(1) : fechaauxTermino;

                            break;
                        case "15 - 14":
                            fechaauxinicio = new DateTime(inicioProyecto.Year, inicioProyecto.Month, 15);
                            fechainicioprimerperiodo = inicioProyecto >= fechaauxinicio ? fechaauxinicio : fechaauxinicio.AddMonths(-1);

                            fechaauxTermino = new DateTime(terminoProyecto.Year, terminoProyecto.Month, 14);
                            fechafinultimoperiodo = terminoProyecto >= fechaauxTermino ? fechaauxTermino.AddMonths(1) : fechaauxTermino;

                            break;
                        case "21 - 20":
                            fechaauxinicio = new DateTime(inicioProyecto.Year, inicioProyecto.Month, 21);
                            fechainicioprimerperiodo = inicioProyecto >= fechaauxinicio ? fechaauxinicio : fechaauxinicio.AddMonths(-1);

                            fechaauxTermino = new DateTime(terminoProyecto.Year, terminoProyecto.Month, 20);
                            fechafinultimoperiodo = terminoProyecto >= fechaauxTermino ? fechaauxTermino.AddMonths(1) : fechaauxTermino;

                            break;
                        default:
                            fechainicioprimerperiodo = new DateTime(inicioProyecto.Year, inicioProyecto.Month, 1);

                            fechaauxTermino = new DateTime(terminoProyecto.Year, terminoProyecto.Month, 01).AddMonths(1);
                            fechafinultimoperiodo = fechaauxTermino.AddDays(-1);

                            break;
                    }

                    List<Pers> periodos = new List<Pers>();
                    int c = 0;
                    int contador = 0;
                    while (c != 1)
                    {
                        Pers pe = new Pers();
                        pe.Fechainicio = fechainicioprimerperiodo;

                        fechaauxTermino = fechainicioprimerperiodo.AddMonths(1);
                        pe.Fechatermino = fechaauxTermino.AddDays(-1);

                        DateTime ff = pe.Fechainicio.Value;

                        pe.Fecha = ff;
                        System.Globalization.TextInfo ti = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
                        pe.MesPalabra = ti.ToTitleCase(ff.ToString("MMMMM yyyy"));
                        pe.Periodo = int.Parse(ff.ToString("yyyyMM"));
                        var se = 0;
                        if (ff.Year == DateTime.Today.Year)
                        {
                            if (DateTime.Today.Month - 1 == ff.Month)
                            {
                                se = 1;
                            }
                        }
                        pe.Seleccionado = se;
                        periodos.Add(pe);

                        if (per.Equals(ff.ToString("yyyyMM")))
                        {
                            numeroEDPC = contador + 1;
                        }

                        fechainicioprimerperiodo = fechainicioprimerperiodo.AddMonths(1);

                        if (fechafinultimoperiodo == pe.Fechatermino)
                        {
                            c = 1;
                        }
                        contador++;
                    }















                    //var fechainicioper = "01-" + per.ToString().Substring(4, 2) + "-" + anio;
                    //var fechafinper = DateTime.DaysInMonth(anio, mes) + "-" + per.ToString().Substring(4, 2) + "-" + anio;

                    /* Fechas Actual */
                    //var fdesdeEP = DateTime.ParseExact(fechainicioper, "dd-MM-yyyy", null);
                    //var fhastaEP = DateTime.ParseExact(fechafinper, "dd-MM-yyyy", null);

                    //var inicioProyecto = proyecto.InicioProyecto;
                    //var terminoProyecto = proyecto.FinProyecto;
                    //var difAnio = ((terminoProyecto.Value.Year - inicioProyecto.Value.Year) * 12);
                    //var difMes = terminoProyecto.Value.Month - inicioProyecto.Value.Month;
                    //var diferencia = difAnio + difMes + 1;
                    //var diferencia_ = diferencia - 1;

                    
                    //List<Pers> periodos = new List<Pers>();
                    //for (var i = 0; i <= diferencia_; i++)
                    //{
                    //    DateTime ff = proyecto.InicioProyecto.Value.AddMonths(i);
                    //    Pers pe = new Pers();
                    //    pe.Fecha = ff;
                    //    System.Globalization.TextInfo ti = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
                    //    pe.MesPalabra = ti.ToTitleCase(ff.ToString("MMMMM yyyy"));
                    //    pe.Periodo = int.Parse(ff.ToString("yyyyMM"));
                    //    periodos.Add(pe);

                    //    if (per.Equals(ff.ToString("yyyyMM")))
                    //    {
                    //        numeroEDPC = i + 1;
                    //    }
                    //}

                    var periodoActual = periodos.Where(p => p.Periodo.ToString().Equals(per)).Select(s => s).FirstOrDefault();
                    var diasLaboralesmes = Dal.Clases.Util.DiasLaborales(periodoActual.Fechainicio.Value, periodoActual.Fechatermino.Value);
                    var montopordia = (((proyecto.Monto / periodos.Count) / diasLaboralesmes) / usuarioProyecto.Count);

                    //var fechaActual = DateTime.Now;
                    //var fechaPeriodoActual_ = periodoActual.Fecha.AddMonths(1);
                    //var fechaActual = new DateTime(fechaPeriodoActual_.Year, fechaPeriodoActual_.Month, DateTime.Now.Day);

                    var fechaHastaEntregables = new DateTime();









                    //var fechainicioreporte_ = new DateTime();
                    //var fechafinreporte_ = new DateTime();
                    //var fechaaux_ = new DateTime();

                    //switch (proyecto.RangoEDP)
                    //{
                    //    case "07 - 06":
                    //        fechaaux_ = new DateTime(inicioProyecto.Value.Year, inicioProyecto.Value.Month, 07);

                    //        if (inicioProyecto.Value > fechaaux_)
                    //        {
                    //            fechafinreporte_ = fechaaux_.AddDays(-1);
                    //            fechainicioreporte_ = fechaaux_.AddMonths(-1);
                    //        }
                    //        else
                    //        {
                    //            fechafinreporte_ = fechaaux_.AddMonths(-1).AddDays(-1);
                    //            fechainicioreporte_ = fechaaux_.AddMonths(-2);
                    //        }

                    //        break;
                    //    case "15 - 14":
                    //        fechaaux_ = new DateTime(inicioProyecto.Value.Year, inicioProyecto.Value.Month, 15);

                    //        if (inicioProyecto.Value > fechaaux_)
                    //        {
                    //            fechafinreporte_ = fechaaux_.AddDays(-1);
                    //            fechainicioreporte_ = fechaaux_.AddMonths(-1);
                    //        }
                    //        else
                    //        {
                    //            fechafinreporte_ = fechaaux_.AddMonths(-1).AddDays(-1);
                    //            fechainicioreporte_ = fechaaux_.AddMonths(-2);
                    //        }
                    //        break;
                    //    case "21 - 20":
                    //        fechaaux_ = new DateTime(inicioProyecto.Value.Year, inicioProyecto.Value.Month, 21);

                    //        if (inicioProyecto.Value > fechaaux_)
                    //        {
                    //            fechafinreporte_ = fechaaux_.AddDays(-1);
                    //            fechainicioreporte_ = fechaaux_.AddMonths(-1);
                    //        }
                    //        else
                    //        {
                    //            fechafinreporte_ = fechaaux_.AddMonths(-1).AddDays(-1);
                    //            fechainicioreporte_ = fechaaux_.AddMonths(-2);
                    //        }
                    //        break;
                    //    default:
                    //        fechaaux_ = new DateTime(inicioProyecto.Value.Year, inicioProyecto.Value.Month, 1);
                    //        fechafinreporte_ = fechaaux_.AddMonths(1).AddDays(-1);
                    //        fechainicioreporte_ = new DateTime(fechafinreporte_.Year, fechafinreporte_.Month, 1);
                    //        break;
                    //}

                    //var listaPeriodosEDPC = new List<PeriodosEDPC>();
                    //for (var i = 0; i <= diferencia_; i++)
                    //{
                    //    var info = new PeriodosEDPC();
                    //    info.periodo = i + 1;
                    //    info.inicioPeriodo = fechainicioreporte_.AddMonths(i);
                    //    info.terminoPeriodo = fechafinreporte_.AddMonths(i);
                    //    TextInfo ti = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
                    //    info.mesTxt = ti.ToTitleCase(info.inicioPeriodo.ToString("MMMMM yyyy"));
                    //    info.hashPeriodo = info.inicioPeriodo.ToString("yyyyMM");
                    //    listaPeriodosEDPC.Add(info);
                    //}



                    var listaPeriodosEDPC = new List<PeriodosEDPC>();
                    contador = 0;
                    foreach (var i in periodos)
                    {
                        var info = new PeriodosEDPC();
                        info.periodo = contador + 1;
                        info.inicioPeriodo = i.Fechainicio.Value;
                        info.terminoPeriodo = i.Fechatermino.Value;
                        TextInfo ti = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
                        info.mesTxt = ti.ToTitleCase(info.inicioPeriodo.ToString("MMMMM yyyy"));
                        info.hashPeriodo = info.inicioPeriodo.ToString("yyyyMM");
                        listaPeriodosEDPC.Add(info);
                        contador++;
                    }






















                    var projectLead = "Paula Vergara M.";
                    double cien = 100;

                    var tipomoneda = "";
                    switch (proyecto.TipoMoneda)
                    {
                        case 1:
                            tipomoneda = "USD";
                            break;
                        case 2:
                            tipomoneda = "CLP";
                            break;
                        case 3:
                            tipomoneda = "UF";
                            break;

                    }

                    System.Drawing.Color[] colors = {
                    System.Drawing.Color.Navy,
                    System.Drawing.Color.DarkSlateGray,
                    System.Drawing.Color.SlateGray,
                    System.Drawing.Color.LightSlateGray,
                    System.Drawing.Color.LightSteelBlue
                };
                    SLDocument sl = new SLDocument();
                    SLPageSettings ps = new SLPageSettings();
                    SLStyle style = sl.CreateStyle();

                    SLStyle styleHeaderTabla = sl.CreateStyle();
                    styleHeaderTabla.Font.FontName = "Calibri";
                    styleHeaderTabla.Font.FontSize = 10;
                    styleHeaderTabla.Font.Bold = true;
                    styleHeaderTabla.Font.FontColor = System.Drawing.Color.White;
                    styleHeaderTabla.Fill.SetPattern(PatternValues.Solid, System.Drawing.ColorTranslator.FromHtml("#45546b"), System.Drawing.ColorTranslator.FromHtml("#45546b"));

                    SLStyle styleHeaderTabla2 = sl.CreateStyle();
                    styleHeaderTabla2.Font.FontName = "Calibri";
                    styleHeaderTabla2.Font.FontSize = 10;
                    styleHeaderTabla2.Font.Bold = true;
                    styleHeaderTabla2.Font.FontColor = System.Drawing.Color.Black;
                    styleHeaderTabla2.Fill.SetPattern(PatternValues.Solid, System.Drawing.ColorTranslator.FromHtml("#ACB9CA"), System.Drawing.ColorTranslator.FromHtml("#ACB9CA"));

                    SLStyle styleContenidoTabla = sl.CreateStyle();
                    styleContenidoTabla.Font.FontName = "Calibri";
                    styleContenidoTabla.Font.FontSize = 10;
                    styleContenidoTabla.Font.Bold = false;

                    SLStyle styleBorderFino = sl.CreateStyle();
                    styleBorderFino.Border.LeftBorder.BorderStyle = BorderStyleValues.Thin;
                    styleBorderFino.Border.BottomBorder.BorderStyle = BorderStyleValues.Thin;
                    styleBorderFino.Border.RightBorder.BorderStyle = BorderStyleValues.Thin;
                    styleBorderFino.Border.TopBorder.BorderStyle = BorderStyleValues.Thin;

                    SLStyle styleBorderGrueso = sl.CreateStyle();
                    styleBorderGrueso.Border.LeftBorder.BorderStyle = BorderStyleValues.Thick;
                    styleBorderGrueso.Border.BottomBorder.BorderStyle = BorderStyleValues.Thick;
                    styleBorderGrueso.Border.RightBorder.BorderStyle = BorderStyleValues.Thick;
                    styleBorderGrueso.Border.TopBorder.BorderStyle = BorderStyleValues.Thick;

                    SLStyle styleColorSolidoOscuro = sl.CreateStyle();
                    styleColorSolidoOscuro.Fill.SetPattern(PatternValues.Solid, System.Drawing.ColorTranslator.FromHtml("#45546b"), System.Drawing.ColorTranslator.FromHtml("#45546b"));

                    SLStyle styleColorSolidoMedio = sl.CreateStyle();
                    styleColorSolidoMedio.Fill.SetPattern(PatternValues.Solid, System.Drawing.ColorTranslator.FromHtml("#ACB9CA"), System.Drawing.ColorTranslator.FromHtml("#ACB9CA"));

                    SLStyle styleColorSolidoClaro = sl.CreateStyle();
                    styleColorSolidoClaro.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.White, System.Drawing.Color.White);

                    SLStyle stylealigcenter = sl.CreateStyle();
                    stylealigcenter.SetHorizontalAlignment(HorizontalAlignmentValues.Center);

                    SLStyle stylealigright = sl.CreateStyle();
                    stylealigright.SetHorizontalAlignment(HorizontalAlignmentValues.Right);

                    SLStyle stylealigleft = sl.CreateStyle();
                    stylealigleft.SetHorizontalAlignment(HorizontalAlignmentValues.Left);

                    SLStyle styletextoleft = sl.CreateStyle();
                    styletextoleft.Font.FontName = "Calibri";
                    styletextoleft.Font.FontSize = 10;
                    styletextoleft.Font.Bold = false;
                    styletextoleft.SetHorizontalAlignment(HorizontalAlignmentValues.Left);

                    SLStyle styletextoright = sl.CreateStyle();
                    styletextoright.Font.FontName = "Calibri";
                    styletextoright.Font.FontSize = 10;
                    styletextoright.Font.Bold = false;
                    styletextoright.SetHorizontalAlignment(HorizontalAlignmentValues.Right);

                    SLStyle styletexto7 = sl.CreateStyle();
                    styletexto7.Font.FontName = "Calibri";
                    styletexto7.Font.FontSize = 7;
                    styletexto7.Font.Bold = false;

                    SLStyle styleformatomoneda = sl.CreateStyle();
                    styleformatomoneda.FormatCode = "$#,##0.00";

                    SLStyle styleformatoporcent = sl.CreateStyle();
                    styleformatoporcent.FormatCode = "0%";



                    //var fechainicioreporte = new DateTime();
                    //var fechafinreporte = new DateTime();
                    //var fechaaux = new DateTime();

                    //switch (proyecto.RangoEDP)
                    //{
                    //    case "07 - 06":
                    //        fechaaux = new DateTime(fechaActual.Year, fechaActual.Month, 07);

                    //        if (fechaActual > fechaaux)
                    //        {
                    //            fechafinreporte = fechaaux.AddDays(-1);
                    //            fechainicioreporte = fechaaux.AddMonths(-1);
                    //        }
                    //        else
                    //        {
                    //            fechafinreporte = fechaaux.AddMonths(-1).AddDays(-1);
                    //            fechainicioreporte = fechaaux.AddMonths(-2);
                    //        }

                    //        break;
                    //    case "15 - 14":
                    //        fechaaux = new DateTime(fechaActual.Year, fechaActual.Month, 15);

                    //        if (fechaActual > fechaaux)
                    //        {
                    //            fechafinreporte = fechaaux.AddDays(-1);
                    //            fechainicioreporte = fechaaux.AddMonths(-1);
                    //        }
                    //        else
                    //        {
                    //            fechafinreporte = fechaaux.AddMonths(-1).AddDays(-1);
                    //            fechainicioreporte = fechaaux.AddMonths(-2);
                    //        }
                    //        break;
                    //    case "21 - 20":
                    //        fechaaux = new DateTime(fechaActual.Year, fechaActual.Month, 21);

                    //        if (fechaActual > fechaaux)
                    //        {
                    //            fechafinreporte = fechaaux.AddDays(-1);
                    //            fechainicioreporte = fechaaux.AddMonths(-1);
                    //        }
                    //        else
                    //        {
                    //            fechafinreporte = fechaaux.AddMonths(-1).AddDays(-1);
                    //            fechainicioreporte = fechaaux.AddMonths(-2);
                    //        }
                    //        break;
                    //    default:
                    //        fechaaux = new DateTime(fechaActual.Year, fechaActual.Month, 1);
                    //        fechafinreporte = fechaaux.AddDays(-1);
                    //        fechainicioreporte = new DateTime(fechafinreporte.Year, fechafinreporte.Month, 1);
                    //        break;
                    //}

                    var listafechas = new List<DateTime>();
                    //while (fechainicioreporte <= fechafinreporte)
                    //{
                    //    var fecha = fechainicioreporte;
                    //    listafechas.Add(fecha);
                    //    fechainicioreporte = fechainicioreporte.AddDays(1);
                    //}
                    var fechainicioreporte = periodoActual.Fechainicio.Value;
                    while (fechainicioreporte <= periodoActual.Fechatermino)
                    {
                        var fecha = fechainicioreporte;
                        listafechas.Add(fecha);
                        fechainicioreporte = fechainicioreporte.AddDays(1);
                    }


                    // Worksheet Consultores
                    contador = 0;
                    foreach (var up in usuarioProyecto)
                    {
                        if (contador == 0)
                        {
                            var nameWorksheet1 = up.FactUsuario.Nombre;
                            sl.RenameWorksheet(SLDocument.DefaultFirstSheetName, nameWorksheet1);
                            ps.TabColor = colors[0];
                            sl.SetPageSettings(ps, nameWorksheet1);
                        }
                        else
                        {
                            var nameWorksheet1 = up.FactUsuario.Nombre;
                            sl.AddWorksheet(nameWorksheet1);
                            ps.TabColor = colors[0];
                            sl.SetPageSettings(ps, nameWorksheet1);
                        }

                        sl.SetCellStyle(1, 1, 100, 100, styleColorSolidoOscuro);

                        sl.SetCellValue(1, 2, "Report");
                        sl.MergeWorksheetCells(1, 2, 1, 5);
                        sl.SetCellStyle(1, 2, stylealigcenter);

                        sl.SetCellValue(3, 2, "Services PO #: " + proyecto.OrdenDeCompra);
                        sl.MergeWorksheetCells(3, 2, 3, 5);
                        sl.SetCellValue(4, 2, "Client Project Manager: " + proyecto.PmCliente);
                        sl.MergeWorksheetCells(4, 2, 4, 5);
                        sl.SetCellValue(5, 2, "VMICA Project Lead:" + projectLead);
                        sl.MergeWorksheetCells(5, 2, 5, 5);

                        style = sl.CreateStyle();
                        style.Font.FontName = "Calibri";
                        style.Font.FontSize = 11;
                        style.Font.FontColor = System.Drawing.Color.White;
                        sl.SetCellStyle(1, 1, 5, 5, style);


                        sl.SetCellValue(7, 2, "");
                        sl.SetCellValue(7, 3, "Description");
                        sl.SetCellValue(7, 4, "days");
                        sl.SetCellValue(7, 5, tipomoneda);

                        style = sl.CreateStyle();
                        style.Font.FontName = "Calibri";
                        style.Font.FontSize = 11;
                        style.Font.Bold = true;
                        sl.SetCellStyle(1, 1, 7, 5, style);

                        sl.SetCellStyle(7, 2, 7, 5, stylealigcenter);



                        var fila = 8;
                        foreach (var fecha in listafechas)
                        {
                            sl.SetCellValue(fila, 2, new DateTime(fecha.Year, fecha.Month, fecha.Day));
                            style = sl.CreateStyle();
                            style.FormatCode = "dd-mmm";
                            sl.SetCellStyle(fila, 2, style);

                            var registrotimesheet = Dal.Clases.TimeSheetDAL.GetregistroTimesheetConsultor(fecha, up.IdUsuarioProyecto);

                            if (registrotimesheet != null)
                            {
                                sl.SetCellValue(fila, 3, registrotimesheet.Descripcion);
                                sl.SetCellValue(fila, 4, registrotimesheet.Dias.Value);
                                sl.SetCellValue(fila, 5, montopordia.Value);
                                sl.SetCellStyle(fila, 5, styleformatomoneda);

                                //totalMesServicio = totalMesServicio + montopordia.Value;
                            }

                            fila++;
                        }

                        sl.SetCellValue(fila, 2, "TOTAL");
                        sl.SetCellValue(fila, 3, "");
                        sl.SetCellValue(fila, 4, "=SUM(D8:D" + (fila - 1) + ")");
                        sl.SetCellValue(fila, 5, "=SUM(E8:E" + (fila - 1) + ")");

                        sl.SetCellStyle(fila, 5, styleformatomoneda);

                        style = sl.CreateStyle();
                        style.Font.FontName = "Calibri";
                        style.Font.FontSize = 10;
                        style.Font.Bold = false;
                        style.Font.FontColor = System.Drawing.Color.Black;
                        style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.White, System.Drawing.Color.White);
                        //style.Border. LeftBorder.BorderStyle = BorderStyleValues.Thick;
                        sl.SetCellStyle(7, 2, fila - 1, 5, style);

                        style = sl.CreateStyle();
                        style.Font.FontName = "Calibri";
                        style.Font.FontSize = 10;
                        style.Font.Bold = true;
                        style.Font.FontColor = System.Drawing.Color.Black;
                        style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.White, System.Drawing.Color.White);
                        //style.Border. LeftBorder.BorderStyle = BorderStyleValues.Thick;
                        sl.SetCellStyle(fila, 2, fila, 5, style);

                        sl.SetCellStyle(7, 2, fila, 5, styleBorderFino);

                        sl.SetCellStyle(7, 2, fila, 2, stylealigcenter);

                        sl.AutoFitColumn(2, 4);

                        sl.SetColumnWidth(5, 20);

                        contador++;
                    }

                    // Worksheet Resumen mes
                    var nameWorksheet2 = periodoActual.MesPalabra;
                    sl.AddWorksheet(nameWorksheet2);
                    ps.TabColor = colors[1];
                    sl.SetPageSettings(ps, nameWorksheet2);

                    sl.SetCellStyle(1, 1, 48, 5, styleColorSolidoClaro);

                    SLFont vmica1 = sl.CreateFont();
                    vmica1.SetFont("Calibri", 28);
                    vmica1.Bold = true;
                    vmica1.FontColor = System.Drawing.ColorTranslator.FromHtml("#6A7D8E");

                    SLFont vmica2 = sl.CreateFont();
                    vmica2.SetFont("Calibri", 28);
                    vmica2.Bold = true;
                    vmica2.FontColor = System.Drawing.ColorTranslator.FromHtml("#404A56");

                    SLRstType rst = sl.CreateRstType();
                    rst.AppendText("VM", vmica1);
                    rst.AppendText("ICA", vmica2);

                    sl.SetCellValue(1, 2, rst.ToInlineString());

                    sl.SetCellValue(1, 3, "Estado de Pago Nº " + numeroEDPC);
                    style = sl.CreateStyle();
                    style.Font.FontName = "Calibri";
                    style.Font.FontSize = 11;
                    style.Font.Bold = true;
                    style.Font.FontColor = System.Drawing.Color.Black;
                    sl.SetCellStyle(1, 3, style);

                    sl.SetCellValue(3, 2, "RUT: 76.306.369-7");
                    sl.SetCellValue(4, 2, "Perez Valenzuela 1634");
                    sl.SetCellValue(5, 2, "7500018 Santiago, Providencia");
                    sl.SetCellStyle(3, 2, 5, 2, styletextoleft);
                    sl.MergeWorksheetCells(3, 2, 3, 3);
                    sl.MergeWorksheetCells(4, 2, 4, 3);
                    sl.MergeWorksheetCells(5, 2, 5, 3);


                    sl.SetCellValue(2, 4, "Fecha EDP:");
                    sl.SetCellValue(3, 4, "PO:");
                    sl.SetCellValue(4, 4, "Total monto PO:");
                    sl.SetCellValue(5, 4, "Fecha inicio serv.:");
                    sl.SetCellValue(6, 4, "Periodo EDP:");

                    //sl.SetCellValue(2, 5, fechaActual.ToString("dd-MM-yyyy"));
                    sl.SetCellValue(2, 5, DateTime.Now.ToString("dd-MM-yyyy"));
                    sl.SetCellValue(3, 5, proyecto.OrdenDeCompra);
                    sl.SetCellValue(4, 5, proyecto.Monto.Value);
                    sl.SetCellValue(5, 5, proyecto.InicioProyecto.Value.ToString("dd-MM-yyyy"));
                    //sl.SetCellValue(6, 5, proyecto.RangoEDP);
                    sl.SetCellValue(6, 5, periodoActual.MesPalabra);  

                    sl.SetCellStyle(2, 4, 6, 5, styletextoright);
                    sl.SetCellStyle(4, 5, styleformatomoneda);

                    sl.SetCellValue(7, 2, "Estimado " + proyecto.PmCliente);
                    sl.SetCellValue(8, 2, proyecto.DimCliente.Nombre);
                    sl.SetCellStyle(7, 2, 8, 2, styletextoleft); 

                    sl.SetCellValue(10, 2, "Servicio");
                    sl.SetCellValue(10, 3, "Total parcial " + tipomoneda);
                    sl.SetCellValue(10, 4, "Acum. " + tipomoneda);
                    sl.SetCellValue(10, 5, "Disp.  PO " + tipomoneda);
                    sl.SetCellStyle(10, 2, 10, 5, styleHeaderTabla2);

                    var row = 11;
                    bool exitLoop = false;
                    //foreach (var serv in periodos)
                    //{
                    //    sl.SetCellValue(row, 2, proyecto.NombreProyecto + " " + serv.MesPalabra);

                    //    if (per.Equals(serv.Periodo.ToString()))
                    //    {
                    //        sl.SetCellValue(row, 3, totalMesServicio);
                    //        exitLoop = true;
                    //    }
                    //    else
                    //    {
                    //        sl.SetCellValue(row, 3, 0);
                    //    }

                    //    sl.SetCellValue(row, 5, "=E4-D" + row);
                    //    if (row == 12)
                    //    {
                    //        sl.SetCellValue(row, 4, "=C" + row);
                    //    }
                    //    else
                    //    {
                    //        sl.SetCellValue(row, 4, "=D" + (row - 1) + "+C" + row);
                    //    }

                    //    row++;

                    //    if (exitLoop) break;
                    //}

                    foreach (var periodoReporte in listaPeriodosEDPC)
                    {
                        sl.SetCellValue(row, 2, proyecto.NombreProyecto + " " + periodoReporte.mesTxt);

                        var totalMes = 0;

                        var diasLaborales = Dal.Clases.Util.DiasLaborales(periodoReporte.inicioPeriodo, periodoReporte.terminoPeriodo);
                        var montodia = (((proyecto.Monto / listaPeriodosEDPC.Count) / diasLaborales) / usuarioProyecto.Count);
                        var cantidadTimesheet = Dal.Clases.TimeSheetDAL.GetCantidadRegistroTimesheetConsultor(periodoReporte.inicioPeriodo, periodoReporte.terminoPeriodo, proyecto.IdProyecto);

                        totalMes = cantidadTimesheet * montodia.Value;
                        sl.SetCellValue(row, 3, totalMes);

                        sl.SetCellValue(row, 5, "=E4-D" + row);
                        if (row == 11)
                        {
                            sl.SetCellValue(row, 4, "=C" + row);
                        }
                        else
                        {
                            sl.SetCellValue(row, 4, "=D" + (row - 1) + "+C" + row);
                        }

                        sl.SetCellStyle(row, 2, row, 5, styleContenidoTabla); 

                        row++;
                        if (per.Equals(periodoReporte.hashPeriodo))
                        {
                            exitLoop = true;
                            fechaHastaEntregables = periodoReporte.inicioPeriodo;
                        }
                        if (exitLoop) break;
                    }



                    sl.SetCellValue(row, 2, "Total a Facturar " + periodoActual.MesPalabra);
                    sl.SetCellValue(row, 3, "=C" + (row - 1));
                    sl.SetCellStyle(row, 2, row, 5, styleHeaderTabla2);

                    sl.SetCellStyle(10, 2, row, 5, styleBorderFino);
                    sl.SetCellStyle(11, 3, row, 5, styleformatomoneda);

                    row = row + 2;
                    var inicioentregables = row;
                    if (listaentregables.Count == 0)
                    {
                        //sl.SetCellValue(row, 2, "El cobro corresponde a:");
                        //row = row + 2;
                        sl.SetCellValue(row, 2, "No se encontraron entregables para el proyecto " + proyecto.NombreProyecto);
                        sl.SetCellStyle(row, 2, styletextoleft);
                    }
                    else
                    {
                        sl.SetCellValue(row, 2, "El cobro corresponde a:");
                        sl.SetCellStyle(row, 2, styletextoleft);

                        row = row + 2;
                        inicioentregables = row;
                        var row_ = row;
                        sl.SetCellValue(row, 2, "Entregable");
                        sl.SetCellValue(row, 3, "Cód. Documento");
                        sl.SetCellValue(row, 4, "Avance");
                        sl.SetCellValue(row, 5, "Observación");
                        sl.SetCellStyle(row, 2, row, 5, styleHeaderTabla);

                        var numEntregable = 1;
                        foreach (var i in listaentregables)
                        {
                            var fechaEntegable = i.FechaModifica != null ? i.FechaModifica : i.FechaCarga;
                            if (i.Avance == 100 && fechaEntegable < fechaHastaEntregables)
                            {

                            }
                            else
                            {
                                row++;
                                sl.SetCellValue(row, 2, numEntregable + " - " + i.Entregable);
                                sl.SetCellValue(row, 3, i.CodigoDocumento);
                                if (i.Avance != null) { sl.SetCellValue(row, 4, (i.Avance.Value / cien)); } else { sl.SetCellValue(row, 4, ""); }
                                sl.SetCellValue(row, 5, i.Observaciones);
                                sl.SetCellStyle(row, 2, row, 5, styleContenidoTabla);
                                sl.SetCellStyle(row, 4, styleformatoporcent);
                                numEntregable++;
                            }
                        }

                        sl.SetCellStyle(row_, 2, row, 5, styleBorderFino);

                        row = row + 2;

                        //var jj = listaentregables.Where(p => p.IdTipoEntregable == 1).Select(j => j).ToList();
                        //if (jj.Count > 0)
                        //{
                        //    var row_ = row;
                        //    sl.SetCellValue(row, 1, "N°");
                        //    sl.SetCellValue(row, 2, "Entregables JJ");
                        //    sl.SetCellValue(row, 3, "Código Documento");
                        //    sl.SetCellValue(row, 4, "Responsable");
                        //    sl.SetCellValue(row, 5, "Avance");
                        //    sl.SetCellValue(row, 6, "Pendiente");
                        //    sl.SetCellValue(row, 7, "Versión Final");
                        //    sl.SetCellValue(row, 8, "Observaciones");
                        //    sl.SetCellStyle(row, 1, row, 8, styleHeaderTabla);

                        //    var numEntregable = 1;
                        //    foreach (var i in jj)
                        //    {
                        //        var fechaEntegable = i.FechaModifica != null ? i.FechaModifica : i.FechaCarga;
                        //        if (i.Avance == 100 && fechaEntegable < fechaHastaEntregables)
                        //        {

                        //        }
                        //        else
                        //        {
                        //            row++;
                        //            sl.SetCellValue(row, 1, numEntregable);
                        //            sl.SetCellValue(row, 2, i.Entregable);
                        //            sl.SetCellValue(row, 3, i.CodigoDocumento);
                        //            sl.SetCellValue(row, 4, i.FactUsuario.Nombre);
                        //            if (i.Avance != null) { sl.SetCellValue(row, 5, (i.Avance.Value / cien)); } else { sl.SetCellValue(row, 5, ""); }
                        //            sl.SetCellValue(row, 6, i.Pendiente == null ? "" : i.Pendiente == 0 ? "Si" : "No");
                        //            sl.SetCellValue(row, 7, i.VersionFinal == null ? "" : i.VersionFinal == 0 ? "Si" : "No");
                        //            sl.SetCellValue(row, 8, i.Observaciones);
                        //            sl.SetCellStyle(row, 1, row, 8, styleContenidoTabla);
                        //            sl.SetCellStyle(row, 5, styleformatoporcent);
                        //            numEntregable++;
                        //        }
                        //    }

                        //    sl.SetCellStyle(row_, 1, row, 8, styleBorderFino);

                        //    row = row + 2;
                        //}


                        //var fm = listaentregables.Where(p => p.IdTipoEntregable == 2).Select(j => j).ToList();
                        //if (fm.Count > 0)
                        //{
                        //    var row_ = row;
                        //    sl.SetCellValue(row, 1, "N°");
                        //    sl.SetCellValue(row, 2, "Entregables FM");
                        //    sl.SetCellValue(row, 3, "Código Documento");
                        //    sl.SetCellValue(row, 4, "Responsable");
                        //    sl.SetCellValue(row, 5, "Avance");
                        //    sl.SetCellValue(row, 6, "Pendiente");
                        //    sl.SetCellValue(row, 7, "Versión Final");
                        //    sl.SetCellValue(row, 8, "Observaciones");
                        //    sl.SetCellStyle(row, 1, row, 8, styleHeaderTabla);

                        //    var numEntregable = 1;
                        //    foreach (var i in fm)
                        //    {
                        //        var fechaEntegable = i.FechaModifica != null ? i.FechaModifica : i.FechaCarga;
                        //        if (i.Avance == 100 && fechaEntegable < fechaHastaEntregables)
                        //        {

                        //        }
                        //        else
                        //        {
                        //            row++;
                        //            sl.SetCellValue(row, 1, numEntregable);
                        //            sl.SetCellValue(row, 2, i.Entregable);
                        //            sl.SetCellValue(row, 3, i.CodigoDocumento);
                        //            sl.SetCellValue(row, 4, i.FactUsuario.Nombre);
                        //            if (i.Avance != null) { sl.SetCellValue(row, 5, (i.Avance.Value / cien)); } else { sl.SetCellValue(row, 5, ""); }
                        //            sl.SetCellValue(row, 6, i.Pendiente == null ? "" : i.Pendiente == 0 ? "Si" : "No");
                        //            sl.SetCellValue(row, 7, i.VersionFinal == null ? "" : i.VersionFinal == 0 ? "Si" : "No");
                        //            sl.SetCellValue(row, 8, i.Observaciones);
                        //            sl.SetCellStyle(row, 1, row, 8, styleContenidoTabla);
                        //            sl.SetCellStyle(row, 5, styleformatoporcent);
                        //            numEntregable++;
                        //        }
                        //    }

                        //    sl.SetCellStyle(row_, 1, row, 8, styleBorderFino);

                        //    row = row + 2;
                        //}

                        //var oc = listaentregables.Where(p => p.IdTipoEntregable == 3).Select(j => j).ToList();
                        //if (oc.Count > 0)
                        //{
                        //    var row_ = row;
                        //    sl.SetCellValue(row, 1, "N°");
                        //    sl.SetCellValue(row, 2, "Entregables OC");
                        //    sl.SetCellValue(row, 3, "Código Documento");
                        //    sl.SetCellValue(row, 4, "Responsable");
                        //    sl.SetCellValue(row, 5, "Avance");
                        //    sl.SetCellValue(row, 6, "Pendiente");
                        //    sl.SetCellValue(row, 7, "Versión Final");
                        //    sl.SetCellValue(row, 8, "Observaciones");
                        //    sl.SetCellStyle(row, 1, row, 8, styleHeaderTabla);

                        //    var numEntregable = 1;
                        //    foreach (var i in oc)
                        //    {
                        //        var fechaEntegable = i.FechaModifica != null ? i.FechaModifica : i.FechaCarga;
                        //        if (i.Avance == 100 && fechaEntegable < fechaHastaEntregables)
                        //        {

                        //        }
                        //        else
                        //        {
                        //            row++;
                        //            sl.SetCellValue(row, 1, numEntregable);
                        //            sl.SetCellValue(row, 2, i.Entregable);
                        //            sl.SetCellValue(row, 3, i.CodigoDocumento);
                        //            sl.SetCellValue(row, 4, i.FactUsuario.Nombre);
                        //            if (i.Avance != null) { sl.SetCellValue(row, 5, (i.Avance.Value / cien)); } else { sl.SetCellValue(row, 5, ""); }
                        //            sl.SetCellValue(row, 6, i.Pendiente == null ? "" : i.Pendiente == 0 ? "Si" : "No");
                        //            sl.SetCellValue(row, 7, i.VersionFinal == null ? "" : i.VersionFinal == 0 ? "Si" : "No");
                        //            sl.SetCellValue(row, 8, i.Observaciones);
                        //            sl.SetCellStyle(row, 1, row, 8, styleContenidoTabla);
                        //            sl.SetCellStyle(row, 5, styleformatoporcent);
                        //            numEntregable++;
                        //        }
                        //    }

                        //    sl.SetCellStyle(row_, 1, row, 8, styleBorderFino);

                        //    row = row + 2;
                        //}

                        //sl.SetCellStyle(inicioentregables, 1, row, 1, stylealigcenter);
                        sl.SetCellStyle(inicioentregables, 4, row, 4, stylealigcenter);
                        sl.SetCellStyle(inicioentregables, 1, row, 8, styletexto7);

                    }

                    row = row + 3;
                    switch (idresponsable)
                    {
                        case 1:
                            sl.SetCellValue(row, 2, "Contacto VMICA:");
                            sl.SetCellValue(row + 1, 2, "Paula Vergara M.");
                            sl.SetCellValue(row + 2, 2, "Telephone: +56963032042");
                            sl.SetCellValue(row + 3, 2, "Email: paula.vergara@vmica.com");
                            break;
                        case 2:
                            sl.SetCellValue(row, 2, "Contacto VMICA:");
                            sl.SetCellValue(row + 1, 2, "Mabel Berríos S.");
                            sl.SetCellValue(row + 2, 2, "Telephone: +56939174975");
                            sl.SetCellValue(row + 3, 2, "Email: mabel.berrios@vmica.com");
                            break;
                        default:
                            sl.SetCellValue(row, 2, "Contacto VMICA");
                            break;
                    }

                    //sl.MergeWorksheetCells(row, 2, row, 3);
                    //sl.MergeWorksheetCells(row + 1, 2, row + 1, 3);
                    //sl.MergeWorksheetCells(row + 2, 2, row + 2, 3);
                    //sl.MergeWorksheetCells(row + 3, 2, row + 3, 3);

                    sl.SetCellStyle(row, 2, row + 3, 2, styleContenidoTabla);
                    sl.SetCellStyle(row, 2, row + 3, 2, stylealigcenter);
                    
                    style = sl.CreateStyle();
                    style.Border.BottomBorder.BorderStyle = BorderStyleValues.Thick;
                    sl.SetCellStyle(row - 1, 2, style);

                    style = sl.CreateStyle();
                    style.Border.BottomBorder.BorderStyle = BorderStyleValues.Thick;
                    sl.SetCellStyle(row - 1, 4, row - 1, 5, style);

                    sl.SetCellValue(row, 4, "Project Manager");
                    sl.MergeWorksheetCells(row, 4, row, 5);
                    sl.SetCellStyle(row, 4, row, 5, styleContenidoTabla);
                    sl.SetCellStyle(row, 4, row, 5, stylealigcenter);

                    sl.DeleteColumn(1, 1);

                    sl.AutoFitColumn(1, 5);

                    

                    // Worksheet Entregables
                    var nameWorksheet3 = "Entregables";
                    sl.AddWorksheet(nameWorksheet3);
                    ps.TabColor = colors[2];
                    sl.SetPageSettings(ps, nameWorksheet3);

                    sl.SetCellStyle(1, 1, 100, 100, styleColorSolidoClaro);

                    row = 1;
                    inicioentregables = row;
                    if (listaentregables.Count == 0)
                    {
                        sl.SetCellValue(1, 4, "No se encontraron entregables para el proyecto " + proyecto.NombreProyecto);
                    }
                    else
                    {

                        var row_ = row;
                        sl.SetCellValue(row, 1, "N°");
                        sl.SetCellValue(row, 2, "Entregables JJ");
                        sl.SetCellValue(row, 3, "Código Documento");
                        sl.SetCellValue(row, 4, "Responsable");
                        sl.SetCellValue(row, 5, "Avance");
                        sl.SetCellValue(row, 6, "Pendiente");
                        sl.SetCellValue(row, 7, "Versión Final");
                        sl.SetCellValue(row, 8, "Observaciones");
                        sl.SetCellStyle(row, 1, row, 8, styleHeaderTabla);

                        var numEntregable = 1;
                        foreach (var i in listaentregables)
                        {
                            var fechaEntegable = i.FechaModifica != null ? i.FechaModifica : i.FechaCarga;
                            if (i.Avance == 100 && fechaEntegable < fechaHastaEntregables)
                            {

                            }
                            else
                            {
                                row++;
                                sl.SetCellValue(row, 1, numEntregable);
                                sl.SetCellValue(row, 2, i.Entregable);
                                sl.SetCellValue(row, 3, i.CodigoDocumento);
                                sl.SetCellValue(row, 4, i.FactUsuario.Nombre);
                                if (i.Avance != null) { sl.SetCellValue(row, 5, (i.Avance.Value / cien)); } else { sl.SetCellValue(row, 5, ""); }
                                sl.SetCellValue(row, 6, i.Pendiente == null ? "" : i.Pendiente == 0 ? "Si" : "No");
                                sl.SetCellValue(row, 7, i.VersionFinal == null ? "" : i.VersionFinal == 0 ? "Si" : "No");
                                sl.SetCellValue(row, 8, i.Observaciones);
                                sl.SetCellStyle(row, 1, row, 8, styleContenidoTabla);
                                sl.SetCellStyle(row, 5, styleformatoporcent);
                                numEntregable++;
                            }
                        }

                        sl.SetCellStyle(row_, 1, row, 8, styleBorderFino);

                        row = row + 2;













                        //var jj = listaentregables.Where(p => p.IdTipoEntregable == 1).Select(j => j).ToList();
                        //if (jj.Count > 0)
                        //{
                        //    row_ = row;
                        //    sl.SetCellValue(row, 1, "N°");
                        //    sl.SetCellValue(row, 2, "Entregables JJ");
                        //    sl.SetCellValue(row, 3, "Código Documento");
                        //    sl.SetCellValue(row, 4, "Responsable");
                        //    sl.SetCellValue(row, 5, "Avance");
                        //    sl.SetCellValue(row, 6, "Pendiente");
                        //    sl.SetCellValue(row, 7, "Versión Final");
                        //    sl.SetCellValue(row, 8, "Observaciones");
                        //    sl.SetCellStyle(row, 1, row, 8, styleHeaderTabla);

                        //    numEntregable = 1;
                        //    foreach (var i in jj)
                        //    {
                        //        var fechaEntegable = i.FechaModifica != null ? i.FechaModifica : i.FechaCarga;
                        //        if (i.Avance == 100 && fechaEntegable < fechaHastaEntregables)
                        //        {

                        //        }
                        //        else
                        //        {
                        //            row++;
                        //            sl.SetCellValue(row, 1, numEntregable);
                        //            sl.SetCellValue(row, 2, i.Entregable);
                        //            sl.SetCellValue(row, 3, i.CodigoDocumento);
                        //            sl.SetCellValue(row, 4, i.FactUsuario.Nombre);
                        //            if (i.Avance != null) { sl.SetCellValue(row, 5, (i.Avance.Value / cien)); } else { sl.SetCellValue(row, 5, ""); }
                        //            sl.SetCellValue(row, 6, i.Pendiente == null ? "" : i.Pendiente == 0 ? "Si" : "No");
                        //            sl.SetCellValue(row, 7, i.VersionFinal == null ? "" : i.VersionFinal == 0 ? "Si" : "No");
                        //            sl.SetCellValue(row, 8, i.Observaciones);
                        //            sl.SetCellStyle(row, 1, row, 8, styleContenidoTabla);
                        //            sl.SetCellStyle(row, 5, styleformatoporcent);
                        //            numEntregable++;
                        //        }
                        //    }

                        //    sl.SetCellStyle(row_, 1, row, 8, styleBorderFino);

                        //    row = row + 2;
                        //}


                        //var fm = listaentregables.Where(p => p.IdTipoEntregable == 2).Select(j => j).ToList();
                        //if (fm.Count > 0)
                        //{
                        //    row_ = row;
                        //    sl.SetCellValue(row, 1, "N°");
                        //    sl.SetCellValue(row, 2, "Entregables FM");
                        //    sl.SetCellValue(row, 3, "Código Documento");
                        //    sl.SetCellValue(row, 4, "Responsable");
                        //    sl.SetCellValue(row, 5, "Avance");
                        //    sl.SetCellValue(row, 6, "Pendiente");
                        //    sl.SetCellValue(row, 7, "Versión Final");
                        //    sl.SetCellValue(row, 8, "Observaciones");
                        //    sl.SetCellStyle(row, 1, row, 8, styleHeaderTabla);

                        //    numEntregable = 1;
                        //    foreach (var i in fm)
                        //    {
                        //        var fechaEntegable = i.FechaModifica != null ? i.FechaModifica : i.FechaCarga;
                        //        if (i.Avance == 100 && fechaEntegable < fechaHastaEntregables)
                        //        {

                        //        }
                        //        else
                        //        {
                        //            row++;
                        //            sl.SetCellValue(row, 1, numEntregable);
                        //            sl.SetCellValue(row, 2, i.Entregable);
                        //            sl.SetCellValue(row, 3, i.CodigoDocumento);
                        //            sl.SetCellValue(row, 4, i.FactUsuario.Nombre);
                        //            if (i.Avance != null) { sl.SetCellValue(row, 5, (i.Avance.Value / cien)); } else { sl.SetCellValue(row, 5, ""); }
                        //            sl.SetCellValue(row, 6, i.Pendiente == null ? "" : i.Pendiente == 0 ? "Si" : "No");
                        //            sl.SetCellValue(row, 7, i.VersionFinal == null ? "" : i.VersionFinal == 0 ? "Si" : "No");
                        //            sl.SetCellValue(row, 8, i.Observaciones);
                        //            sl.SetCellStyle(row, 1, row, 8, styleContenidoTabla);
                        //            sl.SetCellStyle(row, 5, styleformatoporcent);
                        //            numEntregable++;
                        //        }
                        //    }

                        //    sl.SetCellStyle(row_, 1, row, 8, styleBorderFino);

                        //    row = row + 2;
                        //}

                        //var oc = listaentregables.Where(p => p.IdTipoEntregable == 3).Select(j => j).ToList();
                        //if (oc.Count > 0)
                        //{
                        //    row_ = row;
                        //    sl.SetCellValue(row, 1, "N°");
                        //    sl.SetCellValue(row, 2, "Entregables OC");
                        //    sl.SetCellValue(row, 3, "Código Documento");
                        //    sl.SetCellValue(row, 4, "Responsable");
                        //    sl.SetCellValue(row, 5, "Avance");
                        //    sl.SetCellValue(row, 6, "Pendiente");
                        //    sl.SetCellValue(row, 7, "Versión Final");
                        //    sl.SetCellValue(row, 8, "Observaciones");
                        //    sl.SetCellStyle(row, 1, row, 8, styleHeaderTabla);

                        //    numEntregable = 1;
                        //    foreach (var i in oc)
                        //    {
                        //        var fechaEntegable = i.FechaModifica != null ? i.FechaModifica : i.FechaCarga;
                        //        if (i.Avance == 100 && fechaEntegable < fechaHastaEntregables)
                        //        {

                        //        }
                        //        else
                        //        {
                        //            row++;
                        //            sl.SetCellValue(row, 1, numEntregable);
                        //            sl.SetCellValue(row, 2, i.Entregable);
                        //            sl.SetCellValue(row, 3, i.CodigoDocumento);
                        //            sl.SetCellValue(row, 4, i.FactUsuario.Nombre);
                        //            if (i.Avance != null) { sl.SetCellValue(row, 5, (i.Avance.Value / cien)); } else { sl.SetCellValue(row, 5, ""); }
                        //            sl.SetCellValue(row, 6, i.Pendiente == null ? "" : i.Pendiente == 0 ? "Si" : "No");
                        //            sl.SetCellValue(row, 7, i.VersionFinal == null ? "" : i.VersionFinal == 0 ? "Si" : "No");
                        //            sl.SetCellValue(row, 8, i.Observaciones);
                        //            sl.SetCellStyle(row, 1, row, 8, styleContenidoTabla);
                        //            sl.SetCellStyle(row, 5, styleformatoporcent);
                        //            numEntregable++;
                        //        }
                        //    }

                        //    sl.SetCellStyle(row_, 1, row, 8, styleBorderFino);

                        //    row = row + 2;
                        //}

                        sl.SetCellStyle(inicioentregables, 1, row, 1, stylealigcenter);
                        sl.SetCellStyle(inicioentregables, 5, row, 7, stylealigcenter);
                    }

                    sl.AutoFitColumn(1, 8);

                    // Worksheet 4
                    var nameWorksheet4 = "Otros";
                    sl.AddWorksheet(nameWorksheet4);
                    ps.TabColor = colors[3];
                    sl.SetPageSettings(ps, nameWorksheet4);

                    //Volver a la pestaña EDP
                    sl.SelectWorksheet(nameWorksheet2);

                    MemoryStream mr = new MemoryStream();
                    sl.SaveAs(mr);
                    var handler = Guid.NewGuid().ToString();
                    TempData[handler] = mr.GetBuffer();
                    return Json(new { Cod = 1, Msg = "", Handler = handler, proy = id });
                }
                else
                {
                    return Json(new { Cod = 0, Msg = "No se encontró proyecto para el identificador ingresado." });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        public virtual ActionResult Download(string Handler, int? Proy = null)
        {
            if (TempData[Handler] != null)
            {
                var name = "";
                if (Proy != null)
                {
                    var proyecto = Dal.Clases.ProyectoDAL.DatosProyecto(Proy.Value);
                    name = proyecto != null ? proyecto.NombreProyecto : "";
                }

                var nombreArchivo = "EDP " + name + " " + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx";

                byte[] data = TempData[Handler] as byte[];
                return File(data, "application/vnd.ms-excel", nombreArchivo);
            }
            else
            {
                return new EmptyResult();
            }
        }

        #endregion
    }
}
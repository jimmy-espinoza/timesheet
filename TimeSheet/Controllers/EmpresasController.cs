﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TimeSheet.Controllers
{
    public class EmpresasController : Controller
    {
        // GET: Empresas
        public ActionResult Index()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        public JsonResult ListaEmpresas()
        {
            try
            {
                var ListaEmpresas = Dal.Clases.EmpresaDAL.ListaEmpresas().ToList();

                var Datos = from d in ListaEmpresas
                            select new
                            {
                                IdEmpresa = d.IdEmpresa,
                                NombreEmpresa = d.NombreEmpresa,
                                RutEmpresa = d.RutEmpresa,
                                Contacto = d.Contacto,
                                TelefonoContacto = d.TelefonoContacto,
                                MailContacto = d.MailContacto,
                                UfHora = d.UfHora,
                                Estado = d.IsActivo
                            };

                return Json(new { Cod = 1, Msg = "OK", Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GuardarEmpresa()
        {
            var IdEmpresa = int.Parse(Request.Form["IdEmpresa"]);
            var NombreEmpresa = Request.Form["Empresa"];
            var RutEmpresa = Request.Form["Rut"];
            var Contacto = Request.Form["Contacto"];
            var TelefonoContacto = int.Parse(Request.Form["TelefonoContacto"]);
            var MailContacto = (Request.Form["MailContacto"]);
            var AplicaCosto = (Request.Form["AplicaCosto"] == null ? 0 : 1);
            var UfHora = double.Parse(Request.Form["UfHora"]);
            var IsActivo = int.Parse(Request.Form["Estado"]);

            try
            {
                var resultado = Dal.Clases.EmpresaDAL.GuardarEmpresa(IdEmpresa, NombreEmpresa, RutEmpresa, Contacto, TelefonoContacto, MailContacto, AplicaCosto, UfHora, IsActivo);

                if (resultado == 0)
                {
                    return Json(new { Cod = 0, Msg = "No fue posible guardar los datos del proveedor, intente Nuevamente." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Cod = 1, Msg = "Los datos del proveedor " + NombreEmpresa + " se han registrado exitosamente." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public JsonResult DatosEmpresa()
        {
            try
            {
                var IdEmpresa = int.Parse(Request.Form["IdEmpresa"]);

                Dao.Model.DimEmpresa Empresa = Dal.Clases.EmpresaDAL.DatosEmpresa(IdEmpresa);

                return Json(new { Cod = 1, Msg = "OK", Datos = Empresa }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ExisteEmpresa()
        {
            try
            {
                var rut = Request.Form["Rut"];

                int Existe = Dal.Clases.EmpresaDAL.EmpresaRut(rut);

                return Json(new { Cod = 1, Msg = "OK", Existe }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ExisteProveedor()
        {
            try
            {
                var prov = Request.Form["Empresa"];

                int Existe = Dal.Clases.EmpresaDAL.ProveedorExistente(prov);

                return Json(new { Cod = 1, Msg = "OK", Existe }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TimeSheet.Controllers
{
    public class PerfilesController : Controller
    {
        // GET: Perfiles
        public ActionResult Index()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        public JsonResult ListaPerfiles()
        {
            try
            {
                var ListaPerfiles = Dal.Clases.PerfilDAL.ListaPerfiles().ToList();

                var Datos = from d in ListaPerfiles
                            select new
                            {
                                Id = d.Id,
                                Perfil = d.DescPerfil,
                                Estado = d.IsActivo
                            };

                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GuardarPerfil()
        {
            try
            {
                var Id = int.Parse(Request.Form["Id"]);
                var DescPerfil = Request.Form["DescPerfil"];
                var IsActivo = int.Parse(Request.Form["Estado"]);

                var resultado = Dal.Clases.PerfilDAL.GuardarPerfil(Id, DescPerfil, IsActivo);

                if (resultado == 0)
                {
                    return Json(new { Cod = 0, Msg = "No fue posible guardar los datos del perfil, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Cod = 1, Msg = "Los datos del Perfil " + DescPerfil + " se han registrado exitosamente." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DatosPerfil()
        {
            var Id = int.Parse(Request.Form["Id"]);

            try
            {
                Dao.Model.DimPerfil Perfil = Dal.Clases.PerfilDAL.DatosPerfil(Id);

                return Json(new { Cod = 1, Msg = "OK", Datos = Perfil }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
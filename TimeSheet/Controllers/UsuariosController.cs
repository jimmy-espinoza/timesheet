﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TimeSheet.Controllers
{
    public class UsuariosController : Controller
    {
        // GET: Usuarios
        public ActionResult Index()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        public JsonResult ListaUsuarios()
        {
            try
            {
                //var ListaUsuarios = Dal.Clases.UsuarioDAL.LoginUsuario(usuario, password);

                var ListaUsuarios = Dal.Clases.UsuarioDAL.ListaUsuarios().ToList();

                var Datos = from d in ListaUsuarios
                            select new
                            {
                                IdUsuario = d.IdUsuario,
                                NombreUsuario = d.NombreUsuario,
                                Nombre = d.Nombre,
                                Correo = d.Correo,
                                Empresa = d.DimEmpresa.NombreEmpresa,
                                Perfil = d.DimPerfil.DescPerfil,
                                Admin = d.IsAdmin.Value,
                                Estado = d.IsActivo
                            };

                return Json(new { Cod = 1, Msg = "OK", Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        public JsonResult GuardarUsuario()
        {
            try
            {
                HttpPostedFileBase Archivo = Request.Files["firma"];
                var IdUsuario = int.Parse(Request.Form["IdUsuario"]);
                var NombreUsuario = Request.Form["usuario"];
                var Nombre = Request.Form["nombre"];
                var Clave = Dal.Clases.Util.EncriptarBase64(Request.Form["clave"]);
                var Correo = Request.Form["correo"];
                var IdEmpresa = int.Parse(Request.Form["empresa"]);
                var IdPerfil = int.Parse(Request.Form["perfil"]);
                var Admin = (Request.Form["Admin"] == null ? 0 : 1);
                var IsActivo = int.Parse(Request.Form["Estado"]);
                var exFirma = Request.Form["firma-o"];

                var ArchivoFirma = "";
                if (Archivo.ContentLength > 0)
                {
                    string nombreFirma = DateTime.Now.ToString("yyMMddHHmmss") + Archivo.FileName;
                    string mimeType = Archivo.ContentType;
                    System.IO.Stream fileContent = Archivo.InputStream;
                    Archivo.SaveAs(Server.MapPath("~/Content/Firmas/") + nombreFirma);
                    ArchivoFirma = nombreFirma;

                    if (exFirma != "")
                    {
                        string aBorrar = Request.MapPath("~/Content/Firmas/" + exFirma);
                        if (System.IO.File.Exists(aBorrar))
                        {
                            System.IO.File.Delete(aBorrar);
                        }
                    }
                }
                else
                {
                    ArchivoFirma = exFirma;
                }

                var resultado = Dal.Clases.UsuarioDAL.GuardarUsuario(IdUsuario, NombreUsuario, Nombre, Clave, Correo, IdEmpresa, IdPerfil, Admin, IsActivo, ArchivoFirma);

                if (resultado == 0)
                {
                    return Json(new { Cod = 0, Msg = "No fue posible guardar los datos del usuario, intente Nuevamente." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Cod = 1, Msg = "Los datos del usuario " + NombreUsuario + " se han registrado exitosamente." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DatosUsuario()
        {
            try
            {
                var IdUsuario = int.Parse(Request.Form["idusuario"]);

                Dao.Model.FactUsuario usuario = Dal.Clases.UsuarioDAL.DatosUsuario(IdUsuario);
                usuario.Clave = Dal.Clases.Util.DesencriptarBase64(usuario.Clave);
                return Json(new { Cod = 1, Msg = "OK", Datos = usuario }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ExisteUsuario()
        {
            var User = Request.Form["usuario"];

            try
            {
                int Existe = Dal.Clases.UsuarioDAL.UsuarioExistente(User);

                return Json(new { Cod = 1, Msg = "OK", Existe }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
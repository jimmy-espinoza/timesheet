﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TimeSheet.Controllers
{
    public class ActividadController : Controller
    {
        // GET: Actividad
        public ActionResult Index()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        // Registro timesheet
        public ActionResult RegistroTimesheet()
        {
            Dao.Model.FactUsuario ElUser = new Dao.Model.FactUsuario();
            ElUser.NombreUsuario = "Esteban";
            Session["usuario"] = ElUser;

            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
    }
}
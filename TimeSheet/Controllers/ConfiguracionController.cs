﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace TimeSheet.Controllers
{
    public class ConfiguracionController : Controller
    {
        // GET: Configuracion
        public ActionResult Index()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        public JsonResult ListaConfiguracion()
        {
            try
            {
                //string desde = Request.Form["desde"];

                var lc = Dal.Clases.ConfiguracionDAL.ListaConfiguracion();

                return Json(new { Cod = 1, Msg = "OK", Datos = lc }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DatosNotificacion()
        {
            try
            {
                int Id = int.Parse(Request.Form["IdParametroGeneral"]);

                var dc = Dal.Clases.ConfiguracionDAL.DatosConfiguracion(Id);

                return Json(new { Cod = 1, Msg = "OK", Datos = dc }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GuardarNotificacion()
        {
            try
            {
                string HoraEjecucion = Request.Form["HoraEjecucion"];
                string DiasSinRegistro = Request.Form["DiasSinRegistro"];
                var GH = Dal.Clases.ConfiguracionDAL.GuardarConfiguracion(1, "", HoraEjecucion, DateTime.Now);
                var GD = Dal.Clases.ConfiguracionDAL.GuardarConfiguracion(2, "", DiasSinRegistro, DateTime.Now);
                return Json(new { Cod = 1, Msg = "Datos registrados exitosamente" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GuardarHoraEjecucion()
        {
            try
            {
                string HoraEjecucion = Request.Form["HoraEjecucion"];
                var GH = Dal.Clases.ConfiguracionDAL.GuardarConfiguracion(1, "", HoraEjecucion, DateTime.Now);
                return Json(new { Cod = 1, Msg = "Horario notificaciones registrado exitosamente" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Guardarlimite()
        {
            try
            {
                string LimiteDias = Request.Form["LimiteDias"];
                var GL = Dal.Clases.ConfiguracionDAL.GuardarConfiguracion(3, "", LimiteDias, DateTime.Now);
                return Json(new { Cod = 1, Msg = "Límite días registrado exitosamente" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public static void ObtenerUF()
        {
            //string fecha = DateTime.Today.AddDays(1).ToString("dd-MM-yyyy");
            string fecha = DateTime.Today.ToString("dd-MM-yyyy");
            string apiUrl = "https://www.mindicador.cl/api/uf/" + fecha;
            string jsonString = "";
            WebClient http = new WebClient();
            JavaScriptSerializer jss = new JavaScriptSerializer();

            http.Headers.Add(HttpRequestHeader.Accept, "application/json");
            jsonString = http.DownloadString(apiUrl);
            var indicatorsObject = JsonConvert.DeserializeObject<Valores>(jsonString);

            if (indicatorsObject.serie.Count > 0)
            {
                double valorUF = indicatorsObject.serie.FirstOrDefault().valor;
                var PD = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                var UD = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month));
                var eUF = Dal.Clases.ConversionMonedaDAL.ListaConversion("UF", PD, UD);

                if (eUF.Count == 0)
                {
                    Dal.Clases.ConversionMonedaDAL.GuardarConversionMoneda("UF", UD, valorUF);
                }
                else
                {
                    var UfMax = eUF.OrderByDescending(f => f.Fecha.Value).FirstOrDefault();
                    Dal.Clases.ConversionMonedaDAL.ActualizarConversionMoneda(UfMax.Id, "UF", UD, valorUF);
                }
            }
        }

        internal class Serie
        {
            public DateTime fecha { get; set; }
            public double valor { get; set; }

        }

        internal class Valores
        {
            public string version { get; set; }
            public string autor { get; set; }
            public string codigo { get; set; }
            public string nombre { get; set; }
            public string unidad_medida { get; set; }
            public List<Serie> serie { get; set; }

        }
        
    }
}
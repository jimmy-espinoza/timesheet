﻿using DocumentFormat.OpenXml.Spreadsheet;
using ExcelDataReader;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TimeSheet.Controllers
{
    public class ProyectosController : Controller
    {
        // GET: Proyectos
        public ActionResult Index()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        #region Proyectos Desarrollo

        [HttpPost]
        public JsonResult ListaProyectos()
        {
            try
            {
                var ListaProyectos = Dal.Clases.ProyectoDAL.ListaProyectos().Where(p => p.IdTipoProyecto == 1).Select(x => x).ToList();

                var Datos = from d in ListaProyectos
                            select new
                            {
                                IdProyecto = d.IdProyecto,
                                Proyecto = d.NombreProyecto,
                                Cliente = d.DimCliente.Nombre,
                                Descripcion = d.Descripcion,
                                Inicio = d.InicioProyecto.Value.ToString("dd-MM-yyyy"),
                                Fin = d.FinProyecto.Value.ToString("dd-MM-yyyy"),
                                Duracion = d.DuracionDias,
                                TotalHH = d.TotalHH,
                                Estado = d.IsActivo
                            };

                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ListaProyectos2()
        {
            try
            {
                var ListaProyectos2 = Dal.Clases.ProyectoDAL.ListaProyectos().ToList();

                var Datos = from d in ListaProyectos2
                            select new
                            {
                                IdProyecto = d.IdProyecto,
                                Proyecto = d.NombreProyecto,
                                Cliente = d.DimCliente.Nombre,
                                Descripcion = d.Descripcion,
                                Inicio = d.InicioProyecto.Value.ToString("dd-MM-yyyy"),
                                Fin = d.FinProyecto.Value.ToString("dd-MM-yyyy"),
                                Duracion = d.DuracionDias,
                                TotalHH = d.TotalHH,
                                Estado = d.IsActivo
                            };

                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GuardarProyecto()
        {
            try
            {
                var IdProyecto = int.Parse(Request.Form["IdProyecto"]);
                var NombreProyecto = Request.Form["Proyecto"];
                var IdCliente = int.Parse(Request.Form["Cliente"]);
                var Descripcion = Request.Form["Descripcion"];
                var InicioProyecto = DateTime.Parse(Request.Form["inicio"]);
                var FinProyecto = DateTime.Parse(Request.Form["fin"]);
                var DuracionDias = int.Parse(Request.Form["Duracion"]);
                var TotalHH = int.Parse(Request.Form["TotalHH"]);
                var IsActivo = int.Parse(Request.Form["Estado"]);

                var resultado = Dal.Clases.ProyectoDAL.GuardarProyecto(IdProyecto, NombreProyecto, IdCliente, Descripcion, InicioProyecto, FinProyecto, DuracionDias, TotalHH, IsActivo);

                if (resultado == 0)
                {
                    return Json(new { Cod = 0, Msg = "No fue posible guardar los datos del proyecto, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Cod = 1, Msg = "Los datos del proyecto " + NombreProyecto + " se han registrado exitosamente." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GuardarActividad()
        {
            try
            {
                var IdActividad = int.Parse(Request.Form["IdActividad"]);
                var IdProyecto = int.Parse(Request.Form["IdProyecto"]);
                var Descripcion = (Request.Form["Descripcion"]).Trim();
                var IsActivo = int.Parse(Request.Form["IsActivo"]);

                if (String.IsNullOrEmpty(Descripcion))
                {
                    return Json(new { Cod = 0, Msg = "Debe ingresar un ítem Gantt en el cuadro de texto." });
                }

                var actividades = Dal.Clases.ActividadDAL.ActividadesProyecto(IdProyecto);
                var cant = actividades.Where(i => i.IdTipoActividad != IdActividad).Where(a => a.Descripcion == Descripcion).Count();

                if (cant > 0)
                {
                    return Json(new { Cod = 0, Msg = "El ítem '" + Descripcion + "' ya se encuentra registrado en el sistema." });
                }

                var resultado = Dal.Clases.ActividadDAL.GuardarActividad(IdActividad, IdProyecto, Descripcion, IsActivo);

                if (resultado == 0)
                {
                    return Json(new { Cod = 0, Msg = "No fue posible guardar el ítem Gantt." });
                }
                else
                {
                    return Json(new { Cod = 1, Msg = "El ítem Gantt se ha registrado exitosamente." });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult ActividadesProyecto()
        {
            try
            {
                var IdProyecto = int.Parse(Request.Form["IdProyecto"]);

                Dao.Model.FactProyecto Proyecto = Dal.Clases.ProyectoDAL.DatosProyecto(IdProyecto);

                var proyecto = Proyecto.NombreProyecto;

                var Datos = Dal.Clases.ActividadDAL.ActividadesProyecto(IdProyecto);

                return Json(new { Cod = 1, Msg = "OK", proyecto, Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DatosActividad()
        {
            try
            {
                var idActividad = int.Parse(Request.Form["idActividad"]);

                var actividad = Dal.Clases.ActividadDAL.DatosActividad(idActividad);

                var Datos = new
                {
                    id = actividad.IdTipoActividad,
                    descripcion = actividad.Descripcion,
                    estado = actividad.IsActivo
                };

                return Json(new { Cod = 1, Msg = "OK", Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DatosProyecto()
        {
            var IdProyecto = int.Parse(Request.Form["IdProyecto"]);

            try
            {
                Dao.Model.FactProyecto Proyecto = Dal.Clases.ProyectoDAL.DatosProyecto(IdProyecto);

                var Datos = new
                {
                    IdProyecto = Proyecto.IdProyecto,
                    NombreProyecto = Proyecto.NombreProyecto,
                    Cliente = Proyecto.IdCliente,
                    Descripcion = Proyecto.Descripcion,
                    InicioProyecto = Proyecto.InicioProyecto.Value.ToString("dd-MM-yyyy"),
                    FinProyecto = Proyecto.FinProyecto.Value.ToString("dd-MM-yyyy"),
                    DuracionDias = Proyecto.DuracionDias,
                    TotalHH = Proyecto.TotalHH,
                    IsActivo = Proyecto.IsActivo
                };

                return Json(new { Cod = 1, Msg = "OK", Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ExisteProyecto()
        {
            try
            {
                var proy = Request.Form["Proyecto"];

                int Existe = Dal.Clases.ProyectoDAL.ProyectoExistente(proy);

                return Json(new { Cod = 1, Msg = "OK", Existe }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CargaMasiva(HttpPostedFileBase fExcel, int idProyectoM)
        {
            if (Session["usuario"] == null)
            {
                return Json(new { Cod = 0, Msg = "Debe iniciar sesión para usar este recurso." });
            }

            var file = Path.Combine(Server.MapPath("~/Uploads"), string.Format("{0:yyyyMMdd}_{1}", DateTime.Now, fExcel.FileName));
            if (System.IO.File.Exists(file)) System.IO.File.Delete(file);
            fExcel.SaveAs(file);
            FileStream stream = System.IO.File.Open(file, FileMode.Open, FileAccess.Read);

            IExcelDataReader excelReader = null;
            if (new FileInfo(file).Extension.Equals(".xlsx"))
                excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            else
                excelReader = ExcelReaderFactory.CreateBinaryReader(stream);

            var dt = excelReader.AsDataSet(new ExcelDataSetConfiguration
            {
                ConfigureDataTable = _ => new ExcelDataTableConfiguration
                {
                    UseHeaderRow = true
                }
            }).Tables[0];
            stream.Close();

            var proceso = Dal.Clases.ActividadDAL.ProcesoCargaMasiva(idProyectoM, dt);

            if (proceso != null)
            {
                var r = new
                {
                    CantidadRegistros = proceso[0],
                    RegistrosCargados = proceso[1],
                    RegistrosRechazados = proceso[2],
                    RegistrosLeidos = proceso[3],
                    DescripcionError = proceso[4]
                };

                return Json(new { Cod = 1, Msg = "Se ha procesado el archivo correctamente.", Resumen = r });
            }
            return Json(new { Cod = 0, Msg = "Ocurrió un problema en el proceso de carga masiva." });
        }

        [HttpPost]
        public JsonResult GeneraExcelBase()
        {
            try
            {
                SLDocument sl = new SLDocument();

                sl.SetCellValue("A1", "Lista items gantt");
                SLStyle style = sl.CreateStyle();
                style.SetFont(FontSchemeValues.Minor, 14);
                style.SetFontBold(true);
                style.Font.FontColor = System.Drawing.Color.Navy;
                sl.SetCellStyle("A1", style);

                SLComment comm;

                comm = sl.CreateComment();
                comm.Fill.SetAutomaticFill();
                comm.ShadowColor = System.Drawing.Color.Navy;
                comm.Width = 150;
                comm.Height = 110;
                comm.SetText("Reemplace cada ítem presente en la lista con los ítems Gantt correspondientes para su proyecto y agregue los que sean necesarios.");
                sl.InsertComment(1, 1, comm);

                sl.SetCellValue("A2", "Item 1");
                sl.SetCellValue("A3", "Item 2");
                sl.SetCellValue("A4", "Item 3");
                sl.SetCellValue("A5", "Item ...");

                sl.AutoFitColumn(1, 1);

                MemoryStream ms = new MemoryStream();

                sl.SaveAs(ms);
                ms.Position = 0;

                var Handler = Guid.NewGuid().ToString();
                TempData[Handler] = ms.GetBuffer();

                return Json(new { Cod = 1, Msg = "La descarga del archivo comenzará en breve.", Handler = Handler }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Cod = 0, Msg = e.Message });
            }
        }

        public virtual ActionResult DescargaArchivo(string handler)
        {
            if (TempData[handler] != null)
            {
                var data = TempData[handler] as byte[];
                return File(data, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Base Carga items gantt.xlsx");
            }
            else
            {
                return new EmptyResult();
            }

        }

        #endregion

        #region Proyectos Consultoria

        /// <summary>
        /// Carga datos en la vista proyectos
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ListaConsultoria()
        {
            try
            {
                var ListaProyectos = Dal.Clases.ProyectoDAL.ListaProyectos().Where(p => p.IdTipoProyecto == 2).Select(x => x).ToList();

                var Datos = from d in ListaProyectos
                            select new
                            {
                                IdProyecto = d.IdProyecto,
                                Proyecto = d.NombreProyecto,
                                Cliente = d.DimCliente.Nombre,
                                Rut = d.DimCliente.Rut,
                                NombreContacto = d.NombreContacto,
                                TelefonoContacto = d.TelefonoContacto,
                                CorreoContacto = d.CorreoContacto,
                                Descripcion = d.Descripcion,
                                PmCliente = d.PmCliente,
                                Inicio = d.InicioProyecto.Value.ToString("dd-MM-yyyy"),
                                Fin = d.FinProyecto.Value.ToString("dd-MM-yyyy"),
                                Estado = d.IsActivo,
                                OrdenDeCompra = d.OrdenDeCompra,
                                RangoEDP = d.RangoEDP,
                                Monto = Dal.Clases.Util.formatomoneda(d.Monto.Value) + " " + Dal.Clases.Util.tipomoneda(d.TipoMoneda.Value)

                            };

                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Metodo para pbtener el detalle de un proyecto de consultoria
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DatosConsultoria()
        {
            var IdProyecto = int.Parse(Request.Form["IdProyecto"]);

            try
            {
                Dao.Model.FactProyecto Proyecto = Dal.Clases.ProyectoDAL.DatosProyecto(IdProyecto);

                var Datos = new
                {
                    IdProyecto = Proyecto.IdProyecto,
                    Proyecto = Proyecto.NombreProyecto,
                    Cliente = Proyecto.IdCliente,
                    Rut = Proyecto.DimCliente.Rut,
                    NombreContacto = Proyecto.NombreContacto,
                    TelefonoContacto = Proyecto.TelefonoContacto,
                    CorreoContacto = Proyecto.CorreoContacto,
                    OrdenDeCompra = Proyecto.OrdenDeCompra,
                    TipoMoneda = Proyecto.TipoMoneda,
                    Monto = Proyecto.Monto,
                    Descripcion = Proyecto.Descripcion,
                    PmCliente = Proyecto.PmCliente,
                    Inicio = Proyecto.InicioProyecto.Value.ToString("dd-MM-yyyy"),
                    Fin = Proyecto.FinProyecto.Value.ToString("dd-MM-yyyy"),
                    RangoEDP = Proyecto.RangoEDP,
                    IsActivo = Proyecto.IsActivo
                };

                return Json(new { Cod = 1, Msg = "OK", Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult ListaProyectosConsultoria()
        {
            try
            {
                var usr = (Dao.Model.FactUsuario)Session["usuario"];

                var ListaProyectos = Dal.Clases.ProyectoDAL.ListaProyectos().Where(p => p.IdTipoProyecto == 2).Where(p => p.IsActivo == 1).Select(x => x).ToList();

                if (usr.IdPerfil == 5)
                {
                    ListaProyectos = ListaProyectos.Where(l => l.RelUsuarioProyecto.Any(a => a.IdUsuario == usr.IdUsuario)).Select(x => x).ToList();
                }

                var Datos = from d in ListaProyectos
                            select new
                            {
                                IdProyecto = d.IdProyecto,
                                Proyecto = d.NombreProyecto,
                            };

                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        //[HttpPost]
        //public JsonResult ListaTimesheetProyectosConsultoria()
        //{
        //    try
        //    {
        //        var ListaProyectos = Dal.Clases.ProyectoDAL.ListaProyectosConsultoria().Where(p => p.IdTipoProyecto == 1).Select(x => x).ToList();

        //        var Datos = from d in ListaProyectos
        //                    select new
        //                    {
        //                        IdProyecto = d.IdProyecto,
        //                        Proyecto = d.NombreProyecto,
        //                        Cliente = d.DimCliente.Nombre,
        //                        Descripcion = d.Descripcion,
        //                        Inicio = d.InicioProyecto.Value.ToString("dd-MM-yyyy"),
        //                        Fin = d.FinProyecto.Value.ToString("dd-MM-yyyy"),
        //                        Duracion = d.DuracionDias,
        //                        TotalHH = d.TotalHH,
        //                        Estado = d.IsActivo
        //                    };

        //        return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        [HttpPost]
        public JsonResult ListaEntregablesProyectosConsultoria()
        {
            try
            {
                var usr = (Dao.Model.FactUsuario)Session["usuario"];
                if (usr == null)
                {
                    return Json(new { Cod = 0, Msg = "Debe iniciar sesión para utilizar este recuro." });
                }

                string desde = Request.Form["desde"];
                string hasta = Request.Form["hasta"];
                int usuario = int.Parse(Request.Form["usuario"]);
                int proyecto = int.Parse(Request.Form["proyecto"]);

                var listadousuarios = new List<int>();
                var listadoproyectos = new List<int>();

                var fdesde = DateTime.ParseExact(desde, "dd-MM-yyyy", null);
                var fhasta = DateTime.ParseExact(hasta, "dd-MM-yyyy", null);


                if (usuario == 0)
                {
                    if (usr.IsAdmin == 1)
                    {
                        var ListadoU = Dal.Clases.UsuarioDAL.ListaUsuarios().ToList();
                        listadousuarios = ListadoU.Select(u => u.IdUsuario).Distinct().ToList();
                    }
                    else
                    {
                        var pjp = Dal.Clases.RelUsuariosDAL.ProyectosJP(usr.IdUsuario);
                        var ListadoU = Dal.Clases.RelUsuariosDAL.UsuariosProyectoDash(pjp.Select(p => p.IdProyecto).ToList());
                        listadousuarios = ListadoU.Select(u => u.IdUsuario).Distinct().ToList();
                    }
                }
                else
                {
                    listadousuarios.Add(usuario);
                }


                if (proyecto == 0)
                {
                    if (usr.IsAdmin == 0)
                    {
                        var ListadoP = Dal.Clases.ProyectoDAL.ListaProyectos().Where(p => p.IdTipoProyecto == 2).Where(p => p.IsActivo == 1).Select(x => x).ToList();
                        listadoproyectos = ListadoP.Select(p => p.IdProyecto).Distinct().ToList();
                    }
                    else
                    {
                        var ListadoP = Dal.Clases.ProyectoDAL.ListaProyectosPerfilConsultor(usuario).ToList();
                        listadoproyectos = ListadoP.Select(p => p.IdProyecto).Distinct().ToList();
                    }
                }
                else
                {
                    listadoproyectos.Add(proyecto);
                }

                //if (usuario == 0 && proyecto == 0)
                //{
                //    var usr = (Dao.Model.FactUsuario)Session["usuario"];
                //    int usuariologin = usr.IdUsuario;
                //    var pjp = Dal.Clases.RelUsuariosDAL.ProyectosJP(usuariologin);
                //    var Listado1 = Dal.Clases.RelUsuariosDAL.UsuariosProyectoDash(pjp.Select(p => p.IdProyecto).ToList());
                //    listadousuarios = (from x in Listado1 group x by x.IdUsuario into agru select new { IdUsuario = agru.Key }).Select(p => p.IdUsuario).ToList();

                //    var Listado2 = Dal.Clases.TimeSheetConsultorDAL.ListaProyectosTimesheetConsultor(usuariologin).ToList();
                //    listadoproyectos = Listado2.Select(p => p.IdProyecto).ToList();
                //}

                //if (usuario == 0 && proyecto != 0)
                //{
                //    var usr = (Dao.Model.FactUsuario)Session["usuario"];
                //    int usuariologin = usr.IdUsuario;
                //    var pjp = Dal.Clases.RelUsuariosDAL.ProyectosJP(usuariologin);
                //    var Listado1 = Dal.Clases.RelUsuariosDAL.UsuariosProyectoDash(pjp.Select(p => p.IdProyecto).ToList());
                //    listadousuarios = (from x in Listado1 group x by x.IdUsuario into agru select new { IdUsuario = agru.Key }).Select(p => p.IdUsuario).ToList();

                //    listadoproyectos.Add(proyecto);
                //}

                //if (usuario != 0 && proyecto == 0)
                //{
                //    var usr = (Dao.Model.FactUsuario)Session["usuario"];
                //    int usuariologin = usr.IdUsuario;
                //    var Listado1 = Dal.Clases.TimeSheetConsultorDAL.ListaProyectosTimesheetConsultor(usuariologin).ToList();
                //    listadoproyectos = Listado1.Select(p => p.IdProyecto).ToList();

                //    listadousuarios.Add(usuario);
                //}

                //if (usuario != 0 && proyecto != 0)
                //{
                //    listadousuarios.Add(usuario);
                //    listadoproyectos.Add(proyecto);
                //}

                var Listado = Dal.Clases.EntregableDAL.ListaEntregables(fdesde, fhasta, listadousuarios, listadoproyectos).ToList();
                var idu = (Dao.Model.FactUsuario)Session["usuario"];

                var Datos = from d in Listado
                            orderby d.IdProyecto, d.IdEntregable descending
                            select new
                            {
                                IdEntregable = d.IdEntregable,
                                usuario = d.FactUsuario.Nombre,
                                Proyecto = d.FactProyecto.NombreProyecto,
                                Entregable = d.Entregable,
                                Avance = d.Avance + "%",
                                Pendiente = d.Pendiente == 1 ? "NO" : "SI",
                                VersionFinal = d.VersionFinal == 1 ? "NO" : "SI",
                                Observaciones = d.Observaciones,
                                CodigoDocumento = d.CodigoDocumento
                            };


                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GuardarProyectoConsultor()
        {
            try
            {
                var IdProyecto = int.Parse(Request.Form["IdConsultoria"]);
                var NombreProyecto = Request.Form["ProyectoC"];
                var IdCliente = int.Parse(Request.Form["ClienteCon"]);
                var Descripcion = Request.Form["DescripcionC"];
                var NombreContacto = Request.Form["NombreContacto"];
                var TelefonoContacto = Request.Form["TelefonoContacto"];
                var CorreoContacto = Request.Form["CorreoContacto"];
                var PmCliente = Request.Form["PmCliente"];
                var OrdenDeCompra = Request.Form["PO"];
                var InicioProyecto = DateTime.Parse(Request.Form["InicioC"]);
                var FinProyecto = DateTime.Parse(Request.Form["FinC"]);
                var TipoMoneda = int.Parse(Request.Form["TipoMoneda"]);
                var Monto = int.Parse(Request.Form["Monto"]);
                var RangoEDP = Request.Form["RangoEDP"];
                var IsActivo = int.Parse(Request.Form["Estado"]);

                var resultado = Dal.Clases.ProyectoDAL.GuardarProyectoConsultor(IdProyecto, NombreProyecto, IdCliente, Descripcion, NombreContacto, TelefonoContacto, CorreoContacto, PmCliente, OrdenDeCompra, InicioProyecto, FinProyecto, TipoMoneda, Monto, RangoEDP, IsActivo);

                if (resultado == 0)
                {
                    return Json(new { Cod = 0, Msg = "No fue posible guardar los datos de la consultoria, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Cod = 1, Msg = "Los datos de la consultoria " + NombreProyecto + " se han registrado exitosamente." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ExisteProyectoConsultor()
        {
            try
            {
                var proy = Request.Form["ProyectoC"];

                int Existe = Dal.Clases.ProyectoDAL.ProyectoConsultorExistente(proy);

                return Json(new { Cod = 1, Msg = "OK", Existe }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
}
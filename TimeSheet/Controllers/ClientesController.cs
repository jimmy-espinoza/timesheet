﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TimeSheet.Controllers
{
    public class ClientesController : Controller
    {
        // GET: Clientes
        public ActionResult Index()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        public JsonResult ListaClientes()
        {
            try
            {
                var ListaClientes = Dal.Clases.ClienteDAL.ListaClientes().ToList();

                var Datos = from b in ListaClientes
                            select new
                            {
                                IdCliente = b.IdCliente,
                                Nombre = b.Nombre,
                                Rut = b.Rut
                            };

                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GuardarCliente()
        {
            try
            {
                var IdCliente = int.Parse(Request.Form["IdCliente"]);
                var Nombre = Request.Form["Nombre"];
                var Rut = Request.Form["Rut"];

                var resultado = Dal.Clases.ClienteDAL.GuardarCliente(IdCliente, Nombre, Rut);

                if (resultado == 0)
                {
                    return Json(new { Cod = 0, Msg = "No fue posible guardar los datos del cliente, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Cod = 1, Msg = "Los datos del cliente " + Nombre + " se han registrado exitosamente." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult DatosCliente()
        {
            try
            {
                var IdCliente = int.Parse(Request.Form["IdCliente"]);

                Dao.Model.DimCliente Cliente = Dal.Clases.ClienteDAL.DatosCliente(IdCliente);

                return Json(new { Cod = 1, Msg = "OK", Datos = Cliente }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ExisteCliente()
        {
            try
            {
                var rut = Request.Form["Rut"];

                int Existe = Dal.Clases.ClienteDAL.ClienteRut(rut);

                return Json(new { Cod = 1, Msg = "OK", Existe }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ExisteNombreCliente()
        {
            try
            {
                var NomClient = Request.Form["Nombre"];

                int Existe = Dal.Clases.ClienteDAL.ClienteExiste(NomClient);

                return Json(new { Cod = 1, Msg = "OK", Existe }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
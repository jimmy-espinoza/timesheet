﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TimeSheet.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index(string url = "")
        {
            ViewBag.returnUrl = url;
            return View();
        }

        public ActionResult Recuperar()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Login()
        {
            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";
            try
            {
                var usuario = Request.Form["usuario"];
                var password = Request.Form["password"];
                var returnUrl = Request.Form["returnUrl"];
                password = Dal.Clases.Util.EncriptarBase64(password);

                var usuarioDB = Dal.Clases.UsuarioDAL.LoginUsuario(usuario, password);
                if (usuarioDB != null)
                {
                    Session["usuario"] = usuarioDB;
                    var url = "";
                    if (usuarioDB.IdPerfil == 5) {
                        url = (returnUrl == "" ? baseUrl + "Timesheet/Consultor" : returnUrl);  
                    }
                    else {
                        url = (returnUrl == "" ? baseUrl + "Timesheet" : returnUrl);
                    }

                    return Json(new { Cod = 1, Msg = "Autorizado", Url = url }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Cod = 2, Msg = "Usuario y/o contraseña incorrectos." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 3, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult Logout()
        {
            Session["usuario"] = null;
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Test()
        {
            var usuarios = Dal.Clases.UsuarioDAL.ListaUsuarios();
            return View();
        }

        public JsonResult RecuperarContrasena()
        {
            var correo = Request.Form["Correo"];
            try
            {
                string html = RenderViewToString(ControllerContext, "~/Views/Email.cshtml", null, false);

                var usuarioDB = Dal.Clases.UsuarioDAL.UsuarioCorreo(correo);
                var ContrasenaDB = "";
                if (usuarioDB != null)
                {
                    ContrasenaDB = Dal.Clases.Util.DesencriptarBase64(usuarioDB.Clave);
                    //string html = "";
                    string Descripcion = "Hemos recibido su solicitud de recuperaci&oacute;n de contrase&ntilde;a, los datos de acceso a la plataforma son: ";
                    string Contenido = "Usuario: " + usuarioDB.NombreUsuario + "<br /> Contraseña: " + ContrasenaDB;

                    html = html.Replace("[NOMBRE]", usuarioDB.Nombre);
                    html = html.Replace("[DESCRIPCION]", Descripcion);
                    html = html.Replace("[CONTENIDO]", Contenido);

                    Dal.Clases.Util.EnviarMail(correo, "Recuperación Contraseña", html);

                    return Json(new { Cod = 1, Msg = "Se ha enviado su Contraseña al correo " + correo }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Cod = 2, Msg = "Su correo no se encuentra registrado." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 3, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public static string RenderViewToString(ControllerContext context, string viewPath, object model = null, bool partial = false)
        {
            // first find the ViewEngine for this view
            ViewEngineResult viewEngineResult = null;
            if (partial)
                viewEngineResult = ViewEngines.Engines.FindPartialView(context, viewPath);
            else
                viewEngineResult = ViewEngines.Engines.FindView(context, viewPath, null);

            if (viewEngineResult == null)
                throw new FileNotFoundException("View cannot be found.");

            // get the view and attach the model to view data
            var view = viewEngineResult.View;
            context.Controller.ViewData.Model = model;

            string result = null;

            using (var sw = new StringWriter())
            {
                var ctx = new ViewContext(context, view,
                                            context.Controller.ViewData,
                                            context.Controller.TempData,
                                            sw);
                view.Render(ctx, sw);
                result = sw.ToString();
            }

            return result;
        }
    }
}
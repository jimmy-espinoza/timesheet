﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TimeSheet.Controllers
{
    public class AsociacionController : Controller
    {
        // GET: Asociacion
        public ActionResult Index()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        public JsonResult ListaUsuarioProyecto()
        {
            try
            {
                int Proyecto = int.Parse(Request.Form["proy"]);
                int Proveedor = int.Parse(Request.Form["prov"]);
                var ListaUsuarioProyecto = Dal.Clases.RelUsuariosDAL.ListaUsuarioProyecto(Proyecto, Proveedor).ToList();
                var TotalProyecto = Dal.Clases.RelUsuariosDAL.AsignacionProyecto(Proyecto);

                var Datos = from d in ListaUsuarioProyecto
                            select new
                            {
                                IdUsuarioProyecto = d.IdUsuarioProyecto,
                                Usuario = d.FactUsuario.Nombre,
                                Proyecto = d.FactProyecto.NombreProyecto,
                                Porcentaje = d.PorcentajeAsignacion,
                                IsJp = d.IsJp,
                                Proveedor = d.FactUsuario.DimEmpresa.NombreEmpresa
                            };

                return Json(new { Cod = 1, Msg = "OK", Datos, TotalProyecto }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GuardarRelacion()
        {
            try
            {
                var IdUsuarioProyecto = int.Parse(Request.Form["IdUsuarioProyecto"]);
                var IdUsuario = int.Parse(Request.Form["IdUsuario"]);
                var IdProyecto = int.Parse(Request.Form["Proyecto2"]);
                var IsJp = int.Parse(Request.Form["IsJp"]);
                //var PorcentajeAsignacion = int.Parse(Request.Form["Porcentaje"]);

                //var inputPorcentajeAsignacion = Request.Form["Porcentaje"];
                //var replaceporcentajeasignacion = inputPorcentajeAsignacion.Replace('.', ',');
                //var PorcentajeAsignacion = float.Parse(replaceporcentajeasignacion);


                var inputPorcentajeAsignacion = double.Parse(Request.Form["Porcentaje"], CultureInfo.InvariantCulture);
                var PorcentajeAsignacion = double.Parse(inputPorcentajeAsignacion.ToString("0.00"));

                var resultado = Dal.Clases.RelUsuariosDAL.GuardarRelacion(IdUsuarioProyecto, IdUsuario, IdProyecto, PorcentajeAsignacion, IsJp);

                if (resultado == 0)
                {
                    return Json(new { Cod = 0, Msg = "No fue posible guardar los datos de la relación, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Cod = 1, Msg = "Usuario ha sido asignado exitosamente al proyecto." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DatosUsuarioProyecto()
        {
            try
            {
                var IdUsuarioProyecto = int.Parse(Request.Form["IdUsuarioProyecto"]);

                var Relacion = Dal.Clases.RelUsuariosDAL.DatosUsuarioProyecto(IdUsuarioProyecto);
                var TotalProyecto = Dal.Clases.RelUsuariosDAL.AsignacionProyecto(Relacion.IdProyecto);
                var Datos = new
                {
                    IdUsuarioProyecto = IdUsuarioProyecto,
                    IdUsuario = Relacion.IdUsuario,
                    IdProyecto = Relacion.IdProyecto,
                    PorcentajeAsignacion = Relacion.PorcentajeAsignacion,
                    IsJp = Relacion.IsJp,
                    NombreProyecto = Relacion.FactProyecto.NombreProyecto,
                    TotalProyecto
                };
                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult EliminarAsignacion()
        {
            try
            {
                var IdUsuarioProyecto = int.Parse(Request.Form["IdUsuarioProyecto"]);

                var resultado = Dal.Clases.RelUsuariosDAL.EliminarAsignacion(IdUsuarioProyecto);

                if (resultado == 0)
                {
                    return Json(new { Cod = 0, Msg = "No fue posible eliminar la asignación, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Cod = 1, Msg = "La asignación ha sido eliminada." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ListaUsuariosEnProyecto()
        {
            try
            {
                int Proyecto = int.Parse(Request.Form["proy"]);
                int Proveedor = int.Parse(Request.Form["prov"]);
                var ListaUsuariosEnProyecto = Dal.Clases.RelUsuariosDAL.ListaUsuariosEnProyecto(Proyecto, Proveedor).ToList();
                var TotalProyecto = Dal.Clases.RelUsuariosDAL.AsignacionProyecto(Proyecto);
                var datos = new List<Dao.Model.FactUsuario>();
                foreach (var item in ListaUsuariosEnProyecto.ToList())
                {
                    if (item.RelUsuarioProyecto.Where(x => x.IdProyecto == Proyecto).Where(y => y.IsActivo == 1).FirstOrDefault() == null)
                    {
                        datos.Add(item);
                    }

                }
                var Listado = from d in datos
                              select new
                              {
                                  IdUsuario = d.IdUsuario,
                                  NombreUsuario = d.NombreUsuario,
                                  Nombre = d.Nombre,
                                  Correo = d.Correo,
                                  Empresa = d.DimEmpresa.NombreEmpresa,
                                  Admin = d.IsAdmin.Value,
                                  Estado = d.IsActivo,
                                  IdEmpresa = d.IdEmpresa,
                                  TotalProyecto
                              };

                return Json(new { Cod = 1, Msg = "OK", Datos = Listado }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
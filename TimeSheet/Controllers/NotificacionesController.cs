﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TimeSheet.Controllers
{
    public class NotificacionesController : Controller
    {
        // GET: Notificaiones
        public ActionResult Index()
        {
            var user = (Dao.Model.FactUsuario)Session["usuario"];
            if (user == null)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.IsAdmin = user.IsAdmin;
            ViewBag.Nombre = user.Nombre;
            ViewBag.IdUsuario = user.IdUsuario;

            return View();
        }

        [HttpPost]
        public JsonResult ListadoNotificaciones()
        {
            try
            {
                int IdUsuario = int.Parse(Request.Form["IdUsuario"]);
                int Cantidad = int.Parse(Request.Form["Cantidad"]);

                List<Dao.Model.FactNotificacion> res = Dal.Clases.NotificacionDAL.ListadoNotificacionUsuario(IdUsuario, 0).ToList();

                var NL = res.Sum(s => (s.Visto == 1 ? 0 : 1));

                Cantidad = (Cantidad == 0 ? res.Count : Cantidad);

                var Datos = from x in res.Take(Cantidad)
                            let MDif = (DateTime.Now - x.FechaRegistro).Minutes
                            let HDif = (DateTime.Now - x.FechaRegistro).Hours
                            let DDif = (DateTime.Now - x.FechaRegistro).Days
                            select new
                            {
                                x.IdUsuario,
                                FechaAccionDMY = x.FechaAccion.ToString("dd-MM-yyyy"),
                                FechaRegistroDMY = x.FechaRegistro.ToString("dd-MM-yyyy"),
                                x.FechaRegistro,
                                HoraRegistro = x.FechaRegistro.ToString("HH:mm"),
                                x.Mensaje,
                                x.Titulo,
                                x.Importancia,
                                x.IdNotificacion,
                                x.Visto,
                                Hace = "Hace " + (DDif > 0 ? DDif + " días." : (HDif > 0 ? HDif + " horas." : (MDif > 0 ? MDif + " minutos." : "instantes.")))
                            };

                return Json(new { Cod = 1, Msg = "OK", Datos, NL }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DetalleNotificacion()
        {
            try
            {
                int IdNotificacion = int.Parse(Request.Form["IdNotificacion"]);

                var Notif = Dal.Clases.NotificacionDAL.DatosNotificacion(IdNotificacion);

                var MDif = (DateTime.Now - Notif.FechaRegistro).Minutes;
                var HDif = (DateTime.Now - Notif.FechaRegistro).Hours;
                var DDif = (DateTime.Now - Notif.FechaRegistro).Days;

                var Datos = new
                {
                    //x.IdUsuario,
                    //x.FactUsuario.Nombre,
                    FechaAccionDMY = Notif.FechaAccion.ToString("dd-MM-yyyy"),
                    FechaRegistroDMY = Notif.FechaRegistro.ToString("dd-MM-yyyy"),
                    FechaRegistroDH = Notif.FechaRegistro.ToString("dd-MM-yyyy HH:mm"),
                    Notif.FechaRegistro,
                    HoraRegistro = Notif.FechaRegistro.ToString("HH:mm:ss"),
                    Notif.Mensaje,
                    Notif.Titulo,
                    Notif.Importancia,
                    Notif.IdNotificacion,
                    Notif.Visto,
                    Hace = "Hace " + (DDif > 0 ? DDif + " días." : (HDif > 0 ? HDif + " horas." : (MDif > 0 ? MDif + " minutos." : "instantes.")))
                };

                Dal.Clases.NotificacionDAL.MarcarVisto(IdNotificacion, 1);

                return Json(new { Cod = 1, Msg = "OK", Datos }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult MarcarVisto()
        {
            try
            {
                int IdNotificacion = int.Parse(Request.Form["IdNotificacion"]);
                int Accion = int.Parse(Request.Form["Accion"]);
                var Resultado = Dal.Clases.NotificacionDAL.MarcarVisto(IdNotificacion, Accion);
                return Json(new { Cod = 1, Msg = "OK", Resultado }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult MarcarTodoVisto()
        {
            try
            {
                var usr = (Dao.Model.FactUsuario)Session["usuario"];
                if (usr == null)
                {
                    return Json(new { Cod = 2, Msg = "Debe iniciar sesión para utilizar este recurso." });
                }
                var idusuario = usr.IdUsuario;

                List<Dao.Model.FactNotificacion> res = Dal.Clases.NotificacionDAL.ListadoNotificacionUsuario(idusuario, 0).Where(n => n.Visto == 0).ToList();

                if (res.Count == 0)
                {
                    return Json(new { Cod = 2, Msg = "No se encontraron notificaciones sin leer." });
                }

                var cont = 0;
                foreach (var notificacion in res)
                {
                    var resultado = Dal.Clases.NotificacionDAL.MarcarVisto(notificacion.IdNotificacion, 1);
                    if (resultado == 1)
                    {
                        cont++;
                    }
                }

                var mensaje = "";
                switch (cont)
                {
                    case 0:
                        mensaje = "No se ha podido marcar las notificaciones como leídas.";
                        break;
                    case 1:
                        mensaje = "Se ha marcado 1 notificación como leída.";
                        break;
                    default:
                        mensaje = "Se han marcado " + cont + " notificaciones como leídas.";
                        break;
                }
                return Json(new { Cod = 1, Msg = mensaje }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        
        [HttpGet]
        public JsonResult Proceso(string id)
        {
            try
            {
                string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";
                DateTime fecha = DateTime.Today;

                if ((byte)fecha.DayOfWeek >= 1 && (byte)fecha.DayOfWeek <= 5)
                {
                    var Cfg = Dal.Clases.ConfiguracionDAL.DatosConfiguracion(1);
                    DateTime HoraEjecucion = DateTime.ParseExact(DateTime.Today.ToString("yyyy-MM-dd"), "yyyy-MM-dd", null);
                    string[] HE = Cfg.Valor.Split(':');
                    double sM, sH = 0;
                    sH = int.Parse(HE[0]);
                    sM = double.Parse(HE[1]) / 60;
                    HoraEjecucion = HoraEjecucion.AddHours(sH + sM);

                    if (DateTime.Now.Hour == 00)
                    {
                        ConfiguracionController.ObtenerUF();
                    }

                    if (DateTime.Now.Hour != HoraEjecucion.Hour)
                    {
                        //Dal.Clases.NotificacionDAL.RegistrarNotificacion(null, null, DateTime.Now, DateTime.Today, 2, "PRUEBA", "PRUEBA", 1, 0);
                        return Json(new { Status = "success", Mensaje = "Ejecución fuera de horario." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {

                        

                        var token = ConfigurationManager.AppSettings["TokenNotificacion"];
                        if (id != token)
                        {
                            return Json(new { Status = "error", Mensaje = "Token Invalido." }, JsonRequestBehavior.AllowGet);
                        }
                        string html = RenderViewToString(ControllerContext, "~/Views/Email.cshtml", null, false);

                        List<Dao.Model.RelUsuarioProyecto> SinRegistro = Dal.Clases.RelUsuariosDAL.SinTSUsuario(fecha);

                        /*
                        var Datos = from x in SinRegistro
                                    let PorAsig = double.Parse(x.PorcentajeAsignacion.ToString()) / 100
                                    let HHProy = double.Parse(x.FactProyecto.TotalHH.ToString())
                                    select new
                                    {
                                        x.FactProyecto.NombreProyecto,
                                        x.FactUsuario.Nombre,
                                        HHAsig = (HHProy * PorAsig),
                                        HHReg = Math.Round(double.Parse(x.FactTimeSheet.Sum(s => s.TotalHoras).ToString()), 0)
                                    };
                        */

                        //Sin Timesheet Usuario
                        foreach (var r in SinRegistro)
                        {
                            double PorAsig = double.Parse(r.PorcentajeAsignacion.ToString());
                            double HHProy = double.Parse(r.FactProyecto.TotalHH.ToString());
                            double HHAsig = (HHProy * PorAsig);
                            double HHReg = Math.Round(double.Parse(r.FactTimeSheet.Sum(s => s.TotalHoras).ToString()), 0);
                            //Validación horas Registradas v/s Asignadas
                            if (HHReg < HHAsig)
                            {
                                string htmlN = html;
                                string TitNot = "Registro Actividades";
                                string MsgNot = "No has registrado tus actividades del día '" + fecha.ToString("dd-MM-yyyy") + "', asociadas al proyecto: <b>" + r.FactProyecto.NombreProyecto + "</b>";

                                //Registro Notificación
                                Dal.Clases.NotificacionDAL.RegistrarNotificacion(r.IdProyecto, r.IdUsuario, DateTime.Now, DateTime.Today, r.IdUsuario, TitNot, MsgNot, 1, 0);

                                MsgNot += "<br><br> Para registrar actividades lo puedes hacer en el siguiente link: <a href='" + baseUrl + "Timesheet/Registro'>Timesheet</a>";

                                htmlN = htmlN.Replace("[NOMBRE]", r.FactUsuario.Nombre);
                                htmlN = htmlN.Replace("[DESCRIPCION]", TitNot);
                                htmlN = htmlN.Replace("[CONTENIDO]", MsgNot);
                                //Envio Correo
                                var send = Dal.Clases.Util.EnviarMail(r.FactUsuario.Correo, "Notificación Plataforma TimeSheet", htmlN);
                                if (send != "")
                                {
                                    Dal.Clases.NotificacionDAL.RegistrarNotificacion(null, r.IdUsuario, DateTime.Now, DateTime.Today, r.IdUsuario, "Envío Notificación", send, 3, 0);
                                }
                            }
                        }

                        //Sin Timesheet JP
                        var SinRegistroJp = from x in SinRegistro
                                            where x.IsJp == 1
                                            group x by x.IdUsuarioProyecto into gp
                                            select new
                                            {
                                                Pro = gp.FirstOrDefault().IdProyecto,
                                                NomPro = gp.FirstOrDefault().FactProyecto.NombreProyecto,
                                                IdUser = gp.FirstOrDefault().IdUsuario,
                                                NomJp = gp.FirstOrDefault().FactUsuario.Nombre,
                                                EmaJp = gp.FirstOrDefault().FactUsuario.Correo,
                                                Team = SinRegistro.Where(p => p.IdProyecto == gp.FirstOrDefault().IdProyecto).Where(j => j.IdUsuario != gp.FirstOrDefault().IdUsuario)
                                            };

                        foreach (var r in SinRegistroJp.Where(t => t.Team.Count() > 0).ToList())
                        {
                            string htmlN = html;
                            string TitNot = "Actividades Equipo";
                            string MsgNot = "Los siguientes usuarios no han registrado actividades para el día '" + fecha.ToString("dd-MM-yyyy") + "', asociados al proyecto: <b>" + r.NomPro + "</b>.";

                            MsgNot += "<br><br><ul>";
                            foreach (var u in r.Team)
                            {
                                double PorAsig = double.Parse(u.PorcentajeAsignacion.ToString());
                                double HHProy = double.Parse(u.FactProyecto.TotalHH.ToString());
                                double HHAsig = (HHProy * PorAsig);
                                double HHReg = Math.Round(double.Parse(u.FactTimeSheet.Sum(s => s.TotalHoras).ToString()), 0);

                                if (HHReg < HHAsig)
                                {
                                    MsgNot += "<li>" + u.FactUsuario.Nombre + " (" + u.FactUsuario.DimEmpresa.NombreEmpresa + ") </li>";
                                }
                            }
                            MsgNot += "</ul>";

                            //Registro Notificación
                            Dal.Clases.NotificacionDAL.RegistrarNotificacion(r.Pro, null, DateTime.Now, DateTime.Today, r.IdUser, TitNot, MsgNot, 1, 0);

                            MsgNot += "<br><br> Puede revisar el detalle de sus notificaciones en el siguiente link: <a href='" + baseUrl + "/Notificaciones'>Timesheet</a>";

                            htmlN = htmlN.Replace("[NOMBRE]", r.NomJp);
                            htmlN = htmlN.Replace("[DESCRIPCION]", TitNot);
                            htmlN = htmlN.Replace("[CONTENIDO]", MsgNot);
                            //Envio Correo
                            var send = Dal.Clases.Util.EnviarMail(r.EmaJp, "Notificación Plataforma TimeSheet", htmlN);
                            if (send != "")
                            {
                                Dal.Clases.NotificacionDAL.RegistrarNotificacion(null, r.IdUser, DateTime.Now, DateTime.Today, r.IdUser, "Envío Notificación", send, 3, 0);
                            }
                        }
                    }
                }
                return Json(new { Status = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Status = "error", Mensaje = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public static string RenderViewToString(ControllerContext context, string viewPath, object model = null, bool partial = false)
        {
            // first find the ViewEngine for this view
            ViewEngineResult viewEngineResult = null;
            if (partial)
                viewEngineResult = ViewEngines.Engines.FindPartialView(context, viewPath);
            else
                viewEngineResult = ViewEngines.Engines.FindView(context, viewPath, null);

            if (viewEngineResult == null)
                throw new FileNotFoundException("View cannot be found.");

            // get the view and attach the model to view data
            var view = viewEngineResult.View;
            context.Controller.ViewData.Model = model;

            string result = null;

            using (var sw = new StringWriter())
            {
                var ctx = new ViewContext(context, view,
                                            context.Controller.ViewData,
                                            context.Controller.TempData,
                                            sw);
                view.Render(ctx, sw);
                result = sw.ToString();
            }

            return result;
        }

        public JsonResult TestMail()
        {
            try
            {
                string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";
                string htmlN = RenderViewToString(ControllerContext, "~/Views/Email.cshtml", null, false);
                string Link = "Puede revisar el detalle de sus notificaciones en el siguiente link: <a href='" + baseUrl + "/Home/Index/Notificaciones'>Timesheet</a>";


                string Nombre = "NOMBRE PRUEBA";
                string Descripcion = "DESCRIPCIÓN DE PRUEBA";
                string Contenido = "CONTENIDO DE PRUEBA";

                string correo = "jimmy.espinoza@vmica.com";

                htmlN = htmlN.Replace("[NOMBRE]", Nombre);
                htmlN = htmlN.Replace("[DESCRIPCION]", Descripcion);
                htmlN = htmlN.Replace("[CONTENIDO]", Contenido);
                //Envio Correo
                var send = Dal.Clases.Util.EnviarMail(correo, "Notificación Plataforma TimeSheet", htmlN);


                return Json(new { Status = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Status = "error", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }


        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TimeSheet.Controllers
{
    public class TimesheetController : Controller
    {
        // GET: Timesheet
        public ActionResult Index()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

            var usr = (Dao.Model.FactUsuario)Session["usuario"];
            ViewBag.Admin = usr.IsAdmin;
            ViewBag.Nombre = usr.Nombre;
            ViewBag.IdUsuario = usr.IdUsuario;
            ViewBag.Perfil = usr.IdPerfil;
            if (usr.IdPerfil == 5)
            {
                return RedirectToAction("Consultor");
            }


            var pjp = Dal.Clases.RelUsuariosDAL.ProyectosJP(usr.IdUsuario);
            ViewBag.JefePro = (pjp.Count() > 0 ? 1 : 0);

            /*
            DateTime fc = new DateTime(2020, 6, 30);

            var s = Dal.Clases.Util.SemanaLunDom(fc);

            int thisWeekNumber = Dal.Clases.Util.GetIso8601WeekOfYear(fc);
            */

            return View();
        }

        public ActionResult Consultor()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

            var usr = (Dao.Model.FactUsuario)Session["usuario"];
            ViewBag.Admin = usr.IsAdmin;
            ViewBag.Nombre = usr.Nombre;
            ViewBag.IdUsuario = usr.IdUsuario;

            var pjp = Dal.Clases.RelUsuariosDAL.ProyectosJP(usr.IdUsuario);
            ViewBag.JefePro = (pjp.Count() > 0 ? 1 : 0);

            /*
            DateTime fc = new DateTime(2020, 6, 30);

            var s = Dal.Clases.Util.SemanaLunDom(fc);

            int thisWeekNumber = Dal.Clases.Util.GetIso8601WeekOfYear(fc);
            */

            return View();
        }

        [HttpPost]
        public JsonResult ListadoTimesheet()
        {
            try
            {
                string desde = Request.Form["desde"];
                string hasta = Request.Form["hasta"];
                int usuario = int.Parse(Request.Form["usuario"]);
                int proyecto = int.Parse(Request.Form["proyecto"]);

                var listadousuarios = new List<int>();
                var listadoproyectos = new List<int>();

                var fdesde = DateTime.ParseExact(desde, "dd-MM-yyyy", null);
                var fhasta = DateTime.ParseExact(hasta, "dd-MM-yyyy", null);

                if (usuario == 0 && proyecto == 0)
                {
                    var usr = (Dao.Model.FactUsuario)Session["usuario"];
                    int usuariologin = usr.IdUsuario;
                    var pjp = Dal.Clases.RelUsuariosDAL.ProyectosJP(usuariologin);
                    var Listado1 = Dal.Clases.RelUsuariosDAL.UsuariosProyectoDash(pjp.Select(p => p.IdProyecto).ToList());
                    listadousuarios = (from x in Listado1 group x by x.IdUsuario into agru select new { IdUsuario = agru.Key }).Select(p => p.IdUsuario).ToList();

                    var Listado2 = Dal.Clases.TimeSheetDAL.ListaProyectosTimesheet(usuariologin).ToList();
                    listadoproyectos = Listado2.Select(p => p.IdProyecto).ToList();
                }

                if (usuario == 0 && proyecto != 0)
                {
                    var usr = (Dao.Model.FactUsuario)Session["usuario"];
                    int usuariologin = usr.IdUsuario;
                    var pjp = Dal.Clases.RelUsuariosDAL.ProyectosJP(usuariologin);
                    var Listado1 = Dal.Clases.RelUsuariosDAL.UsuariosProyectoDash(pjp.Select(p => p.IdProyecto).ToList());
                    listadousuarios = (from x in Listado1 group x by x.IdUsuario into agru select new { IdUsuario = agru.Key }).Select(p => p.IdUsuario).ToList();

                    listadoproyectos.Add(proyecto);
                }

                if (usuario != 0 && proyecto == 0)
                {
                    var usr = (Dao.Model.FactUsuario)Session["usuario"];
                    int usuariologin = usr.IdUsuario;
                    var Listado1 = Dal.Clases.TimeSheetDAL.ListaProyectosTimesheet(usuariologin).ToList();
                    listadoproyectos = Listado1.Select(p => p.IdProyecto).ToList();

                    listadousuarios.Add(usuario);
                }

                if (usuario != 0 && proyecto != 0)
                {
                    listadousuarios.Add(usuario);
                    listadoproyectos.Add(proyecto);
                }

                var Listado = Dal.Clases.TimeSheetDAL.ListadoTimesheet(fdesde, fhasta, listadousuarios, listadoproyectos).ToList();
                var idu = (Dao.Model.FactUsuario)Session["usuario"];

                var Datos = from x in Listado
                            orderby x.IdUsuarioProyecto, x.Fecha descending, x.HoraInicio ascending
                            select new
                            {
                                IdTimesheet = (idu.IdUsuario == x.RelUsuarioProyecto.IdUsuario ? x.IdRegistro : 0),
                                Usuario = x.RelUsuarioProyecto.FactUsuario.Nombre,
                                Fecha = x.Fecha.Value.ToString("dd-MM-yyyy"),
                                HI = x.HoraInicio,
                                HF = x.HoraFin,
                                //HT = Math.Round(x.TotalHoras.Value, 1),
                                HT = Dal.Clases.Util.DecimalToHrMin(x.TotalHoras.Value),
                                Actividad = x.Actividad,
                                Proyecto = x.RelUsuarioProyecto.FactProyecto.NombreProyecto,
                                Costo = x.DimTipoCosto.Descripcion,
                                Aprobado = x.IsAprobado,
                                Itemgantt = x.FactTipoActividad == null ? "Otro item" : x.FactTipoActividad.Descripcion
                            };

                if (Datos.ToList().Count == 0)
                {
                    return Json(new { Cod = 2, Msg = "No se han encontrado registros de TimeSheet en la búsqueda." }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Registro()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

            var usr = (Dao.Model.FactUsuario)Session["usuario"];
            ViewBag.Admin = usr.IsAdmin;
            ViewBag.Nombre = usr.Nombre;
            ViewBag.IdUsuario = usr.IdUsuario;
            //ViewBag.AplicaCosto = usr.DimEmpresa.AplicaCosto;
            ViewBag.IdUsuario = usr.IdUsuario;
            ViewBag.AplicaCosto = Dal.Clases.EmpresaDAL.DatosEmpresa(usr.IdEmpresa).AplicaCosto;

            var dias = Dal.Clases.ConfiguracionDAL.DatosConfiguracion(3);
            var FechaMaxima = DateTime.Today.AddDays(int.Parse(dias.Valor) * -1);
            ViewBag.fechaMaxinatxt = FechaMaxima.ToString("dd-MM-yyyy");

            return View();
        }

        public ActionResult RegistroConsultoria()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

            var usr = (Dao.Model.FactUsuario)Session["usuario"];
            ViewBag.Admin = usr.IsAdmin;
            ViewBag.Nombre = usr.Nombre;
            ViewBag.IdUsuario = usr.IdUsuario;
            //ViewBag.AplicaCosto = usr.DimEmpresa.AplicaCosto;
            ViewBag.IdUsuario = usr.IdUsuario;
            ViewBag.AplicaCosto = Dal.Clases.EmpresaDAL.DatosEmpresa(usr.IdEmpresa).AplicaCosto;

            var dias = Dal.Clases.ConfiguracionDAL.DatosConfiguracion(3);
            var FechaMaxima = DateTime.Today.AddDays(int.Parse(dias.Valor) * -1);
            ViewBag.fechaMaxinatxt = FechaMaxima.ToString("dd-MM-yyyy");

            return View();
        }

        public ActionResult RegistroEntregable()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

            var usr = (Dao.Model.FactUsuario)Session["usuario"];
            ViewBag.Admin = usr.IsAdmin;
            ViewBag.Nombre = usr.Nombre;
            ViewBag.IdUsuario = usr.IdUsuario;
            //ViewBag.AplicaCosto = usr.DimEmpresa.AplicaCosto;
            ViewBag.IdUsuario = usr.IdUsuario;
            ViewBag.AplicaCosto = Dal.Clases.EmpresaDAL.DatosEmpresa(usr.IdEmpresa).AplicaCosto;

            var dias = Dal.Clases.ConfiguracionDAL.DatosConfiguracion(3);
            var FechaMaxima = DateTime.Today.AddDays(int.Parse(dias.Valor) * -1);
            ViewBag.fechaMaxinatxt = FechaMaxima.ToString("dd-MM-yyyy");

            return View();
        }

        [HttpPost]
        public JsonResult ListadoProyectosRegistro()
        {
            try
            {
                int usuario = int.Parse(Request.Form["usuario"]);
                var Listado = Dal.Clases.TimeSheetDAL.ListaProyectosTimesheet(usuario).ToList();

                var Datos = from x in Listado
                            select new
                            {
                                IdRelacion = x.IdUsuarioProyecto,
                                IdProyecto = x.FactProyecto.IdProyecto,
                                Proyecto = x.FactProyecto.NombreProyecto
                            };

                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ListadoProyectosPerfil()
        {
            try
            {
                int usuario = int.Parse(Request.Form["usuario"]);
                var Listado = Dal.Clases.ProyectoDAL.ListaProyectosPerfil(usuario).ToList();

                var Datos = from x in Listado
                            select new
                            {
                                IdProyecto = x.IdProyecto,
                                Proyecto = x.FactProyecto.NombreProyecto
                            };

                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ListadoProyectosPerfilConsultor()
        {
            try
            {
                int usuario = int.Parse(Request.Form["usuario"]);
                var Listado = Dal.Clases.ProyectoDAL.ListaProyectosPerfilConsultor(usuario).ToList();

                var Datos = from x in Listado
                            select new
                            {
                                IdProyecto = x.IdProyecto,
                                Proyecto = x.FactProyecto.NombreProyecto
                            };

                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UsuariosProyectoDash()
        {
            try
            {
                int IdJP = int.Parse(Request.Form["IdJP"]);
                var pjp = Dal.Clases.RelUsuariosDAL.ProyectosJP(IdJP);

                var Listado = Dal.Clases.RelUsuariosDAL.UsuariosProyectoDash(pjp.Select(p => p.IdProyecto).ToList());

                var Datos = from x in Listado
                            group x by x.IdUsuario into agru
                            select new
                            {
                                IdUsuario = agru.Key,
                                Nombre = agru.FirstOrDefault().Nombre
                            };


                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CargaListaActividades()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(Request.Form["project"]))
                {
                    return Json(new { Cod = 2, Msg = "Debe seleccionar un proyecto." });
                }

                int usuarioproyecto = int.Parse(Request.Form["project"]);


                var proyecto = Dal.Clases.RelUsuariosDAL.DatosUsuarioProyecto(usuarioproyecto);

                var Listado = Dal.Clases.ActividadDAL.ActividadesProyecto(proyecto.IdProyecto);

                Listado = Listado.Where(l => l.IsActivo == 1).Select(l => l).ToList();

                if (Listado.Count == 0)
                {
                    return Json(new { Cod = 2, Msg = "No se encontraron ítems para el proyecto seleccionado." });
                }

                var Datos = (from x in Listado
                             where x.IsActivo == 1
                             orderby x.Descripcion
                             select new
                             {
                                 id = x.IdTipoActividad,
                                 descripcion = x.Descripcion
                             }).ToList();


                return Json(new { Cod = 1, Msg = "OK", Datos });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GuardarTimesheet(FormCollection Campos)
        {
            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";
            string html = RenderViewToString(ControllerContext, "~/Views/Email.cshtml", null, false);

            try
            {
                var usu = (Dao.Model.FactUsuario)Session["usuario"];
                var registros = 0;

                int IdUsuario = int.Parse(Campos["IdUsuario"]);

                if (IdUsuario != usu.IdUsuario)
                {
                    return Json(new { Cod = 3, Msg = "No se registraron los datos, id usuario inválido." });
                }

                string[] Fechas = Request.Form.GetValues("Fecha");


                //var cant = int.Parse(Campos["cantidadregistros"]);

                //string[] Proyectos = new string[cant];
                //for (int i = 0; i < cant; i++)
                //{
                //    string[] proyecto = Request.Form.GetValues("Proyecto" + i);
                //    Proyectos[i] = proyecto[0];
                //}

                //string[] Tipos = new string[cant];
                //for (int i = 0; i < cant; i++)
                //{
                //    string[] Tipo = Request.Form.GetValues("TipoAct" + i);
                //    Tipos[i] = Tipo[0] == "0" ? null : Tipo[0];
                //}

                string[] Proyectos = Request.Form.GetValues("Proyectos");

                string[] Tipos = Request.Form.GetValues("TipoAct");

                string[] Actividades = Request.Form.GetValues("Actividad");

                string[] Costos = Request.Form.GetValues("Costo");
                string[] Inicios = Request.Form.GetValues("Inicio");
                string[] Terminos = Request.Form.GetValues("Termino");

                /* Registro datos 1 x 1 */
                /*
                for (var c = 0; c < Fechas.Count(); c++)
                {
                    var FE = DateTime.ParseExact(Fechas[c], "dd-MM-yyyy", null);
                    registros += Dal.Clases.TimeSheetDAL.GuardarTimesheet(0, FE, Actividades[c], int.Parse(Proyectos[c]), int.Parse(Costos[c]), Inicios[c], Terminos[c]);
                }
                */

                var dias = Dal.Clases.ConfiguracionDAL.DatosConfiguracion(3);
                var FechaMaxima = DateTime.Today.AddDays(int.Parse(dias.Valor) * -1);

                /* Registro datos por bloque */

                List<Dao.Model.FactTimeSheet> Registros = new List<Dao.Model.FactTimeSheet>();

                for (var c = 0; c < Fechas.Count(); c++)
                {
                    var FE = DateTime.ParseExact(Fechas[c], "dd-MM-yyyy", null);
                    var proy = Dal.Clases.RelUsuariosDAL.DatosUsuarioProyecto(int.Parse(Proyectos[c]));

                    if (FE < FechaMaxima)
                    {
                        var existe = Dal.Clases.AprobacionTimesheetDAL.ExisteAtraso(int.Parse(Proyectos[c]));
                        //var men = "La fecha de la actividad <b>'" + Actividades[c] + "'<br></b> para el día <b>'" + Fechas[c] + "'</b>, no puede ser registrada ya que supera los días limite para registrar actividades.";
                        var men = "No es posible registrar la actividad <b>" + Actividades[c] + "</b> para el día " + Fechas[c] + " ya que no fue ingresada en el periodo correspondiente.";

                        if (existe != null)
                        {
                            if (existe.EstadoSolicitud == 0)
                            {
                                men += "<hr> Existe una solicitud de excepción de carga en espera de aprobación, una vez aprobada se habilitará la excepción para cargar el registro.";
                                return Json(new { Cod = 3, Msg = men }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                if (DateTime.Today > existe.FechaValidez)
                                {
                                    var re = Dal.Clases.AprobacionTimesheetDAL.RegistrarAtraso(int.Parse(Proyectos[c]));
                                    if (re == 1)
                                    {
                                        string TitNot = "Límite días Timesheet";
                                        string MsgNot = "Se ha registrado una solicitud de excepción al límite de días para carga de actividades del usuario <b>" + usu.Nombre + "</b>, asociada al proyecto <b>" + proy.FactProyecto.NombreProyecto + "</b>.";
                                        //Registro Notificación
                                        var djp = Dal.Clases.ProyectoDAL.DatosProyectoEP(proy.IdProyecto);

                                        foreach (var j in djp.RelUsuarioProyecto.Where(i => i.IsJp == 1).Where(a => a.IsActivo == 1).ToList())
                                        {
                                            Dal.Clases.NotificacionDAL.RegistrarNotificacion(proy.IdProyecto, proy.IdUsuario, DateTime.Now, DateTime.Today, j.IdUsuario, TitNot, MsgNot, 1, 0);
                                        }
                                        men += "<hr> Se ha generado una solicitud de carga fuera de plazo al Jefe de Proyecto, una vez aprobada se habilitará la excepción para cargar el registro.";
                                    }
                                    else
                                    {
                                        men += "<hr> Ocurrió un error al generar solicitud de excepción, intente nuevamente.";
                                    }
                                    return Json(new { Cod = 3, Msg = men }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    //Si la fecha validez es menor, continua ejecución de forma normal
                                }
                            }
                        }
                        else
                        {
                            var re = Dal.Clases.AprobacionTimesheetDAL.RegistrarAtraso(int.Parse(Proyectos[c]));
                            if (re == 1)
                            {
                                string TitNot = "Límite días Timesheet";
                                string MsgNot = "Se ha registrado una solicitud de excepción al límite de días para carga de actividades del usuario <b>" + usu.Nombre + "</b>, asociada al proyecto <b>" + proy.FactProyecto.NombreProyecto + "</b>.";
                                //Registro Notificación
                                var djp = Dal.Clases.ProyectoDAL.DatosProyectoEP(proy.IdProyecto);

                                foreach (var j in djp.RelUsuarioProyecto.Where(i => i.IsJp == 1).Where(a => a.IsActivo == 1).ToList())
                                {
                                    Dal.Clases.NotificacionDAL.RegistrarNotificacion(proy.IdProyecto, proy.IdUsuario, DateTime.Now, DateTime.Today, j.IdUsuario, TitNot, MsgNot, 1, 0);
                                }
                                men += "<hr> Se ha generado una solicitud de carga fuera de plazo al Jefe de Proyecto, una vez aprobada se habilitará la excepción para cargar el registro.";
                            }
                            else
                            {
                                men += "<hr> Ocurrió un error al generar solicitud de excepción, intente nuevamente.";
                            }
                            return Json(new { Cod = 3, Msg = men }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    DateTime fi = DateTime.ParseExact(FE.ToString("yyyy-MM-dd") + " " + Inicios[c] + ":00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime ft = DateTime.ParseExact(FE.ToString("yyyy-MM-dd") + " " + Terminos[c] + ":00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                    //if (fi <= proy.FactProyecto.InicioProyecto)
                    //{
                    //    return Json(new { Cod = 3, Msg = "La fecha de la actividad <b>'" + Actividades[c] + "'</b>, es anterior a la fecha de inicio del proyecto." }, JsonRequestBehavior.AllowGet);
                    //}

                    //if (fi >= proy.FactProyecto.FinProyecto)
                    //{
                    //    return Json(new { Cod = 3, Msg = "La fecha de la actividad <b>'" + Actividades[c] + "'</b>, es posterior a la fecha de termino del proyecto." }, JsonRequestBehavior.AllowGet);
                    //}
                    if (FE < proy.FactProyecto.InicioProyecto)
                    {
                        return Json(new { Cod = 3, Msg = "La fecha de la actividad <b>'" + Actividades[c] + "'</b>, es anterior a la fecha de inicio del proyecto." }, JsonRequestBehavior.AllowGet);
                    }

                    if (FE > proy.FactProyecto.FinProyecto)
                    {
                        return Json(new { Cod = 3, Msg = "La fecha de la actividad <b>'" + Actividades[c] + "'</b>, es posterior a la fecha de termino del proyecto." }, JsonRequestBehavior.AllowGet);
                    }


                    var ProAsig = Dal.Clases.RelUsuariosDAL.ProyectosAsignados(IdUsuario);

                    var horas = Dal.Clases.TimeSheetDAL.HorariosRegistrados(ProAsig, FE).OrderBy(o => o.HoraInicio);

                    foreach (var x in horas)
                    {
                        DateTime fiCom = DateTime.ParseExact(x.Fecha.Value.ToString("yyyy-MM-dd") + " " + x.HoraInicio + ":00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        DateTime ffCom = DateTime.ParseExact(x.Fecha.Value.ToString("yyyy-MM-dd") + " " + x.HoraFin + ":00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                        if (fi >= fiCom && fi < ffCom)
                        {
                            return Json(new { Cod = 3, Msg = "La hora de inicio de la actividad <b>'" + Actividades[c] + "'<br></b> para el día <b>'" + Fechas[c] + "'</b>, provoca conflicto con otra actividad registrada." }, JsonRequestBehavior.AllowGet);
                        }

                        if (ft > fiCom && ft <= ffCom)
                        {
                            return Json(new { Cod = 3, Msg = "La hora de término de la actividad <b>'" + Actividades[c] + "'<br></b> para el día <b>'" + Fechas[c] + "'</b>, provoca conflicto con otra actividad registrada." }, JsonRequestBehavior.AllowGet);
                        }

                        if (ft > ffCom && fi < ffCom)
                        {
                            return Json(new { Cod = 3, Msg = "La hora de término de la actividad <b>'" + Actividades[c] + "'<br></b> para el día <b>'" + Fechas[c] + "'</b>, provoca conflicto con otra actividad registrada." }, JsonRequestBehavior.AllowGet);
                        }

                    }

                    var duracion = (ft - fi).TotalHours;

                    Dao.Model.FactTimeSheet NuevaTS = new Dao.Model.FactTimeSheet();
                    NuevaTS.Fecha = FE;
                    NuevaTS.Semana = Dal.Clases.Util.GetIso8601WeekOfYear(FE);
                    NuevaTS.Actividad = Actividades[c];
                    NuevaTS.IdUsuarioProyecto = int.Parse(Proyectos[c]);
                    NuevaTS.IdTipoCosto = int.Parse(Costos[c]);
                    NuevaTS.HoraInicio = Inicios[c];
                    NuevaTS.HoraFin = Terminos[c];
                    NuevaTS.TotalHoras = duracion;
                    NuevaTS.IsActivo = 1;
                    NuevaTS.FechaCarga = DateTime.Now;

                    if (Tipos[c] == null || Tipos[c] == "0")
                    {
                        NuevaTS.IdTipoActividad = null;
                    }
                    else
                    {
                        NuevaTS.IdTipoActividad = int.Parse(Tipos[c]);
                    }

                    /*Deshabilitado 28-08-2020
                    //Aprobar actividad automaticamente si es JP
                    if (proy.IsJp == 1)
                    {
                        NuevaTS.IsAprobado = 1;
                        NuevaTS.FechaAprobacion = DateTime.Now;
                        NuevaTS.IdUsuarioAprobacion = IdUsuario;
                    }
                    else
                    {
                        NuevaTS.IsAprobado = 0;
                    }
                    */
                    NuevaTS.IsAprobado = 0;
                    Registros.Add(NuevaTS);
                }

                registros = Dal.Clases.TimeSheetDAL.GuardarTimesheet(Registros);


                // Inicio Validación 45HH semanales
                List<int> FSemanas = Registros.Select(f => f.Semana.Value).Distinct().ToList();
                List<Dao.Model.FactTimeSheet> TSSemana = Dal.Clases.TimeSheetDAL.HorasRegistradasSemana(IdUsuario, FSemanas).ToList();

                var consulta = from x in TSSemana
                               group x by x.Semana into gs
                               select new
                               {
                                   Semana = gs.Key,
                                   Proyecto = gs.Where(a => a.RelUsuarioProyecto.IsActivo == 1).Select(s => new { s.RelUsuarioProyecto.FactProyecto.IdProyecto, s.RelUsuarioProyecto.FactProyecto.NombreProyecto }).Distinct().ToList(),
                                   Horas = Math.Round(gs.Sum(s => s.TotalHoras.Value), 0)
                               };

                int Mas45HH = 0;

                foreach (var item in consulta)
                {
                    if (item.Horas > 45)
                    {
                        Mas45HH++;
                        DateTime PDia = Dal.Clases.Util.FirstDateOfWeek(DateTime.Today.Year, item.Semana.Value, CultureInfo.CurrentCulture);
                        string Lunes = PDia.ToString("dd-MM-yyyy");
                        string Domingo = PDia.AddDays(6).ToString("dd-MM-yyyy");

                        foreach (var proy in item.Proyecto)
                        {
                            //var djp = Dal.Clases.ProyectoDAL.DatosProyectoEP(proy.IdProyecto);
                            //foreach (var j in djp.RelUsuarioProyecto.Where(a => a.IsActivo == 1).Where(j => j.IsJp == 1).ToList())
                            //foreach (var j in djp.RelUsuarioProyecto.Where(a => a.IsActivo == 1).Where(j => j.IsJp == 1).Where(y => y.IdUsuario != IdUsuario).ToList())

                            var djp = Dal.Clases.RelUsuariosDAL.ListaUsuarioProyecto(proy.IdProyecto, 0);
                            var DatosJPs = from x in djp
                                           where x.IsJp == 1
                                           where x.IdUsuario != IdUsuario
                                           select new
                                           {
                                               x.IdUsuario,
                                               x.FactUsuario.Nombre,
                                               x.FactUsuario.Correo
                                           };


                            foreach (var j in DatosJPs)
                            {
                                // notificación a los JP, si usuario que registra es JP no se envía notificación.
                                string htmlN = html;
                                string TitNot = "Registro HH Semanal";
                                string MsgNot = "El usuario <b>" + usu.Nombre + "</b>, ha registrado más de 45 horas semanales en los proyectos que tiene asignados de la semana entre el " + Lunes + " y el " + Domingo + ", ha recibido esta notificación ya que usted es el Jefe del Proyecto <b>" + proy.NombreProyecto + "</b>";
                                Dal.Clases.NotificacionDAL.RegistrarNotificacion(proy.IdProyecto, IdUsuario, DateTime.Now, DateTime.Today, j.IdUsuario, TitNot, MsgNot, 1, 0);

                                htmlN = htmlN.Replace("[NOMBRE]", j.Nombre);
                                htmlN = htmlN.Replace("[DESCRIPCION]", TitNot);
                                htmlN = htmlN.Replace("[CONTENIDO]", MsgNot);
                                //Envio Correo
                                MsgNot += "<br><br> Puede revisar el detalle de las actividades registradas por el usuario en el siguiente link: <a href='" + baseUrl + "/Timesheet'>Timesheet</a>";
                                MsgNot += "<br><br> Si desea realizar la aprobación de las actividades debe ingresar al siguiente link: <a href='" + baseUrl + "/Aprobacion/Actividades'>Aprobación</a>";

                                var send = Dal.Clases.Util.EnviarMail(j.Correo, "Notificación Plataforma TimeSheet", htmlN);
                                if (send != "")
                                {
                                    Dal.Clases.NotificacionDAL.RegistrarNotificacion(null, j.IdUsuario, DateTime.Now, DateTime.Today, j.IdUsuario, "Envío Notificación", send, 3, 0);
                                }

                            }
                        }

                        // notificación al usuario que registra
                        string TitNot2 = "Registro HH Semanal";
                        string MsgNot2 = "Has registrado más de 45 horas semanales a los proyectos que tienes asignados de la semana entre el " + Lunes + " y el " + Domingo + ", se ha enviado una notificación al Jefe de Proyecto.";
                        Dal.Clases.NotificacionDAL.RegistrarNotificacion(null, IdUsuario, DateTime.Now, DateTime.Today, IdUsuario, TitNot2, MsgNot2, 1, 0);
                    }
                }


                // Fin Validación 45HH semanales


                if (registros == 0)
                {
                    return Json(new { Cod = 3, Msg = "No se registraron los datos, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var msg = "Datos registrados exitosamente.";
                    msg += (Mas45HH > 0 ? "<hr>Has registrado más de 45 horas semanales a los proyectos que tienes asignados en el periodo registrado, se ha enviado una notificación al Jefe de Proyecto." : "");
                    return Json(new { Cod = 1, Msg = msg, Registros = registros }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Cod = 2,
                    Msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DatosTimeSheet()
        {
            try
            {
                int IdTimesheet = int.Parse(Request.Form["IdTimesheet"]);
                var TS = Dal.Clases.TimeSheetDAL.DatosTimeSheet(IdTimesheet);


                /*
               var Horarios = Dal.Clases.TimeSheetDAL.HorariosRegistrados(TS.RelUsuarioProyecto.IdUsuario, TS.Fecha.Value);
                Horarios = Horarios.Where(i => i.IdRegistro != IdTimesheet); //Vaidación para no considerar registro actual.

                string[] HIs = TS.HoraInicio.Split(':');
                string[] HTs = TS.HoraFin.Split(':');

                double shrI, shrT = 0;
                double smrI, smrT = 0;

                shrI = int.Parse(HIs[0]);
                shrT = int.Parse(HTs[0]);

                smrI = double.Parse(HIs[1]) / 60;
                smrT = double.Parse(HTs[1]) / 60;


                DateTime FRegDesde = TS.Fecha.Value;
                DateTime FRegHasta = TS.Fecha.Value;



                FRegDesde = FRegDesde.AddHours(shrI + smrI);
                FRegHasta = FRegHasta.AddHours(shrT + smrT);

                var LDesde = "";
                var LHasta = "";
                */

                //var LDesde = Horarios.Where(f => DateTime.ParseExact(f.Fecha.Value.ToString("yyyy-MM-dd") + " " + f.HoraFin + ":00", "yyyy-MM-dd hh:mm:ss", CultureInfo.InvariantCulture) <= FRegDesde).FirstOrDefault();
                //var LHasta = Horarios.Where(f => DateTime.ParseExact(f.Fecha.Value.ToString("yyyy-MM-dd") + " " + f.HoraInicio + ":00", "yyyy-MM-dd hh:mm:ss", CultureInfo.InvariantCulture) >= FRegHasta).FirstOrDefault();

                var Datos = new
                {
                    Fecha = TS.Fecha.Value.ToString("dd-MM-yyyy"),
                    Proyecto = TS.IdUsuarioProyecto,
                    Costo = TS.IdTipoCosto,
                    AplicaCosto = TS.RelUsuarioProyecto.FactUsuario.DimEmpresa.AplicaCosto,
                    Actividad = TS.Actividad,
                    Inicio = TS.HoraInicio,
                    Termino = TS.HoraFin,
                    Gantt = TS.FactTipoActividad == null ? 0 : TS.FactTipoActividad.IdTipoActividad
                };

                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult EditarTimesheet()
        {
            try
            {
                int Id = int.Parse(Request.Form["IdTimesheet"]);
                int Aprobado = int.Parse(Request.Form["IdTimesheetAprobada"]);
                string Fecha = Request.Form["Fecha"];
                string Actividad = Request.Form["Actividad"];
                int IdUsuarioProyecto = int.Parse(Request.Form["Proyecto"]);
                var Idgantt = Request.Form["Gantt"];
                int Costo = int.Parse(Request.Form["Costo"]);
                string Inicio = Request.Form["Inicio"];
                string Termino = Request.Form["Termino"];

                var dias = Dal.Clases.ConfiguracionDAL.DatosConfiguracion(3);
                var FechaMaxima = DateTime.Today.AddDays(int.Parse(dias.Valor) * -1);

                var FE = DateTime.ParseExact(Fecha, "dd-MM-yyyy", null);

                var TSO = Dal.Clases.TimeSheetDAL.DatosTimeSheet(Id);

                DateTime fi = DateTime.ParseExact(FE.ToString("yyyy-MM-dd") + " " + Inicio + ":00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                DateTime ft = DateTime.ParseExact(FE.ToString("yyyy-MM-dd") + " " + Termino + ":00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);


                var FEO = DateTime.ParseExact(TSO.Fecha.Value.ToString("dd-MM-yyyy"), "dd-MM-yyyy", null);
                DateTime fiO = DateTime.ParseExact(FEO.ToString("yyyy-MM-dd") + " " + TSO.HoraInicio + ":00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                DateTime ftO = DateTime.ParseExact(FEO.ToString("yyyy-MM-dd") + " " + TSO.HoraFin + ":00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);


                var TS = Dal.Clases.TimeSheetDAL.DatosTimeSheet(Id);
                var ProAsig = Dal.Clases.RelUsuariosDAL.ProyectosAsignados(TS.RelUsuarioProyecto.IdUsuario);

                var proy = Dal.Clases.RelUsuariosDAL.DatosUsuarioProyecto(IdUsuarioProyecto);

                if (fi < proy.FactProyecto.InicioProyecto)
                {
                    return Json(new { Cod = 3, Msg = "La fecha registrada es anterior al inicio del proyecto." }, JsonRequestBehavior.AllowGet);
                }

                if (fi > proy.FactProyecto.FinProyecto)
                {
                    return Json(new { Cod = 3, Msg = "La fecha registrada es posterior al termino del proyecto." }, JsonRequestBehavior.AllowGet);
                }

                var registros = Dal.Clases.TimeSheetDAL.HorariosRegistrados(ProAsig, FE);

                int ExisteInicio = 0;
                int ExisteTermino = 0;

                //if (fi == fiO && ft == ftO)
                //{

                //}
                //else
                //{
                foreach (var x in registros.Where(i => i.IdRegistro != Id))
                {
                    DateTime fiCom = DateTime.ParseExact(FE.ToString("yyyy-MM-dd") + " " + x.HoraInicio + ":00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime ffCom = DateTime.ParseExact(FE.ToString("yyyy-MM-dd") + " " + x.HoraFin + ":00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                    if (fi >= fiCom && fi < ffCom)
                    {
                        ExisteInicio++;
                    }

                    if (ft > fiCom && ft <= ffCom)
                    {
                        ExisteTermino++;
                    }

                    if (ft > ffCom && fi < ffCom)
                    {
                        ExisteTermino++;
                    }

                }
                //}

                if (ExisteInicio > 0)
                {
                    return Json(new { Cod = 3, Msg = "La hora de inicio provoca conflicto con otra actividad registrada.", registros }, JsonRequestBehavior.AllowGet);
                }

                if (ExisteTermino > 0)
                {
                    return Json(new { Cod = 3, Msg = "La hora de término provoca conflicto con otra actividad registrada.", registros }, JsonRequestBehavior.AllowGet);
                }



                /* Registro datos por bloque */

                //Dao.Model.FactTimeSheet Modificado = Dal.Clases.TimeSheetDAL.DatosTimeSheet(Id);

                //Dao.Model.FactTimeSheet Modificado = new Dao.Model.FactTimeSheet();



                var duracion = (ft - fi).TotalHours;

                if (Aprobado == 0)
                {
                    //Validación Limite de días
                    if (FE < FechaMaxima)
                    {
                        var men = "No fue posible registrar los cambios.";
                        var existe = Dal.Clases.AprobacionTimesheetDAL.ExisteAtraso(TSO.RelUsuarioProyecto.IdUsuarioProyecto);

                        if (existe != null)
                        {
                            if (existe.EstadoSolicitud == 0)
                            {
                                men += "<hr> Existe una solicitud de excepción en espera de aprobación, una vez aprobada se habilitará la excepción para editar el registro.";
                                return Json(new { Cod = 3, Msg = men }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                if (DateTime.Today > existe.FechaValidez)
                                {
                                    var re = Dal.Clases.AprobacionTimesheetDAL.RegistrarAtraso(TSO.RelUsuarioProyecto.IdUsuarioProyecto);
                                    if (re == 1)
                                    {
                                        string TitNot = "Límite días Timesheet";
                                        string MsgNot = "Se ha registrado una solicitud de excepción al límite de días para carga de actividades del usuario <b>" + TSO.RelUsuarioProyecto.FactUsuario.Nombre + "</b>, asociada al proyecto <b>" + proy.FactProyecto.NombreProyecto + "</b>.";
                                        //Registro Notificación
                                        var djp = Dal.Clases.ProyectoDAL.DatosProyectoEP(TSO.RelUsuarioProyecto.IdProyecto);

                                        foreach (var j in djp.RelUsuarioProyecto.Where(i => i.IsJp == 1).Where(a => a.IsActivo == 1).ToList())
                                        {
                                            Dal.Clases.NotificacionDAL.RegistrarNotificacion(TSO.RelUsuarioProyecto.IdProyecto, TSO.RelUsuarioProyecto.IdUsuario, DateTime.Now, DateTime.Today, j.IdUsuario, TitNot, MsgNot, 1, 0);
                                        }
                                        men += "<hr> Se ha generado una solicitud de carga fuera de plazo al Jefe de Proyecto, una vez aprobada se habilitará la excepción para editar el registro.";
                                    }
                                    else
                                    {
                                        men += "<hr> Ocurrió un error al generar solicitud de excepción, intente nuevamente.";
                                    }
                                    return Json(new { Cod = 3, Msg = men }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    //Si la fecha validez es menor, continua ejecución de forma normal
                                }
                            }
                        }
                        else
                        {
                            var re = Dal.Clases.AprobacionTimesheetDAL.RegistrarAtraso(TSO.RelUsuarioProyecto.IdUsuarioProyecto);
                            if (re == 1)
                            {
                                string TitNot = "Límite días Timesheet";
                                string MsgNot = "Se ha registrado una solicitud de excepción al límite de días para carga de actividades del usuario <b>" + TSO.RelUsuarioProyecto.FactUsuario.Nombre + "</b>, asociada al proyecto <b>" + proy.FactProyecto.NombreProyecto + "</b>.";
                                //Registro Notificación
                                var djp = Dal.Clases.ProyectoDAL.DatosProyectoEP(TSO.RelUsuarioProyecto.IdProyecto);

                                foreach (var j in djp.RelUsuarioProyecto.Where(i => i.IsJp == 1).Where(a => a.IsActivo == 1).ToList())
                                {
                                    Dal.Clases.NotificacionDAL.RegistrarNotificacion(TSO.RelUsuarioProyecto.IdProyecto, TSO.RelUsuarioProyecto.IdUsuario, DateTime.Now, DateTime.Today, j.IdUsuario, TitNot, MsgNot, 1, 0);
                                }
                                men += "<hr> Se ha generado una solicitud de carga fuera de plazo al Jefe de Proyecto, una vez aprobada se habilitará la excepción para editar el registro.";
                            }
                            else
                            {
                                men += "<hr> Ocurrió un error al generar solicitud de excepción, intente nuevamente.";
                            }

                            return Json(new { Cod = 3, Msg = men }, JsonRequestBehavior.AllowGet);
                        }
                    }




                    Dao.Model.FactTimeSheet Modificado = Dal.Clases.TimeSheetDAL.DatosTimeSheetNoTrack(Id);

                    Modificado.IdRegistro = Id;
                    Modificado.Fecha = FE;
                    Modificado.Semana = Dal.Clases.Util.GetIso8601WeekOfYear(FE);
                    Modificado.Actividad = Actividad;
                    Modificado.IdUsuarioProyecto = IdUsuarioProyecto;
                    Modificado.IdTipoCosto = Costo;
                    Modificado.HoraInicio = Inicio;
                    Modificado.HoraFin = Termino;
                    Modificado.TotalHoras = duracion;
                    Modificado.IsActivo = 1;
                    Modificado.FechaEdicion = DateTime.Now;

                    if (Idgantt == null || Idgantt == "0")
                    {
                        Modificado.IdTipoActividad = null;
                    }
                    else
                    {
                        Modificado.IdTipoActividad = int.Parse(Idgantt);
                    }

                    var update = Dal.Clases.TimeSheetDAL.EditarTimesheet(Modificado);

                    if (update == 0)
                    {
                        return Json(new { Cod = 3, Msg = "No se registraron los datos, intente nuevamente.", registros }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //Falta validación de 45HH
                        return Json(new { Cod = 1, Msg = "Datos registrados exitosamente", Registros = update }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {

                    var result = Dal.Clases.EdicionTimeSheetAprobadaDAL.RegistrarTimesheetAprobada(Id, FE, Dal.Clases.Util.GetIso8601WeekOfYear(FE), Costo, Actividad, Inicio, Termino, duracion, 1, DateTime.Now, Idgantt);

                    if (result == 1)
                    {

                        var djp = Dal.Clases.ProyectoDAL.DatosProyectoEP(TSO.RelUsuarioProyecto.IdProyecto);

                        var contNot = 0;

                        foreach (var j in djp.RelUsuarioProyecto.Where(i => i.IsJp == 1).Where(a => a.IsActivo == 1).ToList())
                        {
                            string TitNot = "Edición timesheet aprobada";
                            string MsgNot = "Se ha registrado una solicitud de edición de registro timesheet ya aprobado del usuario <b>" + TSO.RelUsuarioProyecto.FactUsuario.Nombre + "</b>, asociada al proyecto <b>" + proy.FactProyecto.NombreProyecto + "</b>.";
                            contNot = contNot + Dal.Clases.NotificacionDAL.RegistrarNotificacion(TSO.RelUsuarioProyecto.IdProyecto, TSO.RelUsuarioProyecto.IdUsuario, DateTime.Now, DateTime.Today, j.IdUsuario, TitNot, MsgNot, 2, 0);
                        }

                        if (contNot > 0)
                        {
                            return Json(new { Cod = 1, Msg = "Datos registrados exitosamente.<hr> Se ha generado una solicitud de aprobación al jefe de proyecto, mientras la solicitud no sea resuelta se verán reflejados en el registro los datos ya aprobados." });

                        }
                        else
                        {
                            return Json(new { Cod = 2, Msg = "Datos registrados exitosamente.<hr> Ocurrió un problema al generar la notificación de aprobación al jefe de proyecto." });
                        }
                    }
                    else
                    {
                        return Json(new { Cod = 3, Msg = "Ocurrió un problema al guardar los datos, intente nuevamente." });
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult EliminarTimeSheet()
        {
            try
            {
                int IdTimesheet = int.Parse(Request.Form["IdTimesheet"]);
                var resultado = Dal.Clases.TimeSheetDAL.EliminarTimesheet(IdTimesheet);

                if (resultado == 1)
                {
                    return Json(new { Cod = 1, Msg = "Registro eliminado" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Cod = 1, Msg = "No fue posible eliminar el registro, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult SolicitudExcepcion()
        {
            try
            {
                var usu = (Dao.Model.FactUsuario)Session["usuario"];
                if (usu == null)
                {
                    return Json(new { Cod = 3, Msg = "Debe iniciar session para acceder a este recurso." });
                }

                if (string.IsNullOrWhiteSpace(Request.Form["project"]))
                {
                    return Json(new { Cod = 2, Msg = "Debe seleccionar un proyecto." });
                }

                var proyecto = int.Parse(Request.Form["project"]);
                var proy = Dal.Clases.RelUsuariosDAL.DatosUsuarioProyecto(proyecto);
                var existe = Dal.Clases.AprobacionTimesheetDAL.ExisteAtraso(proyecto);
                var men = "";

                if (existe != null)
                {
                    if (existe.EstadoSolicitud == 0)
                    {
                        men += "Existe una solicitud de excepción de carga en espera de aprobación, una vez aprobada se habilitará la excepción para cargar el registro.";
                        return Json(new { Cod = 2, Msg = men }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        if (DateTime.Today > existe.FechaValidez)
                        {
                            var re = Dal.Clases.AprobacionTimesheetDAL.RegistrarAtraso(proyecto);
                            if (re == 1)
                            {
                                string TitNot = "Límite días Timesheet";
                                string MsgNot = "Se ha registrado una solicitud de excepción al límite de días para carga de actividades del usuario <b>" + usu.Nombre + "</b>, asociada al proyecto <b>" + proy.FactProyecto.NombreProyecto + "</b>.";
                                //Registro Notificación
                                var djp = Dal.Clases.ProyectoDAL.DatosProyectoEP(proy.IdProyecto);

                                foreach (var j in djp.RelUsuarioProyecto.Where(i => i.IsJp == 1).Where(a => a.IsActivo == 1).ToList())
                                {
                                    Dal.Clases.NotificacionDAL.RegistrarNotificacion(proy.IdProyecto, proy.IdUsuario, DateTime.Now, DateTime.Today, j.IdUsuario, TitNot, MsgNot, 1, 0);
                                }
                                men += "Se ha generado una solicitud de carga fuera de plazo al Jefe de Proyecto, una vez aprobada se habilitará la excepción para cargar el registro.";
                                return Json(new { Cod = 1, Msg = men }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                men += "Ocurrió un error al generar solicitud de excepción, intente nuevamente.";
                                return Json(new { Cod = 3, Msg = men }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            men += "Ya existe una solicitud de excepción de carga aprobada para el proyecto " + proy.FactProyecto.NombreProyecto;
                            return Json(new { Cod = 2, Msg = men }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    var re = Dal.Clases.AprobacionTimesheetDAL.RegistrarAtraso(proyecto);
                    if (re == 1)
                    {
                        string TitNot = "Límite días Timesheet";
                        string MsgNot = "Se ha registrado una solicitud de excepción al límite de días para carga de actividades del usuario <b>" + usu.Nombre + "</b>, asociada al proyecto <b>" + proy.FactProyecto.NombreProyecto + "</b>.";
                        //Registro Notificación
                        var djp = Dal.Clases.ProyectoDAL.DatosProyectoEP(proy.IdProyecto);

                        foreach (var j in djp.RelUsuarioProyecto.Where(i => i.IsJp == 1).Where(a => a.IsActivo == 1).ToList())
                        {
                            Dal.Clases.NotificacionDAL.RegistrarNotificacion(proy.IdProyecto, proy.IdUsuario, DateTime.Now, DateTime.Today, j.IdUsuario, TitNot, MsgNot, 1, 0);
                        }
                        men += "Se ha generado una solicitud de carga fuera de plazo al Jefe de Proyecto, una vez aprobada se habilitará la excepción para cargar el registro.";
                        return Json(new { Cod = 1, Msg = men }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        men += "Ocurrió un error al generar solicitud de excepción, intente nuevamente.";
                        return Json(new { Cod = 3, Msg = men }, JsonRequestBehavior.AllowGet);
                    }

                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 3, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ValidaExcepcionCarga()
        {
            try
            {
                var usu = (Dao.Model.FactUsuario)Session["usuario"];
                if (usu == null)
                {
                    return Json(new { Cod = 3, Msg = "Debe iniciar session para acceder a este recurso." });
                }

                if (string.IsNullOrWhiteSpace(Request.Form["project"]))
                {
                    return Json(new { Cod = 2, Msg = "Debe seleccionar un proyecto." });
                }

                var proyecto = int.Parse(Request.Form["project"]);
                var existe = Dal.Clases.AprobacionTimesheetDAL.ExisteAtraso(proyecto);

                if (existe != null)
                {
                    if (existe.EstadoSolicitud != 0)
                    {
                        if (DateTime.Today < existe.FechaValidez)
                        {
                            return Json(new { Cod = 1, Msg = "Si tiene permiso." }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { Cod = 2, Msg = "no tiene permiso." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 3, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public static string RenderViewToString(ControllerContext context, string viewPath, object model = null, bool partial = false)
        {
            // first find the ViewEngine for this view
            ViewEngineResult viewEngineResult = null;
            if (partial)
                viewEngineResult = ViewEngines.Engines.FindPartialView(context, viewPath);
            else
                viewEngineResult = ViewEngines.Engines.FindView(context, viewPath, null);

            if (viewEngineResult == null)
                throw new FileNotFoundException("View cannot be found.");

            // get the view and attach the model to view data
            var view = viewEngineResult.View;
            context.Controller.ViewData.Model = model;

            string result = null;

            using (var sw = new StringWriter())
            {
                var ctx = new ViewContext(context, view,
                                            context.Controller.ViewData,
                                            context.Controller.TempData,
                                            sw);
                view.Render(ctx, sw);
                result = sw.ToString();
            }

            return result;
        }



        ////////CONSULTORIA

        /*[HttpPost]
        public JsonResult ListadoEntregables()
        {
            try
            {
                string desde = Request.Form["desde"];
                string hasta = Request.Form["hasta"];
                int proyecto = int.Parse(Request.Form["proyecto"]);

                //var listadousuarios = new List<int>();
                //var listadoproyectos = new List<int>();

                //var fdesde = DateTime.ParseExact(desde, "dd-MM-yyyy", null);
                //var fhasta = DateTime.ParseExact(hasta, "dd-MM-yyyy", null);

                //if (usuario == 0 && proyecto == 0)
                //{
                //    var usr = (Dao.Model.FactUsuario)Session["usuario"];
                //    int usuariologin = usr.IdUsuario;
                //    var pjp = Dal.Clases.RelUsuariosDAL.ProyectosJP(usuariologin);
                //    var Listado1 = Dal.Clases.RelUsuariosDAL.UsuariosProyectoDash(pjp.Select(p => p.IdProyecto).ToList());
                //    listadousuarios = (from x in Listado1 group x by x.IdUsuario into agru select new { IdUsuario = agru.Key }).Select(p => p.IdUsuario).ToList();

                //    var Listado2 = Dal.Clases.TimeSheetDAL.ListaProyectosTimesheet(usuariologin).ToList();
                //    listadoproyectos = Listado2.Select(p => p.IdProyecto).ToList();
                //}

                //if (usuario == 0 && proyecto != 0)
                //{
                //    var usr = (Dao.Model.FactUsuario)Session["usuario"];
                //    int usuariologin = usr.IdUsuario;
                //    var pjp = Dal.Clases.RelUsuariosDAL.ProyectosJP(usuariologin);
                //    var Listado1 = Dal.Clases.RelUsuariosDAL.UsuariosProyectoDash(pjp.Select(p => p.IdProyecto).ToList());
                //    listadousuarios = (from x in Listado1 group x by x.IdUsuario into agru select new { IdUsuario = agru.Key }).Select(p => p.IdUsuario).ToList();

                //    listadoproyectos.Add(proyecto);
                //}

                //if (usuario != 0 && proyecto == 0)
                //{
                //    var usr = (Dao.Model.FactUsuario)Session["usuario"];
                //    int usuariologin = usr.IdUsuario;
                //    var Listado1 = Dal.Clases.TimeSheetDAL.ListaProyectosTimesheet(usuariologin).ToList();
                //    listadoproyectos = Listado1.Select(p => p.IdProyecto).ToList();

                //    listadousuarios.Add(usuario);
                //}

                //if (usuario != 0 && proyecto != 0)
                //{
                //    listadousuarios.Add(usuario);
                //    listadoproyectos.Add(proyecto);
                //}

                var Listado = Dal.Clases.EntregableDAL.ListaEntregables().ToList();
                var idu = (Dao.Model.FactUsuario)Session["usuario"];

                var Datos = from x in Listado
                            orderby x.IdProyecto descending
                            select new
                            {
                                id = x.IdEntregable,

                                
                                entregable = x.Entregable,       
                                porc = x.Avance,            
                                pendiente = x.Pendiente == 1 ? "No" : "Si",
                                version = x.VersionFinal == 1 ? "No" : "Si",
                                observacion = x.Observaciones                                            
                            };

                if (Datos.ToList().Count == 0)
                {
                    return Json(new { Cod = 2, Msg = "No se han encontrado entregables en la búsqueda." }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }*/

        [HttpPost]
        public JsonResult ListadoTimesheetConsultor()
        {
            try
            {
                var usr = (Dao.Model.FactUsuario)Session["usuario"];
                if (usr == null)
                {
                    return Json(new { Cod = 0, Msg = "Debe iniciar sesión para utilizar este recuro." });
                }

                string desde = Request.Form["desde"];
                string hasta = Request.Form["hasta"];
                int usuario = int.Parse(Request.Form["usuario"]);
                int proyecto = int.Parse(Request.Form["proyecto"]);

                var listadousuarios = new List<int>();
                var listadoproyectos = new List<int>();

                var fdesde = DateTime.ParseExact(desde, "dd-MM-yyyy", null);
                var fhasta = DateTime.ParseExact(hasta, "dd-MM-yyyy", null);

                if (usuario == 0)
                {
                    if (usr.IsAdmin == 1)
                    {
                        var ListadoU = Dal.Clases.UsuarioDAL.ListaUsuarios().ToList();
                        listadousuarios = ListadoU.Select(u => u.IdUsuario).Distinct().ToList();
                    }
                    else
                    {
                        var pjp = Dal.Clases.RelUsuariosDAL.ProyectosJP(usr.IdUsuario);
                        var ListadoU = Dal.Clases.RelUsuariosDAL.UsuariosProyectoDash(pjp.Select(p => p.IdProyecto).ToList());
                        listadousuarios = ListadoU.Select(u => u.IdUsuario).Distinct().ToList();
                    }
                }
                else
                {
                    listadousuarios.Add(usuario);
                }


                if (proyecto == 0)
                {
                    if (usr.IsAdmin == 0)
                    {
                        var ListadoP = Dal.Clases.ProyectoDAL.ListaProyectos().Where(p => p.IdTipoProyecto == 2).Where(p => p.IsActivo == 1).Select(x => x).ToList();
                        listadoproyectos = ListadoP.Select(p => p.IdProyecto).Distinct().ToList();
                    }
                    else
                    {
                        var ListadoP = Dal.Clases.ProyectoDAL.ListaProyectosPerfilConsultor(usuario).ToList();
                        listadoproyectos = ListadoP.Select(p => p.IdProyecto).Distinct().ToList();
                    }
                }
                else
                {
                    listadoproyectos.Add(proyecto);
                }
                                                                                                                                  
                //if (usuario == 0 && proyecto == 0)
                //{
                //    usr = (Dao.Model.FactUsuario)Session["usuario"];
                //    int usuariologin = usr.IdUsuario;
                //    var pjp = Dal.Clases.RelUsuariosDAL.ProyectosJP(usuariologin);
                //    var Listado1 = Dal.Clases.RelUsuariosDAL.UsuariosProyectoDash(pjp.Select(p => p.IdProyecto).ToList());
                //    listadousuarios = (from x in Listado1 group x by x.IdUsuario into agru select new { IdUsuario = agru.Key }).Select(p => p.IdUsuario).ToList();

                //    var Listado2 = Dal.Clases.TimeSheetConsultorDAL.ListaProyectosTimesheetConsultor(usuariologin).ToList();
                //    listadoproyectos = Listado2.Select(p => p.IdProyecto).ToList();
                //}

                //if (usuario == 0 && proyecto != 0)
                //{
                //    usr = (Dao.Model.FactUsuario)Session["usuario"];
                //    int usuariologin = usr.IdUsuario;
                //    var pjp = Dal.Clases.RelUsuariosDAL.ProyectosJP(usuariologin);
                //    var Listado1 = Dal.Clases.RelUsuariosDAL.UsuariosProyectoDash(pjp.Select(p => p.IdProyecto).ToList());
                //    listadousuarios = (from x in Listado1 group x by x.IdUsuario into agru select new { IdUsuario = agru.Key }).Select(p => p.IdUsuario).ToList();

                //    listadoproyectos.Add(proyecto);
                //}

                //if (usuario != 0 && proyecto == 0)
                //{
                //    usr = (Dao.Model.FactUsuario)Session["usuario"];
                //    int usuariologin = usr.IdUsuario;
                //    var Listado1 = Dal.Clases.TimeSheetConsultorDAL.ListaProyectosTimesheetConsultor(usuariologin).ToList();
                //    listadoproyectos = Listado1.Select(p => p.IdProyecto).ToList();

                //    listadousuarios.Add(usuario);
                //}

                //if (usuario != 0 && proyecto != 0)
                //{
                //    listadousuarios.Add(usuario);
                //    listadoproyectos.Add(proyecto);
                //}

                var Listado = Dal.Clases.TimeSheetConsultorDAL.ListadoTimesheetConsultor(fdesde, fhasta, listadousuarios, listadoproyectos).ToList();
                var idu = (Dao.Model.FactUsuario)Session["usuario"];

                var Datos = from x in Listado
                            orderby x.IdProyectoConsultor, x.Fecha descending
                            select new
                            {
                                IdTimesheetConsultor = (idu.IdUsuario == x.RelUsuarioProyecto.IdUsuario ? x.IdTimesheetConsultor : 0),
                                proyecto = x.RelUsuarioProyecto.FactProyecto.NombreProyecto,
                                usuario = x.RelUsuarioProyecto.FactUsuario.Nombre,
                                descripcion = x.Descripcion,
                                fecha = x.Fecha.Value.ToString("dd-MM-yyyy"),
                                dias = x.Dias,
                                Estado = x.Estado,
                                Aprobado = x.IsAprobado
                            };

                if (Datos.ToList().Count == 0)
                {
                    return Json(new { Cod = 2, Msg = "No se han encontrado registros de TimeSheet en la búsqueda." }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GuardarTimesheetConsultor()
        {
            try
            {
                string[] ProyectoC = Request.Form.GetValues("ProyectoC");

                string[] Actividad = Request.Form.GetValues("Actividad");

                string[] FechaConsultoria = Request.Form.GetValues("FechaConsultoria");

                var Usuario = int.Parse(Request.Form["IdUsuario"]);

                var Proyecto = int.Parse(ProyectoC[0]);
                var Descripcion = Actividad[0];
                var Fecha = DateTime.Parse(FechaConsultoria[0]);



                var resultado = Dal.Clases.TimeSheetConsultorDAL.GuardarTimesheetConsultor(Proyecto, Usuario, Descripcion, Fecha);

                if (resultado == 0)
                {
                    return Json(new { Cod = 0, Msg = "No fue posible guardar los datos de la Timesheet ingresada, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Cod = 1, Msg = "Los datos de la Timesheet ingresada se han registrado exitosamente." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ListaProyectosPerfilConsultor()
        {
            try
            {
                int usuario = int.Parse(Request.Form["usuario"]);
                var Listado = Dal.Clases.ProyectoDAL.ListaProyectosPerfilConsultor(usuario).ToList();

                var Datos = from x in Listado
                            select new
                            {

                                IdProyecto = x.IdProyecto,
                                Proyecto = x.FactProyecto.NombreProyecto
                            };

                return Json(new { Cod = 1, Msg = "OK", Datos = Datos }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GuardarEntregableConsultor()
        {
            try
            {
                string[] ProyectoC = Request.Form.GetValues("ProyectoC");
                string[] Entregable = Request.Form.GetValues("Entregable");
                string[] Avance = Request.Form.GetValues("Avance");
                string[] Pendiente = Request.Form.GetValues("Pendiente");
                string[] VersionFinal = Request.Form.GetValues("VersionFinal");
                string[] Observacion = Request.Form.GetValues("Observacion");
                string[] CodigoDocumento = Request.Form.GetValues("CodigoDocumento");


                var Usuario = int.Parse(Request.Form["IdUsuario"]);
                var Proyecto = int.Parse(ProyectoC[0]);
                var Entrega = Entregable[0];
                var Avances = int.Parse(Avance[0]);
                var Pendient = int.Parse(Pendiente[0]);
                var VersionF = int.Parse(VersionFinal[0]);
                var Observaciones = Observacion[0];
                var CodDocumento = CodigoDocumento[0];

                var existeentregable = Dal.Clases.TimeSheetConsultorDAL.ObjetoTimesheetEntregableEditar(Entrega);
                if (existeentregable == null)
                {
                    var resultado = Dal.Clases.EntregableDAL.GuardarEntregableConsultor(Usuario, Proyecto, Entrega, Avances, Pendient, VersionF, Observaciones, CodDocumento);

                    if (resultado == 0)
                    {
                        return Json(new { Cod = 0, Msg = "No fue posible guardar los datos del registro entregable ingresado, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { Cod = 1, Msg = "Los datos del entregable se han registrado exitosamente." }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    Dao.Model.FactEntregables Modificado = existeentregable;

                    //Modificado.Fecha = FE;
                    Modificado.IdProyecto = existeentregable.IdProyecto;
                    Modificado.Entregable = Entrega;
                    Modificado.Avance = Avances;
                    Modificado.Pendiente = Pendient;
                    Modificado.VersionFinal = VersionF;
                    Modificado.Observaciones = Observaciones;
                    Modificado.CodigoDocumento = CodDocumento;


                    var update = Dal.Clases.TimeSheetConsultorDAL.EditarEntregableConsultor(Modificado);

                    if (update == 0)
                    {
                        return Json(new { Cod = 0, Msg = "No se registraron los datos, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //Falta validación de 45HH
                        return Json(new { Cod = 1, Msg = "Datos registrados exitosamente" }, JsonRequestBehavior.AllowGet);
                    }
                }

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ObjetoTimesheetConsultorEditar()
        {
            try
            {
                var id = int.Parse(Request.Form["id"]);

                var resultado = Dal.Clases.TimeSheetConsultorDAL.ObjetoTimesheetConsultorEditar(id);

                if (resultado == null)
                {
                    return Json(new { Cod = 2, Msg = "No se puede encontrar la timesheet seleccionada." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var proyecto = resultado.IdProyectoConsultor;
                    var descripcion = resultado.Descripcion;
                    var fecha = resultado.Fecha.Value.ToString("dd-MM-yyyy");
                    var aprobado = resultado.IsAprobado;

                    return Json(new { Cod = 1, Msg = "OK.", proyecto, descripcion, fecha, aprobado }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult EditarTimesheetConsultor()
        {
            try
            {
                int Id = int.Parse(Request.Form["IdTimesheetConsultor"]);
                int Aprobado = int.Parse(Request.Form["IsAprobado"]);
                string Fecha = Request.Form["FechaC"];
                string Descripcion = Request.Form["DescripcionC"];
                int Proyecto = int.Parse(Request.Form["ProyectoC"]);


                //var dias = Dal.Clases.ConfiguracionDAL.DatosConfiguracion(3);
                //var FechaMaxima = DateTime.Today.AddDays(int.Parse(dias.Valor) * -1);

                var FE = DateTime.ParseExact(Fecha, "dd-MM-yyyy", null);

                var TSO = Dal.Clases.TimeSheetConsultorDAL.ObjetoTimesheetConsultorEditar(Id);

                var proy = Dal.Clases.RelUsuariosDAL.DatosUsuarioProyecto(TSO.IdUsuarioConsultor.Value);

                if (FE < proy.FactProyecto.InicioProyecto)
                {
                    return Json(new { Cod = 3, Msg = "La fecha registrada es anterior al inicio del proyecto." }, JsonRequestBehavior.AllowGet);
                }

                if (FE > proy.FactProyecto.FinProyecto)
                {
                    return Json(new { Cod = 3, Msg = "La fecha registrada es posterior al termino del proyecto." }, JsonRequestBehavior.AllowGet);
                }


                /*if (Aprobado == 0)
                {
                    //Validación Limite de días
                    if (FE < FechaMaxima)
                    {
                        var men = "No fue posible registrar los cambios.";
                        var existe = Dal.Clases.AprobacionTimesheetDAL.ExisteAtraso(TSO.RelUsuarioProyecto.IdUsuarioProyecto);

                        if (existe != null)
                        {
                            if (existe.EstadoSolicitud == 0)
                            {
                                men += "<hr> Existe una solicitud de excepción en espera de aprobación, una vez aprobada se habilitará la excepción para editar el registro.";
                                return Json(new { Cod = 3, Msg = men }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                if (DateTime.Today > existe.FechaValidez)
                                {
                                    var re = Dal.Clases.AprobacionTimesheetDAL.RegistrarAtraso(TSO.RelUsuarioProyecto.IdUsuarioProyecto);
                                    if (re == 1)
                                    {
                                        string TitNot = "Límite días Timesheet";
                                        string MsgNot = "Se ha registrado una solicitud de excepción al límite de días para carga de actividades del usuario <b>" + TSO.RelUsuarioProyecto.FactUsuario.Nombre + "</b>, asociada al proyecto <b>" + proy.FactProyecto.NombreProyecto + "</b>.";
                                        //Registro Notificación
                                        var djp = Dal.Clases.ProyectoDAL.DatosProyectoEP(TSO.RelUsuarioProyecto.IdProyecto);

                                        foreach (var j in djp.RelUsuarioProyecto.Where(i => i.IsJp == 1).Where(a => a.IsActivo == 1).ToList())
                                        {
                                            Dal.Clases.NotificacionDAL.RegistrarNotificacion(TSO.RelUsuarioProyecto.IdProyecto, TSO.RelUsuarioProyecto.IdUsuario, DateTime.Now, DateTime.Today, j.IdUsuario, TitNot, MsgNot, 1, 0);
                                        }
                                        men += "<hr> Se ha generado una solicitud de carga fuera de plazo al Jefe de Proyecto, una vez aprobada se habilitará la excepción para editar el registro.";
                                    }
                                    else
                                    {
                                        men += "<hr> Ocurrió un error al generar solicitud de excepción, intente nuevamente.";
                                    }
                                    return Json(new { Cod = 3, Msg = men }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    //Si la fecha validez es menor, continua ejecución de forma normal
                                }
                            }
                        }
                        else
                        {
                            var re = Dal.Clases.AprobacionTimesheetDAL.RegistrarAtraso(TSO.RelUsuarioProyecto.IdUsuarioProyecto);
                            if (re == 1)
                            {
                                string TitNot = "Límite días Timesheet";
                                string MsgNot = "Se ha registrado una solicitud de excepción al límite de días para carga de actividades del usuario <b>" + TSO.RelUsuarioProyecto.FactUsuario.Nombre + "</b>, asociada al proyecto <b>" + proy.FactProyecto.NombreProyecto + "</b>.";
                                //Registro Notificación
                                var djp = Dal.Clases.ProyectoDAL.DatosProyectoEP(TSO.RelUsuarioProyecto.IdProyecto);

                                foreach (var j in djp.RelUsuarioProyecto.Where(i => i.IsJp == 1).Where(a => a.IsActivo == 1).ToList())
                                {
                                    Dal.Clases.NotificacionDAL.RegistrarNotificacion(TSO.RelUsuarioProyecto.IdProyecto, TSO.RelUsuarioProyecto.IdUsuario, DateTime.Now, DateTime.Today, j.IdUsuario, TitNot, MsgNot, 1, 0);
                                }
                                men += "<hr> Se ha generado una solicitud de carga fuera de plazo al Jefe de Proyecto, una vez aprobada se habilitará la excepción para editar el registro.";
                            }
                            else
                            {
                                men += "<hr> Ocurrió un error al generar solicitud de excepción, intente nuevamente.";
                            }

                            return Json(new { Cod = 3, Msg = men }, JsonRequestBehavior.AllowGet);
                        }
                    }*/


                Dao.Model.FactTimesheetConsultor Modificado = TSO;

                Modificado.Fecha = FE;
                Modificado.IdProyectoConsultor = Proyecto;
                Modificado.Descripcion = Descripcion;



                var update = Dal.Clases.TimeSheetConsultorDAL.EditarTimesheetConsultor(Modificado);

                if (update == 0)
                {
                    return Json(new { Cod = 3, Msg = "No se registraron los datos, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //Falta validación de 45HH
                    return Json(new { Cod = 1, Msg = "Datos registrados exitosamente" }, JsonRequestBehavior.AllowGet);
                }
                /*}
                else
                {

                    var result = Dal.Clases.EdicionTimeSheetAprobadaDAL.RegistrarTimesheetAprobada(Id, FE, Dal.Clases.Util.GetIso8601WeekOfYear(FE), Costo, Actividad, Inicio, Termino, duracion, 1, DateTime.Now, Idgantt);

                    if (result == 1)
                    {

                        var djp = Dal.Clases.ProyectoDAL.DatosProyectoEP(TSO.RelUsuarioProyecto.IdProyecto);

                        var contNot = 0;

                        foreach (var j in djp.RelUsuarioProyecto.Where(i => i.IsJp == 1).Where(a => a.IsActivo == 1).ToList())
                        {
                            string TitNot = "Edición timesheet aprobada";
                            string MsgNot = "Se ha registrado una solicitud de edición de registro timesheet ya aprobado del usuario <b>" + TSO.RelUsuarioProyecto.FactUsuario.Nombre + "</b>, asociada al proyecto <b>" + proy.FactProyecto.NombreProyecto + "</b>.";
                            contNot = contNot + Dal.Clases.NotificacionDAL.RegistrarNotificacion(TSO.RelUsuarioProyecto.IdProyecto, TSO.RelUsuarioProyecto.IdUsuario, DateTime.Now, DateTime.Today, j.IdUsuario, TitNot, MsgNot, 2, 0);
                        }

                        if (contNot > 0)
                        {
                            return Json(new { Cod = 1, Msg = "Datos registrados exitosamente.<hr> Se ha generado una solicitud de aprobación al jefe de proyecto, mientras la solicitud no sea resuelta se verán reflejados en el registro los datos ya aprobados." });

                        }
                        else
                        {
                            return Json(new { Cod = 2, Msg = "Datos registrados exitosamente.<hr> Ocurrió un problema al generar la notificación de aprobación al jefe de proyecto." });
                        }
                    }
                    else
                    {
                        return Json(new { Cod = 3, Msg = "Ocurrió un problema al guardar los datos, intente nuevamente." });
                    }
                }*/
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ObjetoTimesheetEntregableEditar()
        {
            try
            {
                var id = int.Parse(Request.Form["id"]);

                var resultado = Dal.Clases.TimeSheetConsultorDAL.ObjetoTimesheetEntregableEditar
                    (id);

                if (resultado == null)
                {
                    return Json(new { Cod = 2, Msg = "No se puede encontrar la timesheet seleccionada." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var ProyectoC = resultado.IdProyecto;
                    var EntregableC = resultado.Entregable;
                    var AvanceC = resultado.Avance;
                    var PendienteC = resultado.Pendiente;
                    var VersionFinal = resultado.VersionFinal;
                    var Observaciones = resultado.Observaciones;
                    var CodigoDocumento = resultado.CodigoDocumento;


                    return Json(new { Cod = 1, Msg = "OK.", ProyectoC, EntregableC, AvanceC, PendienteC, VersionFinal, Observaciones, CodigoDocumento }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult EditarEntregableConsultor()
        {
            try
            {
                int Id = int.Parse(Request.Form["IdEntregable"]);
                int Consultoria = int.Parse(Request.Form["ProyectoEC"]);
                string Entregable = Request.Form["EntregableC"];
                int Avance = int.Parse(Request.Form["AvanceC"]);
                int Pendiente = int.Parse(Request.Form["PendienteC"]);
                int VersionFinal = int.Parse(Request.Form["VersionFinal"]);
                string Observaciones = Request.Form["Observaciones"];
                string CodigoDocumento = Request.Form["CodigoDocumento"];


                //var FE = DateTime.ParseExact(Fecha, "dd-MM-yyyy", null);

                var TSO = Dal.Clases.TimeSheetConsultorDAL.ObjetoTimesheetEntregableEditar(Id);

                var proy = Dal.Clases.RelUsuariosDAL.DatosUsuarioProyecto(TSO.IdUsuarioCarga.Value);

                //if (FE < proy.FactProyecto.InicioProyecto)
                //{
                //    return Json(new { Cod = 3, Msg = "La fecha registrada es anterior al inicio del proyecto." }, JsonRequestBehavior.AllowGet);
                //}

                //if (FE > proy.FactProyecto.FinProyecto)
                //{
                //    return Json(new { Cod = 3, Msg = "La fecha registrada es posterior al termino del proyecto." }, JsonRequestBehavior.AllowGet);
                //}


                /*if (Aprobado == 0)
                {
                    //Validación Limite de días
                    if (FE < FechaMaxima)
                    {
                        var men = "No fue posible registrar los cambios.";
                        var existe = Dal.Clases.AprobacionTimesheetDAL.ExisteAtraso(TSO.RelUsuarioProyecto.IdUsuarioProyecto);

                        if (existe != null)
                        {
                            if (existe.EstadoSolicitud == 0)
                            {
                                men += "<hr> Existe una solicitud de excepción en espera de aprobación, una vez aprobada se habilitará la excepción para editar el registro.";
                                return Json(new { Cod = 3, Msg = men }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                if (DateTime.Today > existe.FechaValidez)
                                {
                                    var re = Dal.Clases.AprobacionTimesheetDAL.RegistrarAtraso(TSO.RelUsuarioProyecto.IdUsuarioProyecto);
                                    if (re == 1)
                                    {
                                        string TitNot = "Límite días Timesheet";
                                        string MsgNot = "Se ha registrado una solicitud de excepción al límite de días para carga de actividades del usuario <b>" + TSO.RelUsuarioProyecto.FactUsuario.Nombre + "</b>, asociada al proyecto <b>" + proy.FactProyecto.NombreProyecto + "</b>.";
                                        //Registro Notificación
                                        var djp = Dal.Clases.ProyectoDAL.DatosProyectoEP(TSO.RelUsuarioProyecto.IdProyecto);

                                        foreach (var j in djp.RelUsuarioProyecto.Where(i => i.IsJp == 1).Where(a => a.IsActivo == 1).ToList())
                                        {
                                            Dal.Clases.NotificacionDAL.RegistrarNotificacion(TSO.RelUsuarioProyecto.IdProyecto, TSO.RelUsuarioProyecto.IdUsuario, DateTime.Now, DateTime.Today, j.IdUsuario, TitNot, MsgNot, 1, 0);
                                        }
                                        men += "<hr> Se ha generado una solicitud de carga fuera de plazo al Jefe de Proyecto, una vez aprobada se habilitará la excepción para editar el registro.";
                                    }
                                    else
                                    {
                                        men += "<hr> Ocurrió un error al generar solicitud de excepción, intente nuevamente.";
                                    }
                                    return Json(new { Cod = 3, Msg = men }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    //Si la fecha validez es menor, continua ejecución de forma normal
                                }
                            }
                        }
                        else
                        {
                            var re = Dal.Clases.AprobacionTimesheetDAL.RegistrarAtraso(TSO.RelUsuarioProyecto.IdUsuarioProyecto);
                            if (re == 1)
                            {
                                string TitNot = "Límite días Timesheet";
                                string MsgNot = "Se ha registrado una solicitud de excepción al límite de días para carga de actividades del usuario <b>" + TSO.RelUsuarioProyecto.FactUsuario.Nombre + "</b>, asociada al proyecto <b>" + proy.FactProyecto.NombreProyecto + "</b>.";
                                //Registro Notificación
                                var djp = Dal.Clases.ProyectoDAL.DatosProyectoEP(TSO.RelUsuarioProyecto.IdProyecto);

                                foreach (var j in djp.RelUsuarioProyecto.Where(i => i.IsJp == 1).Where(a => a.IsActivo == 1).ToList())
                                {
                                    Dal.Clases.NotificacionDAL.RegistrarNotificacion(TSO.RelUsuarioProyecto.IdProyecto, TSO.RelUsuarioProyecto.IdUsuario, DateTime.Now, DateTime.Today, j.IdUsuario, TitNot, MsgNot, 1, 0);
                                }
                                men += "<hr> Se ha generado una solicitud de carga fuera de plazo al Jefe de Proyecto, una vez aprobada se habilitará la excepción para editar el registro.";
                            }
                            else
                            {
                                men += "<hr> Ocurrió un error al generar solicitud de excepción, intente nuevamente.";
                            }

                            return Json(new { Cod = 3, Msg = men }, JsonRequestBehavior.AllowGet);
                        }
                    }*/


                Dao.Model.FactEntregables Modificado = TSO;

                //Modificado.Fecha = FE;
                Modificado.IdProyecto = Consultoria;
                Modificado.Entregable = Entregable;
                Modificado.Avance = Avance;
                Modificado.Pendiente = Pendiente;
                Modificado.VersionFinal = VersionFinal;
                Modificado.Observaciones = Observaciones;
                Modificado.CodigoDocumento = CodigoDocumento;



                var update = Dal.Clases.TimeSheetConsultorDAL.EditarEntregableConsultor(Modificado);

                if (update == 0)
                {
                    return Json(new { Cod = 3, Msg = "No se registraron los datos, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //Falta validación de 45HH
                    return Json(new { Cod = 1, Msg = "Datos registrados exitosamente" }, JsonRequestBehavior.AllowGet);
                }
                /*}
                else
                {

                    var result = Dal.Clases.EdicionTimeSheetAprobadaDAL.RegistrarTimesheetAprobada(Id, FE, Dal.Clases.Util.GetIso8601WeekOfYear(FE), Costo, Actividad, Inicio, Termino, duracion, 1, DateTime.Now, Idgantt);

                    if (result == 1)
                    {

                        var djp = Dal.Clases.ProyectoDAL.DatosProyectoEP(TSO.RelUsuarioProyecto.IdProyecto);

                        var contNot = 0;

                        foreach (var j in djp.RelUsuarioProyecto.Where(i => i.IsJp == 1).Where(a => a.IsActivo == 1).ToList())
                        {
                            string TitNot = "Edición timesheet aprobada";
                            string MsgNot = "Se ha registrado una solicitud de edición de registro timesheet ya aprobado del usuario <b>" + TSO.RelUsuarioProyecto.FactUsuario.Nombre + "</b>, asociada al proyecto <b>" + proy.FactProyecto.NombreProyecto + "</b>.";
                            contNot = contNot + Dal.Clases.NotificacionDAL.RegistrarNotificacion(TSO.RelUsuarioProyecto.IdProyecto, TSO.RelUsuarioProyecto.IdUsuario, DateTime.Now, DateTime.Today, j.IdUsuario, TitNot, MsgNot, 2, 0);
                        }

                        if (contNot > 0)
                        {
                            return Json(new { Cod = 1, Msg = "Datos registrados exitosamente.<hr> Se ha generado una solicitud de aprobación al jefe de proyecto, mientras la solicitud no sea resuelta se verán reflejados en el registro los datos ya aprobados." });

                        }
                        else
                        {
                            return Json(new { Cod = 2, Msg = "Datos registrados exitosamente.<hr> Ocurrió un problema al generar la notificación de aprobación al jefe de proyecto." });
                        }
                    }
                    else
                    {
                        return Json(new { Cod = 3, Msg = "Ocurrió un problema al guardar los datos, intente nuevamente." });
                    }
                }*/
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ListaEntregablesAutocompletar()
        {
            try
            {
                int Id = int.Parse(Request.Form["id"]);
                var lista = Dal.Clases.EntregableDAL.ListaEntregables(Id);
                var arreglo = new string[lista.Count];
                var contador = 0;
                foreach (var item in lista) { arreglo[contador] = item.Entregable; contador++; };

                return Json(new { Cod = 1, Msg = "OK", lista = arreglo }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult getdataentregable()
        {
            try
            {
                var entregable = Request.Form["txt"];

                var resultado = Dal.Clases.TimeSheetConsultorDAL.ObjetoTimesheetEntregableEditar(entregable);

                if (resultado == null)
                {
                    return Json(new { Cod = 2, Msg = "No se puede encontrar la timesheet seleccionada." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var ProyectoC = resultado.IdProyecto;
                    var EntregableC = resultado.Entregable;
                    var AvanceC = resultado.Avance;
                    var PendienteC = resultado.Pendiente;
                    var VersionFinal = resultado.VersionFinal;
                    var Observaciones = resultado.Observaciones;
                    var CodigoDocumento = resultado.CodigoDocumento;


                    return Json(new { Cod = 1, Msg = "OK.", ProyectoC, EntregableC, AvanceC, PendienteC, VersionFinal, Observaciones, CodigoDocumento }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult EliminarTimeSheetConsultor()
        {
            try
            {
                int IdTimesheetConsultor = int.Parse(Request.Form["IdTimesheetConsultor"]);
                var resultado = Dal.Clases.TimeSheetConsultorDAL.EliminarTimeSheetConsultor(IdTimesheetConsultor);

                if (resultado == 1)
                {
                    return Json(new { Cod = 1, Msg = "Registro eliminado" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Cod = 1, Msg = "No fue posible eliminar el registro, intente nuevamente." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 2, Msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}

﻿$(document).ready(function () {

    $('.datatable').dataTable();

    $('.js-example-basic-single').select2({
        placeholder: "seleccione proyecto",
        allowClear: true,
        width: '100%'
    });

    $('.costo').select2({
        placeholder: "seleccione costo",
        allowClear: true,
        width: '100%'
    });

    /* initialize the calendar
    -----------------------------------------------------------------*/
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        editable: true,
        droppable: true, // this allows things to be dropped onto the calendar
        drop: function () {
            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }
        },
        /*events: [
            {
                title: 'All Day Event',
                start: new Date(y, m, 1)
            },
            {
                title: 'Long Event',
                start: new Date(y, m, d - 5),
                end: new Date(y, m, d - 2)
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: new Date(y, m, d - 3, 16, 0),
                allDay: false
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: new Date(y, m, d + 4, 16, 0),
                allDay: false
            },
            {
                title: 'Meeting',
                start: new Date(y, m, d, 10, 30),
                allDay: false
            },
            {
                title: 'Lunch',
                start: new Date(y, m, d, 12, 0),
                end: new Date(y, m, d, 14, 0),
                allDay: false
            },
            {
                title: 'Birthday Party',
                start: new Date(y, m, d + 1, 19, 0),
                end: new Date(y, m, d + 1, 22, 30),
                allDay: false
            },
            {
                title: 'Click for Google',
                start: new Date(y, m, 28),
                end: new Date(y, m, 29),
                url: 'http://google.com/'
            }
        ]*/
    });

    $('#inputDatepicker').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        daysOfWeekDisabled: "0,6"
    });

});


function fnClickAddRow() {
    var dataRow = [];
    $('.form-add input').each(function () {
        dataRow1 = $(this).val();
        dataRow.push(dataRow1);
    });
    $('.datatable').dataTable().fnAddData(dataRow);
}

$('.datatable').on('click', '.RemoveRow', function () {

    var row = $(this).closest('tr');
    var nRow = row[0];
    $('.dataTable').dataTable().fnDeleteRow(nRow);

})


﻿$(document).ready(function () {

    var hoy = new Date();
    window.dd = hoy.getDate().toString().padStart(2, "0");
    window.mm = (hoy.getMonth() + 1).toString().padStart(2, "0");
    window.yyyy = hoy.getFullYear();

    window.contador = 0;
    var pathname = window.location.pathname;
    if (pathname == "/Timesheet") {
        ListadoProyectosPerfil();
        ListadoProyectosRegistro();
    }
    else {
        ListadoProyectosRegistro();
    }

    if ($('.datepicker').length) {
        CrearDateP();
    }

    if ($('.clockpicker').length) {
        $('.clockpicker').clockpicker({ autoclose: true });
    }

    if ($('#periodo').length) {
        moment.locale('es', {
            months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
            monthsShort: 'Enero._Feb._Mar_Abr._May_Jun_Jul._Ago_Sept._Oct._Nov._Dec.'.split('_'),
            weekdays: 'Domingo_Lunes_Martes_Miercoles_Jueves_Viernes_Sabado'.split('_'),
            weekdaysShort: 'Dom._Lun._Mar._Mier._Jue._Vier._Sab.'.split('_'),
            weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_')
        }
        );


        //window.fdesde = moment().subtract(6, 'days');
        window.fdesde = moment().startOf('isoWeek');
        //window.fhasta = moment();
        window.fhasta = moment().endOf('isoWeek');


        $('#periodo').daterangepicker({
            "locale": {
                "format": "DD-MM-YYYY",
                "separator": " - ",
                "applyLabel": "Guardar",
                "cancelLabel": "Cancelar",
                "fromLabel": "Desde",
                "toLabel": "Hasta",
                "customRangeLabel": "Personalizar",
                "daysOfWeek": [
                    "Do",
                    "Lu",
                    "Ma",
                    "Mi",
                    "Ju",
                    "Vi",
                    "Sa"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Setiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "firstDay": 1
            },
            startDate: fdesde,
            endDate: fhasta,
            ranges: {
                //'Today': [moment(), moment()],
                //'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Semana Actual': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
                'Últimos 7 Días': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 Días': [moment().subtract(29, 'days'), moment()],
                'Mes Actual': [moment().startOf('month'), moment().endOf('month')],
                'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, OkFecha);

        OkFecha(fdesde, fhasta);
    }

    if ($('#tblTimesheet').length) {

        var jp = $('#UserJP').val();
        var adm = $('#UserAdmin').val();

        if (adm == 1 || jp == 1) {
            window.tabla = $('#tblTimesheet').DataTable({
                paging: true,
                ordering: true,
                info: false,
                bFilter: true,
                autoWidth: false,
                columnDefs: [{ targets: [0, 1], class: "text-nowrap" }, { "orderable": false, "targets": 9 }/*{ targets: [8], width: 70 }*/],
                responsive: true,
                language: {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                pageLength: 10
            });
        }
        else {
            window.tabla = $('#tblTimesheet').DataTable({
                paging: true,
                ordering: true,
                info: false,
                bFilter: true,
                autoWidth: false,
                columnDefs: [{ targets: [0, 1], class: "text-nowrap" }, { "orderable": false, "targets": 8 }/*{ targets: [8], width: 70 }*/],
                responsive: true,
                language: {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                pageLength: 10
            });
        }





        ListadoUsuarios();
        //ListadoProyectos(); //Todos los Proyectos
        //ListadoProyectosRegistro();



        //Carga al iniciar pagina
        ListadoTimesheet(true);

    }

    $("#btnBuscar").on('click', function () {
        ListadoTimesheet(false);
    });

    $("#solicitarexcepcion").on('click', function () {
        CargaModalExcepcionCarga();
    });

    $("#btnGenerarSolicitud").on('click', function () {
        SolicitarExcepcionCarga();
    });

    $("#btnAdd").on('click', function () {
        if (ValidarFormulario(0) == 1) {
            CrearFila();
        }
    });

    $('#form-timesheet').on('submit', function (e) {
        e.preventDefault();
        var err = 0;
        $("#form-timesheet input").removeClass("erI");
        $("#form-timesheet select").removeClass("erI");

        if (ValidarFormulario(1) == 1) {
            var datos = $("#form-timesheet").serialize();

            var l = Ladda.create(document.querySelector('#btnGuardar'));
            l.start();

            $.ajax({
                url: "GuardarTimesheet",
                data: datos,
                cache: false,
                dataType: "json",
                type: 'POST',
                success: function (result) {
                    if (result.Cod == 1) {
                        swal({
                            title: "TimeSheet",
                            text: result.Msg,
                            html: result.Msg,
                            type: "success"
                        },
                            function () {
                                window.location.href = '/Timesheet';
                            });
                    } else {
                        swal({
                            title: 'TimeSheet',
                            text: result.Msg,
                            html: result.Msg,
                            type: 'warning'
                        });
                    }
                    l.stop();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    swal("TimeSheet", jqXHR.responseText, "error");
                    l.stop();
                }
            });
        }
    });

    $('#form-editar').on('submit', function (e) {
        e.preventDefault();
        var err = 0;
        $("#form-editar input").removeClass("erI");
        $("#form-editar select").removeClass("erI");
        var l = Ladda.create(document.querySelector('#btnGuardar'));
        l.start();
        if (ValidarEdicion() == 1) {
            var datos = $("#form-editar").serialize();
            $.ajax({
                url: "/Timesheet/EditarTimesheet",
                data: datos,
                cache: false,
                dataType: "json",
                type: 'POST',
                success: function (result) {
                    if (result.Cod == 1) {
                        swal({
                            title: "TimeSheet",
                            text: result.Msg,
                            html: result.Msg,
                            type: "success"
                        },
                            function () {
                                ListadoTimesheet(false);
                                $("#mdlEditar").modal("toggle");
                                l.stop();
                            });
                    }
                    else {
                        swal({
                            title: 'TimeSheet',
                            text: result.Msg,
                            html: result.Msg,
                            type: 'warning'
                        });
                        l.stop();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    swal("TimeSheet", jqXHR.responseText, "error");
                    l.stop();
                }
            });
        }
        else { l.stop(); }
    });

    $(document).on('change', '#form-timesheet input', function () {
        $(this).removeClass("erI");
    });

    $(document).on('change', '#form-editar select', function () {
        $(this).removeClass("erI");
    });

    $(document).on('change', '#form-editar input', function () {
        $(this).removeClass("erI");
    });

    $(document).on('change', '#form-timesheet select', function () {
        $(this).removeClass("erI");
    });

    $("#tblTimesheet").on('click', '.edt', function () {

        $("titModal").html("Editar Timesheet");

        $("#IdTimesheetAprobada").val(0);

        var idt = $(this).attr('id');
        $("#IdTimesheet").val(idt);
        $("#Proyecto").empty();
        var IU = $("#IdUsuario").val();
        $.ajax({
            data: { usuario: IU },
            url: '/Timesheet/ListadoProyectosRegistro',
            type: 'POST',
            success: function (result) {
                if (result.Datos.length) {
                    $.each(result.Datos, function (i, item) {
                        var str = '<option value="' + item.IdRelacion + '">' + item.Proyecto + '</value>';
                        var res = str.replaceAll("Ç", "'");
                        $("#Proyecto").append(res);
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });

        var gant = 0;

        $.ajax({
            url: 'Timesheet/DatosTimeSheet',
            data: { IdTimesheet: idt },
            type: 'POST',
            success: function (result) {
                if (result.Cod == 1) {
                    $("#Fecha").datepicker("destroy");
                    $("#Fecha").val(result.Datos.Fecha);
                    $("#Proyecto").val(result.Datos.Proyecto);
                    gant = result.Datos.Gantt;
                    $("#Inicio").val(result.Datos.Inicio);
                    $("#Termino").val(result.Datos.Termino);
                    $("#Costo").empty();
                    if (result.Datos.AplicaCosto == 1) {
                        $("#Costo").append('<option value="2">Directo</option>');
                        $("#Costo").append('<option value="3">Indirecto</option>');
                        $("#Costo").val(result.Datos.Costo);
                    }
                    else {
                        $("#Costo").append('<option value="1">No Aplica</option>');
                    }
                    $("#Actividad").val(result.Datos.Actividad);
                    $("#mdlEditar").modal("toggle");
                    $("#Fecha").datepicker({
                        language: 'es',
                        autoclose: true,
                        format: 'dd-mm-yyyy',
                        orientation: 'bottom'
                    });
                    $("#Fecha").datepicker("refresh");

                    CargaOpcionesProyecto(gant);

                    //$.ajax({
                    //    url: 'Timesheet/CargaListaActividades',
                    //    data: { project: result.Datos.Proyecto },
                    //    type: 'POST',
                    //    success: function (result) {
                    //        $("#Gantt").empty();
                    //        if (result.Cod == 1) {

                    //            $("#Gantt").append('<option value="" disabled>Seleccione</option>');

                    //            $.each(result.Datos, function (i, item) {
                    //                $("#Gantt").append("<option value='" + item.id + "'>" + item.descripcion + "</value>");
                    //            });

                    //            $("#Gantt").append("<option value='0'>Otro item</value>");

                    //            $("#Gantt").val(gant);
                    //        }
                    //        else {
                    //            $("#Gantt").append("<option value='0'>Sin items asignados</value>");
                    //        }
                    //    },
                    //    error: function (jqXHR, textStatus, errorThrown) {
                    //        //console.log(jqXHR, textStatus, errorThrown);
                    //        console.log(jqXHR);
                    //        swal("Items Gantt", "Ocurrió un error al procesar la solicitud", "error");
                    //        $("#Gantt").empty();
                    //    }
                    //});
                }
                else {
                    swal("TimeSheet", result.Msg, "warning");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });
    });

    $("#tblTimesheet").on('click', '.edapro', function () {

        $("#titModal").html("Edición Timesheet aprobada");
        $("#IdTimesheetAprobada").val(1);

        var idt = $(this).attr('id');
        $("#IdTimesheet").val(idt);

        $("#Proyecto").empty();
        var IU = $("#IdUsuario").val();
        $.ajax({
            data: { usuario: IU },
            url: '/Timesheet/ListadoProyectosRegistro',
            type: 'POST',
            success: function (result) {
                if (result.Datos.length) {
                    $.each(result.Datos, function (i, item) {

                        var str = '<option value="' + item.IdRelacion + '">' + item.Proyecto + '</value>';
                        var res = str.replaceAll("Ç", "'");
                        $("#Proyecto").append(res);
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });

        var gant = 0;

        $.ajax({
            url: 'Timesheet/DatosTimeSheet',
            data: { IdTimesheet: idt },
            type: 'POST',
            success: function (result) {
                if (result.Cod == 1) {
                    $("#Fecha").datepicker("destroy");
                    $("#Fecha").val(result.Datos.Fecha);
                    $("#Proyecto").val(result.Datos.Proyecto);
                    gant = result.Datos.Gantt;
                    $("#Inicio").val(result.Datos.Inicio);
                    $("#Termino").val(result.Datos.Termino);
                    $("#Costo").empty();
                    if (result.Datos.AplicaCosto == 1) {
                        $("#Costo").append('<option value="2">Directo</option>');
                        $("#Costo").append('<option value="3">Indirecto</option>');
                        $("#Costo").val(result.Datos.Costo);
                    }
                    else {
                        $("#Costo").append('<option value="1">No Aplica</option>');
                    }
                    $("#Actividad").val(result.Datos.Actividad);
                    $("#mdlEditar").modal("toggle");
                    $("#Fecha").datepicker({
                        language: 'es',
                        autoclose: true,
                        format: 'dd-mm-yyyy',
                        orientation: 'bottom'
                    });
                    $("#Fecha").datepicker("refresh");

                    CargaOpcionesProyecto(gant);

                    //$.ajax({
                    //    url: 'Timesheet/CargaListaActividades',
                    //    data: { project: result.Datos.Proyecto },
                    //    type: 'POST',
                    //    success: function (result) {
                    //        $("#Gantt").empty();
                    //        if (result.Cod == 1) {

                    //            $("#Gantt").append('<option value="" disabled>Seleccione</option>');

                    //            $.each(result.Datos, function (i, item) {
                    //                $("#Gantt").append("<option value='" + item.id + "'>" + item.descripcion + "</value>");
                    //            });

                    //            $("#Gantt").append("<option value='0'>Otro item</value>");

                    //            $("#Gantt").val(gant);
                    //        }
                    //        else {
                    //            $("#Gantt").append("<option value='0'>Sin items asignados</value>");
                    //        }
                    //    },
                    //    error: function (jqXHR, textStatus, errorThrown) {
                    //        //console.log(jqXHR, textStatus, errorThrown);
                    //        console.log(jqXHR);
                    //        swal("Items Gantt", "Ocurrió un error al procesar la solicitud", "error");
                    //        $("#Gantt").empty();
                    //    }
                    //});
                }
                else {
                    swal("TimeSheet", result.Msg, "warning");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });
    });

    $("#tblTimesheet").on('click', '.del', function () {
        var idt = $(this).attr('id');

        var l = Ladda.create(this);

        swal({
            title: "Confirmación",
            text: "¿Está seguro que desea eliminar el registro seleccionado?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Aceptar',
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    l.start();
                    $.ajax({
                        url: 'Timesheet/EliminarTimeSheet',
                        data: { IdTimesheet: idt },
                        type: 'POST',
                        success: function (result) {
                            if (result.Cod == 1) {
                                ListadoTimesheet(false);
                                swal("TimeSheet", result.Msg, "success");
                            }
                            else {
                                swal("TimeSheet", result.Msg, "warning");
                                l.stop();
                            }

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            //console.log(jqXHR, textStatus, errorThrown);
                            console.log(jqXHR);
                            l.stop();
                        }
                    });

                }
            });
    });

    $(document).on('change', '#Proyecto', function () {
        CargaOpcionesProyecto();
    });

});

function OkFecha(start, end) {
    //$('#periodo span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    $('#periodo span').html(start.format('DD MMM YYYY') + ' al ' + end.format('DD MMM YYYY'));
    window.fdesde = start.format('DD-MM-YYYY');
    window.fhasta = end.format('DD-MM-YYYY');
}

function ListadoTimesheet(cargainicial) {

    var IdUsuario = $("#usuario").val();
    var IdProyecto = $("#proyectoB").val();

    $.ajax({
        url: 'Timesheet/ListadoTimesheet',
        type: 'POST',
        data: { desde: window.fdesde, hasta: window.fhasta, usuario: IdUsuario, proyecto: IdProyecto },
        success: function (result) {
            tabla.clear().draw();

            switch (result.Cod) {
                case 1:
                    $.each(result.Datos, function (i, item) {
                        var btn = "<div class='btn-group'>";

                        if (item.IdTimesheet > 0) {
                            if (item.Aprobado == 0) {
                                btn += "<button type='button' id='" + item.IdTimesheet + "' class='btn btn-sm btn-success edt' data-toggle='tooltip' data-placement='top' title='' data-original-title='Editar actividad.'><i class='fa fa-edit'></i></button>";
                            }
                            else {
                                btn += "<button type='button' id='" + item.IdTimesheet + "' class='btn btn-sm btn-default edapro' data-toggle='tooltip' data-placement='top' title='' data-original-title='Actividad aprobada. Presione para editar.'><i class='fa fa-edit'></i></button>";

                            }
                            btn += "<button type='button' id='" + item.IdTimesheet + "' class='btn btn-sm btn-danger ladda-button del' data-style='zoom-in'><i class='fa fa-times'></i></button>";
                        }
                        btn += '</div>';

                        if ($("#UserAdmin").val() == 1 || $("#UserJP").val() == 1) {
                            tabla.row.add([
                                item.Usuario,
                                item.Fecha,
                                item.HI,
                                item.HF,
                                item.HT,
                                item.Itemgantt,
                                item.Actividad,
                                item.Proyecto,
                                item.Costo,
                                btn
                            ]).draw(false);
                        }
                        else {
                            tabla.row.add([
                                item.Fecha,
                                item.HI,
                                item.HF,
                                item.HT,
                                item.Itemgantt,
                                item.Actividad,
                                item.Proyecto,
                                item.Costo,
                                btn
                            ]).draw(false);
                        }
                    });
                    $('[data-toggle="tooltip"]').tooltip();
                    break;
                case 2:
                    if (!cargainicial) {
                        swal("TimeSheet", result.Msg, "warning");
                    }
                    break;
                default:
                    swal("TimeSheet", result.Msg, "error");
                    break;
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });

}

$("#tblRegistro").on('click', '.del', function () {
    $(this).parent().parent().remove();
    contador -= 1;
});

function ListadoUsuarios() {
    if ($("#UserAdmin").val() == "1") {
        $.ajax({
            url: 'Usuarios/ListaUsuarios',
            type: 'POST',
            success: function (result) {
                $.each(result.Datos, function (i, item) {
                    $("#usuario").append("<option value='" + item.IdUsuario + "'>" + item.Nombre + "</value>");
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });
    }
    else if ($("#UserJP").val() == "1") {
        var UserJp = $("#IdUsuario").val();
        $.ajax({
            url: 'Timesheet/UsuariosProyectoDash',
            data: { IdJP: UserJp },
            type: 'POST',
            success: function (result) {
                $.each(result.Datos, function (i, item) {
                    $("#usuario").append("<option value='" + item.IdUsuario + "'>" + item.Nombre + "</value>");
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });
    }


}

function ListadoProyectos() {
    $.ajax({
        url: 'Proyectos/ListaProyectos',
        type: 'POST',
        success: function (result) {
            $.each(result.Datos, function (i, item) {
                $("#Proyecto").append("<option value='" + item.IdProyecto + "'>" + item.Proyecto + "</value>");
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });
}

function ListadoProyectosPerfil() {
    var usu = $("#IdUsuario").val();

    var urlp = ($("#UserAdmin").val() == "1" ? 'Proyectos/ListaProyectos' : '/Timesheet/ListadoProyectosPerfil');
    $.ajax({
        url: urlp,
        data: { usuario: usu },
        type: 'POST',
        success: function (result) {
            $.each(result.Datos, function (i, item) {
                $("#proyectoB").append("<option value='" + item.IdProyecto + "'>" + item.Proyecto + "</value>");
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });
}

function ListadoProyectosRegistro() {

    var IU = $("#IdUsuario").val();
    $.ajax({
        data: { usuario: IU },
        url: '/Timesheet/ListadoProyectosRegistro',
        type: 'POST',
        success: function (result) {
            if (result.Datos.length) {
                $("#SinPro").hide();
                $("#tblRegistro").show();
                $.each(result.Datos, function (i, item) {
                    $(".Proyecto0").append("<option value='" + item.IdRelacion + "'>" + item.Proyecto + "</value>");
                });
            }
            else {
                $("#tblRegistro").hide();
                $("#SinPro").show();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });
}

function ListadoProyectosRegistroCarga(id) {
    var IU = $("#IdUsuario").val();
    $.ajax({
        data: { usuario: IU },
        url: '/Timesheet/ListadoProyectosRegistro',
        type: 'POST',
        success: function (result) {
            if (result.Datos.length) {
                $("#SinPro").hide();
                $("#tblRegistro").show();
                $.each(result.Datos, function (i, item) {
                    $("." + id).append("<option value='" + item.IdRelacion + "'>" + item.Proyecto + "</value>");
                });
            }
            else {
                $("#tblRegistro").hide();
                $("#SinPro").show();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });
}

function ListadoCostosRegistro() {

    // Falta controlador de tipo costo
    window.pros = [];

    $.ajax({
        url: '../Proyectos/ListaProyectos',
        type: 'POST',
        success: function (result) {
            $.each(result.Datos, function (i, item) {
                pros[item.IdProyecto] = item.Proyecto;
                $(".proye").append("<option value='" + item.IdProyecto + "'>" + item.Proyecto + "</value>");
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });
}

function CrearFila() {

    var anterior = contador;
    contador += 1;


    var filas;

    filas = $("#tblRegistro tbody tr:last");

    var f;
    var c;

    $.each(filas, function (index, item) {
        var tds = $(item).find("td");

        f = tds.find("[name='Fecha']").val();

        c = tds.find("[name='Costo']").val();
    });

    var fila = "<tr>";

    var str = '<td><select class="form-control input-sm Proyecto' + contador + '" name="Proyectos" id="Proyectos" onchange="ListadoTipoActividades(ÇProyecto' + contador + 'Ç,ÇTipoAct' + contador + 'Ç,' + contador + ')"><option value="">Seleccione</option></select></td>';
    var res = str.replaceAll("Ç", "'");
    fila += res;

    fila += "<td><input type='text' class='form-control input-sm datepicker fecha" + contador + "' value='" + f + "' name='Fecha' id='Fecha' autocomplete='off'></td>";


    var str = '<td><select class="form-control input-sm TipoAct' + contador + '" name="TipoAct" id="TipoAct"><option value="">Seleccione proyecto</option></select></td>';
    var res = str.replaceAll("Ç", "'");
    fila += res;

    fila += "<td><input type='text' class='form-control input-sm' name='Actividad' id='Actividad' autocomplete='off'></td>";

    var aplicacosto = $('#AplicaCosto').val();

    if (aplicacosto == 1) {
        fila += '<td><select class="form-control input-sm" name="Costo" id = "Costo"><option value="">Seleccione</option><option value="2" ' + (c == 2 ? "selected" : "") + ' >Directo</option><option value="3" ' + (c == 3 ? "selected" : "") + '>Indirecto</option></select></td>';
    }
    else {
        fila += '<td><select class="form-control input-sm" name="Costo' + contador + '" id="Costo' + contador + '"><option value="1"' + (c == 1 ? "selected" : "") + '>No Aplica</option></select></td>';
    }

    fila += "<td><input type='text' class='form-control input-sm clockpicker' name='Inicio' id='Inicio' autocomplete='off'></td>";
    fila += "<td><input type='text' class='form-control input-sm clockpicker' name='Termino' id='Termino'  autocomplete='off'></td>";
    fila += "<td><button type='button' class='btn btn-sm btn-danger btn-circle del'><i class='fa fa-minus'></i></td>";
    fila += "</tr>";
    $("#tblRegistro").append(fila);

    //$("#Proyecto").clone().appendTo("#divP" + contador);
    ListadoProyectosRegistroCarga('Proyecto' + contador);

    //$("#Costo0").clone().appendTo("#divC" + contador);

    CrearDateP();
    CrearClockP();




    setTimeout(function () {
        var p = $('.Proyecto' + anterior).val();
        $('.Proyecto' + contador).val(p);

        ListadoTipoActividades('Proyecto' + contador, 'TipoAct' + contador, contador);

        setTimeout(function () {
            var p = $('.TipoAct' + anterior).val();
            $('.TipoAct' + contador).val(p);
        }, 500);
    }, 500);

}

function CrearDateP() {

    var fechamaximatxt = $("#fechaMaxinatxt").val();

    $('.datepicker').datepicker({
        language: 'es',
        autoclose: true,
        format: 'dd-mm-yyyy',
        orientation: 'bottom',
        startDate: fechamaximatxt
    });

}

function CrearClockP() {
    $('.clockpicker').clockpicker({
        autoclose: true
    });
}

function LlenarPros(select) {

    $.each(window.pros, function (i, item) {

        $(sele).append("<option value='" + item.IdProyecto + "'>" + item.Proyecto + "</value>");
    });
}


function ValidarFormulario(form = 0) {

    var filas;
    var err = 0;

    if (form == 0) {
        filas = $("#tblRegistro tbody tr:last");
    }
    else {
        filas = $("#tblRegistro tbody tr");
    }

    $.each(filas, function (index, item) {

        var tds = $(item).find("td");

        var f = tds.find("[name='Fecha']");
        var i = tds.find("[name='Inicio']");
        var t = tds.find("[name='Termino']");

        if (i.val() != "" && t.val() != "") {

            var fs = f.val().split("-");
            var is = i.val().split(":");
            var ts = t.val().split(":");

            //var fi = new Date();
            var fi = new Date(fs[2], fs[1] - 1, fs[0]);
            fi.setHours(is[0], is[1], 0, 0);
            //var ft = new Date();
            var ft = new Date(fs[2], fs[1] - 1, fs[0]);
            ft.setHours(ts[0], ts[1], 0, 0);
        }

        /* Validaciones */
        i.removeClass("erI");
        t.removeClass("erI");
        $(tds.find("input")).each(function (i, n) {
            if ($(this).val().length < 3) {
                $(this).addClass("erI");
                err++;
            }
        });
        $(tds.find("select")).each(function (i, n) {

            if ($(this).val() == null) {
                $(this).addClass("erI");
                err++;
            }
            else {
                if ($(this).val().length == 0) {
                    $(this).addClass("erI");
                    err++;
                }
                if ($(this).val() == '') {
                    $(this).addClass("erI");
                    err++;
                }
            }

        });

        if (fi >= ft) {
            swal("Timesheet", "La hora de término no puede ser menor o igual a la hora de inicio ", "info");
            //i.addClass("erI");
            t.addClass("erI");
            err++;
            return 0;
        }

        var ante = $(item).closest('tr').prev().find("td");
        if (ante.length) {
            var fAnt = ante.find("[name='Fecha']");
            var tAnt = ante.find("[name='Termino']");
            var tsAnt = tAnt.val().split(":");

            var fsAnt = fAnt.val().split("-");
            var ftAnt = new Date(fsAnt[2], fsAnt[1] - 1, fsAnt[0]);



            //var FV1 = fi;
            var FV1 = new Date(fs[2], fs[1] - 1, fs[0]);
            //FV1 = FV1.setHours(0, 0, 0, 0);
            FV1.setHours(0, 0, 0, 0);
            var FV2 = new Date(fsAnt[2], fsAnt[1] - 1, fsAnt[0]);
            //FV2 = FV2.setHours(0, 0, 0, 0);
            FV2.setHours(0, 0, 0, 0);

            //var ftAnt = new Date();
            ftAnt.setHours(tsAnt[0], tsAnt[1], 0, 0);

            if (i.val() != "" && t.val() != "") {



                if (FV1.valueOf() == FV2.valueOf()) {

                    //console.log("I Actual " + fi);
                    //console.log("F Anteri " + ftAnt);

                    if (fi.valueOf() < ftAnt.valueOf()) {
                        swal("Timesheet", "La hora inicio no puede ser menor a la hora de término anterior.", "info");
                        //i.addClass("erI");
                        i.addClass("erI");
                        err++;
                        return 0;
                    }
                }
            }
        }



        /*
        if (fi >= ft) {
            swal("Timesheet", "La hora de termino no puede ser menor o igual a la hora de inicio ", "info");
            //i.addClass("erI");
            t.addClass("erI");
            err++;
        }
        else if (ftAnt > fi ) {
            swal("Timesheet", "La hora inicio no puede ser menor a la hora de termino anterior.", "info");
            //i.addClass("erI");
            i.addClass("erI");
            err++;
        }
        else {
            //i.removeClass("erI");

            i.removeClass("erI");
            t.removeClass("erI");
            $(tds.find("input")).each(function (i, n) {
                if ($(this).val().length < 3) {
                    $(this).addClass("erI");
                    err++;
                }
            });
            $(tds.find("select")).each(function (i, n) {
                if ($(this).val().length == 0) {
                    $(this).addClass("erI");
                    err++;
                }
            });
        }
        */


    });

    return (err == 0 ? 1 : 0);

}

function ValidarEdicion() {

    var err = 0;

    var i = $("#Inicio");
    var t = $("#Termino");

    if (i.val() != "" && t.val() != "") {

        var is = i.val().split(":");
        var ts = t.val().split(":");

        var fi = new Date();
        fi.setHours(is[0], is[1], 0, 0);
        var ft = new Date();
        ft.setHours(ts[0], ts[1], 0, 0);
    }


    if (fi >= ft) {
        swal("Timesheet", "La hora de término no puede ser menor o igual a la hora de inicio.", "info");
        //i.addClass("erI");
        t.addClass("erI");
        err++;
        return 0;
    }

    /* Validaciones */
    i.removeClass("erI");
    t.removeClass("erI");

    $($("#form-editar").find("input")).each(function (i, n) {
        if ($(this).val().length < 1) {
            $(this).addClass("erI");
            err++;
            console.log('error en input' + $(this).attr('id'));
        }
    });
    $($("#form-editar").find("select")).each(function (i, n) {
        if ($(this).val() == null) {
            $(this).addClass("erI");
            err++;
            console.log('error en select' + $(this).attr('id'));
        }
        else {
            if ($(this).val().length == 0) {
                $(this).addClass("erI");
                err++;
                console.log('error en select' + $(this).attr('id'));
            }
        }

    });

    return (err == 0 ? 1 : 0);
}

function ListadoTipoActividades(proyecto, lista, contador) {

    var project = $("." + proyecto).val();

    $.ajax({
        url: 'CargaListaActividades',
        data: { project: project },
        type: 'POST',
        success: function (result) {
            $('.' + lista).empty();
            if (result.Cod == 1) {

                $("." + lista).append('<option value="" selected disabled>Seleccione</option>');

                $.each(result.Datos, function (i, item) {
                    $("." + lista).append("<option value='" + item.id + "'>" + item.descripcion + "</value>");
                });

                $("." + lista).append("<option value='0'>Otro item</value>");
            }
            else {
                $("." + lista).append("<option value='0'>Sin items asignados</value>");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
            swal("Items Gantt", "Ocurrió un error al procesar la solicitud", "error");
            $('.' + lista).empty();
        }
    });

    validaExcepcionCarga(project, contador);

}

function validaExcepcionCarga(proyecto, contador) {

    $.ajax({
        url: 'ValidaExcepcionCarga',
        data: { project: proyecto },
        type: 'POST',
        success: function (result) {
            switch (result.Cod) {
                case 1:

                    $('.fecha' + contador).datepicker("destroy");

                    $('.fecha' + contador).datepicker({
                        language: 'es',
                        autoclose: true,
                        format: 'dd-mm-yyyy',
                        orientation: 'bottom'
                    });

                    $('.fecha' + contador).datepicker("refresh");

                    break;
                case 2:
                    var fechamaximatxt = $("#fechaMaxinatxt").val();

                    $('.fecha' + contador).datepicker("destroy");

                    $('.fecha' + contador).datepicker({
                        language: 'es',
                        autoclose: true,
                        format: 'dd-mm-yyyy',
                        orientation: 'bottom',
                        startDate: fechamaximatxt
                    });

                    $('.fecha' + contador).datepicker("refresh");
                    break;
                default:
                    swal("TimeSheet", result.Msg, "error");
                    break;
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
            swal("Consulta excepción de carga", "Ocurrió un error al procesar la solicitud", "error");
        }
    });
}

function CargaOpcionesProyecto(gant) {
    var project = $("#Proyecto").val();

    $.ajax({
        url: '/Timesheet/CargaListaActividades',
        data: { project: project },
        type: 'POST',
        success: function (result) {
            $("#Gantt").empty();
            if (result.Cod == 1) {

                $("#Gantt").append('<option value="" disabled>Seleccione</option>');

                $.each(result.Datos, function (i, item) {
                    $("#Gantt").append("<option value='" + item.id + "'>" + item.descripcion + "</value>");
                });

                $("#Gantt").append("<option value='0'>Otro item</value>");

                $("#Gantt").val(gant);
            }
            else {
                $("#Gantt").append("<option value='0'>Sin items asignados</value>");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
            swal("Items Gantt", "Ocurrió un error al procesar la solicitud", "error");
            $("#Gantt").empty();
        }
    });
}



function CargaModalExcepcionCarga() {

    var IU = $("#IdUsuario").val();

    $('#ProyectoSol').empty();

    $.ajax({
        data: { usuario: IU },
        url: 'ListadoProyectosRegistro',
        type: 'POST',
        success: function (result) {
            if (result.Datos.length) {
                $.each(result.Datos, function (i, item) {
                    $("#ProyectoSol").append("<option value='" + item.IdRelacion + "'>" + item.Proyecto + "</value>");
                });
                $("#mdlSolicitud").modal("toggle");
            }
            else {
                swal("Solicitud Excepción de carga", "No se encontraron proyectos para el usuario.", "warning");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
            swal("Solicitud Excepción de carga", "Ocurrió un error al procesar la solicitud", "error");
        }
    });

}

function SolicitarExcepcionCarga() {

    var project = $("#ProyectoSol").val();

    $.ajax({
        url: 'SolicitudExcepcion',
        data: { project: project },
        type: 'POST',
        success: function (result) {
            switch (result.Cod) {
                case 1:
                    $("#mdlSolicitud").modal("toggle");
                    swal("TimeSheet", result.Msg, "success");
                    break;
                case 2:
                    swal("TimeSheet", result.Msg, "warning");
                    break;
                default:
                    swal("TimeSheet", result.Msg, "error");
                    break;
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
            swal("Solicitud Excepción de carga", "Ocurrió un error al procesar la solicitud", "error");
        }
    });


}
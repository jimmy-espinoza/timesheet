﻿$(document).ready(function () {

    //***** Desarollo *****//
    if ($('#grafico').length) {
        var ctx2 = document.getElementById("grafico").getContext("2d");
        //new Chart(ctx2, { type: 'horizontalBar', data: barData, options: { responsive: true } });
        //window.Grafico = new Chart(ctx2, { type: 'horizontalBar', options: { responsive: true } });

        var opciones = {
            responsive: true,
            title: {
                display: false,
                text: 'Título del Grafico'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    ticks: { "beginAtZero": true },
                    scaleLabel: {
                        display: false,
                        labelString: 'HH'
                    }
                }],
                yAxes: [{
                    display: true,
                    ticks: { "beginAtZero": true },
                    scaleLabel: {
                        display: false,
                        labelString: 'Empresa'
                    }
                }]
            }
        };

        window.Grafico = new Chart(ctx2, { type: 'horizontalBar', options: opciones });
    }

    if ($('#periodo').length) {
        moment.locale('es', {
            months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
            monthsShort: 'Enero._Feb._Mar_Abr._May_Jun_Jul._Ago_Sept._Oct._Nov._Dec.'.split('_'),
            weekdays: 'Domingo_Lunes_Martes_Miercoles_Jueves_Viernes_Sabado'.split('_'),
            weekdaysShort: 'Dom._Lun._Mar._Mier._Jue._Vier._Sab.'.split('_'),
            weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_')
        }
        );

        //window.fdesde = moment().subtract(6, 'days');
        window.fdesde = moment().startOf('isoWeek');
        //window.fhasta = moment();
        window.fhasta = moment().endOf('isoWeek');


        $('#periodo').daterangepicker({
            "locale": {
                "format": "DD-MM-YYYY",
                "separator": " - ",
                "applyLabel": "Guardar",
                "cancelLabel": "Cancelar",
                "fromLabel": "Desde",
                "toLabel": "Hasta",
                "customRangeLabel": "Personalizar",
                "daysOfWeek": [
                    "Do",
                    "Lu",
                    "Ma",
                    "Mi",
                    "Ju",
                    "Vi",
                    "Sa"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Setiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "firstDay": 1
            },
            startDate: fdesde,
            endDate: fhasta,
            ranges: {
                //'Today': [moment(), moment()],
                //'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Semana Actual': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
                'Últimos 7 Días': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 Días': [moment().subtract(29, 'days'), moment()],
                'Mes Actual': [moment().startOf('month'), moment().endOf('month')],
                'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, OkFecha);

        OkFecha(fdesde, fhasta);
    }

    if ($('#tblProyectos').length) {
        window.tabla = $('#tblProyectos').DataTable({

            paging: true,
            ordering: true,
            info: false,
            bFilter: true,
            autoWidth: false,
            //columnDefs: [{ targets: [2], class: "text-nowrap" }, { targets: [3], class: "text-nowrap" }],
            columnDefs: [{ "orderable": false, "targets": 6 }],
            responsive: true,
            language: {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            pageLength: 10

        });


    }

    $("#anio").on('change', function () {
        ListaProyectos();
    });

    $("#tblEmpresas").on('click', '.rec', function () {
        var idEmp = $(this).attr('id');
        ListaRecursos(idEmp);
    });

    $("#tblEmpresas").on('click', '.EP', function () {
        var idEmp = $(this).attr('id');

        if (idEmp == 0) {
            swal({
                title: 'Estado de Pago',
                text: 'Existen horas sin aprobar para el proyecto,<br> presione <a href="/Aprobacion/Actividades"><b>AQUÍ</b></a> para revisar las actividades.',
                html: 'Existen horas sin aprobar para el proyecto,<br> presione <a href="/Aprobacion/Actividades"><b>AQUÍ</b></a> para revisar las actividades.',
                type: 'info'
            });
            return false;
        }

        var startdate = window.fdesde;
        var enddate = window.fhasta;

        $("#fechainicioproselec").val(startdate);
        $("#fechafinproselec").val(enddate);

        var idPro = $("#proyectoEP").val();
        var nemp = $("#nemp" + idEmp).html();
        $("#empresaEP").val(idEmp);
        $("#nomEmpresaEP").html(nemp);
        $("#ivaEP").val("0");

        $("#valorUFperiodo").val("");

        $.ajax({
            url: '../PeriodosProyecto',
            type: 'POST',
            data: { proyecto: idPro },
            success: function (result) {
                $("#periodoEP").empty();
                $.each(result.Datos, function (i, item) {
                    $("#periodoEP").append("<option value='" + item.Periodo + "'>" + item.MesPalabra + "</option>");
                });
                $("#periodoEP").append("<option selected='selected' value='999'>Personalizado</option>");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });

        $('#FechaUF').datepicker('destroy');

        $("#FechaUF").attr("data-date", enddate);

        $('#FechaUF').datepicker({
            language: 'es',
            autoclose: true,
            format: 'dd-mm-yyyy',
            orientation: 'bottom'
        }).on('changeDate', function (ev) {

            var date = new Date(ev.date.valueOf());

            let day = date.getDate()
            let month = date.getMonth() + 1
            let year = date.getFullYear()

            var dia = day < 10 ? `0${day}` : `${day}`;
            var mes = month < 10 ? `0${month}` : `${month}`;

            fecha = `${dia}-${mes}-${year}`;

            $("#FechaUF").val(fecha);
            $("#FechaUFtxt").val(fecha);

            GetValorUF();
        });



        $("#periodoPersonalizadoInicio").hide();
        $("#periodoPersonalizadoInicio").empty();
        $("#periodoPersonalizadoFin").hide();
        $("#periodoPersonalizadoFin").empty();

        setTimeout(function () { mostarRangoFechas(); }, 100);

        $("#mdlEstadoPago").modal("show");
    });

    $("#formEstadoPago").on('submit', function (e) {
        //e.preventDefault();

        //$(this).submit();
        $("#mdlEstadoPago").modal("hide");
    });

    $("#btnsentForm").on('click', function () {

        if ($("#periodoEP").val() != "999") {
            $("#formEstadoPago").submit();
            $("#mdlEstadoPago").modal("hide");
        }
        else {
            var fechainiciotxt = $("#FechaPersonalizadaInicio").val();
            var fechafintxt = $("#FechaPersonalizadaFin").val();

            var inicio = fechainiciotxt.split("-");
            var fechainicio = new Date(inicio[2], inicio[1] - 1, inicio[0]);

            var fin = fechafintxt.split("-");
            var fechafin = new Date(fin[2], fin[1] - 1, fin[0]);


            if (fechainicio.getMonth() != fechafin.getMonth() || fechainicio.getFullYear() != fechafin.getFullYear()) {
                swal("Generación EDP", "El periodo seleccionado no es válido, seleccione un rango de fechas dentro del mismo mes.", "warning");

            } else if (fechainicio > fechafin) {
                swal("Generación EDP", "La fecha de inicio no puede ser mayor que la fecha fin del periodo.", "warning");
            }
            else {
                $("#formEstadoPago").submit();
                $("#mdlEstadoPago").modal("hide");
            }
        }

    });

    //***** Consultoria *****//
    if ($('#tblProyectosConsultoria').length) {
        window.tabla2 = $('#tblProyectosConsultoria').DataTable({
            paging: true,
            ordering: true,
            info: false,
            bFilter: true,
            columnDefs: [{ "orderable": false, "targets": 3 }, { "orderable": false, "targets": 4 }, { "orderable": false, "targets": 5 }],
            responsive: true,
            language: {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            pageLength: 10
        });
    }

    $("#anioConsultor").on('change', function () {
        ListaProyectosConsultoria();
    });

    $("#btngenEDP").on("click", function () {
        descarfaedp();
    });

    $('[data-toggle="tooltip"]').tooltip();
});

//***** Manejo de vistas *****//
function mostrarDesarrollo() {
    $('#divDesarrollo').show();
    $('#divConsultoria').hide();
    $('#botonesDashboard').hide();

    ListaProyectos();
}

function mostrarConsultoria() {
    $('#divDesarrollo').hide();
    $('#divConsultoria').show();
    $('#botonesDashboard').hide();

    ListaProyectosConsultoria();
}

function volverDashboard() {
    $('#divDesarrollo').hide();
    $('#divConsultoria').hide();
    $('#botonesDashboard').show();
}


//***** Metodos Desarrollo *****//
function ListaProyectos() {
    var anio = $("#anio").val();
    tabla.clear().draw();

    $.ajax({
        url: 'Dashboard/ListadoProyectos',
        data: { anio: anio },
        type: 'POST',
        success: function (data) {

            if (data.Cod == 1) {
                $.each(data.Datos, function (i, item) {
                    var icn = "fa-arrow-";
                    var clr = "btn-";
                    switch (item.Estado) {
                        case 0:
                            icn += "down";
                            clr += "danger";
                            break;
                        case 1:
                            icn += "up";
                            clr += "info";
                            break;
                    }

                    var btn = "<a href='Dashboard/Proyecto/" + item.IdProyecto + "' class='btn " + clr + " btn-circle' > <i class='fa " + icn + "'></i> </button> ";

                    var JpArr = item.JP;
                    var Jp = "";
                    if (JpArr.length == 0) {
                        Jp = "Sin JP asignado";
                    }
                    else if (JpArr.length > 1) {

                        $.each(JpArr, function (i, v) {
                            Jp += v + ", ";
                        });

                        Jp = Jp.substring(0, Jp.length - 2);
                    }
                    else {
                        Jp = JpArr;
                    }

                    tabla.row.add([
                        item.Proyecto,
                        Jp,
                        item.Recursos,
                        item.Consumidas,
                        item.Totales,
                        item.Restantes,
                        "<center>" + btn + "</center>"
                    ]).draw(false);
                });
            }
            else {
                swal("Proyectos", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });

}

function OkFecha(start, end) {
    //$('#periodo span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    $('#periodo span').html(start.format('DD MMM YYYY') + ' al ' + end.format('DD MMM YYYY'));

    $("#tblRecursos tbody").empty();
    $("#titRecursos").html("Seleccione empresa para visualizar detalle de los recursos...");
    $("#tblRecursosAcumulado").hide();
    $("#tblRecursosPeriodo").hide();

    var IdProyecto = $("#IdProyecto").val();
    ListaEmpresas(start.format('DD-MM-YYYY'), end.format('DD-MM-YYYY'), IdProyecto);
    window.fdesde = start.format('DD-MM-YYYY');
    window.fhasta = end.format('DD-MM-YYYY');
    //CargarGrafico('');
}

function ListaEmpresas(desde, hasta, proyecto) {

    var empGraf = [];
    var empHTot = [];
    var emptUtil = [];

    $.ajax({
        url: '../ListaEmpresas',
        type: 'POST',
        data: { desde: desde, hasta: hasta, proyecto: proyecto },
        success: function (result) {
            //console.log(result);
            $("#tblEmpresas tbody").empty();
            $.each(result.Datos, function (i, item) {
                var nemp = "<span id='nemp" + item.Id + "'>" + item.Nombre + "</span>";
                var btn = "";
                if ($("#IsJp").val() == "1") {
                    var idEP = (item.SinAprobar > 0 ? 0 : item.Id);
                    btn += "<button id='" + idEP + "' class='btn btn-info btn-sm btn-circle EP'><i class='fa fa-file-pdf-o'></i></button>";
                }


                var fila = "<tr>";
                fila += "<td><a href='#' class='rec' id='" + item.Id + "' title='Ver recursos " + item.Nombre + "'>" + nemp + "</a></td>";
                fila += "<td>" + item.UtilizadasPeriodo + "</td>";
                fila += "<td>" + item.UtilizadasTotal + "</td>";
                fila += "<td>" + item.Faltantes + "</td>";
                fila += "<td>" + item.HHTotalestxt + "</td>";
                fila += "<td>" + btn + "</td>";

                fila += "</tr>";

                $("#tblEmpresas tbody").append(fila);
                empGraf[i] = item.Nombre;
                empHTot[i] = item.HHTotales;
                emptUtil[i] = item.UtilizadasTotal;
            });

            CargarGrafico(empGraf, empHTot, emptUtil);

            var jjpp = "";
            if (result.Datos.length == 1) {
                ListaRecursos(result.Datos[0].Id);
            }

            //console.log(result.JPs);

            if (result.JPs.length == 1) {
                jjpp += "<input type='hidden' value='" + result.JPs[0]['IdUsuario'] + "' name='jpEP'><h3>" + result.JPs[0]['Nombre'] + "</h3>";
            }
            else {
                jjpp += "<select class='form-control' name='jpEP'>";
                for (c = 0; c < result.JPs.length; c++) {
                    jjpp += "<option value='" + result.JPs[c]['IdUsuario'] + "'>" + result.JPs[c]['Nombre'] + "</option>";
                }
                jjpp += "</select>";
            }

            $("#divJPs").html(jjpp);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        }
    });

}

function ListaRecursos(empresa) {

    var proyecto = $("#IdProyecto").val();

    $.ajax({
        url: '../ListaRecursos',
        data: { desde: window.fdesde, hasta: window.fhasta, proyecto: proyecto, empresa: empresa },
        type: 'POST',
        success: function (result) {

            $("#tblRecursosAcumulado tbody").empty();
            $("#tblRecursosPeriodo tbody").empty();

            $("#titRecursos").html('');
            $.each(result.DatosA, function (i, item) {
                var fila = "<tr>";
                fila += "<td><b>" + item.Nombre + "</b></td>";
                fila += "<td>" + item.Utilizadas + "</td>";

                if (parseFloat(item.Faltantes) < 0) {
                    fila += "<td style='background-color: #ed5565; color: white;'>" + item.Faltantes + "</td>";
                }
                else {
                    fila += "<td>" + item.Faltantes + "</td>";
                }

                fila += "<td>" + item.Totales + "</td>";
                fila += "</tr>";
                $("#tblRecursosAcumulado tbody").append(fila);
            });

            $.each(result.DatosP, function (i, item) {
                var fila = "<tr>";
                fila += "<td><b>" + item.Nombre + "</b></td>";
                fila += "<td>" + item.Utilizadas + "</td>";
                fila += "<td>" + item.Ultima + "</td>";
                fila += "</tr>";
                $("#tblRecursosPeriodo tbody").append(fila);
            });

            $("#titRecursos").html("Recursos " + result.DatosP[0].Empresa);

            $("#tblRecursosAcumulado").show();
            $("#tblRecursosPeriodo").show();

            if (result.DatosA.length == 0) {
                $("#tblRecursosAcumulado tbody").append("<tr><td colspan='4'>No hay recursos asignados</td></tr>");
            };
            if (result.DatosP.length == 0) {
                $("#tblRecursosPeriodo tbody").append("<tr><td colspan='4'>No hay recursos asignados</td></tr>");
            };
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });


}

function CargarGrafico(E, T, U) {

    var datosGrafico = {
        labels: E,
        datasets: [
            {
                label: "HH Totales",
                backgroundColor: 'rgba(255, 159, 64, 0.2)',
                //pointBorderColor: "#fff",
                data: T,
                fill: true,
                borderWidth: 2
            },
            {
                label: "HH Utilizadas",
                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                //borderColor: "rgba(26,179,148,0.7)",
                //pointBackgroundColor: "rgba(26,179,148,1)",
                //pointBorderColor: "#fff",
                data: U,
                fill: false,
                borderWidth: 2
            }
        ]
    };

    Grafico.data = datosGrafico;
    window.Grafico.update();


}

function mostarRangoFechas() {

    var valorCombo = $("#periodoEP").val();

    if (valorCombo == 999) {
        var fechainicioproy = $("#fechainiciopro").val();
        var fechafinproy = $("#fechafinpro").val();
        var fechainicio = $("#fechainicioproselec").val();
        var fechafin = $("#fechafinproselec").val();

        $("#periodoPersonalizadoInicio").append("<h3 class='col-md-4'>Inicio periodo:</h3>");
        $("#periodoPersonalizadoInicio").append("<div class='col-md-8'><input type='text' class='form-control input-sm datepicker' value=" + fechainicio + " name='FechaPersonalizadaInicio' id='FechaPersonalizadaInicio' autocomplete='off' onchange='setrangoperiodo()'></div>");

        $("#FechaPersonalizadaInicio").datepicker({
            language: 'es',
            autoclose: true,
            format: 'dd-mm-yyyy',
            orientation: 'bottom',
            startDate: fechainicioproy,
            endDate: fechafinproy
        });

        $("#periodoPersonalizadoInicio").show();

        $("#periodoPersonalizadoFin").append("<h3 class='col-md-4'>Fin periodo:</h3>");
        $("#periodoPersonalizadoFin").append("<div class='col-md-8'><input type='text' class='form-control input-sm datepicker' value=" + fechafin + " name='FechaPersonalizadaFin' id='FechaPersonalizadaFin' autocomplete='off'></div>");

        $("#FechaPersonalizadaFin").datepicker({
            language: 'es',
            autoclose: true,
            format: 'dd-mm-yyyy',
            orientation: 'bottom',
            startDate: fechainicioproy,
            endDate: fechafinproy
        });

        $("#periodoPersonalizadoFin").show();
    }
    else {
        $("#periodoPersonalizadoInicio").hide();
        $("#periodoPersonalizadoInicio").empty();
        $("#periodoPersonalizadoFin").hide();
        $("#periodoPersonalizadoFin").empty();
    }

    $("#FechaUF").val(fechafin);
    $("#FechaUFtxt").val(fechafin);
    $("#FechaUF").attr("data-date", fechafin);
    GetValorUF();
}

function setrangoperiodo() {

    var fechainicio = $("#FechaPersonalizadaInicio").val();

    var from = fechainicio.split("-")
    var f = new Date(from[2], from[1] - 1, from[0])
    var lastdayofmonth = new Date(f.getFullYear(), f.getMonth() + 1, 0);

    let date = ("0" + lastdayofmonth.getDate()).slice(-2);
    let month = ("0" + (lastdayofmonth.getMonth() + 1)).slice(-2);
    let year = lastdayofmonth.getFullYear();

    var fechafinmes = date + "-" + month + "-" + year;

    $("#FechaPersonalizadaFin").datepicker('destroy');

    $("#FechaPersonalizadaFin").val(fechafinmes);

    $("#FechaPersonalizadaFin").datepicker({
        language: 'es',
        autoclose: true,
        format: 'dd-mm-yyyy',
        orientation: 'bottom',
        startDate: fechainicio,
        endDate: fechafinmes
    });

}

function GetValorUF() {

    $("#valorUFperiodo").val("Obteniendo valor");

    var periodo = $("#periodoEP").val();
    var fecha = "";
    if (periodo == 999) {
        fecha = $("#FechaUF").val();
    }

    $.ajax({
        url: '../ValorUF',
        data: { fecha: fecha, periodo: periodo },
        type: 'POST',
        success: function (result) {

            if (result.Cod == 1) {
                $("#valorUFperiodo").val(result.Datos);
                $('#FechaUF').datepicker('update', result.Fecha);
                $("#FechaUFtxt").val(result.Fecha);
            }
            else {
                swal("Generación EDP", "Ha ocurrido un error al procesar la solicitud.", "warning");
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
            swal("Generación EDP", "Error al procesar la solicitud", "error");
        },
    });

}


//***** Metodos Consultoria *****//
function ListaProyectosConsultoria() {
    var anio = $("#anioConsultor").val();
    tabla2.clear().draw();

    $.ajax({
        url: 'Dashboard/ListadoConsultorias',
        data: { anio: anio },
        type: 'POST',
        success: function (data) {

            if (data.Cod == 1) {
                $.each(data.Datos, function (i, item) {
                    var btn_ = '<center><button class="btn btn-info btn-sm btn-circle" data-toggle="tooltip" title="Descargar EDP" onclick="mostrarmodaledp(' + item.id + ',*' + item.nombre + '*)"><i class="fa fa-file-excel-o"></i></button></center>';
                    var btn = btn_.replaceAll('*', "'");
                    tabla2.row.add([
                        item.nombre,
                        item.descripcion,
                        item.oc,
                        item.inicio,
                        item.fin,
                        btn
                    ]).draw(false);
                });
            }
            else {
                swal("Proyectos consultoria", data.Msg, "error");
            }
            $('[data-toggle="tooltip"]').tooltip();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            swal("Proyectos consultoria", "Ocurrió un error al procesar la solicitud.", "error");
        }
    });

}

function mostrarmodaledp(id, nombre) {

    $("#idProyectoCons").val(id);
    $("#nombreProyectoConsultoria").html(nombre);

    $.ajax({
        url: 'Dashboard/PeriodosConsultoria',
        type: 'POST',
        data: { proyecto: id },
        success: function (result) {
            $("#periodoEDP").empty();
            $.each(result.periodos, function (i, item) {
                var select = "";
                if (item.Seleccionado == 1) {
                    select = "selected = 'selected'";
                }
                $("#periodoEDP").append("<option value='" + item.Periodo + "' " + select + " >" + item.MesPalabra + "</option>");
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {

            console.log(jqXHR);
        }
    });

    $("#mdlEDPConsultoria").modal("show");

}

function descarfaedp() {

    var l = Ladda.create(document.querySelector('#btngenEDP'));
    l.start();

    var id = Number.parseInt($("#idProyectoCons").val())

    if (id != null && id > 0) {
        $.ajax({
            url: 'Dashboard/GetEDP',
            type: "POST",
            dataType: 'Json',
            data: {
                id: id,
                periodo: $("#periodoEDP").val(),
                responsable: $("#responsable").val()
            },
            success: function (data, textStatus, jqXHR) {
                switch (data.Cod) {
                    case 1:

                        l.stop();
                        swal({
                            title: "Generación EDP",
                            text: "EDP generado exitosamente, la descarga del archivo comenzara en breve.",
                            type: "success"
                        }, function () {
                            $("#mdlEDPConsultoria").modal("hide");
                        });

                        window.location = '/Dashboard/Download?Handler=' + data.Handler + '&Proy=' + data.proy;

                        break;
                    case 2:
                        swal("Generación EDP", data.Msg, "warning");
                        l.stop();
                        break;
                    default:
                        swal("Generación EDP", data.Msg, "error");
                        l.stop();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                swal('Generación EDP', 'Error al procesar la consulta.', 'error');
                l.stop();
            }
        });
    } else {
        swal("Generación EDP", "Identificador del proyecto no reconocido.", "warning");
        l.stop();
    }
}
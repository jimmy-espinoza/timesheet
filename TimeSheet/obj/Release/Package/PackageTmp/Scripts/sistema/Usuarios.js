﻿$(document).ready(function () {

    $('input[type="checkbox"]').iCheck({ checkboxClass: 'icheckbox_square-green' });

    window.tabla = $('#tblUsuarios').DataTable({

        paging: true,
        ordering: true,
        info: false,
        bFilter: true,
        //columnDefs: [{ targets: [2], class: "text-nowrap" }, { targets: [3], class: "text-nowrap" }],
        columnDefs: [{ "orderable": false, "targets": 6 }],
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:  ",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10

    });

    ListarUsuarios();

    $("#btnPass").on('click', function () {
        $("#icoPass").removeClass();
        if ($("#clave").get(0).type == 'password') {
            $("#clave").get(0).type = 'text';
            $("#icoPass").addClass('fa fa-eye-slash');
        }
        else {
            $("#clave").get(0).type = 'password';
            $("#icoPass").addClass('fa fa-eye');
        }
    });


    $("#btnNuevo").on('click', function () {
        $("#titModal").html('Nuevo Usuario');
        $("#mdlUsuarios").modal("show");
        $("#formUsuarios")[0].reset();
        fUser.resetForm();
        $("#IdUsuario").val(0);
        $("#Admin").prop('checked', false);
        $("#Admin").iCheck('update');
        $("#clave").get(0).type = 'text';
        $("#btnPass").hide();
        $('#preview').attr('src', 'Content/Firmas/firma.png');
    });

    $('#tblUsuarios').on('click', '.edit', function () {
        var idUser = $(this).attr('id');
        $("#titModal").html('Editar Usuario');
        fUser.resetForm();

        $.ajax({
            url: 'Usuarios/DatosUsuario',
            data: { idusuario: idUser },
            type: 'POST',
            success: function (resultado) {

                //console.log(resultado);

                $("#IdUsuario").val(resultado.Datos.IdUsuario);
                $("#usuario").val(resultado.Datos.NombreUsuario);
                $("#UsuarioOriginal").val(resultado.Datos.NombreUsuario);
                $("#nombre").val(resultado.Datos.Nombre);
                $("#clave").val(resultado.Datos.Clave);
                $("#correo").val(resultado.Datos.Correo);
                $("#empresa").val(resultado.Datos.IdEmpresa);
                $("#perfil").val(resultado.Datos.IdPerfil);
                var ad = (resultado.Datos.IsAdmin == 1 ? true : false);
                $("#Admin").prop('checked', ad);
                $("#Admin").iCheck('update');
                $("#estado").val(resultado.Datos.IsActivo);

                $("#clave").get(0).type = 'password';
                $("#btnPass").show();

                if (resultado.Datos.ArchivoFirma == "") {
                    $('#preview').attr('src', 'Content/Firmas/firma.png');
                }
                else {
                    $('#preview').attr('src', 'Content/Firmas/' + resultado.Datos.ArchivoFirma);
                }

                $("#firma-o").val(resultado.Datos.ArchivoFirma);
                $("#firma").val('');

                $("#mdlUsuarios").modal("show");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR, textStatus, errorThrown);
                console.log(jqXHR);
            }
        });


    });

    $.validator.addMethod("UsuarioExiste", function (value, element) {
        if ($("#EUsuario").val() == 1) {
            return false;
        }
        else {
            return true;
        }
    }, "El usuario ingresado ya existe");


    var fUser = $("#formUsuarios").validate({
        rules: {
            usuario: { UsuarioExiste: true }
        },
        submitHandler: function (form) {

            var l = Ladda.create(document.querySelector('#btnGuardar'));
            l.start();

            //var formulario = new FormData($("#formUsuarios")[0]);
            var formulario = new FormData(document.getElementById("formUsuarios"));

            $.ajax({
                url: "Usuarios/GuardarUsuario",
                data: formulario,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "json",
                type: 'POST',
                success: function (resultado) {

                    //console.log(resultado);
                    if (resultado.Cod == 0) {
                        swal("Usuarios", resultado.Msg, "warning");
                        l.stop();
                    }
                    else if (resultado.Cod == 1) {
                        ListarUsuarios();
                        $("#mdlUsuarios").modal("toggle");
                        swal("Usuarios", resultado.Msg, "success");
                        l.stop();
                    }
                    else {
                        swal("Usuarios", resultado.Msg, "error");
                        l.stop();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    l.stop();
                }
            });
        },
        invalidHandler: function (event, validator) {
            //alert('Completar');
        }

    });

    $("#usuario").on('focus', function () {
        $("#EUsuario").val(0);
    });

    $("#usuario").on('focusout', function () {
        var User1 = $(this).val();

        if (User1 == $("#UsuarioOriginal").val()) {
            return false;
        }

        $.ajax({
            url: "Usuarios/ExisteUsuario",
            data: { usuario: User1 },
            async: true,
            dataType: "json",
            type: 'POST',
            success: function (resultado) {
                if (resultado.Cod == 1) {
                    $("#EUsuario").val(resultado.Existe);
                    if (resultado.Existe == 1) { swal("Usuarios", "El usuario ingresado ya existe", "info"); }
                }
                else {
                    swal("Usuarios", resultado.Msg, "error");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            }
        });

    });


    ListaPerfiles();
    ListaEmpresas();


    //Preview Imagen
    $("#preview").on('click', function () {
        $('#firma').click();
    });

    $('#firma').change(function () {
        var imgPath = this.value;
        var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
            readURL(this);
        else
            if ($("#cliente").val() == "0") {
                $('#preview').attr('src', e.target.result);
            }
            else {

            }
    });

    function readURL(input) {
        if (input.files && input.files[0]) {

            var reader = new FileReader();
            reader.readAsDataURL(input.files[0]);
            reader.onload = function (e) {
                $('#preview').attr('src', e.target.result);
            };
        }
    }

    //Preview Imagen

});





function ListarUsuarios() {
    tabla.clear();

    $.ajax({
        url: 'Usuarios/ListaUsuarios',
        type: 'POST',
        success: function (data) {
            //console.log(data);
            if (data.Cod == 1) {
                $.each(data.Datos, function (i, item) {
                    var btn = "<button class='btn btn-sm btn-info edit' id='" + item.IdUsuario + "'> Editar </button>";
                    var Estado = "";
                    if (item.Estado == 1) {
                        Estado = "Activo";
                    }
                    else {
                        Estado = "Inactivo";
                    }

                    tabla.row.add([
                        item.NombreUsuario,
                        item.Nombre,
                        item.Correo,
                        item.Empresa,
                        item.Perfil,
                        Estado,
                        btn
                    ]).draw(false);
                });
            }
            else {
                swal("Usuarios", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });

}

function ListaPerfiles() {
    $.ajax({
        url: 'Perfiles/ListaPerfiles',
        type: 'POST',
        success: function (data) {
            //console.log(data);
            if (data.Cod == 1) {
                $.each(data.Datos, function (i, item) {
                    var opcion = "<option value='" + item.Id + "'>" + item.Perfil + "</option>";
                    $("#perfil").append(opcion);
                });
            }
            else {
                swal("Usuarios", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });

}

function ListaEmpresas() {
    $.ajax({
        url: 'Empresas/ListaEmpresas',
        type: 'POST',
        success: function (data) {
            //console.log(data);
            if (data.Cod == 1) {
                $.each(data.Datos, function (i, item) {
                    var opcion = "<option value='" + item.IdEmpresa + "'>" + item.NombreEmpresa + "</option>";
                    $("#empresa").append(opcion);
                });
            }
            else {
                swal("Usuarios", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
        },
    });


}
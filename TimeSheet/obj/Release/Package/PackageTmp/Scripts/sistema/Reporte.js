﻿$(document).ready(function () {

    if ($('#periodo').length) {
        moment.locale('es', {
            months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
            monthsShort: 'Enero._Feb._Mar_Abr._May_Jun_Jul._Ago_Sept._Oct._Nov._Dec.'.split('_'),
            weekdays: 'Domingo_Lunes_Martes_Miercoles_Jueves_Viernes_Sabado'.split('_'),
            weekdaysShort: 'Dom._Lun._Mar._Mier._Jue._Vier._Sab.'.split('_'),
            weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_')
        }
        );

        //window.fdesde = moment().subtract(6, 'days');
        window.fdesde = moment().startOf('isoWeek');
        //window.fhasta = moment();
        window.fhasta = moment().endOf('isoWeek');


        $('#periodo').daterangepicker({
            "locale": {
                "format": "DD-MM-YYYY",
                "separator": " - ",
                "applyLabel": "Guardar",
                "cancelLabel": "Cancelar",
                "fromLabel": "Desde",
                "toLabel": "Hasta",
                "customRangeLabel": "Personalizar",
                "daysOfWeek": [
                    "Do",
                    "Lu",
                    "Ma",
                    "Mi",
                    "Ju",
                    "Vi",
                    "Sa"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Setiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "firstDay": 1
            },
            startDate: fdesde,
            endDate: fhasta,
            ranges: {
                //'Today': [moment(), moment()],
                //'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Semana Actual': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
                'Últimos 7 Días': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 Días': [moment().subtract(29, 'days'), moment()],
                'Mes Actual': [moment().startOf('month'), moment().endOf('month')],
                'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, OkFecha);

        OkFecha(fdesde, fhasta);
    }

    $("#btnBuscar").on('click', function () {
        generaReporte();
    });
});

function OkFecha(start, end) {
    //$('#periodo span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    $('#periodo span').html(start.format('DD MMM YYYY') + ' al ' + end.format('DD MMM YYYY'));
    window.fdesde = start.format('DD-MM-YYYY');
    window.fhasta = end.format('DD-MM-YYYY');
}

function ListadoUsuarios() {

    var l = Ladda.create(document.querySelector('#btnBuscar'));
    l.start();

    var project = $("#cboproyecto").val();

    $('#cbousuario').empty().append('<option selected="selected" disabled value="">Cargando opciones...</option>');

    $.ajax({
        url: 'CargaCboUsuarios',
        data: { project: project },
        type: 'POST',
        success: function (result) {
            if (result.Cod == 1) {
                $('#cbousuario').empty();
                if (result.Datos.length > 1) {
                    $('#cbousuario').append('<option value="0">Todos</option>');
                }

                $.each(result.Datos, function (i, item) {
                    $("#cbousuario").append("<option value='" + item.id + "'>" + item.name + "</value>");
                });

                l.stop();
            }
            else {
                swal("Reporte TimeSheet", result.Msg, "warning");
                $('#cbousuario').empty().append('<option selected="selected" disabled value="">' + result.Msg + '</option>');
                l.stop();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
            swal("Reporte TimeSheet", "Ocurrió un error al procesar la solicitud", "error");
            $('#cbousuario').empty().append('<option selected="selected" disabled value="">Debe seleccionar proyecto para ver las opciones.</option>');
            l.stop();
        }
    });
}

function generaReporte() {

    var l = Ladda.create(document.querySelector('#btnBuscar'));
    l.start();

    var IdProyecto = $("#cboproyecto").val();

    var IdUsuario = $("#cbousuario").val();


    $.ajax({
        url: 'GeneraRepoprte',
        type: 'POST',
        data: { desde: window.fdesde, hasta: window.fhasta, usuario: IdUsuario, proyecto: IdProyecto },
        success: function (result) {
            if (result.Cod == 1) {
                swal("Reporte TimeSheet", result.Msg, "success");
                //console.log(window.location.href);
                window.location.href = "DescargaArchivo?handler=" + result.Handler;
                l.stop();
            }
            else {
                swal("Reporte TimeSheet", result.Msg, "warning");
                l.stop();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR, textStatus, errorThrown);
            console.log(jqXHR);
            swal("Reporte TimeSheet", "Se ha producido un error al realizar la búsqueda.", "error");
            l.stop();
        }
    });

}
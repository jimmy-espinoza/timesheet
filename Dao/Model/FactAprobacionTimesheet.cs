//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Dao.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class FactAprobacionTimesheet
    {
        public int IdSolicitud { get; set; }
        public int IdUsuarioProyecto { get; set; }
        public System.DateTime FechaSolicitud { get; set; }
        public int EstadoSolicitud { get; set; }
        public Nullable<System.DateTime> FechaGestion { get; set; }
        public Nullable<int> UsuarioGestion { get; set; }
        public Nullable<System.DateTime> FechaValidez { get; set; }
    
        public virtual FactUsuario FactUsuario { get; set; }
        public virtual RelUsuarioProyecto RelUsuarioProyecto { get; set; }
    }
}

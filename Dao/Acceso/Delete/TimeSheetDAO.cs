﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Delete
{
    public class TimeSheetDAO
    {
        public int EliminarTimesheet(int IdTimesheet)
        {
            try
            {
                var context = new DB_TSEntities();
                FactTimeSheet ts = new FactTimeSheet() { IdRegistro = IdTimesheet };
                context.FactTimeSheet.Attach(ts);
                context.FactTimeSheet.Remove(ts);
                if (context.SaveChanges() == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}

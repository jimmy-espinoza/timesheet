﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Delete
{
    public class TimeSheetConsultorDAO
    {
        public int EliminarTimeSheetConsultor(int IdTimesheetConsultor)
        {
            try
            {
                var context = new DB_TSEntities();
                FactTimesheetConsultor ts = new FactTimesheetConsultor() { IdTimesheetConsultor = IdTimesheetConsultor };
                context.FactTimesheetConsultor.Attach(ts);
                context.FactTimesheetConsultor.Remove(ts);
                if (context.SaveChanges() == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}

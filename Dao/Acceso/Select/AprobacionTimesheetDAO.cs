﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Select
{
    public class AprobacionTimesheetDAO
    {
        public IQueryable<FactAprobacionTimesheet> ListaAtrasos(List<int> IdPros)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactAprobacionTimesheet.Include("RelUsuarioProyecto").Include("RelUsuarioProyecto.FactProyecto").Include("RelUsuarioProyecto.FactUsuario").AsNoTracking()
                           where IdPros.Any(a => a.Equals(x.RelUsuarioProyecto.IdProyecto))
                           where x.EstadoSolicitud == 0
                           select x);
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public FactAprobacionTimesheet DatosAtraso(int IdRegistro)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactAprobacionTimesheet.Include("RelUsuarioProyecto").Include("RelUsuarioProyecto.FactProyecto").Include("RelUsuarioProyecto.FactUsuario").AsNoTracking()
                           where x.IdSolicitud == IdRegistro
                           select x).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public FactAprobacionTimesheet ExisteAtraso(int IdUsuarioProyecto)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactAprobacionTimesheet.AsNoTracking()
                           where x.IdUsuarioProyecto == IdUsuarioProyecto
                           //where x.EstadoSolicitud == 0
                           orderby x.IdSolicitud descending
                           select x).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

    }
}


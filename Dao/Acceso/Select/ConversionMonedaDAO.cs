﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Select
{
    public class ConversionMonedaDAO
    {
        public IQueryable<CfgConversionMoneda> ListaConversion(string Moneda, int Anio, int Mes)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.CfgConversionMoneda.AsNoTracking()
                           where x.Moneda==Moneda
                           select x);

                if (Anio != 0)
                    obj = obj.Where(f => f.Fecha.Value.Year == Anio);

                if (Mes != 0)
                    obj = obj.Where(f => f.Fecha.Value.Month == Mes);


                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<CfgConversionMoneda> ListaConversion(string Moneda, DateTime Desde, DateTime Hasta)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.CfgConversionMoneda.AsNoTracking()
                           where x.Moneda == Moneda
                           where x.Fecha>=Desde && x.Fecha<=Hasta
                           select x);
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }


    }
}


﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Select
{
    public class ActividadDAO
    {
        public FactTipoActividad DatosActividad(int IdActividad)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactTipoActividad.AsNoTracking()
                           where (x.IdTipoActividad == IdActividad)
                           select x).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<FactTipoActividad> ActividadesProyecto(int IdProyecto)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactTipoActividad.AsNoTracking()
                           where (x.IdProyecto == IdProyecto)
                           select x);
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}

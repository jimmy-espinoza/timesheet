﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Select
{
    public class EdicionTimesheetAprobadaDAO
    {

        public IQueryable<FactEdicionTimeSheetAprobada> ListaByProyecto(List<int> pros)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;

                var obj = from x in DB.FactEdicionTimeSheetAprobada.Include("FactTimeSheet").Include("FactTimeSheet.RelUsuarioProyecto").Include("FactTimeSheet.RelUsuarioProyecto.FactProyecto").Include("FactTimeSheet.RelUsuarioProyecto.FactUsuario").AsNoTracking()
                          where pros.Any(a => a.Equals(x.FactTimeSheet.RelUsuarioProyecto.IdProyecto))
                          select x;
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }


        public FactEdicionTimeSheetAprobada GetRegistro(int id)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactEdicionTimeSheetAprobada
                           .Include("FactTimeSheet")
                           .Include("FactTimeSheet.RelUsuarioProyecto")
                           .Include("FactTimeSheet.RelUsuarioProyecto.FactProyecto")
                           .Include("FactTimeSheet.RelUsuarioProyecto.FactUsuario")
                           .Include("DimTipoCosto")
                           .Include("FactTipoActividad").AsNoTracking()
                           where x.IdRegistro == id
                           select x).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

    }
}

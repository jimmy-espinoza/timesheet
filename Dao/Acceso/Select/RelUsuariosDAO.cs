﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Select
{
    public class RelUsuariosDAO
    {
        public IQueryable<RelUsuarioProyecto> ListaUsuarioProyecto(int Proyecto, int Proveedor)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.RelUsuarioProyecto.Include("FactUsuario").Include("FactUsuario.DimEmpresa").Include("FactProyecto").AsNoTracking()
                           where x.IsActivo == 1
                           select x);
                if (Proyecto != 0)
                {
                    obj = obj.Where(a => a.IdProyecto == Proyecto);
                }

                if (Proveedor != 0)
                {
                    obj = obj.Where(a => a.FactUsuario.DimEmpresa.IdEmpresa == Proveedor);
                }

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<FactUsuario> UsuariosProyectoDash(List<int> Proyecto)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.RelUsuarioProyecto.Include("FactUsuario").AsNoTracking()
                           where x.IsActivo == 1
                           where Proyecto.Any(p => p.Equals(x.IdProyecto))
                           select x.FactUsuario);

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public RelUsuarioProyecto DatosUsuarioProyecto(int IdUsuarioProyecto)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.RelUsuarioProyecto.Include("FactProyecto").AsNoTracking()
                           where (x.IdUsuarioProyecto == IdUsuarioProyecto)
                           select x).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<FactUsuario> ListaUsuariosEnProyecto(int Proyecto, int Proveedor)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactUsuario.Include("DimEmpresa").Include("RelUsuarioProyecto").AsNoTracking()
                           select x);
                if (Proveedor != 0)
                {
                    obj = obj.Where(b => b.DimEmpresa.IdEmpresa == Proveedor);
                }

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<RelUsuarioProyecto> AsignacionProyecto(int Proyecto)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.RelUsuarioProyecto.AsNoTracking()
                           where x.IdProyecto == Proyecto
                           where x.IsActivo == 1
                           select x);

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<RelUsuarioProyecto> ProyectosAsignados(int Usuario)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.RelUsuarioProyecto.AsNoTracking()
                           where x.IdUsuario == Usuario
                           select x);

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<RelUsuarioProyecto> SinTSUsuario(DateTime fecha)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.RelUsuarioProyecto.Include("FactProyecto").Include("FactUsuario").Include("FactTimeSheet").Include("FactUsuario.DimEmpresa").AsNoTracking()
                           where x.IsActivo == 1
                           where x.FactProyecto.IsActivo == 1
                           where fecha <= x.FactProyecto.FinProyecto
                           where x.FactTimeSheet.Where(f => f.Fecha == fecha).Count() == 0
                           select x);

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<RelUsuarioProyecto> ProyectosJP(int IdUsuario)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.RelUsuarioProyecto.Include("FactProyecto").Include("FactUsuario").Include("FactTimeSheet").Include("FactUsuario.DimEmpresa").AsNoTracking()
                           where x.IsActivo == 1
                           where x.IsJp == 1
                           where x.IdUsuario == IdUsuario
                           select x);

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public int GetUsuarioProyecto(int Proyecto, int Usuario)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.RelUsuarioProyecto.AsNoTracking()
                           where x.IdProyecto == Proyecto
                           where x.IsActivo == 1
                           where x.IdUsuario == Usuario
                           select x.IdUsuarioProyecto).FirstOrDefault();
                            
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public int GetUsuario(int Usuario)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactUsuario.AsNoTracking()
                           where x.IsActivo == 1
                           where x.IdUsuario == Usuario
                           select x.IdUsuario).FirstOrDefault();

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}

﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Select
{
    public class NotificacionDAO
    {
        public IQueryable<FactNotificacion> ListadoNotificacionUsuario(int IdUsuario, int Cantidad)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactNotificacion.Include("FactUsuario").AsNoTracking()
                           where x.IdUsuarioVisualiza == IdUsuario
                           orderby x.FechaRegistro descending
                           select x);
                if (Cantidad > 0)
                {
                    obj.Take(Cantidad);
                }
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public FactNotificacion DatosNotificacion(int IdNotificacion)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactNotificacion.Include("FactUsuario").AsNoTracking()
                           where x.IdNotificacion == IdNotificacion
                           select x).FirstOrDefault();
               
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

       

    }
}


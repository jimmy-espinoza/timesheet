﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Select
{
    public class ProyectoDAO
    {

        public IQueryable<FactProyecto> ListaProyectos()
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactProyecto.Include("DimCliente").Include("RelUsuarioProyecto").AsNoTracking()
                           select x);
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<RelUsuarioProyecto> ListaProyectosDash(int anio, int usuario)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                /*var obj = (from x in DB.FactProyecto.Include("RelUsuarioProyecto").Include("RelUsuarioProyecto.FactUsuario").Include("RelUsuarioProyecto.FactTimeSheet").AsNoTracking()
                           where x.InicioProyecto.Value.Year == anio
                           select x);*/
                var obj = (from x in DB.RelUsuarioProyecto.Include("FactProyecto").Include("FactUsuario").Include("FactTimesheet").AsNoTracking()
                           where x.FactProyecto.InicioProyecto.Value.Year == anio
                           select x);

                if (usuario != 0)
                {
                    obj = obj.Where(u => u.IdUsuario == usuario && u.IsActivo == 1);
                }

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public FactProyecto DatosProyecto(int IdProyecto)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactProyecto.Include("RelUsuarioProyecto").Include("RelUsuarioProyecto.FactUsuario").Include("DimCliente").AsNoTracking()
                           where (x.IdProyecto == IdProyecto)
                           select x).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public FactProyecto DatosProyectoEP(int IdProyecto)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactProyecto.Include("RelUsuarioProyecto").AsNoTracking()
                           where (x.IdProyecto == IdProyecto)
                           select x).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public List<string> JPProyecto(int Proyecto, int Empresa)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.RelUsuarioProyecto.AsNoTracking()
                           where x.FactProyecto.IdProyecto == Proyecto
                           where x.IsActivo == 1
                           where x.IsJp == 1
                           select x);

                if (Empresa != 0)
                {
                    obj = obj.Where(x => x.FactUsuario.DimEmpresa.IdEmpresa == Empresa);
                }

                List<string> salida = obj.Select(x => x.FactUsuario.Nombre).ToList();

                return salida;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<FactUsuario> JPProyectoList(int Proyecto, int Empresa)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.RelUsuarioProyecto.Include("FactUsuario").AsNoTracking()
                           where x.FactProyecto.IdProyecto == Proyecto
                           where x.IsActivo == 1
                           where x.IsJp == 1
                           select x.FactUsuario);

                if (Empresa != 0)
                {
                    obj = obj.Where(x => x.DimEmpresa.IdEmpresa == Empresa);
                }
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<RelUsuarioProyecto> ListaProyectosPerfil(int usuario)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.RelUsuarioProyecto.Include("FactProyecto").AsNoTracking()
                           select x);

                if(usuario>0)
                    obj = obj.Where(u => u.IdUsuario==usuario).Where(a => a.IsActivo==1);


                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<RelUsuarioProyecto> ListaProyectosPerfilConsultor(int usuario)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.RelUsuarioProyecto.Include("FactProyecto").AsNoTracking()
                           where x.FactProyecto.IdTipoProyecto == 2
                           
                           select x);

                if (usuario > 0)
                    obj = obj.Where(u => u.IdUsuario == usuario).Where(a => a.IsActivo == 1);


                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public FactProyecto ProyectoExistente(string Proyecto)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactProyecto.AsNoTracking()
                           where (x.NombreProyecto == Proyecto)
                           select x).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public FactProyecto ProyectoConsultorExistente(string Proyecto)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactProyecto.AsNoTracking()
                           where (x.NombreProyecto == Proyecto)
                           select x).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

    }
}

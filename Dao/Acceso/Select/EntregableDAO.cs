﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Select
{
    public class EntregableDAO
    {
        public IQueryable<FactEntregables> ListadoEntregables(DateTime desde, DateTime hasta, List<int> usuario, List<int> proyecto)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactEntregables.Include("FactUsuario").Include("FactProyecto").AsNoTracking()
                           where x.FechaCarga >= desde && x.FechaCarga <= hasta
                           where usuario.Any(u => u == x.IdUsuarioCarga)
                           where proyecto.Any(p => p == x.IdProyecto)
                           select x);

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<FactEntregables> ListadoEntregables(int proyecto)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactEntregables.Include("FactUsuario").AsNoTracking()
                           where x.IdProyecto == proyecto
                           select x);

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}

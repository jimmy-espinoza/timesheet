﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Select
{
    public class TimeSheetDAO
    {
        public IQueryable<RelUsuarioProyecto> ListaEmpresasDash(int proyecto)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.RelUsuarioProyecto.Include("FactUsuario").Include("FactUsuario.DimEmpresa").Include("FactProyecto").Include("FactTimeSheet").AsNoTracking()
                           where x.IdProyecto == proyecto
                           //where x.IsActivo == 1 // se muestran todos se valida en la vista
                           select x);
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<RelUsuarioProyecto> ListaRecursosDash(int proyecto, int empresa)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.RelUsuarioProyecto.Include("FactUsuario").Include("FactUsuario.DimEmpresa").Include("FactProyecto").Include("FactTimeSheet").AsNoTracking()
                           where x.IdProyecto == proyecto
                           where x.FactUsuario.IdEmpresa == empresa
                           where x.IsActivo==1
                           select x);
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<FactTimeSheet> ListadoTimesheet(DateTime desde, DateTime hasta, int usuario, int proyecto)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactTimeSheet.Include("RelUsuarioProyecto").Include("RelUsuarioProyecto.FactProyecto").Include("RelUsuarioProyecto.FactUsuario").Include("DimTipoCosto").AsNoTracking()
                           where x.Fecha >= desde && x.Fecha <= hasta
                           select x);

                if (usuario != 0)
                {
                    obj = obj.Where(u => u.RelUsuarioProyecto.IdUsuario == usuario);
                }

                if (proyecto != 0)
                {
                    obj = obj.Where(u => u.RelUsuarioProyecto.IdProyecto == proyecto);
                }

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        
        public FactTimesheetConsultor GetregistroTimesheetConsultor(DateTime fecha, int usuario)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactTimesheetConsultor.AsNoTracking()
                           where x.Fecha == fecha
                           where x.IdUsuarioConsultor == usuario
                           where x.IsAprobado == 1
                           select x).FirstOrDefault();

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<FactTimesheetConsultor> GetregistroTimesheetConsultor(DateTime inicio, DateTime fin, int proyecto)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactTimesheetConsultor.AsNoTracking()
                           where x.Fecha >= inicio
                           where x.Fecha <= fin
                           where x.IdProyectoConsultor == proyecto
                           where x.IsAprobado == 1
                           select x);

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<FactTimeSheet> ListadoTimesheet(DateTime desde, DateTime hasta, List<int> usuario, List<int> proyecto)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;

                var resumen = from x in DB.FactTimeSheet.Include("RelUsuarioProyecto").Include("RelUsuarioProyecto.FactProyecto").Include("RelUsuarioProyecto.FactUsuario").Include("DimTipoCosto").Include("FactTipoActividad").AsNoTracking()
                              where x.Fecha >= desde && x.Fecha <= hasta
                              where usuario.Any(u => u == x.RelUsuarioProyecto.IdUsuario)
                              where proyecto.Any(p => p == x.RelUsuarioProyecto.IdProyecto)
                              select x;

                return resumen;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<RelUsuarioProyecto> ListaProyectosTimesheet(int usuario)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.RelUsuarioProyecto.Include("FactProyecto").AsNoTracking()
                           where x.IdUsuario == usuario
                           where x.IsActivo == 1
                           select x);

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public FactTimeSheet DatosTimesheet(int IdTimesheet)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactTimeSheet.Include("RelUsuarioProyecto.FactUsuario.DimEmpresa").Include("FactTipoActividad").AsNoTracking()
                           where x.IdRegistro == IdTimesheet
                           select x).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public FactTimeSheet DatosTimesheetInclude(int IdTimesheet)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactTimeSheet
                           .Include("RelUsuarioProyecto")
                           .Include("RelUsuarioProyecto.FactProyecto")
                           .Include("RelUsuarioProyecto.FactUsuario")
                           .Include("DimTipoCosto")
                           .Include("FactTipoActividad").AsNoTracking()
                           where x.IdRegistro == IdTimesheet
                           select x).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public FactTimeSheet DatosTimesheetNoTrack(int IdTimesheet)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactTimeSheet.AsNoTracking()
                           where x.IdRegistro == IdTimesheet
                           select x).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<FactTimeSheet> HorariosRegistrados(List<int> pros, DateTime Fecha)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactTimeSheet.AsNoTracking()
                           where pros.Any(a => a.Equals(x.IdUsuarioProyecto))
                           where x.Fecha == Fecha
                           orderby x.HoraInicio ascending
                           select x);

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<FactTimeSheet> HorasProyectoEmpresa(DateTime Inicio, DateTime Termino, int Proyecto, int Empresa)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactTimeSheet.AsNoTracking()
                           where x.Fecha >= Inicio && x.Fecha <= Termino
                           where x.RelUsuarioProyecto.IdProyecto == Proyecto
                           where x.RelUsuarioProyecto.FactUsuario.DimEmpresa.IdEmpresa == Empresa
                           select x);

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<FactTimeSheet> HorariosFecha(DateTime Fecha, int IdUsuarioProyecto)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactTimeSheet.AsNoTracking()
                           where x.Fecha==Fecha
                           where x.IdUsuarioProyecto==IdUsuarioProyecto
                           select x);

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<FactTimeSheet> ListaTimesheetByProyecto(List<int> pros)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;

                var obj = from x in DB.FactTimeSheet.Include("RelUsuarioProyecto").Include("RelUsuarioProyecto.FactProyecto").Include("RelUsuarioProyecto.FactUsuario").Include("DimTipoCosto").Include("FactTipoActividad").AsNoTracking()
                           where pros.Any(a => a.Equals(x.RelUsuarioProyecto.IdProyecto))
                           select x;

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }


        public IQueryable<FactTimeSheet> HorasRegistradasSemana(int IdUsuario, List<int> Semanas)
        {
            try
            {
                
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactTimeSheet.Include("RelUsuarioProyecto").Include("RelUsuarioProyecto.FactProyecto").AsNoTracking()
                           where x.RelUsuarioProyecto.FactUsuario.IdUsuario==IdUsuario
                           where Semanas.Any(f => f.Equals(x.Semana.Value))
                           select x);
                
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

    }
}


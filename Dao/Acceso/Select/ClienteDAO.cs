﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Select
{
    public class ClienteDAO
    {
        public IQueryable<DimCliente> ListaClientes()
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.DimCliente.AsNoTracking()
                           select x);
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public DimCliente DatosCliente(int IdCliente)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.DimCliente.AsNoTracking()
                           where (x.IdCliente == IdCliente)
                           select x).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public DimCliente ClienteRut(string Rut)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.DimCliente.AsNoTracking()
                           where (x.Rut == Rut)
                           select x).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public DimCliente ClienteExiste(string Nombre)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.DimCliente.AsNoTracking()
                           where (x.Nombre == Nombre)
                           select x).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}

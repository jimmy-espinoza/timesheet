﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Select
{
    public class ConfiguracionDAO
    {
        public IQueryable<CfgParametrosGenerales> ListaConfiguracion()
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.CfgParametrosGenerales.AsNoTracking()
                           
                           select x);
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public CfgParametrosGenerales DatosConfiguracion(int IdCfg)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.CfgParametrosGenerales.AsNoTracking()
                           where x.IdParametroGeneral == IdCfg
                           select x).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }

    }
}


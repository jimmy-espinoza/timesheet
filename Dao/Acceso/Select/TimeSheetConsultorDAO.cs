﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Select
{
    public class TimeSheetConsultorDAO
    {
        /*public IQueryable<FactTimesheetConsultor> ListadoTimesheetConsultor()
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactTimesheetConsultor.Include("RelUsuarioProyecto").Include("FactUsuario").AsNoTracking()
                           select x);

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<RelUsuarioProyecto> ListaProyectosTimesheetConsultor(int usuario)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.RelUsuarioProyecto.Include("FactProyecto").AsNoTracking()
                           where x.IdUsuario == usuario
                           where x.IsActivo == 1
                           select x);

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }*/

        public IQueryable<FactTimesheetConsultor> ListadoTimesheetConsultor(DateTime desde, DateTime hasta, int usuario, int proyecto)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactTimesheetConsultor.Include("RelUsuarioProyecto").Include("RelUsuarioProyecto.FactProyecto").Include("RelUsuarioProyecto.FactUsuario").AsNoTracking()
                           where x.Fecha >= desde && x.Fecha <= hasta
                           select x);

                if (usuario != 0)
                {
                    obj = obj.Where(u => u.RelUsuarioProyecto.IdUsuario == usuario);
                }

                if (proyecto != 0)
                {
                    obj = obj.Where(u => u.RelUsuarioProyecto.IdProyecto == proyecto);
                }

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<FactTimesheetConsultor> ListadoTimesheetConsultor(DateTime desde, DateTime hasta, List<int> usuario, List<int> proyecto)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;

                var resumen = from x in DB.FactTimesheetConsultor.Include("RelUsuarioProyecto").Include("RelUsuarioProyecto.FactProyecto").Include("RelUsuarioProyecto.FactUsuario").AsNoTracking()
                              where x.Fecha >= desde && x.Fecha <= hasta
                              where usuario.Any(u => u == x.RelUsuarioProyecto.IdUsuario)
                              where proyecto.Any(p => p == x.IdProyectoConsultor)
                              
                              select x;

                return resumen;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<RelUsuarioProyecto> ListaProyectosTimesheetConsultor(int usuario)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.RelUsuarioProyecto.Include("FactProyecto").AsNoTracking()
                           where x.IdUsuario == usuario
                           where x.IsActivo == 1
                           where x.FactProyecto.IdTipoProyecto == 2
                           select x);

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public Dao.Model.FactTimesheetConsultor ObjetoTimesheetConsultorEditar(int id)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactTimesheetConsultor.AsNoTracking()
                           where x.IdTimesheetConsultor == id
                           select x).FirstOrDefault();

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public Dao.Model.FactEntregables ObjetoTimesheetEntregableEditar(int id)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactEntregables.AsNoTracking()
                           where x.IdEntregable == id
                           select x).FirstOrDefault();

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public Dao.Model.FactEntregables ObjetoTimesheetEntregableEditar(string txt)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactEntregables.AsNoTracking()
                           where x.Entregable.Equals(txt)
                           select x).FirstOrDefault();

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public IQueryable<FactTimesheetConsultor> ListaTimesheetConsultorByProyecto(List<int> pros)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;

                var obj = from x in DB.FactTimesheetConsultor.Include("RelUsuarioProyecto").Include("RelUsuarioProyecto.FactProyecto").Include("RelUsuarioProyecto.FactUsuario").AsNoTracking()
                          where pros.Any(a => a.Equals(x.RelUsuarioProyecto.IdProyecto))
                          select x;

                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public FactTimesheetConsultor DatosTimesheet(int IdTimesheet)
        {
            try
            {
                var DB = new DB_TSEntities();
                DB.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in DB.FactTimesheetConsultor.Include("RelUsuarioProyecto.FactUsuario.DimEmpresa").AsNoTracking()
                           where x.IdTimesheetConsultor == IdTimesheet
                           select x).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {

                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}

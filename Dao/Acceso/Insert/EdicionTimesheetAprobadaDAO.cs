﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Insert
{
    public class EdicionTimesheetAprobadaDAO
    {
        public int RegistrarTimesheetAprobada(FactEdicionTimeSheetAprobada timesheet)
        {
            try
            {
                var context = new DB_TSEntities();
                context.FactEdicionTimeSheetAprobada.Add(timesheet);
                if (context.SaveChanges() == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}

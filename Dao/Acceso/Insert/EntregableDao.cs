﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Insert
{
    public class EntregableDAO
    {
        public int GuardarEntregableConsultor(int Usuario, int Proyecto, string Entregable, int Avance, int Pendiente, int VersionFinal, string Observacion, string CodigoDocumento)
        {
            try
            {
                var context = new DB_TSEntities();

                FactEntregables NuevoEC = new FactEntregables();

                NuevoEC.IdUsuarioCarga = Usuario;
                NuevoEC.IdProyecto = Proyecto;
                NuevoEC.Entregable = Entregable;
                NuevoEC.Avance = Avance;
                NuevoEC.Pendiente = Pendiente;
                NuevoEC.VersionFinal = VersionFinal;
                NuevoEC.Observaciones = Observacion;
                NuevoEC.CodigoDocumento = CodigoDocumento;
                NuevoEC.FechaCarga = DateTime.Now;

                context.FactEntregables.Add(NuevoEC);
                if (context.SaveChanges() == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public int GuardarEntregableConsultor(List<FactEntregables> datos)
        {
            try
            {
                var context = new DB_TSEntities();
                context.FactEntregables.AddRange(datos);

                return context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}

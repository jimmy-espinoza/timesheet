﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Insert
{
    public class TimesheetDAO
    {
        public int GuardarTimesheet(DateTime Fecha, string Actividad, int Proyecto, int Costo, string Inicio, string Termino)
        {
            try
            {

                DateTime fi = DateTime.ParseExact(Fecha.ToString("yyyy-MM-dd") + " " + Inicio + ":00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                DateTime ft = DateTime.ParseExact(Fecha.ToString("yyyy-MM-dd") + " " + Termino + ":00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                var duracion = (ft - fi).TotalHours;

                var context = new DB_TSEntities();

                FactTimeSheet NuevaTS = new FactTimeSheet();
                NuevaTS.Fecha = Fecha;
                NuevaTS.Actividad = Actividad;
                NuevaTS.IdUsuarioProyecto = Proyecto;
                NuevaTS.IdTipoCosto = Costo;
                NuevaTS.HoraInicio = Inicio;
                NuevaTS.HoraFin = Termino;
                NuevaTS.TotalHoras = duracion;
                NuevaTS.IsActivo = 1;
                NuevaTS.IsAprobado = 0;
                
                context.FactTimeSheet.Add(NuevaTS);
                if (context.SaveChanges() == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public int GuardarTimesheet(List<FactTimeSheet> datos)
        {
            try
            {
                var context = new DB_TSEntities();
                context.FactTimeSheet.AddRange(datos);

                return context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}
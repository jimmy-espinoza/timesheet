﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Insert
{
    public class UsuarioDAO
    {
        public int GuardarUsuario(string NombreUsuario, string Nombre, string Clave, string Correo, int IdEmpresa, int IdPerfil, int Admin, int IsActivo, string ArchivoFirma)
        {
            try
            {
                var context = new DB_TSEntities();

                FactUsuario NuevoUsuario = new FactUsuario();
                NuevoUsuario.NombreUsuario = NombreUsuario;
                NuevoUsuario.Nombre = Nombre;
                NuevoUsuario.Clave = Clave;
                NuevoUsuario.Correo = Correo;
                NuevoUsuario.IdEmpresa = IdEmpresa;
                NuevoUsuario.IdPerfil = IdPerfil;
                NuevoUsuario.IsAdmin = Admin;
                NuevoUsuario.IsActivo = IsActivo;
                NuevoUsuario.ArchivoFirma = ArchivoFirma;

                context.FactUsuario.Add(NuevoUsuario);
                if (context.SaveChanges() == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}

﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Insert
{
    public class ActividadDAO
    {

        public int GuardarActividad(int IdProyecto, string Descripcion, int IsActivo)
        {
            try
            {
                var context = new DB_TSEntities();

                FactTipoActividad NuevaActividad = new FactTipoActividad();
                NuevaActividad.Descripcion = Descripcion;
                NuevaActividad.IsActivo = IsActivo;
                NuevaActividad.IdProyecto = IdProyecto;
                context.FactTipoActividad.Add(NuevaActividad);
                return context.SaveChanges() == 1 ? 1 : 0;
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }

    }
}

﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Insert
{
    public class TimeSheetConsultorDAO
    {
        public int GuardarTimesheetConsultor(int Proyecto, int Usuario, DateTime Fecha, string Descripcion)
        {
            try
            {
                var context = new DB_TSEntities();

                FactTimesheetConsultor NuevaTSC = new FactTimesheetConsultor();

                NuevaTSC.IdProyectoConsultor = Proyecto;
                NuevaTSC.IdUsuarioConsultor = Usuario;
                NuevaTSC.Fecha = Fecha;
                NuevaTSC.Descripcion = Descripcion;
                NuevaTSC.Dias = 1;
                NuevaTSC.Estado = 1;
                NuevaTSC.FechaCarga = DateTime.Now;
                NuevaTSC.IsAprobado = 0;

                context.FactTimesheetConsultor.Add(NuevaTSC);
                if (context.SaveChanges() == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public int GuardarTimesheetConsultor(List<FactTimesheetConsultor> datos)
        {
            try
            {
                var context = new DB_TSEntities();
                context.FactTimesheetConsultor.AddRange(datos);

                return context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}

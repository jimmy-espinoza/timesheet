﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Insert
{
    public class ConfiguracionDAO
    {
        public int GuardarConfiguracion(string DesCfg, string ValCfg, DateTime FecCfg)
        {
            try
            {
                var context = new DB_TSEntities();

                CfgParametrosGenerales Nuevo = new CfgParametrosGenerales();
                Nuevo.Descripcion = DesCfg;
                Nuevo.Valor= ValCfg;
                Nuevo.FechaRegistro = FecCfg;
                
                context.CfgParametrosGenerales.Add(Nuevo);
                if (context.SaveChanges() == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public int GuardarConfiguracion(List<CfgParametrosGenerales> datos)
        {
            try
            {
                var context = new DB_TSEntities();
                context.CfgParametrosGenerales.AddRange(datos);

                return context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}

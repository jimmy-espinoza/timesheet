﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Insert
{
    public class EmpresaDAO
    {
        public int GuardarEmpresa(string NombreEmpresa, string RutEmpresa, string Contacto, int TelefonoContacto,string MailContacto, int AplicaCosto, double UfHora, int IsActivo)
        {
            try
            {
                var context = new DB_TSEntities();

                DimEmpresa NuevaEmpresa = new DimEmpresa();
                NuevaEmpresa.NombreEmpresa = NombreEmpresa;
                NuevaEmpresa.RutEmpresa = RutEmpresa;
                NuevaEmpresa.Contacto = Contacto;
                NuevaEmpresa.TelefonoContacto = TelefonoContacto;
                NuevaEmpresa.MailContacto = MailContacto;
                NuevaEmpresa.AplicaCosto = AplicaCosto;
                NuevaEmpresa.UfHora = UfHora;
                NuevaEmpresa.IsActivo = IsActivo;

                context.DimEmpresa.Add(NuevaEmpresa);
                if (context.SaveChanges() == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}

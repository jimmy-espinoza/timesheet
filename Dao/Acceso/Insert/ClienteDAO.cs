﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Insert
{
    public class ClienteDAO
    {
        public int GuardarCliente(string Nombre, string Rut)
        {
            try
            {
                var context = new DB_TSEntities();

                DimCliente NuevoCliente = new DimCliente();
                NuevoCliente.Nombre = Nombre;
                NuevoCliente.Rut = Rut;

                context.DimCliente.Add(NuevoCliente);
                if (context.SaveChanges() == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}

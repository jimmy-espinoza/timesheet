﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Insert
{
    public class PerfilDAO
    {
        public int GuardarPerfil(string DescPerfil, int IsActivo)
        {
            try
            {
                var context = new DB_TSEntities();

                DimPerfil NuevoPerfil = new DimPerfil();
                NuevoPerfil.DescPerfil = DescPerfil;
                NuevoPerfil.IsActivo = IsActivo;

                context.DimPerfil.Add(NuevoPerfil);
                if (context.SaveChanges() == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}

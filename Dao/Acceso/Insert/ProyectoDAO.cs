﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Insert
{
    public class ProyectoDAO
    {
        public int GuardarProyecto(string NombreProyecto, int IdCliente, string Descripcion, DateTime InicioProyecto, DateTime FinProyecto, int DuracionDias, int TotalHH, int IsActivo)
        {
            try
            {
                var context = new DB_TSEntities();

                FactProyecto NuevoProyecto = new FactProyecto();
                NuevoProyecto.NombreProyecto = NombreProyecto;
                NuevoProyecto.IdCliente = IdCliente;
                NuevoProyecto.Descripcion = Descripcion;
                NuevoProyecto.InicioProyecto = InicioProyecto;
                NuevoProyecto.FinProyecto = FinProyecto;
                NuevoProyecto.DuracionDias = DuracionDias;
                NuevoProyecto.TotalHH = TotalHH;
                NuevoProyecto.IsActivo = IsActivo;
                NuevoProyecto.IdTipoProyecto = 1;

                context.FactProyecto.Add(NuevoProyecto);
                if (context.SaveChanges() == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public int GuardarProyectoConsultor(string NombreProyecto, int IdCliente, string Descripcion, string NombreContacto, string TelefonoContacto, string CorreoContacto, string PmCliente, string OrdenDeCompra, DateTime InicioProyecto, DateTime FinProyecto, int TipoMoneda, int Monto, string RangoEDP, int IsActivo)
        {
            try
            {
                var context = new DB_TSEntities();

                FactProyecto NuevoProyectoconsultor = new FactProyecto();
                NuevoProyectoconsultor.NombreProyecto = NombreProyecto;
                NuevoProyectoconsultor.IdCliente = IdCliente;
                NuevoProyectoconsultor.Descripcion = Descripcion;
                NuevoProyectoconsultor.NombreContacto = NombreContacto;
                NuevoProyectoconsultor.TelefonoContacto = TelefonoContacto;
                NuevoProyectoconsultor.CorreoContacto = CorreoContacto;
                NuevoProyectoconsultor.PmCliente = PmCliente;
                NuevoProyectoconsultor.OrdenDeCompra = OrdenDeCompra;
                NuevoProyectoconsultor.InicioProyecto = InicioProyecto;
                NuevoProyectoconsultor.FinProyecto = FinProyecto;
                NuevoProyectoconsultor.TipoMoneda = TipoMoneda;
                NuevoProyectoconsultor.Monto = Monto;
                NuevoProyectoconsultor.RangoEDP = RangoEDP;
                NuevoProyectoconsultor.IsActivo = IsActivo;
                NuevoProyectoconsultor.IdTipoProyecto = 2;
                context.FactProyecto.Add(NuevoProyectoconsultor);
                if (context.SaveChanges() == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }

    }
}

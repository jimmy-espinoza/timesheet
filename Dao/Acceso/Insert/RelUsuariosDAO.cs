﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Insert
{
    public class RelUsuariosDAO
    {
        public int GuardarRelacion(int IdUsuario, int IdProyecto, double PorcentajeAsignacion, int IsJp)
        {
            try
            {
                var context = new DB_TSEntities();

                RelUsuarioProyecto NuevaRelacion = new RelUsuarioProyecto();
                NuevaRelacion.IdUsuario = IdUsuario;
                NuevaRelacion.IdProyecto = IdProyecto;
                NuevaRelacion.PorcentajeAsignacion = PorcentajeAsignacion;
                NuevaRelacion.IsJp = IsJp;
                NuevaRelacion.IsActivo = 1;
                context.RelUsuarioProyecto.Add(NuevaRelacion);
                if (context.SaveChanges() == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}

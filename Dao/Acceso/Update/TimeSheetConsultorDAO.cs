﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Update
{
    public class TimeSheetConsultorDAO
    {
        public int EditarTimesheetConsultor(FactTimesheetConsultor timesheetConsultor)
        {
            try
            {
                var context = new DB_TSEntities();
                context.Entry<FactTimesheetConsultor>(timesheetConsultor).State = System.Data.Entity.EntityState.Modified;
                if (context.SaveChanges() == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }

        public int EditarEntregableConsultor(FactEntregables EntregableConsultor)
        {
            try
            {
                var context = new DB_TSEntities();
                context.Entry<FactEntregables>(EntregableConsultor).State = System.Data.Entity.EntityState.Modified;
                if (context.SaveChanges() == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}

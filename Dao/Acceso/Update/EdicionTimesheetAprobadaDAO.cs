﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Update
{
    public class EdicionTimesheetAprobadaDAO
    {
        public int EditarRegistro(FactEdicionTimeSheetAprobada reg)
        {
            try
            {
                var context = new DB_TSEntities();
                context.Entry<FactEdicionTimeSheetAprobada>(reg).State = System.Data.Entity.EntityState.Modified;
                if (context.SaveChanges() == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}

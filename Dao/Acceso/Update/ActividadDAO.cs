﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Update
{
    public class ActividadDAO
    {
        public int ActualizarActividad(FactTipoActividad actividad)
        {
            try
            {
                var context = new DB_TSEntities();
                context.Entry<FactTipoActividad>(actividad).State = System.Data.Entity.EntityState.Modified;
                return context.SaveChanges() == 1 ? 1 : 0;
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}

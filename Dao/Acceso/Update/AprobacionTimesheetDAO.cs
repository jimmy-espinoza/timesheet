﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Update
{
    public class AprobacionTimesheetDAO
    {
        public FactAprobacionTimesheet AprobarAtraso(FactAprobacionTimesheet registro)
        {
            try
            {
                var context = new DB_TSEntities();
                context.Entry<FactAprobacionTimesheet>(registro).State = System.Data.Entity.EntityState.Modified;
                if (context.SaveChanges() == 1)
                {
                    return registro;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}

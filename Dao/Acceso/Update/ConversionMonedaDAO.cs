﻿using Dao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Acceso.Update
{
    public class ConversionMonedaDAO
    {
        public int ActualizarConversionMoneda(CfgConversionMoneda conversion)
        {
            try
            {
                var context = new DB_TSEntities();
                context.Entry<CfgConversionMoneda>(conversion).State = System.Data.Entity.EntityState.Modified;
                if (context.SaveChanges() == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error SQL: " + ex.Message);
            }
        }
    }
}
